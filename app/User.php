<?php

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, Notifiable;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	protected $fillable = array('firstname', 'lastname', 'email', 'telephone', 'address', 'address2', 'zipcode', 'city', 'state', 'country', 'emailcampaign');

	public static $rules = array(
		'firstname'=>'required|min:2',
		'lastname'=>'required|min:2',
		'email'=>'required|email|unique:users',
		'telephone'=>'',
		'address'=>'',
		'address2'=>'',
		'city'=>'',
		'zipcode'=>'',
		'password'=>'required|between:4,30|confirmed',
		'password_confirmation'=>'required|between:4,30',
		'admin'=>'integer',
		'zipcode'=>''
	);

	public static $updaterules = array(
		'firstname'=>'required|min:2',
		'lastname'=>'required|min:2',
		'email'=>'required|email|unique:users',
		'password'=>'between:4,30|confirmed',
		'password_confirmation'=>'between:4,30',
		'telephone'=>'',
		'admin'=>'integer',
		'zipcode'=>''
	);

	public static $updateBillingRules = array(
		'firstname'=>'required|min:2',
		'lastname'=>'required|min:2',
		'password'=>'between:4,30|confirmed',
		'password_confirmation'=>'between:4,30',
		'telephone'=>'',
		'admin'=>'integer',
		'zipcode'=>''
	);


	public static $updateNErules = array(
		'firstname'=>'required|min:2',
		'lastname'=>'required|min:2',
		'email'=>'required|email',
		'password'=>'between:4,30|confirmed',
		'password_confirmation'=>'between:4,30',
		'telephone'=>'',
		'admin'=>'integer',
		'zipcode'=>''
	);

	public static $pwNERules = array (
		'password'=>'required|between:4,30|confirmed',
		'password_confirmation'=>'between:4,30',
		'email'=>'required|email|unique:users,email'
	);

	public static $pwRules = array (
		'password'=>'required|between:4,30|confirmed',
		'password_confirmation'=>'required|between:4,30'
	);



	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}

}
