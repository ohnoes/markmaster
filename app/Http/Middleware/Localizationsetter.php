<?php namespace markmaster\Http\Middleware;

use Closure;

class Localizationsetter  {

    public function handle($request, Closure $next)
    {
        // Perform action
		if(\Auth::check()) {
			$user = \User::find(\Auth::user()->id);
			if($user->localization != NULL) {
				\LaravelLocalization::setLocale($user->localization);
				\Redirect::to(\LaravelLocalization::getLocalizedURL($user->localization));
			} else if(\Session::get('theSetLocale', 'none') != 'none') {
				\LaravelLocalization::setLocale(\Session::get('theSetLocale', 'none'));
				\Redirect::to(\LaravelLocalization::getLocalizedURL(\Session::get('theSetLocale', 'none')));
			} else {
/*				$ip = \Request::getClientIp();
				$thestrings = 'http://api.wipmania.com/' . $ip . '?' . 'www.markmaster.com';
				$country = file_get_contents($thestrings);
				if ($country == 'NO') {
					\Session::put('theCurrency', 0);
					\LaravelLocalization::setLocale('no');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'no'));
				} else if ($country == 'SE') {
					\Session::put('theCurrency', 2);
					\LaravelLocalization::setLocale('se');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'se'));
				} else if ($country == 'DK') {
					\Session::put('theCurrency', 3);
					\LaravelLocalization::setLocale('dk');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'dk'));
				} else if($country == 'US'){
					\Session::put('theCurrency', 1);
					\LaravelLocalization::setLocale('en');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'en'));
				} else {
					\Session::put('theCurrency', 4);
					\LaravelLocalization::setLocale('en');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'en'));
				}*/
			}
		}else if(\Session::get('theSetLocale', 'none') != 'none') {
				\LaravelLocalization::setLocale(\Session::get('theSetLocale', 'none'));
				\Redirect::to(\LaravelLocalization::getLocalizedURL(\Session::get('theSetLocale', 'none')));
			} else {
/*				$ip = \Request::getClientIp();
				$thestring = 'http://api.wipmania.com/' . $ip . '?' . 'markmaster.com';
				$country = file_get_contents($thestring);
				if ($ip == '192.168.0.99' || $ip == '127.0.0.1') {

				} else if ($country == 'NO') {
					\Session::put('theCurrency', 0);
					\LaravelLocalization::setLocale('no');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'no'));
				} else if ($country == 'SE') {
					\Session::put('theCurrency', 2);
					\LaravelLocalization::setLocale('se');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'se'));
				} else if ($country == 'DK') {
					\Session::put('theCurrency', 3);
					\LaravelLocalization::setLocale('dk');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'dk'));
				} else if($country == 'US'){
					\Session::put('theCurrency', 1);
					\LaravelLocalization::setLocale('en');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'en'));
				} else {
					\Session::put('theCurrency', 4);
					\LaravelLocalization::setLocale('en');
					\Redirect::to(\LaravelLocalization::getLocalizedURL( 'en'));
				}*/
			}

        return $next($request);
    }
}
