<?php namespace markmaster\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class Admin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user() && $request->user()->admin == 1)
        {
          return $next($request);

        }
        $lang = app()->getLocale();
        return Redirect::to($lang . '/users/signin')
          ->with('message', 'Not admin, please log in');

        return $next($request);
    }

}
