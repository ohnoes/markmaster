<?php

// app/Http/Middleware/Language.php
namespace markmaster\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Session;

class Language
{
    public function handle(Request $request, Closure $next)
    {

        // set the Currency
        // check if user have selected Currency
        if(Session::get('currencySet', 0) == 0) {
          if($request->segment(1) == 'no') {
            Session::put('theCurrency', 0);
          } else if($request->segment(1) == 'dk') {
            Session::put('theCurrency', 3);
          } else if($request->segment(1) == 'se') {
            Session::put('theCurrency', 2);
          } else {
            Session::put('theCurrency', 0);
          }
        }

        // Check if the first segment matches a language code
        if (!array_key_exists($request->segment(1), config('translatable.locales')) ) {

            // Store segments in array
            $segments = $request->segments();

            // Set the default language code as the first segment
            $segments = array_prepend($segments, config('app.fallback_locale'));

            // Redirect to the correct url
            return redirect()->to(implode('/', $segments), 301);
        }
        return $next($request);
    }
}
