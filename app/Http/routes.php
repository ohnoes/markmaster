<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array(), function()
{
	Route::controller('admin/cliparts', 'ClipartController');
	Route::controller('admin/categories', 'CategoriesController');
	Route::controller('admin/products', 'ProductsController');
	Route::controller('admin', 'AdminController');

	// Posting routes that should not use localizationRedirect


	// ClipartController
	Route::post('clipart/create', array('uses'=>'ClipartController@postCreate'));
	Route::post('clipart/destroy', array('uses'=>'ClipartController@postDestroy'));
	Route::post('clipart/edit', array('uses'=>'ClipartController@postEdit'));

	// DesignController
	Route::post('design/backgroundget', array('uses'=>'DesignController@postBackgroundget'));
	Route::post('design/clipartget', array('uses'=>'DesignController@postClipartget'));

	// OrdersController
	Route::post('order/reg', array('uses'=>'OrdersController@postReg'));

	// RemindersController
	Route::post('reminders/remind', array('uses'=>'RemindersController@postRemind'));
	Route::post('reminders/reset', array('uses'=>'RemindersController@postReset'));

	// StoreController
	Route::post('store/addtocart', array('uses'=>'StoreController@postAddtocart'));
	Route::post('store/updatecart', array('uses'=>'StoreController@postUpdatecart'));
	Route::post('store/reg', array('uses'=>'StoreController@postReg'));
	Route::post('store/coupon', array('uses'=>'StoreController@postCoupon'));



	// TemplatesController
	Route::post('templates/create', array('uses'=>'TemplatesController@postCreate'));

	// UsersController
	Route::post('users/create', array('uses'=>'UsersController@postCreate'));
	Route::post('users/edit', array('uses'=>'UsersController@postEdit'));
	Route::post('users/signin', array('uses'=>'UsersController@postSignin'));
	Route::post('users/editpassword', array('uses'=>'UsersController@postEditpassword'));
	Route::post('users/editbilling', array('uses'=>'UsersController@postEditbilling'));
	Route::post('users/editshipping', array('uses'=>'UsersController@postEditshipping'));
});


Route::group(
	[
		'prefix' => LaravelLocalization::setLocale(),
		'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
	],
	function()
	{

		Route::controller('store', 'StoreController');
		Route::controller('users', 'UsersController');
		Route::controller('help', 'HelpController');
		//Route::controller('storeTest', 'StoreTestController');
		Route::controller('order', 'OrdersController');
		Route::controller('password', 'RemindersController');
		Route::controller('stickers', 'StickersController');
		Route::controller('invoice', 'InvoiceController');

		Route::controller('merkelapper', 'DesignController');
		Route::controller('etiketter-og-navnelapper', 'TemplatesController');
		Route::controller('navnelapper', 'NametagController');

		Route::get('/', array('uses'=>'StoreController@getIndex'));

		Route::get('/storeTest', array('uses'=>'DesignController@getStoreTest'));

		Route::get('/tempGen', array('uses'=>'DesignController@getTempGen'));

		Route::get('/backgroundGet', function() {
			return View::make('backgroundGet');
		});

		Route::post('/backgroundGet', function() {
			return View::make('backgroundGet');
		});

		Route::get('/clipartGet', function() {
			return View::make('clipartGet');
		});

		Route::post('/clipartGet', function() {
			return View::make('clipartGet');
		});
		Route::get('/stickerSubmit', function() {
			return View::make('stickerSubmit');
		});

		Route::post('/stickerSubmit', function() {
			return View::make('stickerSubmit');
		});

		Route::get('/conditions', function() {
			return View::make('conditions');
		});


	}
);
