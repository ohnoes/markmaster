<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ClipartController extends BaseController {

	public function __construct() {
		parent::__construct();
		//$this->beforeFilter('csrf', array('on'=>'post'));
		//$this->beforeFilter('admin');
	}

	public function getIndex() {
		return View::make('cliparts.index')
			->with('cliparts', Clipart::orderBy('id', 'DESC')->paginate(20));
	}

	public function postCreate() {
		$validator = Validator::make(Input::all(), Clipart::$admin_rules);
		$lang = app()->getLocale();
		if ($validator->passes()) {
			$clipart = new Clipart;
			$clipart->title = Input::get('title');
			$clipart->sortorder = Input::get('sortorder');
			$clipart->user_id = Auth::user()->getId();
			$clipart->public = 1;
			$clipart->category = Input::get('category');
			$clipart->subcat1 = Input::get('subcat1');
			$clipart->subcat2 = Input::get('subcat2');
			$clipart->searchwords = Input::get('searchwords');
			$clipart->can_strech = Input::get('strechable', '0');
			$clipart->can_color = Input::get('colors');
			$image = Input::file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$destination = public_path() . '/img/clipart';
			$upload = Input::file('image')->move($destination, $filename); // Fix the resizing to keep original format here
			$clipart->image = 'img/clipart/'.$filename;


			$clipart->save();

			return redirect()->to($lang . '/admin/cliparts/')
				->with('message', 'Clipart Uploaded');

			}

		return	redirect()->to($lang . '/admin/cliparts/')
				->with('message', 'Noe gikk galt')
				->withErrors($validator)
				->withInput();
	}




	public function postDestroy() {
		$clipart = Clipart::find(Input::get('id'));
		$lang = app()->getLocale();
		if($clipart) {
			$clipart->delete();
			return redirect()->to($lang . '/admin/cliparts/')
				->with('message', 'Clipart Fjernet');
		}

		return redirect()->to($lang . '/admin/cliparts/')
			->with('message', 'Noe gikk galt, Vennligst prøv igjen.');
	}

	public function postEdit() {
		$clipart = Clipart::find(Input::get('id'));
		$lang = app()->getLocale();
		if($clipart) {
			return View::make('cliparts.edit')
				->with('clipart', Clipart::find(Input::get('id')));

		}

		return redirect()->to($lang . '/admin/cliparts/')
			->with('message', 'Noe gikk galt, Vennligst prøv igjen.');
	}
}
