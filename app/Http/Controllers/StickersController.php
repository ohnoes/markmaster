<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
class StickersController extends BaseController {

	public function __construct() {
		parent::__construct();

	}

	public function getIndex() {
		if(Auth::check()) {
			return View::make('sticker.index')
				->with('stickers', Orderitem::where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC')->paginate(10));
			}

			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/signin')
				->with('message', 'Vennligst logg inn.');
	}
}
