<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class NametagController extends BaseController {

	public function __construct() {
		parent::__construct();

	}

	public function getKlistremerker() {
		return View::make('designs.simple')
        ->with('typeofsticker', 1);
	}

    public function getStrykfast_farge() {
		return View::make('designs.simple')
        ->with('typeofsticker', 2);
	}

    public function getKlistremerker_gull() {
		return View::make('designs.simple')
        ->with('typeofsticker', 3);
	}

    public function getKlistremerker_solv() {
		return View::make('designs.simple')
        ->with('typeofsticker', 4);
	}

    public function getKlistremerker_transparent() {
		return View::make('designs.simple')
        ->with('typeofsticker', 5);
	}

    public function getBillige() {
		return View::make('designs.simple')
        ->with('typeofsticker', 6);
	}

    public function getStrykfast() {
		return View::make('designs.simple')
        ->with('typeofsticker', 7);
	}

    public function getRefleksmerker() {
		return View::make('designs.simple')
        ->with('typeofsticker', 8);
	}

    public function getStrykefrie() {
		return View::make('designs.simple')
        ->with('typeofsticker', 9);
	}

	public function getBase() {
		return View::make('designs.simple')
		->with('typeofsticker', 1);
	}

}
