<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class CategoriesController extends BaseController {


	public function __construct() {
		parent::__construct();
		//$this->beforeFilter('csrf', array('on'=>'post'));
		//$this->beforeFilter('admin');
	}

	public function getFabric() {
		return View::make('categories.fabriclabels');
	}

	public function getStick() {
		return View::make('categories.stickerlabels');
	}

	public function getSticker(){
		return View::make('categories.sticker');
	}

	public function getCheap(){
		return View::make('categories.cheap');
	}

	public function getIrononcolor(){
		return View::make('categories.irononcolor');
	}

	public function getIronon(){
		return View::make('categories.ironon');
	}

	public function getIronfree(){
		return View::make('categories.ironfree');
	}

	public function getGold(){
		return View::make('categories.gold');
	}

	public function getSilver(){
		return View::make('categories.silver');
	}

	public function getTransparent(){
		return View::make('categories.transparent');
	}

	public function getReflection(){
		return View::make('categories.reflection');
	}

	public function getBeer(){
		return View::make('categories.beer');
	}

	public function getControll(){
		return View::make('categories.controll');
	}

	public function getForever(){
		return View::make('categories.forever');
	}

	public function postCreate() {
		$validator = Validator::make(Input::all(), Category::$rules);

		if ($validator->passes()) {
			$category = new Category;
			$category->name = Input::get('name');
			$category->save();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/categories/')
				->with('message', 'Category Created');
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/categories/')
			->with('message', 'Noe gikk galt, Vennlist prøv igjen.')
			->withErrors($validator)
			->withInput();
	}

	public function postDestroy() {
		$category = Category::find(Input::get('id'));

		if($category) {
			$category->delete();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/categories/')
				->with('message', 'Category Deleted');
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/categories/')
			->with('message', 'Something went wrong, please try again');
	}
}
