<?php

namespace markmaster\Http\Controllers;

use Illuminate\Support\Facades\View;

class ChatlogController extends BaseController {

	public function __construct() {
		parent::__construct();

	}


	public function getChat() {
		return View::make('chatlog.chatlog');
	}


}
