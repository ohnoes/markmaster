<?php


namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class HelpController extends BaseController {

	public function __construct() {
		parent::__construct();

	}

	public function getIndex() {
		return View::make('help.index');
	}

	public function getDesign() {
		return View::make('help.design')
			->with('agents', \Commercialagent::orderBy('name', 'ASC')->paginate(25) );
	}

	public function getFaq() {
		return View::make('help.faq');
	}

	public function getVideo() {
		return View::make('help.video');
	}

	public function getContact() {
		return View::make('help.contact');
	}

	public function getConditions() {
		return View::make('help.conditions');
	}

}
