<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Hash;
use Input;
use DB;
use DateTime;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class StoreController extends BaseController {

	public function __construct() {
		parent::__construct();

		//$this->middleware('auth', ['only' => 'getCheckout']);

	}

	public function getIndex() {
		return View::make('store.index');

	}

	public function getView($id) {
		return View::make('store.view')->with('product', Product::find($id));
	}

	public function getCategory($cat_id) {
		return View::make('store.category')
			->with('products', Product::where('category_id', '=', $cat_id)->paginate(6))
			->with('category', Category::find($cat_id));
	}

	public function getSearch() {
		$keyword = Input::get('keyword');

		return View::make('store.search')
			->with('products', Product::where('title', 'LIKE', '%'.$keyword.'%')->get())
			->with('keyword', $keyword);
	}

	public function getCurrency($id) {

		if(Auth::check()) {
			$user = User::find(Auth::user()->id);
			$user->currency = $id;
			$user->save();
		}

		Session::put('theCurrency', $id);
		Session::put('currencySet', 1);
		return Redirect::back();

	}

	public function postAddtocart() {
		$validator = Validator::make(Input::all(), Orderitem::$rules);

		if ($validator->passes()) {
			$orderitem = new Orderitem;
			$orderitem->stickertype = Input::get('stickertype');
			if(Input::has('userid')) {
				$orderitem->user_id = Input::get('userid');
			} else {
				$orderitem->user_id = 0;
			}
			$orderitem->json = Input::get('stickerjson');
			if ($orderitem->stickertype == 6 || $orderitem->stickertype == 7) {
				$orderitem->quantity = Input::get('Antall2');
				$orderitem->quantexp = Input::get('Antall2');
			} else {
				$orderitem->quantity = Input::get('quantity');
				$orderitem->quantexp = Input::get('quantity');
			}
			$orderitem->price = intval(Input::get('stickerprice'));
			$orderitem->regularprice = intval(Input::get('stickerprice'));
			$orderitem->width = Input::get('stickerwidth');
			$orderitem->height = Input::get('stickerheight');
			$orderitem->save();

			if($orderitem->stickertype == 1) {
				$type = 'klistremerker';
			} elseif ($orderitem->stickertype == 2) {
				$type = 'strykfast-farge';
			} elseif ($orderitem->stickertype == 3){
				$type = 'klistremerker-gull';
			} elseif ($orderitem->stickertype == 4){
				$type = 'klistremerker-solv';
			} elseif ($orderitem->stickertype == 5){
				$type = 'klistremerker-transparent';
			} elseif ($orderitem->stickertype == 6){
				$type = 'billige';
			} elseif ($orderitem->stickertype == 7){
				$type = 'strykfast';
			} elseif ($orderitem->stickertype == 8){
				$type = 'refleksmerker';
			} elseif ($orderitem->stickertype == 9){
				$type = 'strykefrie';
			} else {
				$type = 'N/A';
			}

			/**
			 * Add a row to the cart
			 *
			 * @param string|Array $id      Unique ID of the item|Item formated as array|Array of items
			 * @param string       $name    Name of the item
			 * @param int          $qty     Item qty to add to the cart
			 * @param float        $price   Price of one item
			 * @param Array        $options Array of additional options, such as 'size' or 'color'
			 */

			$cart = new Cart;
			$cart->id = $orderitem->id;
 			$cart->name = $orderitem->stickertype;
 			$cart->price = $orderitem->price;
			$cart->URL = '/merkelapper/' . $type . '?id=' . $orderitem->id . '&dvos=';
			$cart->image = '/img/stickerthumbnail/' . $orderitem->id . '.png';
			$cart->edit = '/merkelapper/' . $type . '?edit=' . $orderitem->id;
			$cart->width = $orderitem->width;
			$cart->height = $orderitem->height;
			$cart->antall = $orderitem->quantity;
			$cart->itemtype = 'original';
			$cart->session = Session::get('_token');
			$cart->save();

			//$jsonDataString, $stickerName
			include '../resources/views/stickerSubmit.blade.php';
			renderThumbnail($orderitem->json, $orderitem->id);
			$lang = app()->getLocale();
			return Redirect::to($lang . '/store/cart');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/store/cart')
			->with('message', 'Noe gikk galt')
			->withErrors($validator)
			->withInput();
	}

	public function postUpdatecart() {
		$validator = Validator::make(Input::all(), Orderitem::$rules);
		if ($validator->passes()) {
			$orderitem = Orderitem::find(Input::get('stickerid'));
			if($orderitem->order_id == NULL) {
				$orderitem->stickertype = Input::get('stickertype');
				if(Input::has('userid')) {
					$orderitem->user_id = Input::get('userid');
				}
				$orderitem->json = Input::get('stickerjson');
				if ($orderitem->status != 1) { //check if the sticker is locked for price change
					if(Input::has('Antall2')) {
						$orderitem->quantity = Input::get('Antall2');
						$orderitem->quantexp = Input::get('Antall2');
					} else {
						$orderitem->quantity = Input::get('quantity');
						$orderitem->quantexp = Input::get('quantity');
					}
					$orderitem->price = intval(Input::get('stickerprice'));
					$orderitem->width = Input::get('stickerwidth');
					$orderitem->height = Input::get('stickerheight');
				}
				include '../resources/views/stickerSubmit.blade.php';
					//$updateID = $orderitem->id;

					//$checkForUpdate = DB::select('select * from orderitems where connectedsticker = ?', [$updateID]);
					//foreach ($checkForUpdate as $result) {
					//		$stickerToUpdate = Orderitem::find($result->id);
					//		$stickerToUpdate->json = $orderitem->json;
					//		$stickerToUpdate->quantity = $orderitem->quantity;
					//		$stickerToUpdate->width = $orderitem->width;
					//		$stickerToUpdate->height = $orderitem->height;

					//		$item = Cart::find($stickerToUpdate->id);

					//		$item->price = $stickerToUpdate->price;
					//		$item->width = $stickerToUpdate->width;
					//		$item->height = $stickerToUpdate->height;
					//		$item->antall = $stickerToUpdate->quantity;
					//		$item->save();

					//		$stickerToUpdate->save();
					//		renderThumbnail($stickerToUpdate->json, $stickerToUpdate->id);

					//}

				$orderitem->save();

				$item = Cart::find(Input::get('stickerid'));
				$item->price = $orderitem->price;
				$item->width = $orderitem->width;
				$item->height = $orderitem->height;
				$item->antall = $orderitem->quantity;
				$item->save();

				renderThumbnail($orderitem->json, $orderitem->id);

				$lang = app()->getLocale();
				return Redirect::to($lang . '/store/cart');

			}
			// Update the sticker itself.
			$orderitem->stickertype = Input::get('stickertype');
			if(Input::has('userid')) {
				$orderitem->user_id = Input::get('userid');
			}
			$orderitem->json = Input::get('stickerjson');
			if ($orderitem->status != 1) { //check if the sticker is locked for price change
				$orderitem->quantity = Input::get('quantity');
				$orderitem->quantexp = Input::get('quantity');
				$orderitem->price = Input::get('stickerprice');
				$orderitem->width = Input::get('stickerwidth');
				$orderitem->height = Input::get('stickerheight');
			}

			include '../resources/views/stickerSubmit.blade.php';
			//$updateID = $orderitem->id;
			//$checkForUpdate = DB::select('select * from orderitems where connectedsticker = ?', [$updateID]);
			//foreach ($checkForUpdate as $result) {
			//		$stickerToUpdate = Orderitem::find($result->id);
			//		$stickerToUpdate->json = $orderitem->json;
			//		$stickerToUpdate->save();

			//		renderThumbnail($stickerToUpdate->json, $stickerToUpdate->id);

			//}
			$orderitem->save();

				renderThumbnail($orderitem->json, $orderitem->id);
				$lang = app()->getLocale();
			return Redirect::to($lang . '/order/details/' . $orderitem->order_id);
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/store/cart')
			->with('message', 'Noe gikk galt')
			->withErrors($validator)
			->withInput();
	}

	public function getCart() {
		$value = Session::get('_token');
		return View::make('store.cart')->with('products', Cart::where('session', '=', $value)->orderBy('id', 'DESC')->paginate(200));
	}

	public function getCheckout() {
		if(Cart::count() == 0) {
			$lang = app()->getLocale();
			return Redirect::to($lang . '/store/cart');
		}
		return View::make('store.checkout')->with('products', Cart::content());
	}

	public function postReg() {
	  if(Cart::count() == 0) {
	    $lang = app()->getLocale();
	    return Redirect::to($lang . '/order');
	  }
	  $currency = Currency::find(1);
	  //Currency 0 = nok, 1 = usd, 2 = sek, 3 = dkk, 4 = eur
	  $cart = Cart::content();

	  if(Input::get('shippingIsBilling')) {
	    $validator = Validator::make(Input::all(), Order::$rulesSameShipping);
	  } else {
	    $validator = Validator::make(Input::all(), Order::$rules);
	  }

	  if ($validator->passes() && !Input::get('shippingIsBilling')) {
	     $newUserGenerated = false;
	     $newUserId = Input::get('user_id');
	    // If user_id = 0, account is not logged in
	    if($newUserId == 0) {
	      // check if email is already registered
	      $users = DB::table('users')->where('email', '=', Input::get('email'))->get();
	      if($users->count() > 0) {
	         // ask user to log in
	         $lang = app()->getLocale();
	         return Redirect::to($lang . '/users/signin')
	          ->with('message', trans('message.accountfound'));
	      } else {
	         // create a new user
	         $newUserGenerated = true;
	         $user = New User;
	         $user->password = Hash::make(Input::get('firstname'));
	         if(Input::has('orgname')){
	          $user->orgname = Input::get('orgname');
	        }
	        if(Input::has('firstname')){
	          $user->firstname = (Input::get('firstname'));
	        }
	        if(Input::has('lastname')){
	          $user->lastname = (Input::get('lastname'));
	        }
	        if(Input::has('address')){
	          $user->address = (Input::get('address'));
	        }
	        if(Input::has('address2')){
	          $user->address2 = (Input::get('address2'));
	        }
	        if(Input::has('zipcode')){
	          $user->zipcode = (Input::get('zipcode'));
	        }
	        if(Input::has('city')){
	          $user->city = (Input::get('city'));
	        }
	        if(Input::has('state')){
	          $user->state = (Input::get('state'));
	        }
	        if(Input::has('country')){
	          $user->country = (Input::get('country'));
	        }
	        if(Input::has('billingorgname')){
	          $user->billingorgname = Input::get('billingorgname');
	        }
	        if(Input::has('billingfirstname')){
	          $user->billingfirstname = (Input::get('billingfirstname'));
	        }
	        if(Input::has('billinglastname')){
	          $user->billinglastname = (Input::get('billinglastname'));
	        }
	        if(Input::has('billingaddress')){
	          $user->billingaddress = (Input::get('billingaddress'));
	        }
	        if(Input::has('billingaddress2')){
	          $user->billingaddress2 = (Input::get('billingaddress2'));
	        }
	        if(Input::has('billingzipcode')){
	          $user->billingzipcode = (Input::get('billingzipcode'));
	        }
	        if(Input::has('billingcity')){
	          $user->billingcity = (Input::get('billingcity'));
	        }
	        if(Input::has('billingstate')){
	          $user->billingstate = (Input::get('billingstate'));
	        }
	        if(Input::has('billingcountry')){
	          $user->billingcountry = (Input::get('billingcountry'));
	        }
	        if(Input::has('email')){
	          $user->email = (Input::get('email'));
	        }
					 $user->currency = Session::get('theCurrency', 0);
	         $user->save();
	         $newUserId = $user->id;
	      }
	    }

	    $order = New Order;
	    $order->user_id = $newUserId;
	    $user = User::find($order->user_id);

	    if($order->user_id == 0){
	      $orderCurrency = Session::get('theCurrency', 0);
	      $order->currency = $orderCurrency;
	      $orderPaymenttype = 0;
	    } else {
	      $orderCurrency = $user->currency;
	      $order->currency = $user->currency;
	      $orderPaymenttype = $user->paymenttype;
	    }


			$discountTotal = 0;
			$discountTotalSEK = 0;
			$discountTotalDKK = 0;
			$coupon = Session::get('coupon', 'NA');
			if ($coupon != 'NA') {
				$couponEffect 	= Session::get('coupon.effect');
				$couponSticker 	= Session::get('coupon.sticker');
				$couponAmount 	= Session::get('coupon.amount');
			}

			foreach($cart as $item) {
	      $sticker = Orderitem::find($item->id);
				if ($coupon != 'NA') {
					if($couponEffect == 1) {
						if($couponSticker == 99 || $couponSticker == $sticker->stickertype) {
							$discountTotal += floor( $sticker->price *((100 - $couponAmount)/100));
							$discountTotalSEK += floor(($sticker->price/$currency->sek) *((100 - $couponAmount)/100));
							$discountTotalDKK += floor(($sticker->price/$currency->dkk) *((100 - $couponAmount)/100));
	            $sticker->price = floor( $sticker->price *((100 - $couponAmount)/100));
							$sticker->quantexp = $sticker->quantity;
						} else {
							$sticker->quantexp = $sticker->quantity;
							$discountTotal += $sticker->price;
							$discountTotalSEK += floor($sticker->price/$currency->sek);
							$discountTotalDKK += floor($sticker->price/$currency->dkk);
						}
					} else if($couponEffect == 2){ // double
						$sticker->quantexp = $sticker->quantity * 2;
						$discountTotal += $sticker->price;
						$discountTotalSEK += floor($sticker->price/$currency->sek);
						$discountTotalDKK += floor($sticker->price/$currency->dkk);
					} else {
						$sticker->quantexp = $sticker->quantity;
						$discountTotal += $sticker->price;
						$discountTotalSEK += floor($sticker->price/$currency->sek);
						$discountTotalDKK += floor($sticker->price/$currency->dkk);
					}
				} else {
					$sticker->quantexp = $sticker->quantity;
					$discountTotal += $sticker->price;
					$discountTotalSEK += floor($sticker->price/$currency->sek);
					$discountTotalDKK += floor($sticker->price/$currency->dkk);
				}
	      $sticker->save();
	    }

	    $orderamount = $discountTotal;
			$orderamountSEK = $discountTotalSEK;
			$orderamountDKK = $discountTotalDKK;
	    if(Input::has('orderreferance')) {
	      $user->orderreferance = Input::get('orderreferance');
	      $user->save();
	    }
	    $order->currency = $orderCurrency;
	    if($orderPaymenttype == 1) {
	      if($orderCurrency == 0) { // nok
	        $order->amount = ($orderamount + 129);
	        $order->fee = 129;
	      } else if ($orderCurrency == 1) { // usd
	        $order->amount = ($orderamount + 129)/$currency->usd;
	        $order->fee = 129/$currency->usd;
	      } else if ($orderCurrency == 2) { // sek
	        $order->amount = floor($orderamountSEK) + floor(129/$currency->sek);
	        $order->fee = floor(129/$currency->sek);
	      } else if ($orderCurrency == 3) { // dkk
	        $order->amount = floor($orderamountDKK) + floor(129/$currency->dkk);
	        $order->fee = floor(129/$currency->dkk);
	      } else if ($orderCurrency == 4) { // eur
	        $order->amount = ($orderamount + 129)/$currency->eur;
	        $order->fee = 129/$currency->eur;
	      }
	    } else {
				if($orderamount >= 145) {
					if($orderCurrency == 0) { // nok
		        $order->amount = ($orderamount);
		        $order->fee = 0;
		      } else if ($orderCurrency == 1) { // usd
		        $order->amount = ($orderamount)/$currency->usd;
		        $order->fee = 0;
		      } else if ($orderCurrency == 2) { // sek
		        $order->amount = floor($orderamountSEK);
		        $order->fee = 0;
		      } else if ($orderCurrency == 3) { // dkk
		        $order->amount = floor($orderamountDKK);
		        $order->fee = 0;
		      } else if ($orderCurrency == 4) { // eur
		        $order->amount = ($orderamount)/$currency->eur;
		        $order->fee = 0;
		      }
				} else {
		      if($orderCurrency == 0) { // nok
		        $order->amount = ($orderamount + 29);
		        $order->fee = 29;
		      } else if ($orderCurrency == 1) { // usd
		        $order->amount = ($orderamount + 29)/$currency->usd;
		        $order->fee = 29/$currency->usd;
		      } else if ($orderCurrency == 2) { // sek
		        $order->amount = floor($orderamountSEK) + floor(29/$currency->sek);
		        $order->fee = floor(29/$currency->sek);
		      } else if ($orderCurrency == 3) { // dkk
		        $order->amount = floor($orderamountDKK) + floor(29/$currency->dkk);
		        $order->fee = floor(29/$currency->dkk);
		      } else if ($orderCurrency == 4) { // eur
		        $order->amount = ($orderamount + 29)/$currency->eur;
		        $order->fee = 29/$currency->eur;
		      }
				}
	    }

	    $order->order_status = (Input::get('order_status'));

	    if(Input::has('orgname')){
	      $order->orgname = Input::get('orgname');
	    }
	    if(Input::has('firstname')){
	      $order->firstname = (Input::get('firstname'));
	    }
	    if(Input::has('lastname')){
	      $order->lastname = (Input::get('lastname'));
	    }
	    if(Input::has('address')){
	      $order->address = (Input::get('address'));
	    }
	    if(Input::has('address2')){
	      $order->address2 = (Input::get('address2'));
	    }
	    if(Input::has('zipcode')){
	      $order->zipcode = (Input::get('zipcode'));
	    }
	    if(Input::has('city')){
	      $order->city = (Input::get('city'));
	    }
	    if(Input::has('state')){
	      $order->state = (Input::get('state'));
	    }
	    if(Input::has('country')){
	      $order->country = (Input::get('country'));
	    }

	    if(Input::has('billingorgname')){
	      $order->billingorgname = Input::get('billingorgname');
	    }
	    if(Input::has('billingfirstname')){
	      $order->billingfirstname = (Input::get('billingfirstname'));
	    }
	    if(Input::has('billinglastname')){
	      $order->billinglastname = (Input::get('billinglastname'));
	    }
	    if(Input::has('billingaddress')){
	      $order->billingaddress = (Input::get('billingaddress'));
	    }
	    if(Input::has('billingaddress2')){
	      $order->billingaddress2 = (Input::get('billingaddress2'));
	    }
	    if(Input::has('billingzipcode')){
	      $order->billingzipcode = (Input::get('billingzipcode'));
	    }
	    if(Input::has('billingcity')){
	      $order->billingcity = (Input::get('billingcity'));
	    }
	    if(Input::has('billingstate')){
	      $order->billingstate = (Input::get('billingstate'));
	    }
	    if(Input::has('billingcountry')){
	      $order->billingcountry = (Input::get('billingcountry'));
	    }
	    $order_date_stamp = new DateTime;
	    $order->order_date = $order_date_stamp->format('Y-m-d H:i:s');
	    if(Input::has('email')){
	      $order->email = (Input::get('email'));
	    }
	    if($newUserGenerated) {
	      $order->memo = "newuser";
	    }
	    $order->save();
	    $user = User::find($order->user_id);
	    $currency = Currency::find(1);
	    foreach ($cart as $item) {
	      $sticker = Orderitem::find($item->id);
	      $sticker->status = 1;
	      $sticker->order_id = $order->id;
	      $sticker->user_id = $user->id;
	      if($orderCurrency == 0) {
	        $sticker->price = $sticker->price;
	      }else if ($orderCurrency == 1) {
	        $sticker->price =  $sticker->price/$currency->usd;
	      }else if ($orderCurrency == 2) {
	        $sticker->price =  floor($sticker->price/$currency->sek);
	      }else if ($orderCurrency == 3) {
	        $sticker->price =  floor($sticker->price/$currency->dkk);
	      }else if ($orderCurrency == 4) {
	        $sticker->price =  $sticker->price/$currency->eur;
	      }

	      $sticker->save();
	    }
	    Cart::clear();
	    if ($orderPaymenttype == 1) {
	      return View::make('store.status')->with('order',$order);
	    }

	    return View::make('store.reg')->with('order', $order);


	  } elseif($validator->passes()) {
	    $newUserGenerated = false;
	    $newUserId = Input::get('user_id');
	   // If user_id = 0, account is not logged in
	   if($newUserId == 0) {
	     // check if email is already registered
	     $users = DB::table('users')->where('email', '=', Input::get('email'))->get();
	     if($users->count() > 0) {
	        // ask user to log in
	        $lang = app()->getLocale();
	        return Redirect::to($lang . '/users/signin')
	         ->with('message', trans('message.accountfound'));
	     } else {
	        // create a new user
	        $newUserGenerated = true;
	        $user = New User;
	        $user->password = Hash::make(Input::get('firstname'));
	        if(Input::has('orgname')){
	         $user->orgname = Input::get('orgname');
	         $user->billingorgname = Input::get('orgname');
	       }
	       if(Input::has('firstname')){
	         $user->firstname = (Input::get('firstname'));
	         $user->billingfirstname = (Input::get('firstname'));
	       }
	       if(Input::has('lastname')){
	         $user->lastname = (Input::get('lastname'));
	          $user->billinglastname = (Input::get('lastname'));
	       }
	       if(Input::has('address')){
	         $user->address = (Input::get('address'));
	          $user->billingaddress = (Input::get('address'));
	       }
	       if(Input::has('address2')){
	         $user->address2 = (Input::get('address2'));
	          $user->billingaddress2 = (Input::get('address2'));
	       }
	       if(Input::has('zipcode')){
	         $user->zipcode = (Input::get('zipcode'));
	         $user->billingzipcode = (Input::get('zipcode'));
	       }
	       if(Input::has('city')){
	         $user->city = (Input::get('city'));
	         $user->billingcity = (Input::get('city'));
	       }
	       if(Input::has('state')){
	         $user->state = (Input::get('state'));
	          $user->billingstate = (Input::get('state'));
	       }
	       if(Input::has('country')){
	         $user->country = (Input::get('country'));
	         $user->billingcountry = (Input::get('country'));
	       }
	       if(Input::has('email')){
	         $user->email = (Input::get('email'));
	       }
				  $user->currency = Session::get('theCurrency', 0);
	        $user->save();
	        $newUserId = $user->id;
	     }
	   }

	    $order = New Order;
	    $order->user_id = $newUserId;
	    $user = User::find($order->user_id);

			$discountTotal = 0;
			$discountTotalSEK = 0;
			$discountTotalDKK = 0;
			$coupon = Session::get('coupon', 'NA');
			if ($coupon != 'NA') {
				$couponEffect 	= Session::get('coupon.effect');
				$couponSticker 	= Session::get('coupon.sticker');
				$couponAmount 	= Session::get('coupon.amount');
			}
			foreach($cart as $item) {
	      $sticker = Orderitem::find($item->id);
				if ($coupon != 'NA') {
					if($couponEffect == 1) {
						if($couponSticker == 99 || $couponSticker == $sticker->stickertype) {
							$sticker->quantexp = $sticker->quantity;
							$discountTotal += floor( $sticker->price *((100 - $couponAmount)/100));
							$discountTotalSEK += floor(($sticker->price/$currency->sek) *((100 - $couponAmount)/100));
							$discountTotalDKK += floor(($sticker->price/$currency->dkk) *((100 - $couponAmount)/100));
	            $sticker->price = floor($sticker->price *((100 - $couponAmount)/100));
						} else {
							$sticker->quantexp = $sticker->quantity;
							$discountTotal += $sticker->price;
							$discountTotalSEK += floor($sticker->price/$currency->sek);
							$discountTotalDKK += floor($sticker->price/$currency->dkk);
						}
					} else if($couponEffect == 2){ // double
						$sticker->quantexp = $sticker->quantity * 2;
						$discountTotal += $sticker->price;
						$discountTotalSEK += floor($sticker->price/$currency->sek);
						$discountTotalDKK += floor($sticker->price/$currency->dkk);
					}else {
						$sticker->quantexp = $sticker->quantity;
						$discountTotal += $sticker->price;
						$discountTotalSEK += floor($sticker->price/$currency->sek);
						$discountTotalDKK += floor($sticker->price/$currency->dkk);
					}
				} else {
					$sticker->quantexp = $sticker->quantity;
					$discountTotal += $sticker->price;
					$discountTotalSEK += floor($sticker->price/$currency->sek);
					$discountTotalDKK += floor($sticker->price/$currency->dkk);
				}
	      $sticker->save();
	    }
	    $orderamount = $discountTotal;
			$orderamountSEK = $discountTotalSEK;
			$orderamountDKK = $discountTotalDKK;
	    if(Input::has('orderreferance')) {
	      $user->orderreferance = Input::get('orderreferance');
	      $user->save();
	    }

	    if($order->user_id == 0){
	      $orderCurrency = Session::get('theCurrency', 0);
	      $order->currency = $orderCurrency;
	      $orderPaymenttype = 0;
	    } else {
	      $orderCurrency = $user->currency;
	      $order->currency = $user->currency;
	      $orderPaymenttype = $user->paymenttype;
	    }

	    if($orderPaymenttype == 1) {
	      if($order->currency == 0) { // nok
	        $order->amount = ($orderamount + 129);
	        $order->fee = 129;
	      } else if ($order->currency == 1) { // usd
	        $order->amount = ($orderamount + 129)/$currency->usd;
	        $order->fee = 129/$currency->usd;
	      } else if ($order->currency == 2) { // sek
	        $order->amount = floor($orderamountSEK) + floor(129/$currency->sek);
	        $order->fee = floor(129/$currency->sek);
	      } else if ($order->currency == 3) { // dkk
	        $order->amount = floor($orderamountDKK) + floor(129/$currency->dkk);
	        $order->fee = floor(129/$currency->dkk);
	      } else if ($order->currency == 4) { // eur
	        $order->amount = ($orderamount + 129)/$currency->eur;
	        $order->fee = 129/$currency->eur;
	      }
	    } else {
				if($orderamount >= 145) {
					if($orderCurrency == 0) { // nok
		        $order->amount = ($orderamount);
		        $order->fee = 0;
		      } else if ($orderCurrency == 1) { // usd
		        $order->amount = ($orderamount)/$currency->usd;
		        $order->fee = 0;
		      } else if ($orderCurrency == 2) { // sek
		        $order->amount = floor($orderamountSEK);
		        $order->fee = 0;
		      } else if ($orderCurrency == 3) { // dkk
		        $order->amount = floor($orderamountDKK);
		        $order->fee = 0;
		      } else if ($orderCurrency == 4) { // eur
		        $order->amount = ($orderamount)/$currency->eur;
		        $order->fee = 0;
		      }
				} else {
		      if($orderCurrency == 0) { // nok
		        $order->amount = ($orderamount + 29);
		        $order->fee = 29;
		      } else if ($orderCurrency == 1) { // usd
		        $order->amount = ($orderamount + 29)/$currency->usd;
		        $order->fee = 29/$currency->usd;
		      } else if ($orderCurrency == 2) { // sek
		        $order->amount = floor($orderamountSEK) + floor(29/$currency->sek);
		        $order->fee = floor(29/$currency->sek);
		      } else if ($orderCurrency == 3) { // dkk
		        $order->amount = floor($orderamountDKK) + floor(29/$currency->dkk);
		        $order->fee = floor(29/$currency->dkk);
		      } else if ($orderCurrency == 4) { // eur
		        $order->amount = ($orderamount + 29)/$currency->eur;
		        $order->fee = 29/$currency->eur;
		      }
				}
	    }

	    $order->order_status = (Input::get('order_status'));

	    if(Input::has('orgname')){
	      $order->orgname = Input::get('orgname');
	      $order->billingorgname = Input::get('orgname');
	    }
	    if(Input::has('firstname')){
	      $order->firstname = (Input::get('firstname'));
	      $order->billingfirstname = (Input::get('firstname'));
	    }
	    if(Input::has('lastname')){
	      $order->lastname = (Input::get('lastname'));
	      $order->billinglastname = (Input::get('lastname'));
	    }
	    if(Input::has('address')){
	      $order->address = (Input::get('address'));
	      $order->billingaddress = (Input::get('address'));
	    }
	    if(Input::has('address2')){
	      $order->address2 = (Input::get('address2'));
	      $order->billingaddress2 = (Input::get('address2'));
	    }
	    if(Input::has('zipcode')){
	      $order->zipcode = (Input::get('zipcode'));
	      $order->billingzipcode = (Input::get('zipcode'));
	    }
	    if(Input::has('city')){
	      $order->city = (Input::get('city'));
	      $order->billingcity = (Input::get('city'));
	    }
	    if(Input::has('state')){
	      $order->state = (Input::get('state'));
	      $order->billingstate = (Input::get('state'));
	    }
	    if(Input::has('country')){
	      $order->country = (Input::get('country'));
	      $order->billingcountry = (Input::get('country'));
	    }

	    $order_date_stamp = new DateTime;
	    $order->order_date = $order_date_stamp->format('Y-m-d H:i:s');
	    if(Input::has('email')){
	      $order->email = (Input::get('email'));
	    }
	    if($newUserGenerated) {
	      $order->memo = "newuser";
	    }
	    $order->save();
	    $user = User::find($order->user_id);
	    $currency = Currency::find(1);
	    foreach ($cart as $item) {
	      $sticker = Orderitem::find($item->id);
	      $sticker->status = 1;
	      $sticker->order_id = $order->id;
	      $sticker->user_id = $user->id;
	      if($orderCurrency == 0) {
	        $sticker->price = floor($sticker->price);
	      }else if ($orderCurrency == 1) {
	        $sticker->price =  $sticker->price/$currency->usd;
	      }else if ($orderCurrency == 2) {
	        $sticker->price =  floor($sticker->price/$currency->sek);
	      }else if ($orderCurrency == 3) {
	        $sticker->price =  floor($sticker->price/$currency->dkk);
	      }else if ($orderCurrency == 4) {
	        $sticker->price =  $sticker->price/$currency->eur;
	      }
	      $sticker->save();
	    }
	    Cart::clear();
	    if ($orderPaymenttype == 1) {
	      return View::make('store.isuccess')->with('order',$order);
	    }
	    return View::make('store.reg')
	      ->with('order', $order)
	      ->with('user', $user);

	  }

	  $lang = app()->getLocale();
	  return Redirect::to($lang . '/store/checkout')
	    ->with('message', 'Noe gikk galt')
	    ->withErrors($validator)
	    ->withInput()
	    ->with('products', Cart::content());
	}

	public function getStatus() {
		if(\Input::has('orderId')){
			$order_id = \Input::get('orderId');
			$order = \Order::find($order_id);
			$memo = $order->memo;
			$order->memo = "";
			$order->save();
		}

		$theuser = \markmaster\Models\User::find($order->user_id);
		if($order->user_id == 0) {
			$orderPaymenttype = 0;
		} else {
			$orderPaymenttype = $theuser->paymenttype;
		}

		$userfirstname = $theuser->firstname;
		$useremail = $theuser->email;

		$token = "&token=" . "5p_SsQ2-";
		$transaction_id = '&transactionId=' . \Input::get('transactionId');

		$process_auth_url = 'https://epayment.nets.eu/Netaxept/Process.aspx?MerchantId=545558&operation=AUTH';
		$process_auth_url = $process_auth_url . $token . $transaction_id;

		$auth_xml = simplexml_load_file($process_auth_url);
		$auth_response_code = $auth_xml->ResponseCode;
		//echo 'response code: ' . $auth_response_code;
		echo '<br /> <br />';

		// messages to output for error1 and error2
		$errorMessage = "";
		$errorResponseText = "";

		// cases
		$error1 = false;
		$error2 = false;
		$success = false;

		// if auth fails
		if ($auth_response_code != 'OK') {
			$errorXml = $auth_xml->Error;
			$message = $errorXml->Message;
			$responseText = $errorXml->Result->ResponseText;
			$error1 = true;
			$errorMessage = $message;
			$errorResponseText = $responseText;
		} else {
			$amount = '&amount=' . $order->amount;
			$process_capture_url = 'https://epayment.nets.eu/Netaxept/Process.aspx?merchantId=545558&operation=CAPTURE';
			$process_capture_url = $process_capture_url . $token . $transaction_id . $amount;
			$capture_xml = simplexml_load_file($process_capture_url);
			$capture_response_code = $capture_xml->ResponseCode;

			// if capture fails
			if ($capture_response_code != 'OK')
			{
				$errorXml = $capture_xml->Error;
				$message = $errorXml->Message;
				$responseText = $errorXml->Result->ResponseText;
				$error2 = true;
				$errorMessage = $message;
				$errorResponseText = $responseText;
			} else {
				$success = true;
			}
		}
		\Cart::clear();

		if($error1 && $orderPaymenttype != 1) { // auth fail
			$errorType = 'auth';
			return View::make('store.paymenterror')
			->with('memo', $memo)
			->with('errorType', $errorType)
			->with('userfirstname', $userfirstname)
			->with('useremail', $useremail);
		} elseif($error2  && $orderPaymenttype != 1) { // capture fail
			$errorType = 'cap';
			return View::make('store.paymenterror')
			->with('memo', $memo)
			->with('errorType', $errorType)
			->with('userfirstname', $userfirstname)
			->with('useremail', $useremail);
		} elseif($orderPaymenttype == 1) { // invoice
			$order->order_status = 6;
			$order_date = new DateTime;
			$order->payment_date = $order_date->format('Y-m-d H:i:s');
			$order->save();
			$data = array(
				'customernr'=> $order->user_id,
				'ordernr'=> $order->id);

			\Mail::send('emails.orderconfirmation', $data, function($message) use ($order)
			{
				$message->to($order->email, $order->firstname . ' ' . $order->lastname)->subject('Ordrebekreftelse');
			});
			// redirect to success view
			return View::make('store.isuccess')
			->with('order', $order);

		} else { // success
			$order = \Order::find($order_id);
			$order->order_status = 1;
			$order_date = new DateTime;
			$order->payment_date =  $order_date->format('Y-m-d H:i:s');
			$order->save();

			$faktura = new Faktura;
			$faktura->user_id = $order->user_id;
			$faktura->order_id = $order->id;
			$faktura->currency = $order->currency;
			$faktura->fee = $order->fee;
			$faktura->faktura_date = $order->payment_date;
			$nomvaprice = $order->amount / 125 * 100;
			$faktura->priceeksmva = $nomvaprice;
			$faktura->priceinkmva = $order->amount;
			if($order->billingorgname) {
				$faktura->billingorgname = $order->billingorgname;
			}
			$faktura->billingfirstname = $order->billingfirstname;
			$faktura->billinglastname = $order->billinglastname;
			$faktura->billingaddress = $order->billingaddress;
			if($order->billingaddress2){
				$faktura->billingaddress2 = $order->billingaddress2;
			}
			$faktura->billingzipcode = $order->billingzipcode;
			$faktura->billingcity = $order->billingcity;
			$faktura->billingstate = $order->billingstate;
			$faktura->billingcountry = $order->billingcountry;
			$faktura->save();

			$data = array(
				'customernr'=> $order->user_id,
				'ordernr'=> $order_id);

			\Mail::send('emails.orderconfirmation', $data, function($message) use ($order)
			{
				$message->to($order->email, $order->firstname . ' ' . $order->lastname)->subject('Ordrebekreftelse');
			});
		}
		// redirect to success view
		return View::make('store.success')
		->with('memo', $memo)
		->with('order', $order)
		->with('userfirstname', $userfirstname)
		->with('useremail', $useremail);
	}

	public function getRemoveitem($rowid) {

		$itemToDelete = Cart::find($rowid);

		//$checkForDelete = DB::select('select * from orderitems where connectedsticker = ?', [$itemToDelete->id]);
		//foreach ($checkForDelete as $result) {
		//	Cart::remove($result->id);
		//}

		Cart::remove($rowid);
		$lang = app()->getLocale();
		return Redirect::to($lang . '/store/cart');
	}

	public function getContact() {
		return View::make('store.contact');
	}


	public function postCoupon() {
		include '../resources/views/stickerSubmit.blade.php';
		// Check the input posted if it is a valid coupon.
		$inputcode = Input::get('coupon');
		$results = DB::select('select * from coupons where code = ?', [$inputcode]);
		foreach ($results as $result) {
			$coupon = Coupon::find($result->id);
			$coupon->counts = $coupon->counts + 1;
			$coupon->save();
		}

		// If the coupon is valid, Make sure old coupon is removed and only current one is in here
		if($results){
			// Check if the coupon is valid
			$time = strtotime($coupon->expirationdate);
			$mytime = time();
			if ( ($mytime < $time) && $coupon->validation == 1) {
				// Clear any old coupon

				//get cart content
				$cart = Cart::content();
				//Check if item is either coupon value item or coupon
				foreach ($cart as $cartitem){
					if($cartitem->itemtype == 'coupon'){
						Cart::remove($cartitem->id);
					}
				}

				if($coupon->type == 1) {
					$type = 'klistremerker';
				} elseif ($coupon->type == 2) {
					$type = 'strykfast-farge';
				} elseif ($coupon->type == 3){
					$type = 'klistremerker-gull';
				} elseif ($coupon->type == 4){
					$type = 'klistremerker-solv';
				} elseif ($coupon->type == 5){
					$type = 'klistremerker-transparent';
				} elseif ($coupon->type == 6){
					$type = 'billige';
				} elseif ($coupon->type == 7){
					$type = 'strykfast';
				} elseif ($coupon->type == 8){
					$type = 'refleksmerker';
				} elseif ($coupon->type == 9){
					$type = 'strykefrie';
				} else {
					$type = 'N/A';
				}

				// If coupon is of double item type
				if ($coupon->effect == 2) {
					$sessionCoupon = ['amount' => $coupon->amount, 'sticker' => $coupon->type, 'effect' => $coupon->effect];
					Session::put('coupon', $sessionCoupon);
				} else { //Coupon is of subtract % type
					// Add or modify Session
					$sessionCoupon = ['amount' => $coupon->amount, 'sticker' => $coupon->type, 'effect' => $coupon->effect];
					Session::put('coupon', $sessionCoupon);

				}

				// return user to cart page
				$lang = app()->getLocale();
				return Redirect::to($lang . '/store/cart')->with('message', 'Rabattkode lagt inn');

			} // is not valid, let user know that it is an expired coupon code

			$lang = app()->getLocale();
			return Redirect::to($lang . '/store/cart')->with('message', 'Ikke lengre gyldig kode');
		} // invalid, return user to cart page with error message, check spelling etc.

		$lang = app()->getLocale();
		return Redirect::to($lang . '/store/cart')->with('message', 'Sjekk riktig staving');

	}
}
