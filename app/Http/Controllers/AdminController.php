<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class AdminController extends BaseController {

	public function __construct() {
		parent::__construct();
		//$this->beforeFilter('csrf', array('on'=>'post'));
		//$this->beforeFilter('admin');
	}

	public function getPreview() {
		return View::make('admin.preview');
	}

	public function getFixstickers() {
		return View::make('admin.fixstickers');
	}

	public function getIndex() {
		return View::make('admin.index');
	}

	public function getExport() {
		return View::make('admin.export');
	}

	public function getNewiexport() {
		return View::make('admin.newiexport');
	}

	public function getNewexport() {
		return View::make('admin.newexport');
	}

	public function postHandleexport() {
		return View::make('admin.handleexport');
	}

	public function getIcast() {
		return View::make('admin.icast'); // change to only take orders ready for export.
	}

	public function getProcessexports() {
		return View::make('admin.processexports');

	}

	public function getSingleexport($id) {
		if(Order::find($id)) {
			return View::make('admin.singleexport')
				->with('order', Order::find($id))
				->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));
		}
		$lang = app()->getLocale();
		return redirect()->to($lang . '/admin/orders')
				->with('message', 'Ugyldig Bestillings ID.');
	}

	public function getProcessicast() {
		return View::make('admin.processicast');

	}

	public function getOrders() {
		return View::make('admin.orders')
		->with('orders', Order::orderBy('id', 'DESC')->paginate(50));

	}

	public function getOrderedit($id) {
		if(Order::find($id))
		{
			return View::make('admin.orderedit')
				->with('order', Order::find($id))
				->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));
		}
		$lang = app()->getLocale();
		return redirect()->to($lang . '/admin/orders')
				->with('message', 'Ugyldig Bestillings ID.');
	}

	public function getCredit($id) {
		if(Order::find($id))
		{
			$fakturas = Faktura::where('order_id', '=', $id)->paginate(1);

			foreach ($fakturas as $faktura) {
				$faktura_id = $faktura->id;
			}


			return View::make('admin.credit')
			->with('order', Order::find($id))
			->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999))
			->with('faktura', Faktura::find($faktura_id));
		}
	}

	public function postCredit($id) {
		if(Faktura::find($id))
		{
			$faktura = new Cfaktura;


			$faktura->faktura_date =  Input::get('faktura_date');

			$faktura->user_id = Input::get('user_id');
			$faktura->order_id = Input::get('order_id');
			$faktura->faktura_id = Input::get('faktura_id');
			$faktura->note = Input::get('note');
			$faktura->priceinkmva = Input::get('amount');
			$faktura->priceeksmva = $faktura->priceinkmva * 0.8;
			if (Input::has('billingorgname')) {
				$faktura->billingorgname = Input::get('billingorgname');
			}
			if (Input::has('billingfirstname')) {
				$faktura->billingfirstname = Input::get('billingfirstname');
			}
			if (Input::has('billinglastname')) {
				$faktura->billinglastname = Input::get('billinglastname');
			}
			if (Input::has('billingaddress')) {
				$faktura->billingaddress = Input::get('billingaddress');
			}
			if (Input::has('billingaddress2')) {
				$faktura->billingaddress2 = Input::get('billingaddress2');
			}
			$faktura->billingzipcode = Input::get('billingzipcode');
			$faktura->billingcity = Input::get('billingcity');
			if (Input::has('billingcountry')) {
				$faktura->billingcountry = Input::get('billingcountry');
			}

			$faktura->save();

			$lang = app()->getLocale();
			return redirect()->to($lang . '/admin/orders')
					->with('message', 'Bestillingen er kreditert');

		}
	}

	public function postOrderedit($id){
		if(Orderitem::find($id))
		{
			$orderitem = Orderitem::find($id);
			$orderitem->stickertype = Input::get('stickertype');
			$orderitem->json = Input::get('stickerjson');
			if ($orderitem->status != 1) { //check if the sticker is locked for price change
				$orderitem->quantity = Input::get('quantity');
				$orderitem->price = Input::get('stickerprice');
				$orderitem->width = Input::get('stickerwidth');
				$orderitem->height = Input::get('stickerheight');
			}
			$orderitem->save();

			include '../resources/views/stickerSubmit.blade.php';
				renderThumbnail($orderitem->json, $orderitem->id);

				$lang = app()->getLocale();

			return Redirect::to($lang . '/admin/orderedit/' . $orderitem->order_id)
				->with('message', 'Bestilling er oppdatert');
		}
	}

	public function postUpdatesticker(){
		include '../resources/views/stickerSubmit.blade.php';

		$lang = app()->getLocale();

		if(Orderitem::find(Input::get('stickerid')) )
		{
			$orderitem = Orderitem::find(Input::get('stickerid'));
			$orderitem->stickertype = Input::get('stickertype');
			if($orderitem->status == 2) {
				$orderitem->status = 1;
			}
			$orderitem->json = Input::get('stickerjson');
			if($orderitem->stickertype == 6 || $orderitem->stickertype == 7) {
				$orderitem->quantity = Input::get('Antall2');
			} else {
				$orderitem->quantity = Input::get('quantity');
			}
			$orderitem->price = Input::get('stickerprice');
			$orderitem->width = Input::get('stickerwidth');
			$orderitem->height = Input::get('stickerheight');

			//$updateID = $orderitem->id;
			//		$checkForUpdate = DB::select('select * from orderitems where connectedsticker = ?', [$updateID]);
			//		foreach ($checkForUpdate as $result) {
			//			$stickerToUpdate = Orderitem::find($result->id);
			//			$stickerToUpdate->json = $orderitem->json;
			//			$stickerToUpdate->quantity = $orderitem->quantity;
			//			$stickerToUpdate->width = $orderitem->width;
			//			$stickerToUpdate->height = $orderitem->height;

			//			$stickerToUpdate->save();
			//			renderThumbnail($stickerToUpdate->json, $stickerToUpdate->id);

			//		}


			$orderitem->save();

				renderThumbnail($orderitem->json, $orderitem->id);
			return Redirect::to($lang . '/admin/orderedit/' . $orderitem->order_id)
				->with('message', 'Bestilling er oppdatert');
		}
		return Redirect::to($lang . '/admin/orderedit/' . $orderitem->order_id)
				->with('message', 'Noe gikk galt. Fant ikke etiketten.');
	}

	public function getOrderdelete($id) {
		$lang = app()->getLocale();
		if(Order::find($id))
		{
			$order = Order::find($id);
			$order->delete();
			return Redirect::to($lang . '/admin/orders')
				->with('message', 'Bestilling slettet.');
		}

		return Redirect::to($lang . '/admin/orders')
				->with('message', 'Ugyldig Bestillings ID.');

	}

	public function getOrderreset($id) {
		$lang = app()->getLocale();
		if(Order::find($id))
		{
			// change the order status
			$order = Order::find($id);

			// find the user
			if(User::find($order->user_id)) {
				$user = User::find($order->user_id);
				if($user->paymenttype == 1) { // user is invoice type
					$order->order_status = 6;
				} else { // regular user
					$order->order_status = 1;
				}
			} else {
					$order->order_status = 1;
			}

			$order->save();

			$checkForUpdate = DB::select('select * from orderitems where order_id = ?', [$id]);
			foreach ($checkForUpdate as $result) {
					$stickerToUpdate = Orderitem::find($result->id);
					$stickerToUpdate->status = 1;
					$stickerToUpdate->save();
			}

			// change the sticker status
			return Redirect::to($lang . '/admin/orders')
				->with('message', 'Export Reset Complete.');
		}

		return Redirect::to($lang . '/admin/orders')
				->with('message', 'Ugyldig Bestillings ID.');

	}

	public function getOrdersearch() {
		// if (!(Input::has('field') && Input::has('search')))
			// return;

		include '../resources/views/admin/adminutil.php';

		if (Input::has('page'))
		{
			$numPerPage = 50;
			$orders = Order::orderBy('id', 'DESC')->paginate($numPerPage);

			foreach ($orders as $order)
			{
				echo getOrderAsTableRow($order);
			}
			return;
		}

		$field = Input::get('field');
		$search = Input::get('search');

		if ($field == 'any')
		{
			$orders = 	 Order::where('id', 'LIKE', '%' . $search . '%')
							->orWhere('firstname', 'LIKE', '%' . $search . '%')
							->orWhere('lastname', 'LIKE', '%' . $search . '%')
							->orWhere('user_id', 'LIKE', '%' . $search . '%')
							->orWhere('order_date', 'LIKE', '%' . $search . '%')
							->orWhere('amount', 'LIKE', '%' . $search . '%')
							->orWhere('order_status', 'LIKE', '%' . $search . '%')
							->orWhere('export_date', 'LIKE', '%' . $search . '%')
							->orWhere('credit_date', 'LIKE', '%' . $search . '%')
							->orderBy('id', 'desc')
							->get();

			foreach ($orders as $order)
			{
				echo getOrderAsTableRow($order);
			}
		}
		else if ($field == 'name')
		{
			$names = explode(" ", $search);
			if (sizeof($names) == 2)
			{
				$firstname = $names[0];
				$lastname = $names[1];

				$orders = Order::where('firstname', 'LIKE',  '%' . $firstname . '%')
								->where('lastname', 'LIKE', '%' . $lastname . '%');
			}
			else
			{
				$orders = Order::where('firstname', 'LIKE', '%' . $search . '%')
								->orWhere('lastname', 'LIKE', '%' . $search . '%');

				// $orders = Order::where($field, 'LIKE', '%' . $search . '%')
			}

			$orders = $orders->orderBy('id', 'desc')
							 ->get();
			foreach ($orders as $order)
			{
				echo getOrderAsTableRow($order);
			}
		}
		else
		{
			$orders = Order::where($field, $search);

			$orders = Order::where($field, 'LIKE', '%' . $search . '%')
							->orderBy('id', 'desc')
							->get();
			foreach ($orders as $order)
			{
				echo getOrderAsTableRow($order);
			}
		}
	}

	public function getUsersearch() {
		// if (!(Input::has('field') && Input::has('search')))
			// return;

		include '../resources/views/admin/adminutil.php';

		if (Input::has('page'))
		{
			$numPerPage = 50;
			$users = User::orderBy('id', 'DESC')->paginate($numPerPage);

			foreach ($users as $user)
			{
				echo getUserAsTableRow($user);
			}
			return;
		}

		$field = Input::get('field');
		$search = Input::get('search');



		if ($field == 'any')
		{
			$users = 	 User::where('id', 'LIKE', '%' . $search . '%')
							->orWhere('firstname', 'LIKE', '%' . $search . '%')
							->orWhere('lastname', 'LIKE', '%' . $search . '%')
							->orWhere('telephone', 'LIKE', '%' . $search . '%')
							->orWhere('email', 'LIKE', '%' . $search . '%')
							->orWhere('address', 'LIKE', '%' . $search . '%')
							->orWhere('city', 'LIKE', '%' . $search . '%')
							->orWhere('state', 'LIKE', '%' . $search . '%')
							->orWhere('zipcode', 'LIKE', '%' . $search . '%')
							->orWhere('country', 'LIKE', '%' . $search . '%')
							->orderBy('id', 'desc')
							->get();

			foreach ($users as $user)
			{
				echo getUserAsTableRow($user);
			}
		}
		else if ($field == 'name')
		{
			$names = explode(" ", $search);
			if (sizeof($names) == 2)
			{
				$firstname = $names[0];
				$lastname = $names[1];

				$users = User::where('firstname', 'LIKE',  '%' . $firstname . '%')
								->where('lastname', 'LIKE', '%' . $lastname . '%');
			}
			else
			{
				$users = User::where('firstname', 'LIKE', '%' . $search . '%')
								->orWhere('lastname', 'LIKE', '%' . $search . '%');

				// $orders = Order::where($field, 'LIKE', '%' . $search . '%')
			}

			$users = $users->orderBy('id', 'desc')
							 ->get();
			foreach ($users as $user)
			{
				echo getUserAsTableRow($user);
			}
		}
		else
		{
			$users = User::where($field, $search);

			$users = User::where($field, 'LIKE', '%' . $search . '%')
							->orderBy('id', 'desc')
							->get();
			foreach ($users as $user)
			{
				echo getUserAsTableRow($user);
			}
		}
	}

	public function postUpdateorder($id) {
		if(Order::find($id))
		{
			$order = Order::find($id);
			// opdatere bestillingen
			$order->amount = Input::get('amount');
			$order->order_status = Input::get('order_status');
			$order->orgname = Input::get('orgname');
			$order->firstname = Input::get('firstname');
			$order->lastname = Input::get('lastname');
			$order->address = Input::get('address');
			if(Input::has('address2')){
				$order->address2 = Input::get('address2');
			}
			$order->zipcode = Input::get('zipcode');
			$order->city = Input::get('city');
			$order->state = Input::get('state');
			$order->country = Input::get('country');
			$order->billingorgname = Input::get('billingorgname');
			$order->billingfirstname = Input::get('billingfirstname');
			$order->billinglastname = Input::get('billinglastname');
			$order->billingaddress = Input::get('billingaddress');
			if(Input::has('billingaddress2')){
				$order->billingaddress2 = Input::get('billingaddress2');
			}
			$order->billingzipcode = Input::get('billingzipcode');
			$order->billingcity = Input::get('billingcity');
			$order->billingstate = Input::get('billingstate');
			$order->billingcountry = Input::get('billingcountry');
			$order->memo = Input::get('order_memo');
			$order->save();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/orders')
				->with('message', 'Bestilling Oppdatert.');
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/orders')
				->with('message', 'Ugyldig Bestillings ID.');
	}

	public function postUpdatecustomer($id) {
		if(User::find($id))
		{
			$user = User::find($id);
			// opdatere bestillingen
			if ($user->email == Input::get('email'))
			{
				$validator = Validator::make(Input::all(), User::$updateNErules);
			} else {
				$validator = Validator::make(Input::all(), User::$updaterules);
			}

			if ($validator->passes()) {
				$user->discount = Input::get('discount');
				$user->email = Input::get('email');
				$user->telephone = Input::get('telephone');
				if(Input::has('emailcampaign')) {
					$user->emailcampaign = true;
				} else {
					$user->emailcampaign = false;
				}
				$user->orgname = Input::get('orgname');
				$user->firstname = Input::get('firstname');
				$user->lastname = Input::get('lastname');
				$user->address = Input::get('address');
				$user->address2 = Input::get('address2');
				$user->zipcode = Input::get('zipcode');
				$user->city = Input::get('city');
				$user->state = Input::get('state');
				$user->country = Input::get('country');
				$user->billingorgname = Input::get('billingorgname');
				$user->billingfirstname = Input::get('billingfirstname');
				$user->billinglastname = Input::get('billinglastname');
				$user->billingaddress = Input::get('billingaddress');
				$user->billingaddress2 = Input::get('billingaddress2');
				$user->billingzipcode = Input::get('billingzipcode');
				$user->billingcity = Input::get('billingcity');
				$user->billingstate = Input::get('billingstate');
				$user->billingcountry = Input::get('billingcountry');
				$user->save();
				$lang = app()->getLocale();
				return Redirect::to($lang . '/admin/customers')
					->with('message', 'Bruker Oppdatert.');
			}
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/customers')
				->with('message', 'Noe gikk galt.')
				->withErrors($validator)
				->withInput();

		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/customers')
				->with('message', 'Ugyldig Bruker ID.');
	}

	public function getCustomers() {
		return View::make('admin.customers')
			->with('users', User::orderBy('id', 'DESC')->paginate(50));
	}

	public function getCustomeredit($id) {
		if(User::find($id))
		{
			return View::make('admin.customeredit')
				->with('user', User::find($id));
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/customers')
				->with('message', 'Invalid Id passed');
	}

	public function getCustomerdelete($id) {
		if(User::find($id))
		{
			$user = User::find($id);
			$user->delete();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/customers')
				->with('message', 'Bruker slettet.');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/customers')
				->with('message', 'Ugyldig Bruker ID.');
	}

	public function getAdminsticker($id){
		if(Orderitem::find($id)){
			return View::make('admin.adminsticker')
				->with('sticker', Orderitem::find($id));
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/')
				->with('message', 'Kunne ikke finne etiketten.');
	}

	public function getRemoveitem($id) {
		$sticker = Orderitem::find($id);
		$sticker->order_id = null;
		$sticker->save();

		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/orders');
	}

	public function getClipartget() {
		return View::make('admin.clipartget');
	}

	public function postClipartget() {
		return View::make('admin.clipartget');
	}

	public function getBackgroundget() {
		return View::make('admin.backgroundget');
	}

	public function postBackgroundget() {
		return View::make('admin.backgroundget');
	}

	public function postAgent() {
	$validator = Validator::make(Input::all(), Commercialagent::$rules);

		if ($validator->passes()) {
			$agent = new Commercialagent;
			$agent->name = Input::get('name');
			$agent->address = Input::get('address');
			$agent->zip = Input::get('zip');
			$agent->city = Input::get('city');
			$agent->telephone = Input::get('telephone');
			$agent->url = Input::get('url');
			$agent->contactname = Input::get('contactname');
			$agent->contacttelephone = Input::get('contacttelephone');
			$agent->contactmail = Input::get('contactmail');
			$agent->save();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/')->with('message', 'Ny Reklame agent opprettet');

		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/')
			->with('message', 'Noe gikk galt')
			->withErrors($validator)
			->withInput();
	}

	public function getAgent() {
		return View::make('admin.agent');
	}

	public function getCurrency() {
		return View::make('admin.currency')
			->with('currency', Currency::find(1));
	}

	public function postCurrency() {

		$currency = Currency::find(1);
		$currency->usd = Input::get('usd');
		$currency->sek = Input::get('sek');
		$currency->dkk = Input::get('dkk');
		$currency->eur = Input::get('eur');
		$currency->save();
		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/')->with('message', 'Oppdatert currency');
	}

	public function postUpdatestickerstatus($id) {
		if(Orderitem::find($id))
		{
			$orderitem = Orderitem::find($id);
			$orderitem->status = Input::get('stickerstatus');
			$orderitem->save();

			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/orderedit/' . $orderitem->order_id)
				->with('message', 'Status er oppdatert');
		}
	}

}
