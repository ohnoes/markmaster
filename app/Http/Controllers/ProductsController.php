<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ProductsController extends BaseController {

	public function __construct() {
		parent::__construct();
		//$this->beforeFilter('csrf', array('on'=>'post'));
		//$this->beforeFilter('admin');
	}

	public function getIndex() {
		$categories = array();

		foreach(Category::all() as $category) {
			$categories[$category->id] = $category->name;
		}

		return View::make('products.index')
			->with('products', Product::all())
			->with('categories', $categories);
	}

	public function postCreate() {
		$validator = Validator::make(Input::all(), Product::$rules);

		if ($validator->passes()) {
			$product = new Product;
			$product->category_id = Input::get('category_id');
			$product->title = Input::get('title');
			$product->description = Input::get('description');
			$product->price = Input::get('price');

			$image = Input::file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$destination = public_path() . '/img/products';
			Image::make($image->getRealPath())->resize(468, 249)->save($destination .'/'. $filename);
			$product->image = 'img/products/'.$filename;
			$product->save();

			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/products/')
				->with('message', 'Product Created');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/products/')
			->with('message', 'Something went wrong')
			->withErrors($validator)
			->withInput();
	}

	public function postDestroy() {
		$product = Product::find(Input::get('id'));

		if($product) {
			File::delete($product->image);
			$product->delete();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/products/')
				->with('message', 'Product Deleted');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/products/')
			->with('message', 'Something went wrong, please try again');
	}

	public function PostToggleAvailability() {
		$product = Product::find(Input::get('id'));

		if ($product) {
			$product->availability = Input::get('availability');
			$product->save();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/admin/products/')->with('message', 'Product Updated');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/admin/products/')->with('message', 'Invalid Product');
	}
}
