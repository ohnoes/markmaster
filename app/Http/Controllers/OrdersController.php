<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class OrdersController extends BaseController {

	public function __construct() {
		parent::__construct();


	}

	public function getIndex() {
		if(Auth::check()) {
			return View::make('order.index')
			->with('orders', Order::where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC')->paginate(10))
			->with('fakturas', Faktura::where('user_id', '=', Auth::user()->id)->paginate(9999));
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/users/signin')
			->with('message', 'Vennligst logg inn.');
	}

	public function getDetails($id) {
		if(Auth::check()) {
			if(Order::find($id)->user_id == Auth::user()->id)
			{
				return View::make('order.details')
					->with('order', Order::find($id))
					->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(100));
			}
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function getPay($id) {
		if(Order::find($id)->user_id == Auth::user()->id)
		{
			return View::make('order.pay')
				->with('order', Order::find($id));
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function getDelete($id) {
		if(Order::find($id)->user_id == Auth::user()->id)
		{
			$order = Order::find($id);
			$order->delete();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/order/')
				->with('message', 'Bestilling slettet.');
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function postReg() {

		if(Input::get('shippingIsBilling')) {
			$validator = Validator::make(Input::all(), Order::$rulesSameShipping);
		} else {
			$validator = Validator::make(Input::all(), Order::$rules);
		}

		if ($validator->passes() && !Input::get('shippingIsBilling')) {

			$order = Order::find(Input::get('order_id'));

			$user = User::find($order->user_id);
			if(Input::has('orderreferance')) {
				$user->orderreferance = Input::get('orderreferance');
				$user->save();
			}
			$user = User::find($order->user_id);
			if(Input::has('orderreferance')) {
				$user->orderreferance = Input::get('orderreferance');
				$user->save();
			}

			if(Input::has('orgname')){
				$order->orgname = Input::get('orgname');
			}
			if(Input::has('firstname')){
				$order->firstname = (Input::get('firstname'));
			}
			if(Input::has('lastname')){
				$order->lastname = (Input::get('lastname'));
			}
			if(Input::has('address')){
				$order->address = (Input::get('address'));
			}
			if(Input::has('address2')){
				$order->address2 = (Input::get('address2'));
			}
			if(Input::has('zipcode')){
				$order->zipcode = (Input::get('zipcode'));
			}
			if(Input::has('city')){
				$order->city = (Input::get('city'));
			}
			if(Input::has('state')){
				$order->state = (Input::get('state'));
			}
			if(Input::has('country')){
				$order->country = (Input::get('country'));
			}

			if(Input::has('billingorgname')){
				$order->billingorgname = Input::get('billingorgname');
			}
			if(Input::has('billingfirstname')){
				$order->billingfirstname = (Input::get('billingfirstname'));
			}
			if(Input::has('billinglastname')){
				$order->billinglastname = (Input::get('billinglastname'));
			}
			if(Input::has('billingaddress')){
				$order->billingaddress = (Input::get('billingaddress'));
			}
			if(Input::has('billingaddress2')){
				$order->billingaddress2 = (Input::get('billingaddress2'));
			}
			if(Input::has('billingzipcode')){
				$order->billingzipcode = (Input::get('billingzipcode'));
			}
			if(Input::has('billingcity')){
				$order->billingcity = (Input::get('billingcity'));
			}
			if(Input::has('billingstate')){
				$order->billingstate = (Input::get('billingstate'));
			}
			if(Input::has('billingcountry')){
				$order->billingcountry = (Input::get('billingcountry'));
			}

			$order->save();

			return View::make('order.reg')->with('order', $order);

		} elseif($validator->passes()) {
			$order = Order::find(Input::get('order_id'));

			$user = User::find($order->user_id);
			if(Input::has('orderreferance')) {
				$user->orderreferance = Input::get('orderreferance');
				$user->save();
			}

			$user = User::find($order->user_id);
			if(Input::has('orderreferance')) {
				$user->orderreferance = Input::get('orderreferance');
				$user->save();
			}

			if(Input::has('orgname')){
				$order->orgname = Input::get('orgname');
			}
			if(Input::has('firstname')){
				$order->firstname = (Input::get('firstname'));
			}
			if(Input::has('lastname')){
				$order->lastname = (Input::get('lastname'));
			}
			if(Input::has('address')){
				$order->address = (Input::get('address'));
			}
			if(Input::has('address2')){
				$order->address2 = (Input::get('address2'));
			}
			if(Input::has('zipcode')){
				$order->zipcode = (Input::get('zipcode'));
			}
			if(Input::has('city')){
				$order->city = (Input::get('city'));
			}
			if(Input::has('state')){
				$order->state = (Input::get('state'));
			}
			if(Input::has('country')){
				$order->country = (Input::get('country'));
			}

			if(Input::has('orgname')){
				$order->billingorgname = Input::get('orgname');
			}
			if(Input::has('firstname')){
				$order->billingfirstname = (Input::get('firstname'));
			}
			if(Input::has('lastname')){
				$order->billinglastname = (Input::get('lastname'));
			}
			if(Input::has('address')){
				$order->billingaddress = (Input::get('address'));
			}
			if(Input::has('address2')){
				$order->billingaddress2 = (Input::get('address2'));
			}
			if(Input::has('zipcode')){
				$order->billingzipcode = (Input::get('zipcode'));
			}
			if(Input::has('city')){
				$order->billingcity = (Input::get('city'));
			}
			if(Input::has('state')){
				$order->billingstate = (Input::get('state'));
			}
			if(Input::has('country')){
				$order->billingcountry = (Input::get('country'));
			}

			$order->save();

			return View::make('order.reg')->with('order', $order);

		}

		//////////////////////////////////////

		$validator = Validator::make(Input::all(), Order::$rules);

		if ($validator->passes()) {

			$order = Order::find(Input::get('order_id'));
			if(Input::has('orgname')){
				$order->orgname = Input::get('orgname');
			}
			if(Input::has('firstname')){
				$order->firstname = (Input::get('firstname'));
			}
			if(Input::has('lastname')){
				$order->lastname = (Input::get('lastname'));
			}
			if(Input::has('address')){
				$order->address = (Input::get('address'));
			}
			if(Input::has('address2')){
				$order->address2 = (Input::get('address2'));
			}
			if(Input::has('zipcode')){
				$order->zipcode = (Input::get('zipcode'));
			}
			if(Input::has('city')){
				$order->city = (Input::get('city'));
			}
			if(Input::has('state')){
				$order->state = (Input::get('state'));
			}
			if(Input::has('country')){
				$order->country = (Input::get('country'));
			}

			if(Input::has('billingorgname')){
				$order->billingorgname = Input::get('billingorgname');
			}
			if(Input::has('billingfirstname')){
				$order->billingfirstname = (Input::get('billingfirstname'));
			}
			if(Input::has('billinglastname')){
				$order->billinglastname = (Input::get('billinglastname'));
			}
			if(Input::has('billingaddress')){
				$order->billingaddress = (Input::get('billingaddress'));
			}
			if(Input::has('billingaddress2')){
				$order->billingaddress2 = (Input::get('billingaddress2'));
			}
			if(Input::has('billingzipcode')){
				$order->billingzipcode = (Input::get('billingzipcode'));
			}
			if(Input::has('billingcity')){
				$order->billingcity = (Input::get('billingcity'));
			}
			if(Input::has('billingstate')){
				$order->billingstate = (Input::get('billingstate'));
			}
			if(Input::has('billingcountry')){
				$order->billingcountry = (Input::get('billingcountry'));
			}

			$order->save();
			return View::make('order.reg')->with('order', $order);
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
			->with('message', 'Noe gikk galt')
			->withErrors($validator)
			->withInput();
	}

	public function getDeletesticker($id){
		if(Orderitem::find($id)->user_id == Auth::user()->id)
		{

			$itemToDelete = Orderitem::find($id);
			$deleteid = $itemToDelete->id;
			$checkForDelete = DB::select('select * from orderitems where connectedsticker = ?', [$deleteid]);
			foreach ($checkForDelete as $result) {
					$order = Order::find($result->order_id);
					$order->amount = $order->amount - $result->price;
					$order->save();
					$deleteSticker = Orderitem::find($result->id);
					$deleteSticker->delete();
			}


			$sticker = Orderitem::find($id);
			$order = Order::find($sticker->order_id);
			$order->amount = $order->amount - $sticker->price;
			$order->save();
			$sticker->delete();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/order/details/' . $order->id)
				->with('message', 'Etikett slettet.');
		}
		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

}
