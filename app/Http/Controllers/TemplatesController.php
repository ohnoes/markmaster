<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class TemplatesController extends BaseController {

	public function __construct() {
		parent::__construct();
		//$this->beforeFilter('csrf', array('on'=>'post'));
	}

	public function postCreate() {
		$validator = Validator::make(Input::all(), Template::$rules);

		if ($validator->passes()) {
			$template = new Template;
			$template->sortorder = Input::get('sortorder');
			$template->product_type = Input::get('product_type');
			$template->json = Input::get('json');
			$template->title = Input::get('title');
			$template->description = Input::get('description');
			$image = Input::file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$destination = public_path() . '/img/template';
			$upload = Input::file('image')->move($destination, $filename); // Fix the resizing to keep original format here
			$template->image = 'img/template/'.$filename;
			if(Input::has('searchwords')) {
				$template->searchwords = Input::get('searchwords');
			} else {
				$template->searchwords = '';
			}

			$template->save();
			$lang = app()->getLocale();
			return Redirect::to($lang . '/')
				->with('message', 'Nytt template opplastet.');
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/')
			->with('message', 'Noe gikk galt, vennlist prøv igjen.')
			->withErrors($validator)
			->withInput();
	}

	public function getKlistremerker() {

		$templates = DB::table('templates')->orderBy('sortorder', 'ASC')->paginate(9999);

		return View::make('templates.klisterfarge', ['templates' => $templates]);
			//->with('templates', Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getStrykfast_farge() {
		return View::make('templates.strykfastfarge')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getKlistremerker_gull() {
		return View::make('templates.gold')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getKlistremerker_solv() {
		return View::make('templates.silver')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getKlistremerker_transparent() {
		return View::make('templates.transparent')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getBillige() {
		return View::make('templates.klister')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getGull_eller_solv() {
		return View::make('templates.goldsilver');

	}

	public function getStrykfast() {
		return View::make('templates.strykfast')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getRefleksmerker() {
		return View::make('templates.reflection')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

	public function getStrykefrie() {
		return View::make('templates.fabricsticker')
			->with('templates', \Template::orderBy('sortorder', 'ASC')->paginate(9999));
	}

}
