<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class InvoiceController extends BaseController {

	public function getInvoice($id) {
			$fakturas = Faktura::where('order_id', '=', $id)->paginate(1);

			foreach ($fakturas as $faktura) {
				$faktura_id = $faktura->id;
			}
			if ($fakturas->count() > 0) {
				return View::make('invoice.invoice')
					->with('faktura', Faktura::find($faktura_id))
					->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));
			}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function getInvoicegen($id) {

			$fakturas = Faktura::where('order_id', '=', $id)->paginate(1);

			foreach ($fakturas as $faktura) {
				$faktura_id = $faktura->id;
			}

			if ($fakturas->count() > 0) {
				return View::make('invoice.invoicegen')
					->with('faktura', Faktura::find($faktura_id))
					->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));
			}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function getCredit($id) {

		$fakturas = Cfaktura::where('order_id', '=', $id)->paginate(1);
		$faktura_id = null;
			foreach ($fakturas as $faktura) {
				$faktura_id = $faktura->id;
			}

		if(Order::find($id) && Cfaktura::find($faktura_id))
		{
			$fakturas = Cfaktura::where('order_id', '=', $id)->paginate(1);

			foreach ($fakturas as $faktura) {
				$faktura_id = $faktura->id;
			}

			return View::make('invoice.credit')
				->with('faktura', Cfaktura::find($faktura_id))
				->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function getCreditgen($id) {
		if(Order::find($id))
		{
			$fakturas = Cfaktura::where('order_id', '=', $id)->paginate(1);

			foreach ($fakturas as $faktura) {
				$faktura_id = $faktura->id;
			}

			return View::make('invoice.creditgen')
				->with('faktura', Cfaktura::find($faktura_id))
				->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));
		}

		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function getPakkseddel($id) {


			$order = Order::find($id);
			return View::make('invoice.pakkseddel')
				->with('order', $order)
				->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));

		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}

	public function getPakkseddelgen($id) {
			$order = Order::find($id);
			return View::make('invoice.pakkseddelgen')
				->with('order', $order)
				->with('stickers', Orderitem::where('order_id', '=', $id)->paginate(999));


		$lang = app()->getLocale();
		return Redirect::to($lang . '/order/')
				->with('message', 'Noe gikk galt. Kunne ikke finne bestilligen.');
	}


}
