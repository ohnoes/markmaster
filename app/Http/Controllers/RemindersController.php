<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Password;
use Session;
use Redirect;
use Validator;
use Input;
use DB;
use App;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		return View::make('password.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		if(User::where('email', Input::only('email'))->first()) {

			$user = User::where('email', Input::only('email'))->first();
			switch ($response = Password::sendResetLink(Input::only('email'), function($message){
				$message->subject('Passord for Markmaster');
			}))
			{
				case Password::INVALID_USER:
					return Redirect::back()->with('error', Lang::get($response));

				case Password::RESET_LINK_SENT:
					return Redirect::back()->with('status', Lang::get($response));
			}
		} else {
			return Redirect::back()->with('message', 'That e-mail is not connected with a registered user');
		}

	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);
		
		return View::make('password.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
			return Redirect::back()->with('error', Lang::get($response));

			case Password::PASSWORD_RESET:
				$lang = app()->getLocale();
				return Redirect::to($lang . '/');
		}
	}

}
