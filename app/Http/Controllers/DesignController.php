<?php

namespace markmaster\Http\Controllers;

use Background;
use Cart;
use Category;
use Cfaktura;
use Clipart;
use Commercialagent;
use Coupon;
use Currency;
use Faktura;
use Order;
use Orderitem;
use Product;
use Template;

use Auth;
use Session;
use Redirect;
use Validator;
use Input;
use DB;

use markmaster\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class DesignController extends BaseController {

	public function __construct() {
		parent::__construct();
		//$this->beforeFilter('csrf', array('on'=>'post'));
		//$this->beforeFilter('auth', array('only'=>array('postAddtocart', 'getCart', 'getRemoveitem')));
		//$this->beforeFilter('admin', array('only'=>array('getTempGen')));

	}

	public function getStoreTest() {
		return View::make('designs.storeTest');
	}

	public function getClipartget() {
		return View::make('designs.clipartget');
	}

	public function postClipartget() {
		return View::make('designs.clipartget');
	}

	public function postAddtocart() {
		return View::make('designs.addtocart');
	}

	public function getAddtocart() {
		return View::make('designs.addtocart');
	}

	public function getBackgroundget() {
		return View::make('designs.backgroundget');
	}

	public function postBackgroundget() {
		return View::make('designs.backgroundget');
	}

	public function getDesigner() {
		return View::make('designs.designer');
	}

	public function getTempgen() {
		return View::make('designs.tempgen');
	}

	public function getStayactive() {
		Session::reflash();
		Session::regenerate();
		//echo "Success";
	}

	public function getKlistremerker() {
		return View::make('designs.designer')
        ->with('typeofsticker', 1);
	}

    public function getStrykfast_farge() {
		return View::make('designs.designer')
        ->with('typeofsticker', 2);
	}

    public function getKlistremerker_gull() {
		return View::make('designs.designer')
        ->with('typeofsticker', 3);
	}

    public function getKlistremerker_solv() {
		return View::make('designs.designer')
        ->with('typeofsticker', 4);
	}

    public function getKlistremerker_transparent() {
		return View::make('designs.designer')
        ->with('typeofsticker', 5);
	}

    public function getBillige() {
		return View::make('designs.designer')
        ->with('typeofsticker', 6);
	}

    public function getStrykfast() {
		return View::make('designs.designer')
        ->with('typeofsticker', 7);
	}

    public function getRefleksmerker() {
		return View::make('designs.designer')
        ->with('typeofsticker', 8);
	}

    public function getStrykefrie() {
		return View::make('designs.designer')
        ->with('typeofsticker', 9);
	}

	public function getBase() {
		return View::make('designs.designer')
		->with('typeofsticker', 1);
	}

}
