<?php namespace markmaster\Providers;

use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider {

	/**
	 * Overwrite any vendor / package configuration.
	 *
	 * This service provider is intended to provide a convenient location for you
	 * to overwrite any "vendor" or package configuration that you may want to
	 * modify before the application handles the incoming request / command.
	 *
	 * @return void
	 */
	public function register()
	{
		config([
			'laravellocalization.supportedLocales' => [
                    'en' => array( 'name' => 'English', 'script' => 'Latn', 'native' => 'English' ),
                    'no'  => array( 'name' => 'Norwegian', 'script' => 'Latn', 'native' => 'Norsk' ),
                    'dk'  => array( 'name' => 'Danish', 'script' => 'Latn', 'native' => 'Dansk' ),
					'se'  => array( 'name' => 'Swedish', 'script' => 'Latn', 'native' => 'Svenska' ),
					//'de'  => array( 'name' => 'German', 'script' => 'Latn', 'native' => 'Deutsch' ),
					'is'  => array( 'name' => 'Icelandic', 'script' => 'Latn', 'native' => 'Islenska' ),
                ],

                'laravellocalization.useAcceptLanguageHeader' => true,

                'laravellocalization.hideDefaultLocaleInURL' => false
		]);
	}

}
