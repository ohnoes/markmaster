<?php

class Coupon extends Eloquent {

	protected $fillable = array('code', 'type', 'effect');

	public static $rules = array(
		'code'=>'required|unique:coupons',
		'type'=>'required',
		'effect'=>'required',
	
	);

}