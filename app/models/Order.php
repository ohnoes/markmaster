<?php

class Order extends Eloquent {

	protected $fillable = array('firstname', 'lastname', 'address', 'address2', 'zipcode', 'city', 'state', 'country',
								'billingfirstname', 'billinglastname', 'billingaddress', 'billingaddress2', 'billingzipcode', 'billingcity', 'billingstate', 'billingcountry');

	public static $rules = array(
		'orgname'=>'',
		'firstname'=>'required|min:2',
		'lastname'=>'required|min:2',
		'address'=>'',
		'address2'=>'',
		'city'=>'required',
		'country'=>'required',
		'zipcode'=>'required',
		'billingorgname'=>'',
		'billingfirstname'=>'required|min:2',
		'billinglastname'=>'required|min:2',
		'billingaddress'=>'',
		'billingaddress2'=>'',
		'billingcity'=>'required',
		'billingzipcode'=>'required',
		'billingcountry'=>'required'
	);

	public static $rulesSameShipping = array(
		'orgname'=>'',
		'firstname'=>'required|min:2',
		'lastname'=>'required|min:2',
		'address'=>'',
		'address2'=>'',
		'city'=>'required',
		'country'=>'required',
		'zipcode'=>'required',
	);

}
