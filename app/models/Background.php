<?php

class Background extends Eloquent {

	protected $fillable = array('title', 'user_id', 'public', 'image');

	public static $rules = array(
		'image'=>'required|max:10000|mimes:jpeg,jpg,bmp,png,gif'
	);

}