<?php

class Orderitem extends Eloquent {

	protected $fillable = array('order_id', 'user_id', 'product_type', 'json', 'image', 'quantity', 'price', 'area');

	public static $rules = array(
		'order_id'=>'integer',
		'user_id'=>'integer',
		'image'=>'image|mimes:jpeg,jpg,bmp,png,gif',
	);

}