<?php

class Commercialagent extends Eloquent {

	protected $fillable = array();

	public static $rules = array(
		'name'=>'',
		'address'=>'',
		'contactmail'=>'|email',
		'contacttelephone'=>'',
		'telephone'=>'',
		'contactname'=>'',
		'city'=>'',
		'zip'=>'',
		'url'=>''
	);

}