<?php

class Template extends Eloquent {

	protected $fillable = array('product_type', 'json', 'title', 'description', 'image', 'searchwords');

	public static $rules = array(
		'product_type'=>'required|integer',
		'title'=>'required|min:2',
		'description'=>'required|min:10',
		'image'=>'required|image|mimes:jpeg,jpg,bmp,png,gif'
	);

}