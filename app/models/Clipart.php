<?php

class Clipart extends Eloquent {

	protected $fillable = array('title', 'user_id', 'public', 'category', 'image', 'subcat1', 'subcat2', 'searchwords', 'colors', 'strechable');

	public static $rules = array(
		'image'=>'required|image|max:10000|mimes:jpeg,jpg,png,gif'
	);

	public static $admin_rules = array(
		'title'=>'required|min:2',
		'image'=>'required|mimes:jpeg,jpg,png,gif,svg'
	);

}