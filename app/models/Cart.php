<?php

class Cart extends Eloquent {
	protected $fillable = array('name', 'price', 'price', 'URL', 'image', 'edit', 'width', 'height', 'antall', 'itemtype', 'session');

	public static $rules = array(

	);

    public static function total() {
        $value = Session::get('_token');
        $items = DB::table('carts')->where('session', '=', $value)->get();

        $total = 0;

        foreach ($items as $item) {
            $total += $item->price;
        }

		return $total;
	}

    public static function remove($id) {
		return DB::table('carts')->where('id', '=', $id)->delete();
	}

    public static function count(){
        $value = Session::get('_token');
        return DB::table('carts')->where('session', '=', $value)->count();
    }

    public static function clear(){
        $value = Session::get('_token');
        return DB::table('carts')->where('session', '=', $value)->delete();
    }

    public static function content(){
        $value = Session::get('_token');
        return DB::table('carts')->where('session', '=', $value)->get();
    }
}
