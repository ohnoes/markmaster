<?php

if (Input::has('getBackground') && Input::get('getBackground') == 1 && Input::has('category') && Auth::check())
{
	getBackground(Input::get('category'));
}
else if (Input::has('uploadBackground') && Input::get('uploadBackground') == 1 && Input::has('category') && Auth::check())
{
	uploadBackground();
	getBackground(Input::get('category'));
}


// called when user submits background to be uploaded and saved to database
function uploadBackground()
{
	$validator = Validator::make(Input::all(), Background::$rules);

	// ensure valid data
	if ($validator->passes()) {
		$background = new Background;
		$background->title = 'uploadedBackground';


		// if user logged in
		if (Auth::check())
		{
			// save user id with clipart
			$background->user_id = Auth::user()->getId();


			$image = Input::file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$destination = public_path() . '/img/background';
			Image::make($image->getRealPath())->save($destination .'/'. $filename); // Fix the resizing to keep original format here
			$background->image = 'img/background/'.$filename;
			$background->save();
			

			 return Redirect::to('storeTest')
			 	->with('message', 'Background Created');
		}
	}
}

// echos out backgrounds styled with the class 'backgroundImg', and the onclick event 'addBackground'
function getBackground($category)
{
	if (!Auth::user())
		$category = 0;
	
	foreach(Background::orderBy('sortorder', 'ASC')->paginate(9999999) as $background)
	{
		$name = '../../' . $background->image;
		$onclick = 'addBackground(\'' . $name . '\')';
		if ($category == 0) // show public clipart
		{
			if ($background->public == 1)
			{
				echo '<img src="', $name, '" name="', $name, '" class="backgroundImg" onclick="', $onclick, '">';
			}
	    }
	  	else if ($category == 1) // show user's cilpart
	  	{
			if ($background->user_id==Auth::user()->getId() )
			{
				echo '<img src="', $name, '" name="', $name, '" class="backgroundImg" onclick="', $onclick, '">';
			}
	    }
	}
}

?>