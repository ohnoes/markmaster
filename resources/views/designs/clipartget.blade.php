<?php
if (Input::has('getClipart') && Input::get('getClipart') == 1 && Input::has('category'))// && Auth::check())
{
	$search = '';
	if (Input::has('search'))
	{
		$search = Input::get('search');
	}
	$clipIndex = 0;
	if (Input::has('clipIndex'))
	{
		$clipIndex = Input::get('clipIndex');
	}
	$numToLoad = 20;
	if (Input::has('numToLoad'))
	{
		$numToLoad = Input::get('numToLoad');
	}
	$stickerType = 2;
	if (Input::has('stickerType'))
	{
		$stickerType = Input::get('stickerType');
	}
	getClipart(Input::get('category'), $search, $clipIndex, $numToLoad, $stickerType);
}
else if (Input::has('uploadClipart') && Input::get('uploadClipart') == 1 && Input::has('category') && Auth::check())
{
	 // return Redirect::to('storeTest')
		// ->with('message', 'Failed to create clipart.');
		
		// return Response::json(array('success' => false,
									// 'errors' => $validator->getMessageBag()->toArray()
		// ), 400);
	$stickerType = 2;
	if (Input::has('stickerType')) {
		$stickerType = Input::get('stickerType');
	}
	uploadClipart($stickerType);
	// getClipart(Input::get('category'), '', 0, 20);
	echoClipart(Input::get('category'), 20, $stickerType);
}
else
{
	// echo 'error';
	 // return Redirect::to('storeTest')
		// ->with('message', 'Failed to create clipart.');
}


// called when user submits clipart to be uploaded and saved to database
function uploadClipart($colorValue)
{
	$validator = Validator::make(Input::all(), Clipart::$rules);
	if ($validator->fails())
	{
		echo 'failed';
		foreach($validator->getMessageBag()->toArray()['image'] as $msg)
		{
			echo $msg;
		}
		echo 'failed';
		// echo 'validator failed';
		return;
		// return Redirect::to('storeTest')
		// ->with('message', 'Failed to create clipart.');
	
		// return Response::json(array('success' => false,
									// 'errors' => $validator->getMessageBag()->toArray()
		// ), 400);
	}

	// ensure valid data
	if ($validator->passes()) {
		$clipart = new Clipart;
		if (Input::has('title')) {
			$clipart->title = Input::get('title');
		} else {
			$clipart->title = 'userClipart';
		}
		
		// if user logged in
		if (Auth::check())
		{
			// save user id with clipart
			$clipart->user_id = Auth::user()->getId();

			$image = Input::file('image');
			$extension = $image->getClientOriginalExtension();
			$filenameNoExt = time();
			$filename = $filenameNoExt . '.' . $extension;
			$destination = public_path() . '/img/clipart';
			Image::make($image->getRealPath())->save($destination .'/'. $filename); // Fix the resizing to keep original format here
			
			$clipart->image = 'img/clipart/'.$filename;
			
			if($colorValue == 3 || $colorValue == 4 || $colorValue == 5) { //grayscale image
				$file_name = $destination .'/'. $filename;
				if ($extension == 'jpg' || $extension == 'jpeg') {
					$clipart->can_color = 1;
					$file_namePNG = $destination . '/'. $filenameNoExt . '.png';
					$makePNG = "convert " . $file_name . " PNG32:" . $file_namePNG;
					exec($makePNG);
					$conversion = "convert " . $file_namePNG . " -set colorspace gray -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_namePNG;   //konverterer bilden til grayscale transp
					exec($conversion);
					$resize = "convert " . $file_namePNG . " -resize 3200x3200\> " . $file_namePNG;  //****************************************************Her angis maks filstørrelse

					exec($resize);
					$clipart->image = 'img/clipart/'.$filenameNoExt.'.png';
					unlink($file_name);
				} else {
					$clipart->can_color = 1;
					//$file_name = $destination .'/'. $filename;
					$conversion = "convert " . $file_name . " -set colorspace gray -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_name;   //konverterer bilden til grayscale transp
					exec($conversion);
					$resize = "convert " . $file_name . " -resize 3200x3200\> " . $file_name;  //****************************************************Her angis maks filstørrelse

					exec($resize);
				}

			} else if ($colorValue == 6 || $colorValue == 7) { //black and white image
				$file_name = $destination .'/'. $filename;
				if ($extension == 'jpg' || $extension == 'jpeg') {
					$clipart->can_color = 0;
					$file_namePNG = $destination . '/'. $filenameNoExt . '.png';
					$makePNG = "convert " . $file_name . " PNG32:" . $file_namePNG;
					exec($makePNG);
					$conversion = "convert " . $file_namePNG . " -threshold 50% -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_namePNG;   //konverterer bilden til grayscale transp
					exec($conversion);
					$resize = "convert " . $file_namePNG . " -resize 3200x3200\> " . $file_namePNG;  //****************************************************Her angis maks filstørrelse

					exec($resize);
					$clipart->image = 'img/clipart/'.$filenameNoExt.'.png';
					unlink($file_name);
				} else {
					$clipart->can_color = 0;
					//$file_name = $destination .'/'. $filename;
					$conversion = "convert " . $file_name . " -threshold 50% -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_name;      //konverterer bilden til sort/hvit transp
					exec($conversion);
					$resize = "convert " . $file_name . " -resize 3200x3200\> " . $file_name;  //****************************************************Her angis maks filstørrelse

					exec($resize);
				}
				
			} else { //full color
				$clipart->can_color = 2;
				$file_name = $destination .'/'. $filename;
				$resize = "convert " . $file_name . " -resize 3200x3200\> " . $file_name;  //****************************************************Her angis maks filstørrelse

				exec($resize);
			}
			
			$clipart->save();
			echo $clipart->image;
			echo 'success';
			
			 // return Redirect::to('storeTest')
			 	// ->with('message', 'Clipart Created');
		}
	}
	
	 // return Redirect::to('storeTest')
		// ->with('message', 'Failed to create clipart.');
}


// echos json object containing the urls to clipart matching the category, and subcategories matching $search
// 
function getClipart($category, $search, $startIndex, $numToLoad, $clipartType)
{
	// build array containing id of every category matching (partial or full) $search
	// $matches = array();
	// if ($search !== '')
	// {
	// 	foreach(Category::all() as $cat)
	// 	{
	// 		if (strpos($cat->name, $search) !== false)
	// 		{
	// 			array_push($matches, $cat->id);
	// 		}
	// 	}
	// }
	
	$search = strtolower($search);
	
	if (!Auth::user() && $category == 1)
	{
		$data = array('clipIndex' => 0,
				  'numLoaded' => 0,
				  'names'     => 0,
				  'user'	  => false);
		echo json_encode($data);
	}
	else
	{
	
		$names = array();
		$numLoaded = 0;
		$clipIndex = 0;
		foreach(Clipart::all() as $clip)
		{
			$subcat1 = $clip->subcat1;
			$subcat2 = $clip->subcat2;
			$name = '../../' . $clip->image;
			$onclick = 'addClipart(\'' . $name . '\')';
			if ($clip->can_color <= $clipartType)
			{
				if ($category == 0) // show all
				{
					if ($clip->public == 1 || (Auth::user() && $clip->user_id==Auth::user()->getId()))
					{
						// if searching for subcategory and match not found, skip clipart
						if ($search !== '' && strpos($clip->searchwords, $search) === false)
							continue;

						$clipIndex++;
						if ($clipIndex <= $startIndex)
							continue;
						array_push($names, $name);
						// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
						// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" data-subcat1="', $subcat1, '" data-subcat2="', $subcat2, '" onclick="', $onclick, '">';
						$numLoaded++;
						if ($numLoaded >= $numToLoad)
							break;
					}
				}
				else if ($category == 1) // show user's cilpart
				{
					if ($clip->user_id==Auth::user()->getId())
					{
						if ($search !== '' && strpos($clip->searchwords, $search) === false)
							continue;
						$clipIndex++;
						if ($clipIndex <= $startIndex)
							continue;
						array_push($names, $name);
						// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
						$numLoaded++;
						if ($numLoaded >= $numToLoad)
							break;
					}
				}
				else // show requested category clipart
				{
					if ($clip->category == $category || $clip->subcat1 == $category || $clip->subcat2 == $category)
					{
						if ($search !== '' && strpos($clip->searchwords, $search) === false)
							continue;
						$clipIndex++;
						if ($clipIndex <= $startIndex)
							continue;
						array_push($names, $name);
						// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
						$numLoaded++;
						if ($numLoaded >= $numToLoad)
							break;
					}
				}
			}
		}
		$data = array('clipIndex' => $clipIndex,
					  'numLoaded' => $numLoaded,
					  'names'     => $names,
					  'user'	  => true);
		echo json_encode($data);
	}
}

// echos out $numToLoad many html clipart matching the category as images, styled with the class 'clipartImg', and the onclick event 'addClipart'
function echoClipart($category, $numToLoad, $clipartType)
{
	$numToLoad = intval($numToLoad);
	$clipartType = intval($clipartType);
	$numLoaded = 0;
	foreach(Clipart::orderBy('sortorder', 'ASC')->paginate(9999999) as $clip)
	{
		if ($clip->can_color <= $clipartType)
		{
			$subcat1 = $clip->subcat1;
			$subcat2 = $clip->subcat2;
			$name = '../../' . $clip->image;
			$onclick = 'addClipart(\'' . $name . '\')';
			if ($category == 0) // show all
			{
				if ($clip->public == 1 || (Auth::user() && $clip->user_id==Auth::user()->getId()))
				{
					echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
					$numLoaded++;
					if ($numLoaded >= $numToLoad)
						break;
				}
			}
			else if ($category == 1) // show user's cilpart
			{
				if ($clip->user_id==Auth::user()->getId() )
				{
					echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
					$numLoaded++;
					if ($numLoaded >= $numToLoad)
						break;
				}
			}
			else // show requested category clipart
			{
				if ($clip->category == $category || $clip->subcat1 == $category || $clip->subcat2 == $category )
				{
					echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
					$numLoaded++;
					if ($numLoaded >= $numToLoad)
						break;
				}
			}
		}
	}
}


?>
