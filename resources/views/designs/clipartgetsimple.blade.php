<?php

// echos out $numToLoad many html clipart matching the category as images, styled with the class 'clipartImg', and the onclick event 'addClipart'
/*
function echoClipart($category, $numToLoad, $clipartType)
{
	$numToLoad = intval($numToLoad);
	$clipartType = intval($clipartType);
	$numLoaded = 0;
	foreach(Clipart::orderBy('sortorder', 'ASC')->paginate(9999999) as $clip)
	{
		if ($clip->can_color <= $clipartType)
		{
			$subcat1 = $clip->subcat1;
			$subcat2 = $clip->subcat2;
			$name = '../../' . $clip->image;
			$onclick = 'addClipart(\'' . $name . '\')';

			if ($clip->category == $category || $clip->subcat1 == $category || $clip->subcat2 == $category )
			{
				echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
				$numLoaded++;
				if ($numLoaded >= $numToLoad)
					break;
			}
		}
	}
}
*/

//returns an array with all of the cliparts and their category
function getAllClipart()
{
	//new array that we're going to store the name of the clipart in
	$clips = array();

	//get all clipart from the database
	foreach(Clipart::orderBy('sortorder', 'ASC')->paginate(9999999) as $clip)
	{
		//if it isn't a user uploaded clipart
		if($clip->category != 0)
		{
			//create an array with two keys, name and category
			$clipArray = array(
				"name" => '../../' . $clip->image,
				"category" => $clip->category,
				'can_color' => $clip->can_color,
				"subcat" => $clip->subcat1,
				"subcat2" => $clip->subcat2
			);

			//add it to the clipart array
			$clips[] = $clipArray;
		}
	}

	//return it back to the calling function
	return $clips;
}


?>
