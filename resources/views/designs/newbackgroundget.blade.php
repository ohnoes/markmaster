<?php

//returns an array with all of the cliparts and their category
function getAllBackground()
{
	//new array that we're going to store the name of the clipart in
	$clips = array();

	//get all clipart from the database
	foreach(Background::orderBy('sortorder', 'ASC')->paginate(9999999) as $clip)
	{
		//if it isn't a user uploaded clipart
		if($clip->public != 0 || ($clip->public == 0 && Auth::user() && $clip->user_id == Auth::user()->getId()))
		{
			//create an array with two keys, name and category
			$clipArray = array(
				"name" => '../../' . $clip->image,
				"public" => $clip->public,
				"can_color" => $clip->color
			);

			//add it to the clipart array
			$clips[] = $clipArray;
		}
	}

	//return it back to the calling function
	return $clips;
}


?>
