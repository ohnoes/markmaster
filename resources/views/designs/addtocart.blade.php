<?php
if(Input::has('updatesticker') && Input::get('updatesticker')){
  $thumbnail = '';
	if (Input::has('thumbnail'))
	{
		$thumbnail = Input::get('thumbnail');
	}
  $quantity = 0;
	if (Input::has('quantity'))
	{
		$quantity = Input::get('quantity');
	}
  $price = 0;
	if (Input::has('price'))
	{
		$price = Input::get('price');
	}
	$height = 0;
	if (Input::has('height'))
	{
		$height = Input::get('height');
	}
	$width = 0;
	if (Input::has('width'))
	{
		$width = Input::get('width');
	}
	$json = '';
	if (Input::has('json'))
	{
		$json = Input::get('json');
	}
	$stickerType = 2;
	if (Input::has('stickerType'))
	{
		$stickerType = Input::get('stickerType');
	}
  $userid = 0;
	if (Input::has('userid'))
	{
		$userid = Input::get('userid');
	}

  if (Input::has('stickerid'))
	{
		$stickerid = Input::get('stickerid');
	}
	updateSticker($thumbnail, $quantity, $price, $height, $width, $json, $stickerType, $userid, $stickerid);
} else if (Input::get('update')) {
  $thumbnail = '';
	if (Input::has('thumbnail'))
	{
		$thumbnail = Input::get('thumbnail');
	}
  $quantity = 0;
	if (Input::has('quantity'))
	{
		$quantity = Input::get('quantity');
	}
  $price = 0;
	if (Input::has('price'))
	{
		$price = Input::get('price');
	}
	$height = 0;
	if (Input::has('height'))
	{
		$height = Input::get('height');
	}
	$width = 0;
	if (Input::has('width'))
	{
		$width = Input::get('width');
	}
	$json = '';
	if (Input::has('json'))
	{
		$json = Input::get('json');
	}
	$stickerType = 2;
	if (Input::has('stickerType'))
	{
		$stickerType = Input::get('stickerType');
	}
  $userid = 0;
	if (Input::has('userid'))
	{
		$userid = Input::get('userid');
	}

  if (Input::has('stickerid'))
	{
		$stickerid = Input::get('stickerid');
	}
	updateCart($thumbnail, $quantity, $price, $height, $width, $json, $stickerType, $userid, $stickerid);
} else {
  $thumbnail = '';
	if (Input::has('thumbnail'))
	{
		$thumbnail = Input::get('thumbnail');
	}
  $quantity = 0;
	if (Input::has('quantity'))
	{
		$quantity = Input::get('quantity');
	}
  $price = 0;
	if (Input::has('price'))
	{
		$price = Input::get('price');
	}
	$height = 0;
	if (Input::has('height'))
	{
		$height = Input::get('height');
	}
	$width = 0;
	if (Input::has('width'))
	{
		$width = Input::get('width');
	}
	$json = '';
	if (Input::has('json'))
	{
		$json = Input::get('json');
	}
	$stickerType = 2;
	if (Input::has('stickerType'))
	{
		$stickerType = Input::get('stickerType');
	}
  $userid = 0;
  if (Input::has('userid'))
  {
    $userid = Input::get('userid');
  }
	addCart($thumbnail, $quantity, $price, $height, $width, $json, $stickerType, $userid);
}


// called when user submits clipart to be uploaded and saved to database
function addCart($thumbnail, $quantity, $price, $height, $width, $json, $stickerType, $userid)
{
    $orderitem = new Orderitem;
    $orderitem->stickertype = $stickerType;
    $orderitem->user_id = $userid;
    $orderitem->json = $json;
    $orderitem->quantity = $quantity;
    $orderitem->price = intval($price);
    $orderitem->regularprice = intval($price);
    $orderitem->width = $width;
    $orderitem->height = $height;
    $orderitem->thumbnail = '/img/stickerthumbnail/' . $orderitem->id . '.png';
    $orderitem->save();

    if($orderitem->stickertype == 1) {
      $type = 'klistremerker';
    } elseif ($orderitem->stickertype == 2) {
      $type = 'strykfast-farge';
    } elseif ($orderitem->stickertype == 3){
      $type = 'klistremerker-gull';
    } elseif ($orderitem->stickertype == 4){
      $type = 'klistremerker-solv';
    } elseif ($orderitem->stickertype == 5){
      $type = 'klistremerker-transparent';
    } elseif ($orderitem->stickertype == 6){
      $type = 'billige';
    } elseif ($orderitem->stickertype == 7){
      $type = 'strykfast';
    } elseif ($orderitem->stickertype == 8){
      $type = 'refleksmerker';
    } elseif ($orderitem->stickertype == 9){
      $type = 'strykefrie';
    } else {
      $type = 'N/A';
    }

    $cart = new Cart;
    $cart->id = $orderitem->id;
    $cart->name = $orderitem->stickertype;
    $cart->price = $orderitem->price;
    $cart->URL = '/merkelapper/' . $type . '?id=' . $orderitem->id . '&dvos=';
    $cart->image = '/img/stickerthumbnail/' . $orderitem->id . '.png';
    $cart->edit = '/merkelapper/' . $type . '?edit=' . $orderitem->id;
    $cart->width = $orderitem->width;
    $cart->height = $orderitem->height;
    $cart->antall = $orderitem->quantity;
    $cart->itemtype = 'original';
    $cart->session = Session::get('_token');
    $cart->save();

		$image = $thumbnail;
		$extension = 'png';
		$filenameNoExt = $orderitem->id;
		$filename = $filenameNoExt . '.' . $extension;
		$destination = public_path() . '/img/stickerthumbnail';
    $base=base64_decode($image);
		Image::make($base)->save($destination .'/'. $filename); // Fix the resizing to keep original format here

		echo 'success';

}

function updateCart($thumbnail, $quantity, $price, $height, $width, $json, $stickerType, $userid, $stickerid)
{
  $orderitem = Orderitem::find($stickerid);
  $orderitem->stickertype = $stickerType;
  $orderitem->user_id = $userid;
  $orderitem->json = $json;
  $orderitem->quantity = $quantity;
  $orderitem->price = intval($price);
  $orderitem->regularprice = intval($price);
  $orderitem->width = $width;
  $orderitem->height = $height;
  $orderitem->thumbnail = '/img/stickerthumbnail/' . $orderitem->id . '.png';
  $orderitem->save();

  $item = Cart::find(Input::get('stickerid'));
  $item->price = $orderitem->price;
  $item->width = $orderitem->width;
  $item->height = $orderitem->height;
  $item->antall = $orderitem->quantity;
  $item->save();

  $image = $thumbnail;
  $extension = 'png';
  $filenameNoExt = $orderitem->id;
  $filename = $filenameNoExt . '.' . $extension;
  $destination = public_path() . '/img/stickerthumbnail';
  $base=base64_decode($image);
  Image::make($base)->save($destination .'/'. $filename); // Fix the resizing to keep original format here

		echo 'success';
}

function updateSticker($thumbnail, $quantity, $price, $height, $width, $json, $stickerType, $userid, $stickerid)
{
  $orderitem = Orderitem::find($stickerid);
  $orderitem->json = $json;
  $orderitem->thumbnail = '/img/stickerthumbnail/' . $orderitem->id . '.png';
  $orderitem->save();

  $image = $thumbnail;
  $extension = 'png';
  $filenameNoExt = $orderitem->id;
  $filename = $filenameNoExt . '.' . $extension;
  $destination = public_path() . '/img/stickerthumbnail';
  $base=base64_decode($image);
  Image::make($base)->save($destination .'/'. $filename); // Fix the resizing to keep original format here

		echo 'success';
}
?>
