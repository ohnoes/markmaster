@extends('layouts.main')

@section('title')
   @if($typeofsticker == 1)
       {{ trans('designs/simple.Title-1') }}
   @elseif($typeofsticker == 2)
       {{ trans('designs/simple.Title-2') }}
   @elseif($typeofsticker == 3)
       {{ trans('designs/simple.Title-3') }}
   @elseif($typeofsticker == 4)
       {{ trans('designs/simple.Title-4') }}
   @elseif($typeofsticker == 5)
       {{ trans('designs/simple.Title-5') }}
   @elseif($typeofsticker == 6)
       {{ trans('designs/simple.Title-6') }}
   @elseif($typeofsticker == 7)
       {{ trans('designs/simple.Title-7') }}
   @elseif($typeofsticker == 8)
       {{ trans('designs/simple.Title-8') }}
   @elseif($typeofsticker == 9)
       {{ trans('designs/simple.Title-9') }}
   @endif
@stop

@section('head-meta')
    @if($typeofsticker == 1)
        <meta name="description" content="{{ trans('designs/simple.Translate19') }} ">
    @elseif($typeofsticker == 2)
        <meta name="description" content="{{ trans('designs/simple.Translate20') }} ">
    @elseif($typeofsticker == 3)
        <meta name="description" content="{{ trans('designs/simple.Translate21') }} ">
    @elseif($typeofsticker == 4)
        <meta name="description" content="{{ trans('designs/simple.Translate22') }} ">
    @elseif($typeofsticker == 5)
        <meta name="description" content="{{ trans('designs/simple.Translate23') }} ">
    @elseif($typeofsticker == 6)
        <meta name="description" content="{{ trans('designs/simple.Translate24') }} ">
    @elseif($typeofsticker == 7)
        <meta name="description" content="{{ trans('designs/simple.Translate25') }} ">
    @elseif($typeofsticker == 8)
        <meta name="description" content="{{ trans('designs/simple.Translate26') }} ">
    @elseif($typeofsticker == 9)
        <meta name="description" content="{{ trans('designs/simple.Translate27') }} ">
    @endif

    <link rel="canonical" href="http://www.markmaster.com/{{ app()->getLocale() }}/{{ trans('routes.namebase') }}" />
@stop

@section('head-extra-styles')
    {!! Html::style('css/canvas.css') !!}
    {!! Html::style('css/simple.css') !!}
@stop

@section('content')

    @if($typeofsticker == 1)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-1') }}  <br> {{ trans('designs/simple.size-1') }} </div>
    @elseif($typeofsticker == 2)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-2') }}  <br> {{ trans('designs/simple.size-1') }} </div>
    @elseif($typeofsticker == 3)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-3') }}  <br> {{ trans('designs/simple.size-1') }} </div>
    @elseif($typeofsticker == 4)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-4') }}  <br> {{ trans('designs/simple.size-1') }} </div>
    @elseif($typeofsticker == 5)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-5') }}  <br> {{ trans('designs/simple.size-1') }} </div>
    @elseif($typeofsticker == 6)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-6') }}  <br> {{ trans('designs/simple.size-2') }} </div>
    @elseif($typeofsticker == 7)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-7') }}  <br> {{ trans('designs/simple.size-3') }} </div>
    @elseif($typeofsticker == 8)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-8') }}  <br> {{ trans('designs/simple.size-1') }} </div>
    @elseif($typeofsticker == 9)
        <div class="alert alert-info" role="alert"> {{ trans('designs/simple.info-9') }}  <br> {{ trans('designs/simple.size-1') }} </div>
    @endif

    <div class="container">
        <!-- Temp hidden elements -->
        {!! Form::hidden('stickertype', $typeofsticker, array('id'=>'stickertype')) !!}

        {!! Form::hidden('backgroundFileSelect', '1', array('id'=>'backgroundFileSelect')) !!}

        <!-- shape and size holders -->
        {!! Form::hidden('isRect', '1', array('id'=>'isRect')) !!}
        {!! Form::hidden('isEllipse', '0', array('id'=>'isEllipse')) !!}

        @if($typeofsticker == 6)
            {!! Form::hidden('3010', '0', array('id'=>'3010')) !!}
            {!! Form::hidden('3716', '1', array('id'=>'3716')) !!}
            {!! Form::hidden('6026', '0', array('id'=>'6026')) !!}
        @elseif($typeofsticker == 7)
            {!! Form::hidden('3010', '1', array('id'=>'3010')) !!}
            {!! Form::hidden('3716', '0', array('id'=>'3716')) !!}
            {!! Form::hidden('6026', '0', array('id'=>'6026')) !!}
        @else
            {!! Form::hidden('3010', '0', array('id'=>'3010')) !!}
            {!! Form::hidden('3716', '0', array('id'=>'3716')) !!}
            {!! Form::hidden('6026', '0', array('id'=>'6026')) !!}
        @endif

        <!-- Text options holders -->
        {!! Form::hidden('strokeWidthTextbox', '1', array('id'=>'strokeWidthTextbox')) !!}
        {!! Form::hidden('stroke', '1', array('id'=>'stroke')) !!}

        <!-- Header for the page -->
        @if($typeofsticker == 1)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate19') }}</h1>
        @elseif($typeofsticker == 2)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate20') }}</h1>
        @elseif($typeofsticker == 3)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate21') }}</h1>
        @elseif($typeofsticker == 4)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate22') }}</h1>
        @elseif($typeofsticker == 5)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate23') }}</h1>
        @elseif($typeofsticker == 6)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate24') }}</h1>
        @elseif($typeofsticker == 7)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate25') }}</h1>
        @elseif($typeofsticker == 8)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate26') }}</h1>
        @elseif($typeofsticker == 9)
            <h1 class="tempHeader">{{ trans('designs/simple.Translate27') }}</h1>
        @endif
        <!-- 3 Text lines -->
        <div id="container">
            <div class="row no-margin">

                <div id="canvasCon" class="col-xs-12 col-sm-6 col-sm-push-6">
                  <div id="canvasDiv">
                    <canvas id="myCanvas" width="900" height="450"></canvas>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-sm-pull-6" id="textBox">
                        <label class="textHeader">{{ trans('designs/simple.Translate6') }}</label>
                    <div class="form-group">
                        <label for="text1">{{ trans('designs/simple.Translate3') }}</label>
                        {!! Form::text('text1', '', array('id' => 'text1', 'oninput' => 'addSimpleText()'))  !!}
                    </div>
                    <div class="form-group">
                        <label for="text2">{{ trans('designs/simple.Translate4') }}</label>
                        {!! Form::text('text2', '', array('id' => 'text2', 'oninput' => 'addSimpleText()'))  !!}
                    </div>
                    @if($typeofsticker != 7)
                    <div class="form-group">
                        <label for="text3">{{ trans('designs/simple.Translate5') }}</label>
                        {!! Form::text('text3', '', array('id' => 'text3', 'oninput' => 'addSimpleText()'))  !!}
                    </div>
                    @else
                        {!! Form::hidden('text3', '', array('id' => 'text3', 'oninput' => 'addSimpleText()'))  !!}
                    @endif
                </div>


            </div>

        <!-- Sticker color -->
                <div class="row no-margin">
                    @if($typeofsticker != 3 && $typeofsticker != 4 && $typeofsticker != 5 && $typeofsticker != 6 && $typeofsticker != 7)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 smallwindowpadding">
                        <div class="form-group backgroundForm">
                            <label for="backgroundColor"> {{ trans('designs/simple.Translate7') }} </label> </br>
                            {!! Form::select('backgroundColorPickerSelect', array(
                                '#000000' => 'Black',
                                '#ffffff' => 'White',
                                '#E6E6E6' => '10% Black',
                                '#B1B1B1' => '40% Black',
                                '#888887' => '60% Black',
                                '#5C5C5B' => '80% Black',
                                '#EBB5C3' => 'PANTONE 182 C',
                                '#C8112E' => 'PANTONE 185 C',
                                '#B01D2B' => 'PANTONE 1797 C',
                                '#871630' => 'PANTONE 201 C',
                                '#E6D5A8' => 'PANTONE 155 C',
                                '#E9954A' => 'PANTONE 804 C',
                                '#E64A00' => 'PANTONE Orange 021 C',
                                '#EAEBBC' => 'PANTONE 607 C',
                                '#EFED84' => 'PANTONE 100 C',
                                '#EFE032' => 'PANTONE Yellow C',
                                '#C9D8E7' => 'PANTONE 290 C',
                                '#8ACBE5' => 'PANTONE 305 C',
                                '#1A35A8' => 'PANTONE 286 C',
                                '#0F2867' => 'PANTONE 281 C',
                                '#549AA3' => 'PANTONE 320 C',
                                '#EACDCF' => 'PANTONE 698 C',
                                '#E8A3D0' => 'PANTONE 230 C',
                                '#B50970' => 'PANTONE 226 C',
                                '#D7CAE3' => 'PANTONE 263 C',
                                '#9E70C1' => 'PANTONE 528 C',
                                '#680E92' => 'PANTONE 527 C',
                                '#BC8F70' => 'PANTONE 7515 C',
                                '#9E520F' => 'PANTONE 471 C',
                                '#B6DD8E' => 'PANTONE 358 C',
                                '#A4D426' => 'PANTONE 375 C',
                                '#61AE56' => 'PANTONE 354 C',
                                '#4A7229' => 'PANTONE 364 C',
                                '#cfb53b' => 'Gold-like',
                                '#d7d8d8' => 'Silver-like'),
                                '#000000',
                                array('id' => 'backgroundColor' ))
                            !!}
                        </div>
                    </div>
                    @endif
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 smallwindowpadding">
                        <button id="clearStickerButton" class="mybtn" onclick="clearTheSticker()">Reset </button>
                    </div>
                </div>
        </div>

        <!-- Font settings -->
        {!! Form::label('fontFamily', ' ') !!}

        {!! Form::select('fontFamily', array(
                  'boogaloottf' => 'boogaloottf',
                  'crimsontextttf' => 'crimsontextttf',
                  'englandttf' => 'englandttf',
                  'felipattf' => 'felipattf',
                  'goblinonettf' => 'goblinonettf',
                  'gravitasonettf' => 'gravitasonettf',
                  'greatvibesttf' => 'greatvibesttf',
                  'hammersmithonettf' => 'hammersmithonettf',
                  'hennepennyttf' => 'hennepennyttf',
                  'kaushanscriptttf' => 'kaushanscriptttf',
                  'leaguegothicttf' => 'leaguegothicttf',
                  'limelightttf' => 'limelightttf',
                  'lobstertwottf' => 'lobstertwottf',
                  'maidenoragettf' => 'maidenoragettf',
                  'nunitottf' => 'nunitottf',
                  'robotottf' => 'robotottf',
                  'robotocondensedttf' => 'robotocondensedttf')) !!}

        <div id="container">
            <div class="row no-margin">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 smallwindowpadding">
                    <label for="fontSelect" class="textHeader"> {{ trans('designs/simple.Translate8') }} </label> </br>
                    <div id="fontSelect" class="fontSelect" style="width: 80% !important;" onchange="addSimpleText()">
                        <div class="arrow-down"></div>
                    </div>
                </div>

                @if($typeofsticker != 3 && $typeofsticker != 4 && $typeofsticker != 5 && $typeofsticker != 6 && $typeofsticker != 7 )
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 smallwindowpadding">
                        <label for="color"> {{ trans('designs/simple.Translate9') }} </label> </br>
                        {!! Form::select('colorpickerRaise', array(
                           '#000000' => 'Black',
                            '#ffffff' => 'White',
                            '#E6E6E6' => '10% Black',
                            '#B1B1B1' => '40% Black',
                            '#888887' => '60% Black',
                            '#5C5C5B' => '80% Black',
                            '#EBB5C3' => 'PANTONE 182 C',
                            '#C8112E' => 'PANTONE 185 C',
                            '#B01D2B' => 'PANTONE 1797 C',
                            '#871630' => 'PANTONE 201 C',
                            '#E6D5A8' => 'PANTONE 155 C',
                            '#E9954A' => 'PANTONE 804 C',
                            '#E64A00' => 'PANTONE Orange 021 C',
                            '#EAEBBC' => 'PANTONE 607 C',
                            '#EFED84' => 'PANTONE 100 C',
                            '#EFE032' => 'PANTONE Yellow C',
                            '#C9D8E7' => 'PANTONE 290 C',
                            '#8ACBE5' => 'PANTONE 305 C',
                            '#1A35A8' => 'PANTONE 286 C',
                            '#0F2867' => 'PANTONE 281 C',
                            '#549AA3' => 'PANTONE 320 C',
                            '#EACDCF' => 'PANTONE 698 C',
                            '#E8A3D0' => 'PANTONE 230 C',
                            '#B50970' => 'PANTONE 226 C',
                            '#D7CAE3' => 'PANTONE 263 C',
                            '#9E70C1' => 'PANTONE 528 C',
                            '#680E92' => 'PANTONE 527 C',
                            '#BC8F70' => 'PANTONE 7515 C',
                            '#9E520F' => 'PANTONE 471 C',
                            '#B6DD8E' => 'PANTONE 358 C',
                            '#A4D426' => 'PANTONE 375 C',
                            '#61AE56' => 'PANTONE 354 C',
                            '#4A7229' => 'PANTONE 364 C',
                            '#cfb53b' => 'Gold-like',
                            '#d7d8d8' => 'Silver-like'),
                            '#000000',
                            array('id' => 'fill', 'onchange' => 'addSimpleText()') )
                        !!}
                    </div>

                @else
                {!! Form::hidden('fill', '#000000', array('id'=>'fill')) !!}
                @endif
            </div>
        </div>
        <!-- Clipart NB CREATE NEW CLIPART BOX HERE -->

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 simpleclipartBox" style="padding-top: 20px">
			<div style="display:block;">
				<ul class="simpleClipartList">
					<li class="simpleClipartList"><a class="simpleClipartList" onclick="changeCategory(11);">{{ trans('designs/simple.Translate12') }}</a></li>
					<li class="simpleClipartList"><a class="simpleClipartList" onclick="changeCategory(12);">{{ trans('designs/simple.Translate13') }}</a></li>
					<li class="simpleClipartList"><a class="simpleClipartList" onclick="changeCategory(13);">{{ trans('designs/simple.Translate14') }}</a></li>
					<li class="simpleClipartList"><a class="simpleClipartList" onclick="changeCategory(14);">{{ trans('designs/simple.Translate15') }}</a></li>
          <li class="simpleClipartList"><a class="simpleClipartList" onclick="changeCategory(15);">{{ trans('designs/simple.Translate16') }}</a></li>
					<li class="simpleClipartList"><a class="simpleClipartList" onclick="changeCategory(16);">{{ trans('designs/simple.Translate17') }}</a></li>
                    <!--<li class="simpleClipartList"><a class="simpleClipartList" onclick="changeCategory(16);">{{ trans('designs/simple.Translate18') }}</a></li>-->
				</ul>
			</div>

			<div class="clipartContainer" id="clipartContainer">
				<div id="innerContainer">
				</div>
			</div>

			<div class="simpleClipartFooter" id="simpleClipartFooter">
			</div>
		</div>

		<!-- use design from https://www.nordicprint.no/navnelapper as reference -->


        <!-- quantity and price -->
        <?php
            $thecur = Currency::find(1);
            if(Auth::check()) {
                $usercur = Auth::user()->currency;
                if ($usercur == 0) {
                    $currencyValue = 1;
                } else if ($usercur == 1) {
                    $currencyValue = $thecur->usd;
                } else if ($usercur == 2) {
                    $currencyValue = $thecur->sek;
                } else if ($usercur == 3) {
                    $currencyValue = $thecur->dkk;
                }else if ($usercur == 4) {
                    $currencyValue = $thecur->eur;
                }
            } else {
                $usercur = Session::get('theCurrency', 0);
                if ($usercur == 0) {
                    $currencyValue = 1;
                } else if ($usercur == 1) {
                    $currencyValue = $thecur->usd;
                } else if ($usercur == 2) {
                    $currencyValue = $thecur->sek;
                } else if ($usercur == 3) {
                    $currencyValue = $thecur->dkk;
                }else if ($usercur == 4) {
                    $currencyValue = $thecur->eur;
                }
            }

        ?>
        <!-- price and quantity holders -->
        {!! Form::hidden('priceHolder', '100', array('id'=>'priceHolder')) !!}
        {!! Form::hidden('thecurrency', $usercur, array('id'=>'thecurrency')) !!}
        {!! Form::hidden('currencycheck', $currencyValue, array('id'=>'currencycheck')) !!}
        {!! Form::hidden('thePrice2', '1', array('id'=>'thePrice2')) !!}
        <div class="row no-margin">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding-top: 20px">
                <div class="form-group">
                    @if($typeofsticker == 6 || $typeofsticker == 7)
                        <label for="quantity">{{ trans('designs/simple.Translate10') }}</label>
                        {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'onchange' => 'setPrice()')) !!}
                        <select id="Antall" onchange="setPrice()">
                            <option value="1" >{{ trans('designs/ironon.Translate16') }}</option>
                            <option value="5">{{ trans('designs/ironon.Translate17') }}</option>
                            <option value="10">{{ trans('designs/ironon.Translate18') }}</option>
                            <option value="20">{{ trans('designs/ironon.Translate19') }}</option>
                            <option value="50">{{ trans('designs/ironon.Translate20') }}</option>
                            <option value="75">{{ trans('designs/ironon.Translate21') }}</option>
                            <option value="100" selected>{{ trans('designs/ironon.Translate22') }}</option>
                            <option value="250">{{ trans('designs/ironon.Translate23') }}</option>
                            <option value="500">{{ trans('designs/ironon.Translate24') }}</option>
                            <option value="1000">{{ trans('designs/ironon.Translate25') }}</option>
                        </select>
                    @else
                        <label for="quantity">{{ trans('designs/simple.Translate10') }}</label>
                        {!! Form::number('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'min'=>'1', 'onchange' => 'setPrice()')) !!}
                    @endif
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding-top: 20px">
                <div class="form-group">
                    <label for="thePrice" id="thePrice">{{ trans('designs/simple.Translate11') }}</label>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding-top: 20px">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  @if(Auth::check())
                    {!! Form::hidden('userid', Auth::user()->id)!!}
                  @endif

                  @if($typeofsticker == 6 )
                      {!! Form::hidden('width', '37', array('id'=>'width')) !!}
                      {!! form::hidden('height', '16', array('id'=>'height')) !!}
                  @elseif($typeofsticker == 7 )
                      {!! Form::hidden('width', '30', array('id'=>'width')) !!}
                      {!! Form::hidden('height', '10', array('id'=>'height')) !!}
                  @else
                    {!! Form::hidden('width', '30', array('id'=>'width')) !!}
                    {!! Form::hidden('height', '13', array('id'=>'height')) !!}
                  @endif
                      <select id="Antall2" name="Antall2" onchange="setPrice()" style="position: absolute; left: -2500px;">
                          <option value="1" >{{ trans('designs/ironon.Translate16') }}</option>
                          <option value="5">{{ trans('designs/ironon.Translate17') }}</option>
                          <option value="10">{{ trans('designs/ironon.Translate18') }}</option>
                          <option value="20">{{ trans('designs/ironon.Translate19') }}</option>
                          <option value="50">{{ trans('designs/ironon.Translate20') }}</option>
                          <option value="75">{{ trans('designs/ironon.Translate21') }}</option>
                          <option value="100" selected>{{ trans('designs/ironon.Translate22') }}</option>
                          <option value="250">{{ trans('designs/ironon.Translate23') }}</option>
                          <option value="500">{{ trans('designs/ironon.Translate24') }}</option>
                          <option value="1000">{{ trans('designs/ironon.Translate25') }}</option>
                      </select>
                  {!! Form::open(array('files' => true, 'id' => 'addToCart')) !!}
                  <input type="hidden" name="redirect" value="{{ app()->getLocale() }}/store/cart" id="redirect">
                    {!! Form::hidden('stickerid', 0, array('id'=>'stickerid')) !!}
                    {!! Form::hidden('edit', '0', array('id'=>'edit')) !!}
                    {!! Form::hidden('stickertype', '$typeofsticker', array('id'=>'stickertype')) !!}
                    {!! Form::hidden('postjson', '', array('id'=>'postjson')) !!}
                    {!! Form::hidden('postquantity', '', array('id'=>'postquantity')) !!}
                    {!! Form::hidden('postwidth', '', array('id'=>'postwidth')) !!}
                    {!! Form::hidden('postheight', '', array('id'=>'postheight')) !!}
                    {!! Form::hidden('postprice', '', array('id'=>'postprice')) !!}
                    <input type="submit" value="BESTILL" class="mybtn">
                  {!! Form::close() !!}
            </div>
            <div class="col-xs-12 padding-line"></div>
        </div>
        {!! Form::hidden('stickerpriceholder', trans('designs/stickereco.Translate26'), array('id'=>'stickerpriceholder'))!!}
    </div>
<div id='mask'></div>
<div id='popup'>
    <p>{{ trans('designs/simple.Translate74') }}</p>
	<span id='popupSpin'></span>
</div>

{!! Html::style('css/uploadingSpin.css') !!}

{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/jquery.simplefontselector.js') !!}
{!! Html::script('js/fabric.js-master/dist/fabric.js') !!}
{!! Html::script('js/jquery.simplesimplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/spin.min.js') !!}
{!! Html::script('js/customiseControls.min.js') !!}
{!! Html::script('js/simpleoutline.js') !!}
{!! Html::script('js/background-2.js') !!}
{!! Html::script('js/clipart-2.js') !!}
{!! Html::script('js/price-2.js') !!}
{!! Html::script('js/text-2.js') !!}
{!! Html::script('js/bootstrap-colorpicker.js') !!}
{!! Html::script('js/simple-2.js') !!}
{!! Html::script('js/store-2.js') !!}
{!! Html::script('js/simpleCanvasToImage-2.js') !!}
{!! Html::script('js/textbuttons-2.js') !!}

<!-- php script goes here to load the cliparts from the database -->
<?php
	include '../resources/views/designs/clipartgetsimple.blade.php';
	$clipart = getAllClipart();
?>

<script>

	var stickervalg = $('#stickertype').val();
	var clipart = <?php echo json_encode($clipart); ?>;
	var currentCategory = 11;

	if (stickervalg == 6 || stickervalg == 7) {
		currentCategory = 16;
	}

	function changeCategory(cat) {
		currentCategory = cat;
		loadSimpleClipart(clipart, currentCategory);
	}
</script>

<!-- load the clipart container script -->
{!! Html::script('js/simple.clipart.js') !!}
<script>
	$(document).ready(function() {
		loadSimpleClipart(clipart, currentCategory);
        resizeTopMargin();

		var id;
		$(window).resize(function() {
			clearTimeout(id);
			id = setTimeout(doneResizing, 500);
		});

		function doneResizing() {
			loadSimpleClipart(clipart, currentCategory);
            resizeTopMargin();
		}
	});

</script>

<script>
  var sticktype = document.getElementById('stickertype').value;

  if(sticktype == 7 || sticktype == 6) { // black n white stickers
    $('#fontSelect').fontSelector({
        'hide_fallbacks' : true,
        'initial' : 'Boogaloo, boogaloottf, Courier, monospace',
        'fonts' : [
            'Boogaloo, boogaloottf, Helvetica, sans-serif',
            'Felipa, felipattf, Courier, monospace',
            'Gravitas One, gravitasonettf, sans-serif',
            'Hammersmith One, hammersmithonettf, sans-serif',
            'Leaguegothic, leaguegothicttf, serif',
            'Limelight, limelightttf, sans-serif',
            'Lobster Two, lobstertwottf, sans-serif',
            'Maidenorange, maidenoragettf, sans-serif',
            'Roboto, robotottf, sans-serif',
            'Roboto Condensed, robotocondensedttf, sans-serif',
            ]
    });
  } else {
    $('#fontSelect').fontSelector({
        'hide_fallbacks' : true,
        'initial' : 'Boogaloo, boogaloottf, Courier, monospace',
        'fonts' : [
            'Boogaloo, boogaloottf, Helvetica, sans-serif',
            'Crimson Text, crimsontextttf, Gadget, sans-serif',
            'England, englandttf, cursive',
            'Felipa, felipattf, Courier, monospace',
            'Gravitas One, gravitasonettf, sans-serif',
            'Great Vibes, greatvibesttf, monospace',
            'Hammersmith One, hammersmithonettf, sans-serif',
            'Henny Penny, hennepennyttf, Palatino,serif',
            'KaushanScript, kaushanscriptttf, sans-serif',
            'Leaguegothic, leaguegothicttf, serif',
            'Limelight, limelightttf, sans-serif',
            'Lobster Two, lobstertwottf, sans-serif',
            'Maidenorange, maidenoragettf, sans-serif',
            'Nunito, nunitottf, sans-serif',
            'Roboto, robotottf, sans-serif',
            'Roboto Condensed, robotocondensedttf, sans-serif',
            ]
    });
  }

    // Script to make the canvas sticky
    var $window = $(window),
       $stickyEl = $('#canvasCon'),
       elTop = $stickyEl.offset().top;
       $stickyEl2 = $('#myCanvas');
       $stickyEl3 = $('#canvasDiv');
       $stickyEl4 = $('#textBox');

    $window.scroll(function() {
        $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
        $stickyEl2.toggleClass('sticky', $window.scrollTop() > elTop);
        $stickyEl3.toggleClass('sticky', $window.scrollTop() > elTop);
        $stickyEl4.toggleClass('sticky', $window.scrollTop() > elTop);
    });

</script>
<script>
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' &&
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' ||
                 d.type.toUpperCase() === 'FILE' ||
                 d.type.toUpperCase() === 'SEARCH' ||
                 d.type.toUpperCase() === 'EMAIL' ||
                 d.type.toUpperCase() === 'NUMBER' ||
                 d.type.toUpperCase() === 'DATE' )
             ) ||
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});
</script>
@if (Input::has('id') && Orderitem::find(Input::get('id')))

  <?php
  $sticker = Orderitem::find(Input::get('id'));
    if(Input::has('id') && Input::has('dvos') && $sticker->user_id == Input::get('dvos')) {
      $jsonstring = $sticker->json;
    } else {
      $jsonstring = 'missmatch';
    }
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson(<?php echo $jsonstring ?>);
	  setPrice();
      });
  </script>
@elseif (Input::has('id') && Input::has('dvos'))
  <?php
    $jsonstring = 'missmatch';
  ?>

  <script>
    $(document).ready(function() {

      loadfromJson(<?php echo $jsonstring ?>);
	  setPrice();
      });
  </script>
@endif

<script>
$(document).ready(function() {
  if(document.getElementById('stickertype').value == 3) {
      var bgColor = '#cfb53b';
      setBackgroundColor(bgColor);
  } else if (document.getElementById('stickertype').value == 4) {
      var bgColor = '#d7d8d8';
      setBackgroundColor(bgColor);
  }
  });
</script>

@if (Input::has('tmp') && Template::find(Input::get('tmp')))
  <?php
    $sticker = Template::find(Input::get('tmp'));
    $jsonstring = $sticker->json;
	$tmpType = $sticker->product_type;
	$tmpQuantity = $sticker->quantity;
  ?>
  @if($tmpType == 0)
	  <script>
		$(document).ready(function() {

			var tempAntall = <?php echo $tmpQuantity ?>;
			document.getElementById('quantity').value = tempAntall;
			document.getElementById('quantity2').value = tempAntall;

		  loadfromJson(<?php echo $jsonstring ?>);
		  setPrice();
		  });
	  </script>
	 @endif
@endif

@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
	<?php
	$sticker = Orderitem::find(Input::get('edit'));
	$jsonstring = $sticker->json;

	?>
	@if (Auth::user() && $sticker->user_id == Auth::user()->id)
		<script>
			$(document).ready(function() {
			  loadfromJson(<?php echo $jsonstring ?>);
			  setPrice();
			  });
		</script>

	@endif
@endif

@if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
	<?php
	$sticker = Orderitem::find(Input::get('adminedit'));
	$jsonstring = $sticker->json;

	?>
	@if (Auth::user() && Auth::user()->admin == 1)
		<script>
			$(document).ready(function() {
			  loadfromJson(<?php echo $jsonstring ?>);
			  setPrice();
			  });
		</script>

	@endif
@endif

@stop
