<?php

if (Input::has('getBackground') && Input::get('getBackground') == 1 && Input::has('category') && Auth::check())
{
	$stickerType = 2;
	if (Input::has('stickerType')) {
		$stickerType = Input::get('stickerType');
	}
	getBackground(Input::get('category'), $stickerType);

}
else if (Input::has('uploadBackground') && Input::get('uploadBackground') == 1 && Auth::check())
{

	$stickerType = 2;
	if (Input::has('stickerType')) {
		$stickerType = Input::get('stickerType');
	}
	uploadBackground($stickerType);
}


// called when user submits background to be uploaded and saved to database
function uploadBackground($colorValue)
{
	$validator = Validator::make(Input::all(), Background::$rules);
	if ($validator->fails())
	{
		echo 'failed';
		foreach($validator->getMessageBag()->toArray()['image'] as $msg)
		{
			echo $msg;
		}
		echo 'failed';
		// echo 'validator failed';
		// return;
		return Redirect::to('storeTest')
		->with('message', 'Failed to create clipart.');

		// return Response::json(array('success' => false,
									// 'errors' => $validator->getMessageBag()->toArray()
		// ), 400);
	}

	// ensure valid data
	if ($validator->passes()) {
		$background = new Background;
		$background->title = 'uploadedBackground';


		// if user logged in
		if (Auth::check())
		{
			// save user id with clipart
			$background->user_id = Auth::user()->getId();


			$image = Input::file('image');
			$extension = $image->getClientOriginalExtension();
			$filenameNoExt = time();
			$filename = $filenameNoExt . '.' . $extension;
			$destination = public_path() . '/img/background';
			Image::make($image->getRealPath())->save($destination .'/'. $filename); // Fix the resizing to keep original format here
			$background->image = 'img/background/'.$filename;

			if($colorValue == 3 || $colorValue == 4 || $colorValue == 5) { //grayscale image
				$background->color = 1;
				$file_name = $destination .'/'. $filename;
				if ($extension == 'jpg' || $extension == 'jpeg') {
					$file_namePNG = $destination . '/'. $filenameNoExt . '.png';
					$makePNG = "convert " . $file_name . " PNG32:" . $file_namePNG;
					exec($makePNG);
					$conversion = "convert " . $file_namePNG . " -set colorspace gray -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_namePNG;   //konverterer bilden til grayscale transp
					exec($conversion);
					$resize = "convert " . $file_namePNG . " -resize 3200x3200\> " . $file_namePNG;  //****************************************************Her angis maks filstørrelse

					exec($resize);
					$background->image = 'img/background/'.$filenameNoExt.'.png';
					unlink($file_name);
				} else {
					$conversion = "convert " . $file_name . " -set colorspace gray -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_name;   //konverterer bilden til grayscale transp
					exec($conversion);
					$resize = "convert " . $file_name . " -resize 3200x3200\> " . $file_name;  //****************************************************Her angis maks filstørrelse

					exec($resize);
				}

			} else if ($colorValue == 6 || $colorValue == 7) { //black and white image
				$background->color = 0;
				$file_name = $destination .'/'. $filename;
				if ($extension == 'jpg' || $extension == 'jpeg') {
					$file_namePNG = $destination . '/'. $filenameNoExt . '.png';
					$makePNG = "convert " . $file_name . " PNG32:" . $file_namePNG;
					exec($makePNG);
					$conversion = "convert " . $file_namePNG . " -threshold 50% -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_namePNG;   //konverterer bilden til grayscale transp
					exec($conversion);
					$resize = "convert " . $file_namePNG . " -resize 3200x3200\> " . $file_namePNG;  //****************************************************Her angis maks filstørrelse

					exec($resize);
					$background->image = 'img/background/'.$filenameNoExt.'.png';
					unlink($file_name);
				} else {
					$conversion = "convert " . $file_name . " -threshold 50% -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_name;      //konverterer bilden til sort/hvit transp
					exec($conversion);
					$resize = "convert " . $file_name . " -resize 3200x3200\> " . $file_name;  //****************************************************Her angis maks filstørrelse

					exec($resize);
				}

			} else { //full color
				$background->color = 2;
				$file_name = $destination .'/'. $filename;
				$resize = "convert " . $file_name . " -resize 3200x3200\> " . $file_name; //****************************************************Her angis maks filstørrelse
				exec($resize);
			}


			$background->save();


			echo $background->image;
			echo 'success';
			 // return Redirect::to('storeTest')
			 	// ->with('message', 'Background Created');
		}
	}
}

// echos out backgrounds styled with the class 'backgroundImg', and the onclick event 'addBackground'
function getBackground($category, $clipartType)
{

	if (!Auth::user()) {
		$category = 0;
	}


	foreach(Background::orderBy('sortorder', 'ASC')->paginate(9999999) as $background)
	{
		if ($background->color <= $clipartType)
		{

			$name = '../../' . $background->image;
			$onclick = 'addBackground(\'' . $name . '\')';
			if ($category == 0) // show public clipart
			{
				if ($background->public == 1)
				{
					echo '<img src="', $name, '" name="', $name, '" class="backgroundImg" onclick="', $onclick, '">';
				}
			}
			else if ($category == 1) // show user's cilpart
			{
				if ($background->user_id==Auth::user()->getId() )
				{
					echo '<img src="', $name, '" name="', $name, '" class="backgroundImg" onclick="', $onclick, '">';
				}
			}
		}
	}
}

?>
