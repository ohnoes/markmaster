@extends('layouts.main')

@section('title')
  @include('designer.title')
@stop

@section('head-meta')
    @include('designer.head-meta')
@stop

@section('head-extra-styles')
  {!! Html::style('css/canvas.css') !!}
	{!! Html::style('css/clipart.new.css') !!}
@stop

@section('content')

@if (!Auth::user())
	 <!-- <div class="alert alert-info" role="alert"> {{ trans('designs/designer.Alert-login') }} </div> -->
@endif

<div> <!-- Main container -->
  <!-- MOBILE FIRST, top has the menu bar, then canvas, then order bar -->

    @include('designer.topmenu')

    <div class="row canvas-row no-margin"> <!-- CANVAS ROW -->
      <div class="col-sm-12 col-md-12 col-lg-12 design-tools-canvas"> <!-- Canvas block -->
      <!-- Canvas section -->
              <div id="canvasDiv">
                <canvas id="myCanvas" width="900" height="450"></canvas>
              </div>
      </div>
    </div>

    <div class="row tools-row no-margin"> <!-- MENU ROW -->
      <div class="col-xs-5ths tools-box" onclick="shapeBoxToggle()">
        <b class="fa fa-fw fa-lg fa-scissors"></b> <br>
        SHAPE
      </div>
      <div class="col-xs-5ths tools-box" onclick="backgroundBoxToggle()">
        <b class="fa fa-fw fa-lg fa-image"></b> <br>
        BACK
      </div>
      <div class="col-xs-5ths tools-box" onclick="textBoxToggle()">
        <b class="fa fa-fw fa-lg fa-font"></b> <br>
        TEXT
      </div>
      <div class="col-xs-5ths tools-box" onclick="imageBoxToggle()">
        <b class="fa fa-fw fa-lg fa-heart"></b> <br>
        CLIPART
      </div>
      <div class="col-xs-5ths tools-box" onclick="layerBoxToggle()">
        <b class="fa fa-fw fa-lg fa-list-ul"></b> <br>
        LAYERS
      </div>
    </div>

	@include('designer.hiddeninput')
  {!! Form::hidden('stickerpriceholder', trans('designs/designer.Label-price'), array('id'=>'stickerpriceholder'))!!}
  {!! Form::hidden('isGeneratedCutline', '0', array('id'=>'isGeneratedCutline')) !!}

	</div>
</div> <!-- container -->

 <!-- new MENU bar -->

 @include('designer.shapebox')
 @include('designer.backgroundbox')
 @include('designer.textbox')
 @include('designer.clipartbox')
 @include('designer.editbox')
 @include('designer.editclipartbox')
 @include('designer.layerbox')
<!-- NEW CLIPART BOX -->
@include('designer.newclipartbox')
@include('designer.newbackgroundbox')

<div id='mask'></div>
<div id='popup'>
    <p>Laster opp fil</p>
	<span id='popupSpin'></span>
</div>

<!--canvas id="myCanvas" width="200" height="100"></canvas-->
{!! Html::style('css/uploadingSpin.css') !!}
<!-- {!! Html::style('css/stickerMeasureBar.css') !!} -->

<!-- Javascript -->
@include('designer.javascript')

@stop
