@extends('layouts.main')

@section('content')

<!--   @if (!Auth::user())
     {!! Redirect::to('users/signin')->with('message', 'Du m� v�re innlogget om du �nsker � laste opp bakgrunner eller bilder') !!}
  @endif -->

 <div id="tabs">
  <ul id="tabLinks">
    <li class="tabStyle"><a href="#sizeAndShapeControl">{!! Html::image('img/size_and_shape.png', 'Size and Shape') !!} </a></li>
    <li class="tabStyle"><a href="#backgroundColorControl">{!! Html::image('img/background_color.png', 'background color') !!} </a></li>
    <li class="tabStyle"><a href="#textControl" >{!! Html::image('img/add_text.png', 'Text') !!} </a></li>
    <li class="tabStyle"><a href="#clipartControl" >{!! Html::image('img/add_clipart.png', 'Clipart') !!} </a></li>
    <li class="tabStyle"><a href="#cartControl" >{!! Html::image('img/shopping_cart.png', 'Shopping Cart', array('onclick'=>'rdycheckoutone()')) !!} </a></li>
  </ul>
  
  <div id="tabs_container">
    <!-- {!! Form::open(array('url'=>'users/create')) !!} -->
    <div id="sizeAndShapeControl">
       
        <p>
          {!! Form::radio('shape','','1',array('id'=>'isRect')) !!}
          {!! Html::image('img/rektangel.png', 'Rektangel', array('onclick'=>'makeRect()' )) !!}
          {!! Form::label('isRect','Rectangle') !!}
        </p>
        <p>
          {!! Form::radio('shape','','',array('id'=>'isEllipse')) !!}
          {!! Html::image('img/elipse.png', 'Elipse', array('onclick'=>'makeElipse()' )) !!}
          {!! Form::label('isElipse','Elipse') !!}
        </p>
        <p>
		@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
			<?php
				$sticker = Orderitem::find(Input::get('edit'));
			?>
			@if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1) 
				{!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
				{!! Form::text('widthtext', $sticker->width, array('disabled')) !!}
				{!! Form::label('width mm') !!}
				</br>
				{!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
				{!! Form::text('heighttext', $sticker->height, array('disabled')) !!} 
				{!! Form::label('height mm') !!}
			@else
				{!! Form::text('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
				{!! Form::label('width mm') !!}
				</br>
				{!! Form::text('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
				{!! Form::label('height mm') !!}
			@endif
		@else
          {!! Form::text('width','37', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
          {!! Form::label('width mm') !!}
		  </br>
		  {!! Form::text('height','16', array('id' => 'height', 'oninput' => 'setPrice()')) !!}
		  {!! Form::label('height mm') !!}
		@endif
         <!--  {!! Form::label('width', '', array('id' => 'widthValidate')) !!} -->
        </br>
          *Width and height must be between 6-300mm
        </p>
        <p>
		@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
			<?php
				$sticker = Orderitem::find(Input::get('edit'));
			?>
			@if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
			{!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
			{!! Form::text('quantitytext', $sticker->quantity, array('disabled')) !!}
			@elseif ($sticker->status == 0 || $sticker->status == 2)
			 {!! Form::text('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
			@endif
		@else
		 {!! Form::text('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
		@endif
		{!! Form::label('quantity') !!}
       </br></br>
          {!! Form::label('thePrice', 'Price: ', array('id'=>'thePrice')) !!}
        </p>

      <!-- </div> -->
    </div>


    <!-- tab 2 -->
    <div id="backgroundColorControl"> 
      <p>
        {!! Form::label('color', 'Choose color from chart') !!}
        
      </p>
        <span id="backgroundPicker">
            {!! Form::select('backgroundColorPicker', array(
                '#000000' => 'Black',
                '#ffffff' => 'White',
                '#E6E6E6' => '10% Black',
                '#B1B1B1' => '40% Black',
                '#888887' => '60% Black',
                '#5C5C5B' => '80% Black',
                '#C8112E' => 'PANTONE 185 C',
                '#B01D2B' => 'PANTONE 1797 C',
                '#871630' => 'PANTONE 201 C',
                '#EBB5C3' => 'PANTONE 182 C',
                '#E64A00' => 'PANTONE Orange 021 C',
                '#E9954A' => 'PANTONE 804 C',
                '#E6D5A8' => 'PANTONE 155 C',
                '#EFE032' => 'PANTONE Yellow C',
                '#EFED84' => 'PANTONE 100 C',
                '#EAEBBC' => 'PANTONE 607 C',
                '#0F2867' => 'PANTONE 281 C',
                '#1A35A8' => 'PANTONE 286 C',
                '#8ACBE5' => 'PANTONE 305 C',
                '#C9D8E7' => 'PANTONE 290 C',
                '#549AA3' => 'PANTONE 320 C',
                '#4A7229' => 'PANTONE 364 C',
                '#61AE56' => 'PANTONE 354 C',
                '#B6DD8E' => 'PANTONE 358 C',
                '#A4D426' => 'PANTONE 375 C',
                '#9E520F' => 'PANTONE 471 C',
                '#BC8F70' => 'PANTONE 7515 C',
                '#B50970' => 'PANTONE 226 C',
                '#E8A3D0' => 'PANTONE 230 C',
                '#680E92' => 'PANTONE 527 C',
                '#EACDCF' => 'PANTONE 698 C',
                '#9E70C1' => 'PANTONE 528 C',
                '#D7CAE3' => 'PANTONE 263 C',
                '#cfb53b' => 'Gold-like',
                '#d7d8d8' => 'Silver-like',),
                '#000000',

                array('id' => 'backgroundColor' ))
              !!}
          <!-- 
          <span style="background:#6096ed" id="customBackgroundColor" onclick="setBackgroundColorInput()">
          </span>

          {!! Form::text('bgColorInput','#6096ed', array('id' => 'bgColorInput', 'oninput' => 'setBackgroundColorInput()')) !!} -->
        </span>
          <br/></br></br>
      
          {!! Form::select('backCategory', array(
          '0' => 'Public Backgrounds',
          '1' => 'My Backgrounds'),
          '0',

          array('id' => 'backCategory'))
          !!}
        <span id="backgroundShow">
          <?php include '../resources/views/backgroundGet.blade.php'; getBackground(0); ?>
        </span>
      </br></br><br/>

      {!! Form::label('uploadLine', 'Upload custom background') !!}
    </br></br>

        <span class="uploadControls">
            {!! Form::open(array('files' => true, 'id' => 'backgroundForm')) !!}
             <!-- {!! Form::label('title', 'Name your background') !!}
            {!! Form::text('title') !!} -->
            @if (Auth::user())
              {!! Form::file('image', array('id' => 'backgroundFileSelect')) !!}
              {!! Form::submit('Upload Background', array()) !!}
            @else
              {!! Form::file('image', array('disabled' => '', 'id' => 'backgroundFileSelect')) !!}
              {!! Form::submit('Upload Background', array('disabled' => '')) !!}
            @endif
            {!! Form::close() !!}
          </span>

      </br></br></br>


<!-- 
          
        </span> -->
       
    </div>

    <!-- tab 3 -->
    <div id="textControl">
      <p>

          {!! Form::textarea('stickerText','Text goes here!', array('id' => 'stickerText', 'oninput' => 'setText()')) !!}
          <span>
            {!! form::button('Add Text', array('onclick' => 'setTextInput()', 'id' => 'addTextButton' )) !!}
            {!! form::button('Delete', array('onclick' => 'deleteText()', 'id' => 'deleteTextButton' )) !!}
          </span>
        </br>
          {!! Form::label('textAlign', 'Text Settings', array('id' => 'textAlign')) !!}

          <span class="textAlignImg" id="textBoldImg" onclick="setTextBold()">
          </span>

          <span class="textAlignImg" id="textItalicImg" onclick="setTextItalic()">
          </span>

          <span class="textAlignImg" id="textUnderlineImg" onclick="setTextUnderline()">
          </span>

          <span class="textAlignImg" id="textLeftImg" onclick="setTextAlign('left')">
          </span>
            
          <span class="textAlignImg" id="textCenterImg" onclick="setTextAlign('center')">
          </span>
            
          <span class="textAlignImg" id="textRightImg" onclick="setTextAlign('right')">
          </span>
            {!! Form::label('fontFamily', ' ') !!}
            
            {!! Form::select('fontFamily', array(
					  'boogaloottf' => 'boogaloottf',
					  'crimsontextttf' => 'crimsontextttf',
					  'englandttf' => 'englandttf',
					  'felipattf' => 'felipattf',
					  'gravitasonettf' => 'gravitasonettf',
					  'greatvibesttf' => 'greatvibesttf',
					  'hammersmithonettf' => 'hammersmithonettf',
					  'hennepennyttf' => 'hennepennyttf',
					  'kaushanscriptttf' => 'kaushanscriptttf',
					  'leaguegothicttf' => 'leaguegothicttf',
					  'limelightttf' => 'limelightttf',
					  'lobstertwottf' => 'lobstertwottf',
					  'maidenoragettf' => 'maidenoragettf',
					  'nunitottf' => 'nunitottf',
					  'robotottf' => 'robotottf',
					  'robotocondensedttf' => 'robotocondensedttf')) !!}

            <ul id="fontList" class="dropdown">
                <li id="boogaloottf" class="selected">Boogaloo</li>
                <li id="crimsontextttf">Crimson Text</li>
                <li id="englandttf">England</li>
                <li id="felipattf">Felipa</li>
                <li id="gravitasonettf">Gravitas One</li>
                <li id="greatvibesttf">Great Vibes</li>
                <li id="hammersmithonettf">Hammersmith One</li>
                <li id="hennepennyttf"> Henny Penny </li>
                <li id="kaushanscriptttf"> KaushanScript </li>
                <li id="leaguegothicttf"> Leaguegothic </li>
                <li id="limelightttf"> Limelight </li>
                <li id="lobstertwottf"> Lobster Two </li>
                <li id="maidenoragettf"> Maidenorange </li>
				<li id="nunitottfb"> Nunito </li>
				<li id="robotottf"> Roboto </li>
				<li id="robotocondensedttf"> Roboto Condensed </li>
            </ul>

            <span id="color">
            {!! Form::label('color', 'Color') !!}
            {!! Form::select('colorpickerRaise', array(
                '#000000' => 'Black',
                '#ffffff' => 'White',
                '#E6E6E6' => '10% Black',
                '#B1B1B1' => '40% Black',
                '#888887' => '60% Black',
                '#5C5C5B' => '80% Black',
                '#C8112E' => 'PANTONE 185 C',
                '#B01D2B' => 'PANTONE 1797 C',
                '#871630' => 'PANTONE 201 C',
                '#EBB5C3' => 'PANTONE 182 C',
                '#E64A00' => 'PANTONE Orange 021 C',
                '#E9954A' => 'PANTONE 804 C',
                '#E6D5A8' => 'PANTONE 155 C',
                '#EFE032' => 'PANTONE Yellow C',
                '#EFED84' => 'PANTONE 100 C',
                '#EAEBBC' => 'PANTONE 607 C',
                '#0F2867' => 'PANTONE 281 C',
                '#1A35A8' => 'PANTONE 286 C',
                '#8ACBE5' => 'PANTONE 305 C',
                '#C9D8E7' => 'PANTONE 290 C',
                '#549AA3' => 'PANTONE 320 C',
                '#4A7229' => 'PANTONE 364 C',
                '#61AE56' => 'PANTONE 354 C',
                '#B6DD8E' => 'PANTONE 358 C',
                '#A4D426' => 'PANTONE 375 C',
                '#9E520F' => 'PANTONE 471 C',
                '#BC8F70' => 'PANTONE 7515 C',
                '#B50970' => 'PANTONE 226 C',
                '#E8A3D0' => 'PANTONE 230 C',
                '#680E92' => 'PANTONE 527 C',
                '#EACDCF' => 'PANTONE 698 C',
                '#9E70C1' => 'PANTONE 528 C',
                '#D7CAE3' => 'PANTONE 263 C',
                '#cfb53b' => 'Gold-like',
                '#d7d8d8' => 'Silver-like',),
                '#000000',
                array('id' => 'fill'))
            !!}
          </span>
        </br>
          {!! Form::label('fontSize', 'Text Size')!!}
          <input type="range" name="fontSizeSlider" min="5" max="140" value="10" id="fontSizeSlider" step="0.1">
          {!! Form::text('fontSizeTextbox', '10', array('id' => 'fontSizeTextbox')) !!}
        </br>
          {!! Form::label('lineHeight', 'Line Spacing')!!}
          <input type="range" name="lineHeightSlider" min="0" max="2" value="0.9" id="lineHeightSlider" step="0.01">
          {!! Form::text('lineHeightTextbox', '1', array('id' => 'lineHeightTextbox')) !!}
        </br>
          {!! Form::label('clipartRotate', 'Rotation')!!}
          <input type="range" name="textAngleSlider" min="0" max="360" value="0" id="textAngleSlider" step="any">
          {!! Form::text('textAngleTextbox', '0', array('id' => 'textAngleTextbox')) !!}
          <!-- <input class="color" value="#000000" id="fill"> -->
        </br></br>          
          <!--
          <span style="background:#6096ed" id="customTextColor" onclick="setTextColorInput()">
          </span>

           {!! Form::text('textColorInput','#6096ed', array('id' => 'textColorInput', 'oninput' => 'setTextColorInput()')) !!} -->

           </br>
            {!! Form::label('stroke', 'Outline Color')!!}
            {!! Form::select('colorpickerStroke', array(
                '#000000' => 'Black',
                '#ffffff' => 'White',
                '#E6E6E6' => '10% Black',
                '#B1B1B1' => '40% Black',
                '#888887' => '60% Black',
                '#5C5C5B' => '80% Black',
                '#C8112E' => 'PANTONE 185 C',
                '#B01D2B' => 'PANTONE 1797 C',
                '#871630' => 'PANTONE 201 C',
                '#EBB5C3' => 'PANTONE 182 C',
                '#E64A00' => 'PANTONE Orange 021 C',
                '#E9954A' => 'PANTONE 804 C',
                '#E6D5A8' => 'PANTONE 155 C',
                '#EFE032' => 'PANTONE Yellow C',
                '#EFED84' => 'PANTONE 100 C',
                '#EAEBBC' => 'PANTONE 607 C',
                '#0F2867' => 'PANTONE 281 C',
                '#1A35A8' => 'PANTONE 286 C',
                '#8ACBE5' => 'PANTONE 305 C',
                '#C9D8E7' => 'PANTONE 290 C',
                '#549AA3' => 'PANTONE 320 C',
                '#4A7229' => 'PANTONE 364 C',
                '#61AE56' => 'PANTONE 354 C',
                '#B6DD8E' => 'PANTONE 358 C',
                '#A4D426' => 'PANTONE 375 C',
                '#9E520F' => 'PANTONE 471 C',
                '#BC8F70' => 'PANTONE 7515 C',
                '#B50970' => 'PANTONE 226 C',
                '#E8A3D0' => 'PANTONE 230 C',
                '#680E92' => 'PANTONE 527 C',
                '#EACDCF' => 'PANTONE 698 C',
                '#9E70C1' => 'PANTONE 528 C',
                '#D7CAE3' => 'PANTONE 263 C',
                '#cfb53b' => 'Gold-like',
                '#d7d8d8' => 'Silver-like',),
                '#000000',
                array('id' => 'stroke'))
              !!}
              
              <!-- 
              <span style="background:#6096ed" id="customStrokeColor" onclick="setStrokeColorInput()">
              </span>

              {!! Form::text('strokeColorInput','#6096ed', array('id' => 'strokeColorInput', 'oninput' => 'setStrokeColorInput()')) !!} -->
              </br>

            {!! Form::label('strokeWidth', 'Text Outline')!!}
            <input type="range" step="any" name="strokeWidth" min="0.01" max="1" value="0.01" id="strokeWidth">
            {!! Form::text('strokeWidthTextBox', '0.01', array('id' => 'strokeWidthTextbox')) !!}

          <p>
            {!! Form::label('centerObject', 'Center Text') !!}
            <img src="img/center_h.png" name="centerH Object" onclick="centerObjectH()" id="centerH">
            <img src="img/center_v.png" name="centerV Object" onclick="centerObjectV()" id="centerV">
          </p>
          <p>
            {!! Form::label('layerControl', 'Layer Controls') !!}
            <img src="img/back_img.png" name="Send to Back" onclick="sendToBack()" id="sendToBackButton">
            <img src="img/down_img.png" name="Move Down" onclick="moveDown()" id="moveDownButton">
            <img src="img/up_img.png" name="Move Up" onclick="moveUp()" id="moveUpButton">
            <img src="img/front_img.png" name="Bring to Front" onclick="bringToFront()" id="sendToFrontButton">

          </p>
          </p>
      </p>
    </div>


    <!-- tab 4 -->
    <div id="clipartControl">
      
      <p>
      <!--     {!! Form::button('Upload Clipart', array('id' => 'uploadControlsOpen')) !!} -->
    <!--   <select id="category" name="category">
        <option value="0" selected="selected">All Clipart</option>
        <option value="1">My Clipart</option>
        <?php 

          foreach(Category::all() as $cat)
          {
            $catID = $cat->id;
            $catName = $cat->name;
            echo '<option value="', $catID, '">', $catName, '</option>';
          }
        ?>
      </select> -->
      {!! Form::select('category', array(
          '0' => 'Alle Clipart',
          '1' => 'Mine Clipart',
          '2' => 'Mest popul�re',
          '3' => 'Dyr',
          '4' => 'Stjernetegn',
          '5' => 'Figurer',
          '6' => 'Symboler',
          '7' => 'Sport',
          '8' => 'Maskiner',
          '9' => 'Former'),
          '0',

          array('id' => 'category'))
        !!}
      {!! Form::text('categorySearchBox', '', array('id' => 'categorySearchBox', 'placeholder' => 'Search within this category', 'width' => '50')) !!}
      {!! Form::label('clipLoadingProgress', ' ', array('id' => 'clipLoadingProgress'))!!}
      <span id="clipartShow">
        <?php include '../resources/views/clipartGet.blade.php'; echoClipart(0, 5); ?>
      </span>

<!-- CROP PART -->
      
      <!--<p>
        Crop Art
        {!! form::button('Crop', array('id' => 'startCropB', 'value' => 'cropDisabled')) !!}
        {!! form::button('Finish Crop', array('id' => 'finishCropB', 'disabled' =>'')) !!}
      </p> -->

        Flip Art
        <img src="img/flip_v.png" name="flip X" onclick="flipXImg()" id="flipX">
        <img src="img/flip_h.png" name="flip Y" onclick="flipYImg()" id="flipY">

      @if(true)
        <!-- Fix so this is disabled when the clipart can't change color -->
        {!! Form::label('clipcolor', 'Clipart Color')!!}
        {!! Form::select('colorpickerClipart', array(
                '#000000' => 'Black',
                '#ffffff' => 'White',
                '#E6E6E6' => '10% Black',
                '#B1B1B1' => '40% Black',
                '#888887' => '60% Black',
                '#5C5C5B' => '80% Black',
                '#C8112E' => 'PANTONE 185 C',
                '#B01D2B' => 'PANTONE 1797 C',
                '#871630' => 'PANTONE 201 C',
                '#EBB5C3' => 'PANTONE 182 C',
                '#E64A00' => 'PANTONE Orange 021 C',
                '#E9954A' => 'PANTONE 804 C',
                '#E6D5A8' => 'PANTONE 155 C',
                '#EFE032' => 'PANTONE Yellow C',
                '#EFED84' => 'PANTONE 100 C',
                '#EAEBBC' => 'PANTONE 607 C',
                '#0F2867' => 'PANTONE 281 C',
                '#1A35A8' => 'PANTONE 286 C',
                '#8ACBE5' => 'PANTONE 305 C',
                '#C9D8E7' => 'PANTONE 290 C',
                '#549AA3' => 'PANTONE 320 C',
                '#4A7229' => 'PANTONE 364 C',
                '#61AE56' => 'PANTONE 354 C',
                '#B6DD8E' => 'PANTONE 358 C',
                '#A4D426' => 'PANTONE 375 C',
                '#9E520F' => 'PANTONE 471 C',
                '#BC8F70' => 'PANTONE 7515 C',
                '#B50970' => 'PANTONE 226 C',
                '#E8A3D0' => 'PANTONE 230 C',
                '#680E92' => 'PANTONE 527 C',
                '#EACDCF' => 'PANTONE 698 C',
                '#9E70C1' => 'PANTONE 528 C',
                '#D7CAE3' => 'PANTONE 263 C',
                '#cfb53b' => 'Gold-like',
                '#d7d8d8' => 'Silver-like',),
                '#000000',
                array('id' => 'clipcolor', 'onchange' => 'clipColor()'))
              !!}
      @endif
        {!! form::button('Delete', array('onclick' => 'deleteClipart()', 'id' => 'deleteClipartButton' )) !!}

      </br>
        Center Clipart
        <img src="img/center_h.png" name="centerH Object" onclick="centerObjectH()" id="centerH">
        <img src="img/center_v.png" name="centerV Object" onclick="centerObjectV()" id="centerV">
      </br>
         {!! Form::label('clipartScale', 'Clipart Size')!!}
        <input type="range" name="clipartScaleSlider" min="0.0001" max="6" value="1" id="clipartScaleSlider" step="any">
        {!! Form::text('clipartScaleTextbox', '1', array('id' => 'clipartScaleTextbox')) !!}<!-- 
        {!! Form::text('fontSizeTextbox', '10', array('id' => 'fontSizeTextbox')) !!} -->
      </br>
        {!! Form::label('clipartRotate', 'Rotation')!!}
        <input type="range" name="clipartAngleSlider" min="0" max="360" value="0" id="clipartAngleSlider" step="any">
        {!! Form::text('clipartAngleTextbox', '0', array('id' => 'clipartAngleTextbox')) !!}
      </br>
        {!! Form::label('layerControl', 'Layer Controls') !!}
        <img src="img/back_img.png" name="Send to Back" onclick="sendToBack()" id="sendToBackButton">
        <img src="img/down_img.png" name="Move Down" onclick="moveDown()" id="moveDownButton">
        <img src="img/up_img.png" name="Move Up" onclick="moveUp()" id="moveUpButton">
        <img src="img/front_img.png" name="Bring to Front" onclick="bringToFront()" id="sendToFrontButton">


      </p>
          <span class="uploadControls">
            {!! Form::open(array('files' => true, 'id' => 'cilpartForm')) !!}
            <!-- {!! Form::label('title', 'Name your clipart') !!}
            {!! Form::text('title') !!} -->
            @if (Auth::user())
              {!! Form::file('image', array('id' => 'clipartFileSelect')) !!}
              {!! Form::submit('Upload Clipart', array()) !!}
            @else
              {!! Form::file('image', array('disabled' => '', 'id' => 'clipartFileSelect')) !!}
              {!! Form::submit('Upload Clipart', array('disabled' => '')) !!}
            @endif
            {!! Form::close() !!}
          </span>
    </div>
    <div id="cartControl">
      
	  @if (Input::has('edit') && Orderitem::find(Input::get('edit')))
		<?php
			$sticker = Orderitem::find(Input::get('edit'));
		?>
		@if (Auth::user() && $sticker->user_id == Auth::user()->id && ($sticker->status == 0 || $sticker->status == 1) )
			{!! Form::open(array('url'=>'store/updatecart')) !!}
			{!! Form::hidden('stickerid', $sticker->id) !!}
			{!! Form::hidden('userid', Auth::user()->id) !!}
			{!! Form::hidden('stickertype', 'stickertype', array('id'=>'stickertype')) !!}
			{!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
			@if($sticker->status == 1)
				{!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
				{!! Form::text('quantitytext', $sticker->quantity, array('disabled')) !!}
			@else
				{!! Form::text('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
			@endif
			{!! Form::label('quantity') !!}
			{!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
			{!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
			{!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
			{!! Form::label('thePrice', 'Price: ', array('id'=>'thePrice2')) !!}
			{!! Form::submit('Oppdater etikett', array('class'=>'buttonBox')) !!}
			{!! Form::close() !!}
		@endif
		
	@else
	<p>
          {!! Form::open(array('url'=>'store/addtocart')) !!}
          @if(Auth::check())
            {!! Form::hidden('userid', Auth::user()->id)!!}
          @endif
          {!! Form::hidden('stickertype', 'stickertype', array('id'=>'stickertype')) !!}
          {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
		  {!! Form::text('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
		  {!! Form::label('quantity') !!}
       </br></br>
          {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
		  {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
		  {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
          {!! Form::label('thePrice', 'Price: ', array('id'=>'thePrice2')) !!}
          {!! Form::submit('Legg i handlevogn', array('class'=>'buttonBox')) !!}
          {!! Form::close() !!}
      </p> 
	@endif
	  
	  
      @if (Input::has('dev') && Input::get('dev') == 1)
        {!! Form::button('-test- Preview Sticker', array('onclick' => 'submitSticker()')) !!}
        {!! '<span id="stickerPreview">
        </span>' !!}    
      @endif

    </div>
    <!-- {!! Form::submit('CREATE NEW ACCOUNT', array('class'=>'secondary-cart-btn')) !!} -->
  </div>
  <!--End tabs container--> 
<!-- </div> -->
<!--End tabs-->

<!--   {!! Form::close() !!} -->

</div>


<div id="canvasDiv">
  <canvas id="myCanvas" width="600" height="600" style="border: 1px solid rgb(170, 170, 170); position: absolute; width: 600px; height: 600px; left: 0px; top: 0px; -webkit-user-select: none;" class="lower-canvas"></canvas>
</div>
<!--canvas id="myCanvas" width="200" height="100"></canvas-->
{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/fabric/dist/fabric.min.js') !!}
{!! Html::script('js/tabulous.min.js') !!}
{!! Html::script('js/jquery.simplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/store.js') !!}

@if (Input::has('id') && Orderitem::find(Input::get('id')))

  <?php
  $sticker = Orderitem::find(Input::get('id')); 
    if(Input::has('id') && Input::has('dvos') && $sticker->user_id == Input::get('dvos')) {
      $jsonstring = $sticker->json;
    } else {
      $jsonstring = 'missmatch';
    }
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson('<?php echo $jsonstring ?>');
      });
  </script>
@elseif (Input::has('id') && Input::has('dvos'))
  <?php 
    $jsonstring = 'missmatch';
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson('<?php echo $jsonstring ?>');
      });
  </script>
@endif

@if (Input::has('tmp') && Template::find(Input::get('tmp')))
  <?php  
    $sticker = Template::find(Input::get('tmp'));
    $jsonstring = $sticker->json;
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson('<?php echo $jsonstring ?>');
      });
  </script>
@endif

@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
	<?php
	$sticker = Orderitem::find(Input::get('edit'));
	$jsonstring = $sticker->json;
	
	?>
	@if (Auth::user() && $sticker->user_id == Auth::user()->id)
		<script>
			$(document).ready(function() {
			  loadfromJson('<?php echo $jsonstring ?>');
			  });
		</script>

	@endif
@endif
@stop