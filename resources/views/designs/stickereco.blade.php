@extends('layouts.main')

@section('title')

    {{ trans('designs/stickereco.Translate1') }}

@stop

@section('head-meta')
	<meta name="description" content="{{ trans('designs/stickereco.Translate2') }}">
    <link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/designs/stickereco" />

@stop

@section('head-extra-styles')
    {!! Html::style('css/canvas.css') !!}
	{!! Html::style('css/clipart.new.css') !!}
@stop

@section('content')

@if (!Auth::user())

	<div class="alert alert-info" role="alert"> {{ trans('designs/stickereco.Translate3') }} </div>

@endif
<div class="container"> <!-- Main container -->
	<h1 class="tempHeader">{{ trans('designs/stickereco.Translate4') }}</h1>
	<div id="container"> <!-- Grid container -->
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 design-tools-canvas"> <!-- Canvas block -->
			<!-- Canvas section -->
				<div id="container">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div id="canvasDiv">
								<canvas id="myCanvas" width="600" height="600"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 design-tools"> <!-- Design controll block -->
				<!-- Tab section (left side) -->
				<ul id="tabs" class="nav nav-tabs nav-justified nav-margin" data-tabs="tabs">
					<li class="active"><a href="#sizeAndShapeControl" data-toggle="tab">{{ trans('designs/stickereco.Translate6') }}</a></li>
					<li><a href="#backgroundColorControl" data-toggle="tab">{{ trans('designs/stickereco.Translate7') }}</a></li>
					<li><a href="#textControl" data-toggle="tab">{{ trans('designs/stickereco.Translate8') }} </a></li>
					<li><a href="#clipartControl" data-toggle="tab">{{ trans('designs/stickereco.Translate9') }}</a></li>
					<li onclick="rdycheckout(6)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/stickereco.Translate10') }}</a></li>
				</ul>
				<div id="my-tab-content" class="tab-content tab-padding">
					<div class="tab-pane active" id="sizeAndShapeControl"> <!-- Size&Shape tab -->
						{!! Form::hidden('shape','1',array('id'=>'isRect')) !!}
						{!! Form::hidden('shape','',array('id'=>'isEllipse')) !!}
						{!! Form::hidden('size', 'size', array('id'=>'3010')) !!}
					<?php
							$thecur = Currency::find(1);
							if(Auth::check()) {
								$usercur = Auth::user()->currency;
								if ($usercur == 0) {
									$currencyValue = 1;
								} else if ($usercur == 1) {
									$currencyValue = $thecur->usd;
								} else if ($usercur == 2) {
									$currencyValue = $thecur->sek;
								} else if ($usercur == 3) {
									$currencyValue = $thecur->dkk;
								}else if ($usercur == 4) {
									$currencyValue = $thecur->eur;
								}
							} else {
								$usercur = Session::get('theCurrency', 0);
								if ($usercur == 0) {
									$currencyValue = 1;
								} else if ($usercur == 1) {
									$currencyValue = $thecur->usd;
								} else if ($usercur == 2) {
									$currencyValue = $thecur->sek;
								} else if ($usercur == 3) {
									$currencyValue = $thecur->dkk;
								}else if ($usercur == 4) {
									$currencyValue = $thecur->eur;
								}
							}

						?>
							{!! Form::hidden('thecurrency', $usercur, array('id'=>'thecurrency')) !!}
							{!! Form::hidden('currencycheck', $currencyValue, array('id'=>'currencycheck')) !!}
							{!! Form::hidden('priceHolder', 'Price: ', array('id'=>'priceHolder')) !!}

						<div class="form-group">
						<label for="size">{{ trans('designs/stickereco.Translate64') }} </label>
						</div>
						<div class="form-group">
						{!! Form::radio('size','','1',array('id'=>'3716')) !!}
						<label for="3716">{{ trans('designs/stickereco.Translate65') }} </label>
						</div>
						<div class="form-group">
						{!! Form::radio('size','','',array('id'=>'6026')) !!}
						<label for="6026">{{ trans('designs/stickereco.Translate66') }} </label>
						</div>

					@if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
						<?php
							$sticker = Orderitem::find(Input::get('adminedit'));
						?>
						@if (Auth::user() && Auth::user()->admin == 1 && $sticker->status == 1)
							{!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
							{!! Form::hidden('widthtext', $sticker->width, array('disabled')) !!}
							{!! Form::hidden('bredde mm') !!}

							{!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
							{!! Form::hidden('heighttext', $sticker->height, array('disabled')) !!}
							{!! Form::hidden('høyde mm') !!}
						@else
							{!! Form::hidden('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
							{!! Form::hidden('bredde mm') !!}

							{!! Form::hidden('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
							{!! Form::hidden('høyde mm') !!}
						@endif
					@elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
						<?php
							$sticker = Orderitem::find(Input::get('edit'));
						?>
						@if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
							{!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
							{!! Form::hidden('widthtext', $sticker->width, array('disabled')) !!}
							{!! Form::hidden('bredde mm') !!}

							{!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
							{!! Form::hidden('heighttext', $sticker->height, array('disabled')) !!}
							{!! Form::hidden('høyde mm') !!}
						@else
							{!! Form::hidden('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
							{!! Form::hidden('bredde mm') !!}

							{!! Form::hidden('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
							{!! Form::hidden('høyde mm') !!}
						@endif
					@else
					  {!! Form::hidden('width','37', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
					  {!! Form::hidden('bredde mm') !!}

					  {!! Form::hidden('height','16', array('id' => 'height', 'oninput' => 'setPrice()')) !!}
					  {!! Form::hidden('høyde mm') !!}
					@endif
					 <!--  {!! Form::label('width', '', array('id' => 'widthValidate')) !!} -->

					@if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
						<?php
							$sticker = Orderitem::find(Input::get('adminedit'));
						?>
						@if (Auth::user() && Auth::user()->admin == 1 )
						<div class="form-group">
							{!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
							{!! Form::hidden('Antall', $sticker->quantity, array('id'=>'Antall')) !!}
							{!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
						</div>
						@elseif ($sticker->status == 0 || $sticker->status == 2)
						<div class="form-group">
							{!! Form::hidden('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
							<label> {{ trans('designs/stickereco.Translate15') }} </label>
							<select id="Antall" onchange="setPrice()">
								<option value="1" >{{ trans('designs/stickereco.Translate16') }}</option>
								<option value="5">{{ trans('designs/stickereco.Translate17') }}</option>
								<option value="10">{{ trans('designs/stickereco.Translate18') }}</option>
								<option value="20">{{ trans('designs/stickereco.Translate19') }}</option>
								<option value="50">{{ trans('designs/stickereco.Translate20') }}</option>
								<option value="75">{{ trans('designs/stickereco.Translate21') }}</option>
								<option value="100">{{ trans('designs/stickereco.Translate22') }}</option>
								<option value="250">{{ trans('designs/stickereco.Translate23') }}</option>
								<option value="500">{{ trans('designs/stickereco.Translate24') }}</option>
								<option value="1000">{{ trans('designs/stickereco.Translate25') }}</option>
							</select>
						</div>
						@endif
					@elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
						<?php
							$sticker = Orderitem::find(Input::get('edit'));
						?>
						@if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
						<div class="form-group">
							{!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
							{!! Form::hidden('Antall', $sticker->quantity, array('id'=>'Antall')) !!}
							{!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
						</div>
						@elseif ($sticker->status == 0 || $sticker->status == 2)
						<div class="form-group">
							{!! Form::hidden('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
							<label> {{ trans('designs/stickereco.Translate15') }} </label>
							<select id="Antall" onchange="setPrice()">
								<option value="1" >{{ trans('designs/stickereco.Translate16') }}</option>
								<option value="5">{{ trans('designs/stickereco.Translate17') }}</option>
								<option value="10">{{ trans('designs/stickereco.Translate18') }}</option>
								<option value="20">{{ trans('designs/stickereco.Translate19') }}</option>
								<option value="50">{{ trans('designs/stickereco.Translate20') }}</option>
								<option value="75">{{ trans('designs/stickereco.Translate21') }}</option>
								<option value="100">{{ trans('designs/stickereco.Translate22') }}</option>
								<option value="250">{{ trans('designs/stickereco.Translate23') }}</option>
								<option value="500">{{ trans('designs/stickereco.Translate24') }}</option>
								<option value="1000">{{ trans('designs/stickereco.Translate25') }}</option>
							</select>
						</div>
						@endif
					@else
					<div class="form-group">
					 {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
					<label> {{ trans('designs/stickereco.Translate15') }} </label>
					<select id="Antall" onchange="setPrice()">
						<option value="1" >{{ trans('designs/stickereco.Translate16') }}</option>
						<option value="5">{{ trans('designs/stickereco.Translate17') }}</option>
						<option value="10">{{ trans('designs/stickereco.Translate18') }}</option>
						<option value="20">{{ trans('designs/stickereco.Translate19') }}</option>
						<option value="50">{{ trans('designs/stickereco.Translate20') }}</option>
						<option value="75">{{ trans('designs/stickereco.Translate21') }}</option>
						<option value="100">{{ trans('designs/stickereco.Translate22') }}</option>
						<option value="250">{{ trans('designs/stickereco.Translate23') }}</option>
						<option value="500">{{ trans('designs/stickereco.Translate24') }}</option>
						<option value="1000">{{ trans('designs/stickereco.Translate25') }}</option>
					</select>
					</div>
					@endif

					<div class="form-group">
						<label for="thePrice" id="thePrice">{{ trans('designs/stickereco.Translate26') }}</label>
					</div>

					</div>

					<div class="tab-pane" id="backgroundColorControl"> <!-- Background tab -->

						<div class="form-group">
							<label>{{ trans('designs/stickereco.Translate27') }}</label>
						</div>
						<div class="form-group">
							<select id="backCategory">
								<option value="0" selected>{{ trans('designs/stickereco.Translate28') }}</option>
								<option value="1">{{ trans('designs/stickereco.Translate29') }}</option>
							 </select>
							<span id="backgroundShow">
							  <?php include '../resources/views/backgroundGet.blade.php'; getBackground(0, 0); ?>
							</span>
						</div>

						<div class="form-group">
							<label for="uploadLine">{{ trans('designs/stickereco.Translate30') }}</label>
						</div>

						<span class="uploadControls">
							{!! Form::open(array('files' => true, 'id' => 'backgroundForm')) !!}
							@if (Auth::user())
								<div class="form-group">
									{!! Form::file('image', array('id' => 'backgroundFileSelect')) !!}
								</div>
								<div class="form-group">
									<input type="submit" value="{{ trans('designs/stickereco.Translate31') }}" class="payBox">
								</div>
							@else
								<div class="form-group">
									{!! Form::file('image', array('disabled' => '', 'id' => 'backgroundFileSelect')) !!}
								</div>
								<div class="form-group">
									<input type="submit" value="{{ trans('designs/stickereco.Translate31') }}" class="payBox">
								</div>
							@endif
							{!! Form::close() !!}
						</span>
					</div>
					<div class="tab-pane" id="textControl"> <!-- Text tab -->
                        <textarea id="hidden-input"> </textarea>
						{!! Form::hidden('fill', 'fill', array('id'=>'fill', 'value'=>'#000')) !!}
						{!! Form::hidden('stroke', 'stroke', array('id'=>'stroke', 'value'=>'#000')) !!}
                        <div class="row"> <!-- Size, linedistance, rotation & position -->
                            {!! Form::hidden('stickerText', trans('designs/simple.Translate28') , array('id'=>'stickerText')) !!}
                            <div class="col-xs-1 col-sm-1 col-md-0 col-lg-0">
                            </div>
							<div class="col-xs-5 col-sm-5 col-md-2 col-lg-2">
                                <div class="row">
                                    <label> {{ trans('designs/stickercolor.Translate74') }} </label>
                                </div>
                                <div class="row">
                                    <div style="width: 60px; height: 45px; background-image: url('/img/add_text.png'); display:block; cursor: pointer; cursor: hand;" onclick="setTextInput()"/></div>
                                </div>
                                <div class="row" style="padding-top: 10px;">
								    <button id="deleteTextButton" onclick="deleteText()">{{ trans('designs/stickercolor.Translate31') }} </button>
                                </div>
							</div>
                            <div class="col-xs-5 col-sm-5 col-md-2 col-lg-2" style="text-align: center">
								<div class="row">
									<label for="position"> {{ trans('designs/stickercolor.Translate36') }} </label>
								</div>
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-top textIconBox" aria-hidden="true"
									onclick="moveObjectTop()"
									onmousedown="inter=setInterval(moveObjectTop, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
									</span> </div>
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
								</div>
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-left textIconBox" aria-hidden="true"
									onclick="moveObjectLeft()"
									onmousedown="inter=setInterval(moveObjectLeft, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span></div>
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-right textIconBox" aria-hidden="true"
									onclick="moveObjectRight()"
									onmousedown="inter=setInterval(moveObjectRight, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span> </div>
								</div>
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-bottom textIconBox" aria-hidden="true"
									onclick="moveObjectDown()"
									onmousedown="inter=setInterval(moveObjectDown, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span> </div>
									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
								</div>
							</div>
                                <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1" style="text-align: center">
                            </div>
							<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" style="text-align: center">
								<div class="row">
									<label for="fontSize" >{{ trans('designs/stickercolor.Translate33') }}</label>
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-resize-full textIconBoxControll" aria-hidden="true"
									onclick="increaseFont()"
									onmousedown="inter=setInterval(increaseFont, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-plus" aria-hidden="true" >
										  </span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('fontSizeTextbox', '2.5', array('id' => 'fontSizeTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-resize-small textIconBoxControll" aria-hidden="true"
									onclick="decreaseFont()"
									onmousedown="inter=setInterval(decreaseFont, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-minus" aria-hidden="true">  </span>
									</span>
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" style="text-align: center">
								<div class="row">
									<label for="lineHeight">{{ trans('designs/stickercolor.Translate34') }}</label>
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-gift textIconBoxControll" aria-hidden="true"
									onclick="increaseLineHeight()"
									onmousedown="inter=setInterval(increaseLineHeight, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('lineHeightTextbox', '0.95', array('id' => 'lineHeightTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-leaf textIconBoxControll" aria-hidden="true"
									onclick="decreaseLineHeight()"
									onmousedown="inter=setInterval(decreaseLineHeight, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-minus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" style="text-align: center">
								<div class="row">
									<label for="lineRotate"> {{ trans('designs/stickercolor.Translate35') }} </label>
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-repeat textIconBoxControll"
									onclick="increaseTextRotation()"
									onmousedown="inter=setInterval(increaseTextRotation, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('textAngleTextbox', '0', array('id' => 'textAngleTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-minus textIconBoxControll" id="flipped-icon"
									onclick="decreaseTextRotation()"
									onmousedown="inter=setInterval(decreaseTextRotation, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-repeat" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
							</div>
						</div>
						<div class="row"> <!-- Text formatting controlls & font -->
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-top: 20px">
								<label for="textAlign" id="textAligh">{{ trans('designs/stickercolor.Translate32') }}</label>
								<span class="glyphicon glyphicon-bold textIconBox" aria-hidden="true" onclick="setTextBold()"></span>
								<span class="glyphicon glyphicon-italic textIconBox" aria-hidden="true" onclick="setTextItalic()"></span>


								<!--<span class="glyphicon glyphicon-align-left textIconBox" aria-hidden="true" onclick="setTextAlign('left')"></span> -->
								<!-- <span class="glyphicon glyphicon-align-center textIconBox" aria-hidden="true" onclick="setTextAlign('center')"></span> -->
								<!--<span class="glyphicon glyphicon-align-right textIconBox" aria-hidden="true" onclick="setTextAlign('right')"></span> -->

                                </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 " style="padding-top: 20px">
                                        <div id="fontSelect" class="fontSelect" style="margin-left: 0px">
                                            <div class="arrow-down"></div>
                                        </div>
                                    </div>
                            </div>


								{!! Form::label('fontFamily', ' ') !!}

								{!! Form::select('fontFamily', array(
										  'boogaloottf' => 'boogaloottf',
										  'crimsontextttf' => 'crimsontextttf',
										  'englandttf' => 'englandttf',
										  'felipattf' => 'felipattf',
										  'goblinonettf' => 'goblinonettf',
										  'gravitasonettf' => 'gravitasonettf',
										  'greatvibesttf' => 'greatvibesttf',
										  'hammersmithonettf' => 'hammersmithonettf',
										  'hennepennyttf' => 'hennepennyttf',
										  'kaushanscriptttf' => 'kaushanscriptttf',
										  'leaguegothicttf' => 'leaguegothicttf',
										  'limelightttf' => 'limelightttf',
										  'lobstertwottf' => 'lobstertwottf',
										  'maidenoragettf' => 'maidenoragettf',
										  'nunitottf' => 'nunitottf',
										  'robotottf' => 'robotottf',
										  'robotocondensedttf' => 'robotocondensedttf')) !!}
						<div class="row"> <!-- Center,Layer & line thickness -->
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 grid-breathingroom" style="text-align: center">
								<div id="row">
									<label for="centerObject">{{ trans('designs/stickereco.Translate40') }}</label>
								</div>
								<div id="row">
									<span class="glyphicon glyphicon-eye-open textIconBox" aria-hidden="true" onclick="centerObjectV()"></span>
									<span class="glyphicon glyphicon-eye-close textIconBox" aria-hidden="true" onclick="centerObjectH()"></span>
								</div>
								<div id="row">
									<label for="layerControl">{{ trans('designs/stickereco.Translate41') }}</label>
								</div>
								<div id="row">
									<!--<span class="glyphicon glyphicon-fast-backward textIconBox" aria-hidden="true" onclick="sendToBack()"></span>-->
									<span class="glyphicon glyphicon-step-backward textIconBox" aria-hidden="true" onclick="moveDown()"></span>
									<span class="glyphicon glyphicon-step-forward textIconBox" aria-hidden="true" onclick="moveUp()"></span>
									<!--<span class="glyphicon glyphicon-fast-forward textIconBox" aria-hidden="true" onclick="bringToFront()"></span>-->
								</div>
							</div>
							<div class="col-xs-3 col-sm-4 col-md-3 col-lg-3 grid-breathingroom" style="text-align: center">
								<div id="row">
									<label for="strokeWidth">{{ trans('designs/stickereco.Translate42') }}</label>
								</div>
								<div id="row">
									<span class="glyphicon glyphicon-plus textIconBoxControll" aria-hidden="true"
									onclick="increaseTextStroke()"
									onmousedown="inter=setInterval(increaseTextStroke, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span>
								</div>
								<div id="row">
									{!! Form::text('strokeWidthTextbox', '0.01', array('id' => 'strokeWidthTextbox')) !!}
								</div>
								<div id="row">
									<span class="glyphicon glyphicon-minus textIconBoxControll" aria-hidden="true"
									onclick="decreaseTextStroke()"
									onmousedown="inter=setInterval(decreaseTextStroke, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span>
								</div>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 grid-breathingroom" style="text-align: center">
								<div id="row">

								</div>
								<div id="row">

								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="clipartControl"> <!-- Clipart tab -->
                        <div class="row"> <!-- Clipart View Box -->
                                <div class="col-xs-1 col-sm-1 col-md-0 col-lg-0">
                                </div>
    							<div class="col-xs-5 col-sm-5 col-md-2 col-lg-2">
                                    <div class="row">
                                        <label> {{ trans('designs/stickercolor.Translate74') }} </label>
                                    </div>
                                    <div class="row">
                                        <div style="width: 60px; height: 45px; background-image: url('/img/add_image2.png'); display:block; cursor: pointer; cursor: hand;" onclick="clipartBoxShow()"/></div>
                                    </div>
                                    <div class="row" style="padding-top: 10px;">
    								    <button id="deleteClipartButton" onclick="deleteClipart()">{{ trans('designs/stickercolor.Translate57') }}</button>
                                    </div>
    							</div>
                                <div class="col-xs-5 col-sm-5 col-md-2 col-lg-2" style="text-align: center">
    								<div class="row">
    									<label for="position"> {{ trans('designs/stickercolor.Translate36') }} </label>
    								</div>
    								<div class="row">
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-top textIconBox" aria-hidden="true"
    									onclick="moveObjectTop()"
    									onmousedown="inter=setInterval(moveObjectTop, 50);"
    									onmouseup="clearInterval(inter);"
    									onmouseout="clearInterval(inter);">
    									</span> </div>
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
    								</div>
    								<div class="row">
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-left textIconBox" aria-hidden="true"
    									onclick="moveObjectLeft()"
    									onmousedown="inter=setInterval(moveObjectLeft, 50);"
    									onmouseup="clearInterval(inter);"
    									onmouseout="clearInterval(inter);"></span></div>
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-right textIconBox" aria-hidden="true"
    									onclick="moveObjectRight()"
    									onmousedown="inter=setInterval(moveObjectRight, 50);"
    									onmouseup="clearInterval(inter);"
    									onmouseout="clearInterval(inter);"></span> </div>
    								</div>
    								<div class="row">
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-bottom textIconBox" aria-hidden="true"
    									onclick="moveObjectDown()"
    									onmousedown="inter=setInterval(moveObjectDown, 50);"
    									onmouseup="clearInterval(inter);"
    									onmouseout="clearInterval(inter);"></span> </div>
    									<div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
    								</div>
    							</div>
                                    <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1" style="text-align: center">
                                </div>
    							<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" style="text-align: center">
                                    <div class="row">
    									<label for="clipartScale">{{ trans('designs/stickercolor.Translate58') }}</label>
    								</div>
    								<div class="row">
    									<span class="glyphicon glyphicon-resize-full textIconBoxControll" aria-hidden="true"
    									onclick="increaseObjectScale()"
    									onmousedown="inter=setInterval(increaseObjectScale, 50);"
    									onmouseup="clearInterval(inter);"
    									onmouseout="clearInterval(inter);">
    										<span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="">  </span>
    									</span>
    								</div>
    								<div class="row">
    									{!! Form::text('clipartScaleTextbox', '1', array('id' => 'clipartScaleTextbox')) !!}
    								</div>
    								<div class="row">
    									<span class="glyphicon glyphicon-resize-small textIconBoxControll" aria-hidden="true"
    									onclick="decreaseObjectScale()"
    									onmousedown="inter=setInterval(decreaseObjectScale, 50);"
    									onmouseup="clearInterval(inter);"
    									onmouseout="clearInterval(inter);">
    										<span class="glyphicon glyphicon-minus" aria-hidden="true" onclick="">  </span>
    									</span>
    								</div>
    							</div>
    							<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" style="text-align: center">
                                    <div class="row">
    									<label for="clipartRotate">{{ trans('designs/stickercolor.Translate59') }}</label>
    								</div>
    								<div class="row">
    									<span class="glyphicon glyphicon-repeat textIconBoxControll">
    										<span class="glyphicon glyphicon-plus" aria-hidden="true"
    										onclick="increaseObjectRotation()"
    										onmousedown="inter=setInterval(increaseObjectRotation, 50);"
    										onmouseup="clearInterval(inter);"
    										onmouseout="clearInterval(inter);">	</span>
    									</span>
    								</div>
    								<div class="row">
    									{!! Form::text('clipartAngleTextbox', '0', array('id' => 'clipartAngleTextbox')) !!}
    								</div>
    								<div class="row">
    									<span class="glyphicon glyphicon-minus textIconBoxControll" id="flipped-icon"
    									onclick="decreaseObjectRotation()"
    									onmousedown="inter=setInterval(decreaseObjectRotation, 50);"
    									onmouseup="clearInterval(inter);"
    									onmouseout="clearInterval(inter);">
    										<span class="glyphicon glyphicon-repeat" aria-hidden="true" onclick="">  </span>
    									</span>
    								</div>
    							</div>
    						</div>


						<div class="row"><!-- Flip, color & delete -->
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									<label for="flipArt">{{ trans('designs/stickercolor.Translate54') }}</label>
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-warning-sign textIconBox" aria-hidden="true" onclick="flipXImg()"></span>
									<span class="glyphicon glyphicon-plane textIconBox" aria-hidden="true" onclick="flipYImg()"></span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									<label for="centerArt">{{ trans('designs/stickercolor.Translate55') }}</label>
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-eye-close textIconBox" aria-hidden="true" onclick="centerObjectH()"></span>
									<span class="glyphicon glyphicon-eye-open textIconBox" aria-hidden="true" onclick="centerObjectV()"></span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
                                    {!! Form::hidden('clipartType', '0', array('id'=>'clipartType')) !!}
                                    @if(false)
										<!-- Fix so this is disabled when the clipart can't change color -->
										<label for="clipcolor">{{ trans('designs/stickercolor.Translate56') }}</label>
										{!! Form::select('colorpickerClipart', array(
											'#000000' => 'Black',
											'#ffffff' => 'White',
											'#E6E6E6' => '10% Black',
											'#B1B1B1' => '40% Black',
											'#888887' => '60% Black',
											'#5C5C5B' => '80% Black',
											'#EBB5C3' => 'PANTONE 182 C',
											'#C8112E' => 'PANTONE 185 C',
											'#B01D2B' => 'PANTONE 1797 C',
											'#871630' => 'PANTONE 201 C',
											'#E6D5A8' => 'PANTONE 155 C',
											'#E9954A' => 'PANTONE 804 C',
											'#E64A00' => 'PANTONE Orange 021 C',
											'#EAEBBC' => 'PANTONE 607 C',
											'#EFED84' => 'PANTONE 100 C',
											'#EFE032' => 'PANTONE Yellow C',
											'#C9D8E7' => 'PANTONE 290 C',
											'#8ACBE5' => 'PANTONE 305 C',
											'#1A35A8' => 'PANTONE 286 C',
											'#0F2867' => 'PANTONE 281 C',
											'#549AA3' => 'PANTONE 320 C',
											'#EACDCF' => 'PANTONE 698 C',
											'#E8A3D0' => 'PANTONE 230 C',
											'#B50970' => 'PANTONE 226 C',
											'#D7CAE3' => 'PANTONE 263 C',
											'#9E70C1' => 'PANTONE 528 C',
											'#680E92' => 'PANTONE 527 C',
											'#BC8F70' => 'PANTONE 7515 C',
											'#9E520F' => 'PANTONE 471 C',
											'#B6DD8E' => 'PANTONE 358 C',
											'#A4D426' => 'PANTONE 375 C',
											'#61AE56' => 'PANTONE 354 C',
											'#4A7229' => 'PANTONE 364 C',
											'#cfb53b' => 'Gold-like',
											'#d7d8d8' => 'Silver-like'),
											'#000000',
											array('id' => 'clipcolor', 'onchange' => 'clipColor()'))
										!!}
									@endif
								</div>
							</div>
						</div>
						<div class="row grid-breathingroom"><!-- Size, rotation & layer -->
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div id="row">
									<label for="layerControl">{{ trans('designs/stickercolor.Translate60') }}</label>
								</div>
								<div id="row">
									<!--<span class="glyphicon glyphicon-fast-backward textIconBox" aria-hidden="true" onclick="sendToBack()"></span>-->
									<span class="glyphicon glyphicon-step-backward textIconBox" aria-hidden="true" onclick="moveDown()"></span>
									<span class="glyphicon glyphicon-step-forward textIconBox" aria-hidden="true" onclick="moveUp()"></span>
									<!--<span class="glyphicon glyphicon-fast-forward textIconBox" aria-hidden="true" onclick="bringToFront()"></span>-->
								</div>
							</div>
						</div>
						<div class="row grid-breathingroom"><!-- Upload controlls -->
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<span class="uploadControls">
									{!! Form::open(array('files' => true, 'id' => 'cilpartForm')) !!}
									<!-- {!! Form::label('title', 'Name your clipart') !!}
									{!! Form::text('title') !!} -->
									@if (Auth::user())
										<div class="form-group">
											{!! Form::file('image', array('id' => 'clipartFileSelect')) !!}
										</div>
										<div class="form-group">
											<input type="submit" value="{{ trans('designs/stickercolor.Translate53') }}" class="payBox">
										</div>
									@else
										<div class="form-group">
											{!! Form::file('image', array('disabled' => '', 'id' => 'clipartFileSelect')) !!}
										</div>
										<div class="form-group">
											<input type="submit" value="{{ trans('designs/stickercolor.Translate53') }}" class="payBox">
										</div>
									@endif
									{!! Form::close() !!}
								</span>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="cartControl"> <!-- Cart tab -->
					{!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
						@if(Auth::user() && Auth::user()->admin == 1)
							{!! Form::button('-test- Preview Sticker', array('onclick' => 'submitSticker()')) !!}
							{!! '<span id="stickerPreview">
								</span>' !!}
						@endif


						@if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
							<?php
								$sticker = Orderitem::find(Input::get('adminedit'));
							?>
							@if (Auth::user() && Auth::user()->admin == 1  )
								{!! Form::open(array('url'=>'admin/updatesticker')) !!}
								{!! Form::hidden('stickerid', $sticker->id) !!}
								{!! Form::hidden('userid', Auth::user()->id) !!}
								{!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
								{!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
								<label> {{ trans('designs/stickereco.Translate15') }}</label>
								@if($sticker->status == 1)
									{!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
									{!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}
									{!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
								@else

									<select id="Antall2" name="Antall2" onchange="setPrice()">
										<option value="1" >{{ trans('designs/stickereco.Translate16') }}</option>
										<option value="5">{{ trans('designs/stickereco.Translate17') }}</option>
										<option value="10">{{ trans('designs/stickereco.Translate18') }}</option>
										<option value="20">{{ trans('designs/stickereco.Translate19') }}</option>
										<option value="50">{{ trans('designs/stickereco.Translate20') }}</option>
										<option value="75">{{ trans('designs/stickereco.Translate21') }}</option>
										<option value="100">{{ trans('designs/stickereco.Translate22') }}</option>
										<option value="250">{{ trans('designs/stickereco.Translate23') }}</option>
										<option value="500">{{ trans('designs/stickereco.Translate24') }}</option>
										<option value="1000">{{ trans('designs/stickereco.Translate25') }}</option>
									</select>

								@endif
								{!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
								{!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
								{!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
								<br><br>
								<label for="thePrice" id="thePrice2"> {{ trans('designs/stickereco.Translate26') }}</label>
								<br><br>
								<input type="submit" value="{{ trans('designs/stickereco.Translate60') }}" class="buttonBox">
								{!! Form::close() !!}
							@endif
						@elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
							<?php
								$sticker = Orderitem::find(Input::get('edit'));
							?>
							@if (Auth::user() && $sticker->user_id == Auth::user()->id && ($sticker->status == 0 || $sticker->status == 1) )
								{!! Form::open(array('url'=>'store/updatecart')) !!}
								{!! Form::hidden('stickerid', $sticker->id) !!}
								{!! Form::hidden('userid', Auth::user()->id) !!}
								{!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
								{!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
								<label> {{ trans('designs/stickereco.Translate15') }}</label>
								@if($sticker->status == 1)
									{!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
									{!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}
									{!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
								@else
									<select id="Antall2" name="Antall2" onchange="setPrice()">
										<option value="1" >{{ trans('designs/stickereco.Translate16') }}</option>
										<option value="5">{{ trans('designs/stickereco.Translate17') }}</option>
										<option value="10">{{ trans('designs/stickereco.Translate18') }}</option>
										<option value="20">{{ trans('designs/stickereco.Translate19') }}</option>
										<option value="50">{{ trans('designs/stickereco.Translate20') }}</option>
										<option value="75">{{ trans('designs/stickereco.Translate21') }}</option>
										<option value="100">{{ trans('designs/stickereco.Translate22') }}</option>
										<option value="250">{{ trans('designs/stickereco.Translate23') }}</option>
										<option value="500">{{ trans('designs/stickereco.Translate24') }}</option>
										<option value="1000">{{ trans('designs/stickereco.Translate25') }}</option>
									</select>
								@endif
								{!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
								{!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
								{!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
								<br><br>
								<label for="thePrice" id="thePrice2"> {{ trans('designs/stickereco.Translate26') }}</label>
								<br><br>
								<input type="submit" value="{{ trans('designs/stickereco.Translate60') }}" class="buttonBox">
								{!! Form::close() !!}
							@endif
						@else
							<p>
							{!! Form::open(array('url'=>'store/addtocart')) !!}
							@if(Auth::check())
								{!! Form::hidden('userid', Auth::user()->id)!!}
							@endif
							{!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
							{!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
							{!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}

							<label> {{ trans('designs/stickereco.Translate15') }}</label>

							<select id="Antall2" name="Antall2" onchange="setPrice()">
								<option value="1" >{{ trans('designs/stickereco.Translate16') }}</option>
								<option value="5">{{ trans('designs/stickereco.Translate17') }}</option>
								<option value="10">{{ trans('designs/stickereco.Translate18') }}</option>
								<option value="20">{{ trans('designs/stickereco.Translate19') }}</option>
								<option value="50">{{ trans('designs/stickereco.Translate20') }}</option>
								<option value="75">{{ trans('designs/stickereco.Translate21') }}</option>
								<option value="100">{{ trans('designs/stickereco.Translate22') }}</option>
								<option value="250">{{ trans('designs/stickereco.Translate23') }}</option>
								<option value="500">{{ trans('designs/stickereco.Translate24') }}</option>
								<option value="1000">{{ trans('designs/stickereco.Translate25') }}</option>
							</select>

							<br/><br/>
							{!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
							{!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
							{!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
							<label for="thePrice" id="thePrice2"> {{ trans('designs/stickereco.Translate26') }}</label>

							<br/><br/>
							<input type="submit" value="{{ trans('designs/stickereco.Translate61') }}" class="buttonBox">
							{!! Form::close() !!}
						</p>
						@endif

					</div>
				</div>
			</div>
            {!! Form::hidden('stickerpriceholder', trans('designs/stickereco.Translate26'), array('id'=>'stickerpriceholder'))!!}
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<font color="red" size="5px">*</font>{{ trans('designs/stickereco.Translate62') }}
			</div>
		</div>
	</div>
</div> <!-- container -->


<div id='mask'></div>
<div id='popup'>

    <p> {{ trans('designs/stickereco.Translate63') }}	<p>

	<span id='popupSpin'></span>
</div>

<!-- BEGIN NEW CLIPART BOX -->
<?php
	include '../resources/views/designs/newclipartget.blade.php';
	$clipart = getAllClipart();
?>

<script>
	var clipart = <?php echo json_encode($clipart); ?>;
</script>

<div class="clipartBox" >
	<div class="clipartImageBox" >
	</div>
	<div class="clipartCategoryBox" >
		<div>
			<img onclick="clipartBoxHide();" src="../../img/remove.gif" style="float: right; padding: 10px;" />
		</div>
		<div class="clipartSearchBox">
			<input id="csb" class="clipartSearchBox" type="text" oninput="searchClipart()" placeholder="Search" />
		</div>

        <a onclick="clipartCategorySelect(2)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate42') }}</a>		<br />
		<a onclick="clipartCategorySelect(3)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate43') }}</a>		<br />
		<a onclick="clipartCategorySelect(4)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate44') }}</a>		<br />
		<a onclick="clipartCategorySelect(5)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate45') }}</a>		<br />
		<a onclick="clipartCategorySelect(6)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate46') }}</a>		<br />
		<a onclick="clipartCategorySelect(7)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate47') }}</a>		<br />
		<a onclick="clipartCategorySelect(8)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate48') }}</a>		<br />
		<a onclick="clipartCategorySelect(9)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate49') }}</a>		<br />
		<a onclick="clipartCategorySelect(10)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate50') }}</a>      <br />
        <a onclick="clipartCategorySelect(0)" class="clipartCategoryText">{{ trans('designs/stickercolor.Translate41') }}</a>       <br />

	</div>
	<br style="clear:both;"/>
</div>

{!! Html::script('js/clipart.new.js') !!}
<!-- END NEW CLIPART BOX -->
<!--canvas id="myCanvas" width="200" height="100"></canvas-->
{!! Html::style('css/uploadingSpin.css') !!}
<!-- {!! Html::style('css/stickerMeasureBar.css') !!} -->
{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/fabric.js-master/dist/fabric.min.js') !!}
{!! Html::script('js/jquery.fontselector.js') !!}
{!! Html::script('js/jquery.simplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/spin.min.js') !!}

{!! Html::script('js/outline.js') !!}
{!! Html::script('js/background.js') !!}
{!! Html::script('js/clipart.js') !!}
{!! Html::script('js/price.js') !!}
{!! Html::script('js/text.js') !!}
{!! Html::script('js/upload.js') !!}
{!! Html::script('js/store.js') !!}

<script>
  $('#fontSelect').fontSelector({
    'hide_fallbacks' : true,
    'initial' : 'Boogaloo, boogaloottf, Courier, monospace',
    'fonts' : [
        'Boogaloo, boogaloottf, Helvetica, sans-serif',
        'Crimson Text, crimsontextttf, Gadget, sans-serif',
        'England, englandttf, cursive',
        'Felipa, felipattf, Courier, monospace',
        'Gravitas One, gravitasonettf, sans-serif',
        'Great Vibes, greatvibesttf, monospace',
        'Hammersmith One, hammersmithonettf, sans-serif',
        'Henny Penny, hennepennyttf, Palatino,serif',
        'KaushanScript, kaushanscriptttf, sans-serif',
        'Leaguegothic, leaguegothicttf, serif',
        'Limelight, limelightttf, sans-serif',
        'Lobster Two, lobstertwottf, sans-serif',
        'Maidenorange, maidenoragettf, sans-serif',
        'Nunito, nunitottf, sans-serif',
        'Roboto, robotottf, sans-serif',
        'Roboto Condensed, robotocondensedttf, sans-serif',
        ]
});
</script>
<script>
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' &&
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' ||
                 d.type.toUpperCase() === 'FILE' ||
                 d.type.toUpperCase() === 'SEARCH' ||
                 d.type.toUpperCase() === 'EMAIL' ||
                 d.type.toUpperCase() === 'NUMBER' ||
                 d.type.toUpperCase() === 'DATE' )
             ) ||
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});
</script>
<script>
 //		id = Antall
 //		document.getElementById("robotocondensedttf").className = "";
 //		document.getElementById(object.fontFamily).className = "selected";

	var jsQuantity = document.getElementById('quantity').value;

	var element = document.getElementById('Antall');
    element.value = jsQuantity;
	var element2 = document.getElementById('Antall2');
    element2.value = jsQuantity;


</script>

<script>
	$(document).ready(function() {
		var bgColor = '#ffffff';
		setBackgroundColor(bgColor);
	});
</script>

@if (Input::has('id') && Orderitem::find(Input::get('id')))

  <?php
  $sticker = Orderitem::find(Input::get('id'));
    if(Input::has('id') && Input::has('dvos') && $sticker->user_id == Input::get('dvos')) {
      $jsonstring = $sticker->json;
    } else {
      $jsonstring = 'missmatch';
    }
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson(<?php echo $jsonstring ?>);
	  setPrice();
      });
  </script>
@elseif (Input::has('id') && Input::has('dvos'))
  <?php
    $jsonstring = 'missmatch';
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson(<?php echo $jsonstring ?>);
	  setPrice();
      });
  </script>
@endif

@if (Input::has('tmp') && Template::find(Input::get('tmp')))
  <?php
    $sticker = Template::find(Input::get('tmp'));
    $jsonstring = $sticker->json;
	$tmpType = $sticker->product_type;
  ?>
  @if($tmpType == 5)
	  <script>
		$(document).ready(function() {
		  loadfromJson(<?php echo $jsonstring ?>);
		  if(document.getElementById('width').value == 60){
			  document.getElementById('6026').checked = true;
		  }
		  setPrice();
		  });
	  </script>
	 @endif
@endif

@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
	<?php
	$sticker = Orderitem::find(Input::get('edit'));
	$jsonstring = $sticker->json;

	?>
	@if (Auth::user() && $sticker->user_id == Auth::user()->id)
		<script>
			$(document).ready(function() {
			  loadfromJson(<?php echo $jsonstring ?>);
			  setPrice();
			  });
		</script>

	@endif
@endif

@if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
	<?php
	$sticker = Orderitem::find(Input::get('adminedit'));
	$jsonstring = $sticker->json;

	?>
	@if (Auth::user() && Auth::user()->admin == 1)
		<script>
			$(document).ready(function() {
			  loadfromJson(<?php echo $jsonstring ?>);
			  setPrice();
			  });
		</script>

	@endif
@endif

<script>
			$(document).ready(function() {
			  setPrice();
			  });
	</script>

@stop
