@extends('layouts.main')

@section('title')
	{{ trans('store/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/index.Translate2') }}">
@stop

@section('head-keyword')
	<meta name="keywords" content="{{ trans('store/index.keywords') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop


@section('promo')
<!--
<div class="promo-wrapper">
	<div id="carousel1" class="carousel slide" data-ride="carousel" data-pg-collapsed>
			<div class="carousel-inner">
					<div class="item active imageitem">
							<img src="/img/banner1.png" alt="" />
							<div class="carousel-caption">
									<h1 style="-webkit-text-stroke: 1px black;">KLISTREMERKER</h1>
							</div>
					</div>
			</div>
	</div>
</div>
-->
@stop

@section('content')
<div class="content-wrapper">

	<div class="row row-heading" data-pg-collapsed>
			<div class="col-xs-12">
					<h3 class="text-center">{{ trans('templates/klisterfarge.Title')}}</h3>
			</div>
	</div>

	<div class="row no-margin" data-pg-collapsed>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.namesticker') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/sticker/simple.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('templates/klisterfarge.Translate14') }}</h3>
						<p> {{ trans('templates/klisterfarge.Translate15') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksticker') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/sticker/design.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('templates/klisterfarge.Translate16') }}</h3>
						<p> {{ trans('templates/klisterfarge.Translate17') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.tempsticker') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/sticker/templates.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('templates/klisterfarge.Translate18') }}</h3>
						<p> {{ trans('templates/klisterfarge.Translate19') }}</p>
					</div>
				</div>
			</a>
		</div>
	</div>

	<div class="row row-information" data-pg-collapsed>
			<div class="col-xs-12">
	      <h4>
	      {{ trans('templates/klisterfarge.Translate4') }}
	    </h4>

	      <h4>
	      {{ trans('templates/klisterfarge.Translate5') }}
	      </h4>

	      <h4>
	      {{ trans('templates/klisterfarge.Translate6') }}
	      </h4>
	      <h4>
	      {{ trans('templates/klisterfarge.Translate7') }}
	      </h4>

			</div>
	</div>
</div>
@stop
