@extends('layouts.main')

@section('title')
	{{ trans('store/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/index.Translate2') }}">
@stop

@section('head-keyword')
	<meta name="keywords" content="{{ trans('store/index.keywords') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('promo')
<div class="promo-wrapper">
	<div id="carousel1" class="carousel slide" data-ride="carousel" data-pg-collapsed>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
					<div class="item active imageitem">
							<img src="/img/views/category/toymerker/banner-1.png" alt="" />
							<div class="carousel-caption">
									<!--<h1>{{ trans('category/fabricmain.Title') }}</h1>-->
							</div>
					</div>
			</div>
	</div>
</div>

@stop

@section('content')
<div class="content-wrapper">

	<div class="row row-heading" data-pg-collapsed>
			<div class="col-xs-12">
					<h3 class="text-center">{{ trans('category/fabricmain.Title') }}</h3>
			</div>
	</div>

	<div class="row no-margin" data-pg-collapsed>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catironon') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/toymerker/strykfast.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('store/index.Translate9') }}</h3>
						<p> {{ trans('store/index.Translate10') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catironfree') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/toymerker/strykefrie.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('store/index.Translate13') }}</h3>
						<p> {{ trans('store/index.Translate14') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catirononcolor') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/toymerker/strykfastfarge.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('store/index.Translate7') }}</h3>
						<p>{{ trans('store/index.Translate8') }}</p>
					</div>
				</div>
			</a>
		</div>
	</div>

	<!--
	<div class="row row-heading" data-pg-collapsed>
			<div class="col-xs-12">
					<h3 class="text-center">BRUK*</h3>
			</div>
	</div>
	<div class="row no-margin" data-pg-collapsed>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.nameironfree') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/stockkids.jpg" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>Enkel navnelapp*</h3>
						<p>Merk dine plagg raskt og enkelt!</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.nameiron') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/stockkids.jpg" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>Enkel strykemerke*</h3>
						<p>Her kan du enkelt lage dine strykemerker!</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironc') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/stockkids.jpg" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>Fargerike strykemerker*</h3>
						<p>Her setter kun din kreativitet grenser! Lag fargerike strykemerker!</p>
					</div>
				</div>
			</a>
		</div>
	</div> -->

</div>

@stop
