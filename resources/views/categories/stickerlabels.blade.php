@extends('layouts.main')

@section('title')
	{{ trans('store/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/index.Translate2') }}">
@stop

@section('head-keyword')
	<meta name="keywords" content="{{ trans('store/index.keywords') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('promo')
<div class="promo-wrapper">
	<div id="carousel1" class="carousel slide" data-ride="carousel" data-pg-collapsed>
			<!-- Indicators -->
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
					<div class="item active imageitem">
							<img src="/img/views/category/klistremerker/banner-1.png" alt="" />
							<div class="carousel-caption">
									<!--<h1>{{ trans('category/stickermain.Title') }}</h1>-->
							</div>
					</div>
			</div>
			<!-- Controls -->
	</div>
</div>

@stop

@section('content')
<div class="content-wrapper">

	<div class="row row-heading" data-pg-collapsed>
			<div class="col-xs-12">
					<h3 class="text-center">{{ trans('category/stickermain.Title') }}</h3>
			</div>
	</div>

	<div class="row no-margin" data-pg-collapsed>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catsticker') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/klistremerker/klistre-i-farge.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('store/index.Translate3') }}</h3>
						<p> {{ trans('store/index.Translate4') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catcheapsticker') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/klistremerker/sort-hvit.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('store/index.Translate5') }}</h3>
						<p> {{ trans('store/index.Translate6') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catreflection') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/klistremerker/refleks-merke.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('store/index.Translate17') }}</h3>
						<p>{{ trans('store/index.Translate18') }}</p>
					</div>
				</div>
			</a>
		</div>
	</div>

	<div class="row no-margin" data-pg-collapsed>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catgold') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/klistremerker/gull-etikett.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('templates/goldsilver.Translate13') }}</h3>
						<p> {{ trans('templates/goldsilver.Translate14') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catsilver') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/klistremerker/silver-etikett.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('templates/goldsilver.Translate15') }}</h3>
						<p> {{ trans('templates/goldsilver.Translate16') }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.cattransparent') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/views/category/klistremerker/transparent-etikett.png" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>{{ trans('store/index.Translate15') }}</h3>
						<p>{{ trans('store/index.Translate16') }}</p>
					</div>
				</div>
			</a>
		</div>
	</div>

<!--
	<div class="row row-heading" data-pg-collapsed>
			<div class="col-xs-12">
					<h3 class="text-center">BRUK*</h3>
			</div>
	</div>
	<div class="row no-margin" data-pg-collapsed>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catcontroll') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/stockkids.jpg" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>Kontrollmerker*</h3>
						<p>Kontrollmerker hurra hurra!</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catbeer') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/stockkids.jpg" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>Øletiketter*</h3>
						<p>Her kan du enkelt lage dine egne Øletiketter!</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4 box-padding">
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.catforever') }}">
				<div class="row no-margin">
					<div class="col-sm-12 col-xs-4 no-padding">
						<img src="/img/stockkids.jpg" width="100%" />
					</div>
					<div class="col-sm-12 col-xs-8 textbox">
						<h3>Foreverliving merking*</h3>
						<p>Trenger du merker til foreverliving?!</p>
					</div>
				</div>
			</a>
		</div>
	</div> -->

</div>
@stop
