<!DOCTYPE html>

{!! Html::style('css/storeTest.css') !!}

<div id="topLeft" style="float: left; width: 200px;">
	<div id="markinfo">
		{!! Html::image('img/markmaster_ny_logo_2.png', 'MM Logo', array('width'=>'100%'))!!}
		</br></br>
		<div style="padding-left: 30px;">
			{!! 'Markmaster AS' !!}<br>
			{!! 'Legevegen 16' !!}<br>
			{!! '5542 Karmsund' !!}<br>
		</div>
	</div>

	<br>
	<br>

	<?php

	$customer = markmaster\Models\User::find($customernr);
	$order = Order::find($ordernr);
	$stickers = Orderitem::where('order_id', '=', $ordernr)->paginate(200);

	?>
	<div id="customerinfo">
		<div style="padding-left: 30px;">
			<b> {{ trans('emails/orderconfirmation.Translate1') }} </b> <br>
			@if($order->orgname)
				{!! $order->orgname !!} <br>
			 @endif
			{!! $order->firstname . ' ' . $order->lastname !!} <br>
			{!! $order->address !!} <br>
			@if($order->address2)
			{!! $order->address2 !!} <br>
			@endif
			{!! $order->zipcode . ' ' !!}
			{!! $order->city !!}<br>
		</div>
	</div>
</div>


<div id="orderinfo" style="float: left; width: 400px; padding-left: 160px; text-align: center;">
			<h1> {{ trans('emails/orderconfirmation.Translate2') }} </h1> <br>
			<div style="float: left;">
				<b> {{ trans('emails/orderconfirmation.Translate3') }} </b><br>
				{!! $order->order_date !!}
			</div>
			<div style="float: left; padding-left: 40px;">
				<b> {{ trans('emails/orderconfirmation.Translate4') }} </b><br>
				{!! $order->user_id!!}
			</div>
			<div style="float: left; padding-left: 40px;">
				<b> {{ trans('emails/orderconfirmation.Translate5') }} </b><br>
				{!! $order->id !!}
			</div>
			<br>
			<br>
		<div id="customerinfo" style="text-align: left;">
			<b> {{ trans('emails/orderconfirmation.Translate6') }} </b> <br>
			@if($order->billingorgname)
				{!! $order->billingorgname !!} <br>
			 @endif
			{!! $order->billingfirstname . ' ' . $order->billinglastname !!} <br>
			{!! $order->billingaddress !!} <br>
			@if($order->billingaddress2)
			{!! $order->billingaddress2 !!} <br>
			@endif
			{!! $order->billingzipcode . ' ' !!}
			{!! $order->city !!}<br>
		</div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div id="stickerinfo" style="float: left; text-align: center; padding-left: 30px;">
	 <table border="1" id="tableStyle">
                <tr>
                    <th>{{ trans('emails/orderconfirmation.Translate7') }}</th>
                    <th>{{ trans('emails/orderconfirmation.Translate8') }}</th>
                    <th>{{ trans('emails/orderconfirmation.Translate9') }}</th>
                    <th>{{ trans('emails/orderconfirmation.Translate10') }}</th>
					<th>{{ trans('emails/orderconfirmation.Translate11') }}</th>
                </tr>

                @foreach($stickers as $sticker)
                <tr>
                    <td>

						{!! Html::image($sticker->thumbnail, $sticker->stickertype, array('class'=>'thumbnailpic')) !!}

                    </td>
                    <td>
						@if($sticker->stickertype == 1)
							{{ trans('emails/orderconfirmation.Translate12') }}
						@elseif($sticker->stickertype == 2)
							{{ trans('emails/orderconfirmation.Translate13') }}
						@elseif($sticker->stickertype == 3)
							{{ trans('emails/orderconfirmation.Translate14') }}
						@elseif($sticker->stickertype == 4)
							{{ trans('emails/orderconfirmation.Translate15') }}
						@elseif($sticker->stickertype == 5)
							{{ trans('emails/orderconfirmation.Translate16') }}
						@elseif($sticker->stickertype == 6)
							{{ trans('emails/orderconfirmation.Translate17') }}
						@elseif($sticker->stickertype == 7)
							{{ trans('emails/orderconfirmation.Translate18') }}
						@elseif($sticker->stickertype == 8)
							{{ trans('emails/orderconfirmation.Translate22') }}
						@elseif($sticker->stickertype == 9)
							{{ trans('emails/orderconfirmation.Translate23') }}
						@endif
					</td>
					<td>
						{!! '' . $sticker->width . ' x ' . $sticker->height !!}
					</td>
                    <td>
                       {!! $sticker->quantity !!}
											 @if($sticker->quantity != $sticker->quantexp)
											 	(x2)
											 @endif
                    </td>
                    <td>
                       @if($order->currency == 0)
							{!! number_format ($sticker->price , 2 ) . ' nok' !!}
						@elseif($order->currency == 1)
							{!! '$ ' . number_format ($sticker->price , 2 ) !!}
						@elseif($order->currency == 2)
							{!! number_format ($sticker->price , 2 ) . ' sek' !!}
						@elseif($order->currency == 3)
							{!! number_format ($sticker->price , 2 ) . ' dkk' !!}
						@elseif($order->currency == 4)
							{!! '€ ' . number_format ($order->amount , 2 ) . ' ' !!}
						@endif
					</td>
                </tr>
                @endforeach
            </table>
</div>

<div id="sum" style="width: 250px; height: 100px; padding-left: 30px; padding-top: 200px;">
	<b>{{ trans('emails/orderconfirmation.Translate19') }} </b>
	<div style="float: right;">
		@if($order->currency == 0)
			{!! number_format ($order->amount-$order->fee , 2 ) . ' nok' !!}
		@elseif($order->currency == 1)
			{!! '$ ' . number_format ($order->amount-$order->fee , 2 ) !!}
		@elseif($order->currency == 2)
			{!! number_format ($order->amount-$order->fee , 2 ) . ' sek' !!}
		@elseif($order->currency == 3)
			{!! number_format ($order->amount-$order->fee , 2 ) . ' dkk' !!}
		@elseif($order->currency == 4)
			{!! '€ ' . number_format ($order->amount-$order->fee , 2 ) !!}
		@endif

	</div><br>
	<b>{{ trans('emails/orderconfirmation.Translate20') }} </b>
	<div style="float: right;">
		@if($order->currency == 0)
			{!! number_format ($order->fee , 2 ) . ' nok' !!}
		@elseif($order->currency == 1)
			{!! '$ ' . number_format ($order->fee , 2 ) !!}
		@elseif($order->currency == 2)
			{!! number_format ($order->fee , 2 ) . ' sek' !!}
		@elseif($order->currency == 3)
			{!! number_format ($order->fee , 2 ) . ' dkk' !!}
		@elseif($order->currency == 4)
			{!! '€ ' . number_format ($order->fee , 2 ) !!}
		@endif
	</div> <br>
	<hr>
	<b>{{ trans('emails/orderconfirmation.Translate21') }} </b>
	<div style="float: right;">
		@if($order->currency == 0)
			{!! number_format ($order->amount , 2 ) . ' nok' !!}
		@elseif($order->currency == 1)
			{!! '$ ' . number_format ($order->amount , 2 ) !!}
		@elseif($order->currency == 2)
			{!! number_format ($order->amount , 2 ) . ' sek' !!}
		@elseif($order->currency == 3)
			{!! number_format ($order->amount , 2 ) . ' dkk' !!}
		@elseif($order->currency == 4)
			{!! '€ ' . number_format ($order->amount , 2 ) !!}
		@endif	</div>
</div>
