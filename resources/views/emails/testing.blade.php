<!DOCTYPE html>

{!! Html::style('css/storeTest.css') !!}

<div id="topLeft" style="float: left; width: 200px;">
	<div id="markinfo">
		{!! Html::image('img/markmaster_ny_logo_2.png', 'MM Logo', array('width'=>'100%'))!!}
		</br></br>
		<div style="padding-left: 30px;">
			{!! 'Markmaster AS' !!}<br>
			{!! 'Legevegen 16' !!}<br>
			{!! '5542 Karmsund' !!}<br>
		</div>
	</div>

	<br>
	<br>

	<?php

	$customer = markmaster\Models\User::find($customernr);
	$order = Order::find($ordernr);
	$stickers = Orderitem::where('order_id', '=', $order->id)->paginate(200);

	?>

	<div id="customerinfo">
		<div style="padding-left: 30px;">
			<b> {!! 'Leveringsadresse' !!} </b> <br>
			{!! $customer->firstname . ' ' . $customer->lastname !!} <br>
			{!! $customer->address !!} <br>
			@if($customer->address2)
			{!! $customer->address2 !!} <br>
			@endif
			{!! $customer->zipcode . ' ' !!}
			{!! $customer->city !!}<br>
		</div>
	</div>
</div>


<div id="orderinfo" style="float: left; width: 400px; padding-left: 160px; text-align: center;">
			<h1> {!! 'Ordrebekreftelse' !!} </h1> <br>
			<div style="float: left;">
				<b> {!! 'Dato' !!} </b><br>
				{!! $order->order_date !!}
			</div>
			<div style="float: left; padding-left: 40px;">
				<b> {!! 'Kundenr.' !!} </b><br>
				{!! $order->user_id!!}
			</div>
			<div style="float: left; padding-left: 40px;">
				<b> {!! 'Ordrenr.' !!} </b><br>
				{!! $order->id !!}
			</div>

		<div id="customerinfo" style="text-align: left;">
			<b> {!! 'Fakturaadresse' !!} </b> <br>
			{!! $order->billingfirstname . ' ' . $order->billinglastname !!} <br>
			{!! $order->billingaddress !!} <br>
			@if($order->billingaddress2)
			{!! $order->billingaddress2 !!} <br>
			@endif
			{!! $order->billingzipcode . ' ' !!}
			{!! $order->city !!}<br>
		</div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div id="stickerinfo" style="float: left; text-align: center; padding-left: 30px;">
	 <table border="1" id="tableStyle">
                <tr>
                    <th>Etikett</th>
                    <th>Type</th>
                    <th>St�rrelse mm</th>
                    <th>Antall</th>
					<th>Pris inkl. mva </th>
                </tr>

                @foreach($stickers as $sticker)
                <tr>
                    <td>

						{!! Html::image($sticker->thumbnail, $sticker->stickertype, array('class'=>'thumbnailpic')) !!}

                    </td>
                    <td>
						@if($sticker->stickertype == 1)
							{!! 'Klisteretikett farge, med laminat' !!}
						@elseif($sticker->stickertype == 2)
							{!! 'Strykfastetikett farge' !!}
						@elseif($sticker->stickertype == 3)
							{!! 'Klistreetikett i gullfarget vinyl' !!}
						@elseif($sticker->stickertype == 4)
							{!! 'Klistreetikett i s�lvfarget vinyl' !!}
						@elseif($sticker->stickertype == 5)
							{!! 'Klistreetikett i transparent vinyl' !!}
						@elseif($sticker->stickertype == 6)
							{!! 'Klistreetikett i sort/hvit' !!}
						@elseif($sticker->stickertype == 7)
							{!! 'Strykfastetikett' !!}
						@endif
					</td>
					<td>
						{!! '' . $sticker->width . ' x ' . $sticker->height !!}
					</td>
                    <td>
                       {!! $sticker->quantity !!}
                    </td>
                    <td>
                        {!! $sticker->price !!}
					</td>
                </tr>
                @endforeach
            </table>
</div>

<div id="sum" style="width: 250px; height: 100px; padding-left: 30px; padding-top: 200px;">
	<b>{!! 'Sum. inkl. mva.' !!} </b>
	<div style="float: right;">
		{!! $order->amount-29 . ',-'!!}
	</div><br>
	<b>{!! 'Porto/eksp.' !!} </b>
	<div style="float: right;">
		{!! '29,-' !!}
	</div> <br>
	<hr>
	<b>{!! 'Total sum.' !!} </b>
	<div style="float: right;">
		{!! $order->amount . ',-'!!}
	</div>
</div>
