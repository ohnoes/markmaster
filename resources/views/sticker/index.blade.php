@extends('layouts.main')

@section('title')
	{{ trans('sticker/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('sticker/index.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/stickers/">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<div class="container ">

		<h1>{{ trans('sticker/index.Translate3') }}</h1><hr>

		<div class="table-responsive">
		<table class="table table-hover table-bordered ">

			<tr>
				<th> {{ trans('sticker/index.Translate4') }}</th>
				<th> {{ trans('sticker/index.Translate5') }}</th>
				<th> {{ trans('sticker/index.Translate15') }}</th>
				<th> {{ trans('sticker/index.Translate6') }} </th>
			</tr>

			@foreach($stickers as $sticker)
			<tr>
						@if($sticker->stickertype == 1)
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksticker') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merksticker') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
								</a>
							</td>
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksticker') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
							</td>
					 @elseif($sticker->stickertype == 2)
						 <td>
							 <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironc') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
								 {!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkironc') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
							 </a>
						 </td>
						 <td>
							 <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironc') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
								 {!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
						 </td>
					 @elseif($sticker->stickertype == 3)
						 <td>
							 <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkgold') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
								 {!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkgold') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
							 </a>
						 </td>
						 <td>
							 <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkgold') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
								 {!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
						 </td>
					 @elseif($sticker->stickertype == 4)
							 <td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksolv') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merksolv') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
								</a>
							</td>
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksolv') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
							</td>
					 @elseif($sticker->stickertype == 5)
							 <td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merktrans') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merktrans') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
								</a>
							</td>
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merktrans') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
							</td>
					 @elseif($sticker->stickertype == 6)
							 <td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkcheap') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkcheap') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
								</a>
							</td>
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkcheap') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
							</td>
					 @elseif($sticker->stickertype == 7)
							 <td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkiron') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkiron') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
								</a>
							</td>
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkiron') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
							</td>
					 @elseif($sticker->stickertype == 8)
							 <td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkreflection') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkreflection') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
								</a>
							</td>
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkreflection') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
							</td>
					 @elseif($sticker->stickertype == 9)
							 <td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironfree') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkironfree') . '?id=' . $sticker->id . '&dvos=' . Auth::user()->id !!}
								</a>
							</td>
							<td>
								<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironfree') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
									{!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </a>
							</td>
					 @endif
				<td>
				{{ $sticker->width}} x {{ $sticker->height}}
				</td>

				<td>
					@if($sticker->stickertype == 1)
							{{ trans('sticker/index.Translate7') }}
						@elseif($sticker->stickertype == 2)
							{{ trans('sticker/index.Translate8') }}
						@elseif($sticker->stickertype == 3)
							{{ trans('sticker/index.Translate9') }}
						@elseif($sticker->stickertype == 4)
							{{ trans('sticker/index.Translate10') }}
						@elseif($sticker->stickertype == 5)
							{{ trans('sticker/index.Translate11') }}
						@elseif($sticker->stickertype == 6)
							{{ trans('sticker/index.Translate12') }}
						@elseif($sticker->stickertype == 7)
							{{ trans('sticker/index.Translate13') }}
						@elseif($sticker->stickertype == 8)
							{{ trans('sticker/index.Translate14') }}
						@elseif($sticker->stickertype == 9)
							{{ trans('sticker/index.Translate16') }}
						@endif
				</td>
			</tr>
			@endforeach

		</table>
		</div>


	<div id="pagination" align="center">
		{!! $stickers->render() !!}
	</div>
	</div><!-- end orders -->
@stop
