
<!DOCTYPE html>

<head>
    <title>{{ trans('invoice/invoicegen.Translate1') }}</title>
    <meta charset="utf-8" />
</head>

{!! Html::style('css/storeTest.css') !!}
<div id="container" style="position: absolute; width:900px;">
	<div id="topInfo" style="height:300px;">
		<div id="header" style="padding-left: 350px;">
			<h1> {{ trans('invoice/invoicegen.Translate1') }} {!! $faktura->id !!}</h1>
		</div>

		<div id="topLeft" style="position:absolute; left:0px; width: 400px;">
			<div id="markinfo" style="width: 200px;">
				{!! Html::image('img/markmaster_ny_logo_2.png', 'MM Logo', array('width'=>'100%'))!!}
			</div>

			<br>
			<br>

			<div id="customerinfo">
				<div style="padding-left: 30px;">
					@if($faktura->billingorgname)
					{!!$faktura->billingorgname !!} <br>
					@endif
					{!! $faktura->billingfirstname . ' ' . $faktura->billinglastname !!} <br>
					{!! $faktura->billingaddress !!} <br>
					@if($faktura->billingaddress2)
					{!! $faktura->billingaddress2 !!} <br>
					@endif
					{!! $faktura->billingzipcode . ' ' !!}
					{!! $faktura->billingcity !!}<br> <br>
				</div>
			</div>
		</div>


		<div id="topRight" style="position:absolute; right:0px; width: 400px; text-align: center;">
			<div id="markinfo">
				{!! 'MARKMASTER AS' !!} <br>
				{!! 'LEGEVEGEN 16' !!} <br>
				{!! '5542 KARMSUND' !!}<br>
				{!! 'Org.nr: NO 979 555 458 MVA' !!} <br>
			</div>
			<div id="fakturainfo">
				<b> {{ trans('invoice/invoicegen.Translate2') }} {!! $faktura->id !!}</b><br>
				{{ trans('invoice/invoicegen.Translate3') }} {!! $faktura->faktura_date !!} <br>
				{{ trans('invoice/invoicegen.Translate28') }} {!! $faktura->user_id !!} <br>
			</div>

		</div>
	</div>
	<!--br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br-->
	<div id="bodyInfo" style="">
		<div id="stickerinfo" style="text-align: center; padding-left: 30px; padding-top: 40px;">
			 <table border="1" id="tableStyle">
						<tr>
							<th>{{ trans('invoice/invoicegen.Translate4') }}</th>
							<th>{{ trans('invoice/invoicegen.Translate5') }}</th>
							<th>{{ trans('invoice/invoicegen.Translate6') }}</th>
							<th>{{ trans('invoice/invoicegen.Translate7') }}</th>
							<th>{{ trans('invoice/invoicegen.Translate8') }}</th>
							<th>{{ trans('invoice/invoicegen.Translate9') }} </th>
						</tr>

						@foreach($stickers as $sticker)
						<tr>
							<td>
								{!! $sticker->order_id!!} <br>
								{!! $sticker->id!!}
							</td>
							<td>
								@if($sticker->stickertype == 1)
									{!! '1' !!} <br>
									{{ trans('invoice/invoicegen.Translate10') }}
								@elseif($sticker->stickertype == 2)
									{!! '2' !!} <br>
									{{ trans('invoice/invoicegen.Translate11') }}
								@elseif($sticker->stickertype == 3)
									{!! '3' !!} <br>
									{{ trans('invoice/invoicegen.Translate12') }}
								@elseif($sticker->stickertype == 4)
									{!! '4' !!} <br>
									{{ trans('invoice/invoicegen.Translate13') }}
								@elseif($sticker->stickertype == 5)
									{!! '5' !!} <br>
									{{ trans('invoice/invoicegen.Translate14') }}
								@elseif($sticker->stickertype == 6)
									{!! '6' !!} <br>
									{{ trans('invoice/invoicegen.Translate15') }}
								@elseif($sticker->stickertype == 7)
									{!! '7' !!} <br>
									{{ trans('invoice/invoicegen.Translate16') }}
								@elseif($sticker->stickertype == 8)
									{!! '8' !!} <br>
									{{ trans('invoice/invoicegen.Translate17') }}
								@elseif($sticker->stickertype == 9)
									{!! '9' !!} <br>
									{{ trans('invoice/invoicegen.Translate25') }}
								@endif
							</td>

							<td>
								<?php
								$imgsrc = $sticker->thumbnail;
								$imgsrc = str_replace('http://79.161.166.153', '', $imgsrc);
								$imgsrc = str_replace('http://www.markmaster.no', '', $imgsrc);
								$imgsrc = str_replace('http://markmaster.no', '', $imgsrc);
								$imgsrc = str_replace('http://192.168.1.50', '', $imgsrc);
								echo '<img src="' . $imgsrc . '" class="thumbnailpic" alt="' . $sticker->thumbnail . '1">';
								?>
								<!--{!! Html::image($sticker->thumbnail, $sticker->stickertype, array('class'=>'thumbnailpic')) !!}-->
							</td>

							<td>
								{!! '' . $sticker->width . ' x ' . $sticker->height !!}
							</td>

							<td>
								{!! $sticker->quantity !!}
                @if($sticker->quantity != $sticker->quantexp)
                 (x2)
                @endif
							</td>

							<td>
								@if($faktura->currency == 0)
									{!! round($sticker->price , 2, PHP_ROUND_HALF_DOWN) . ' nok' !!}
								@elseif($faktura->currency == 1)
									{!! '$ ' . round($sticker->price , 2, PHP_ROUND_HALF_DOWN) !!}
								@elseif($faktura->currency == 2)
									{!! round($sticker->price , 2, PHP_ROUND_HALF_DOWN) . ' sek' !!}
								@elseif($faktura->currency == 3)
									{!! round($sticker->price , 2, PHP_ROUND_HALF_DOWN) . ' dkk' !!}
								@elseif($faktura->currency == 4)
									{!! '€ ' . round($sticker->price , 2, PHP_ROUND_HALF_DOWN) !!}
								@endif
							</td>
						</tr>
						@endforeach
						<tr>
							<td>

							</td>
							<td>
								{{ trans('invoice/invoicegen.Translate18') }}
							</td>

							<td>

							</td>

							<td>

							</td>

							<td>

							</td>

							<td>
								@if($faktura->currency == 0)
									{!! round($faktura->fee , 2, PHP_ROUND_HALF_DOWN) . ' nok' !!}
								@elseif($faktura->currency == 1)
									{!! '$ ' . round($faktura->fee , 2, PHP_ROUND_HALF_DOWN) !!}
								@elseif($faktura->currency == 2)
									{!! round($faktura->fee , 2, PHP_ROUND_HALF_DOWN) . ' sek' !!}
								@elseif($faktura->currency == 3)
									{!! round($faktura->fee , 2, PHP_ROUND_HALF_DOWN) . ' dkk' !!}
								@elseif($faktura->currency == 4)
									{!! '€ ' . round($faktura->fee , 2, PHP_ROUND_HALF_DOWN) !!}
								@endif
							</td>

						</tr>
					</table>
		</div>

		<!--br><br><br><br><br><br><br><br><br><br><br><br-->

		<div id="header" style="text-align: center; ">
			<h1> {{ trans('invoice/invoicegen.Translate19') }} </h1>
		</div>

		<div id="mvaInfo">
			 <table border="1" id="tableStyle">
				<tr>
					<th>{{ trans('invoice/invoicegen.Translate20') }}</th>
					<th>{{ trans('invoice/invoicegen.Translate21') }}</th>
					<th>{{ trans('invoice/invoicegen.Translate22') }}</th>
					<th>{{ trans('invoice/invoicegen.Translate23') }} </th>
					<th>{{ trans('invoice/invoicegen.Translate24') }}</th>
				</tr>
				<tr>
					<td> {!! round($faktura->priceeksmva, 2, PHP_ROUND_HALF_DOWN) !!}</td>
					<td> {!! round($faktura->priceeksmva, 2, PHP_ROUND_HALF_DOWN) !!}</td>
					<td> {!! round($faktura->priceinkmva - $faktura->priceeksmva, 2, PHP_ROUND_HALF_DOWN) !!}</td>
					<td> {!! round($faktura->priceinkmva, 2, PHP_ROUND_HALF_DOWN) !!}</td>
					<td> {!!'0,00'!!}</td>
				</tr>
			</table>
		</div>
	</div>

</div>
