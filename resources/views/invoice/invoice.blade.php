{{$lang = app()->getLocale() }}

<?php

	$fileurl = '/invoices/' . $faktura->order_id . '.pdf';

	//--old
	// $filename = '/var/www/html/markmaster/invoices/' . $faktura->order_id . '.pdf';
	// $filename = '/var/www/html/markmaster/invoices/' . $faktura->order_id . '.pdf';
	// $command = 'xvfb-run --server-args="-screen 0, 1024x768x24" wkhtmltopdf "http://127.0.0.1/markmaster/public/invoice/invoicegen/' . $faktura->order_id . '" ' . $filename;
	//--old

	// save html page
	// $htmlFile = '/var/www/html/markmaster/public/invoices/' . $faktura->order_id . '.html';
	// $command = 'wget http://127.0.0.1/invoice/invoicegen/' . $faktura->order_id . ' -O ' . $htmlFile;
	// exec($command, $output, $return_var);

	$server_path = config('constants.server_path');

	//--new
	$filename = '/var/www/html/markmaster/public/invoices/' . $faktura->order_id . '.pdf';
	$command = 'xvfb-run --server-args="-screen 0, 1024x768x24" wkhtmltopdf  "' . $server_path . '/' . $lang . '/invoice/invoicegen/' . $faktura->order_id . '" ' . $filename;
	// $command = 'xvfb-run --server-args="-screen 0, 1024x768x24" wkhtmltopdf ' . $htmlFile . ' ' . $filename;
	exec($command, $output, $return_var);
	//--new

	// header("Content-Length: " . filesize ( $filename ) );
                // header("Content-type: application/octet-stream");
                // header("Content-disposition: attachment; filename=".$filename);
                // header('Expires: 0');
                // header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                // ob_clean();
                // flush();

                // readfile($filename);

?>

<object height="950" data="<?php echo $fileurl ?>" type="application/pdf" width="860">
	<p>It appears you don't have a PDF plugin for this browser.
		No biggie... you can <a href="<?php echo $fileurl ?>">click here to
		download the PDF file.</a>
	</p>

</object>
