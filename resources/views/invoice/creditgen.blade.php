<!DOCTYPE html>

<head>
    <title>{{ trans('invoice/creditgen.Translate1') }}</title>
    <meta charset="utf-8" />
</head>

{!! Html::style('css/storeTest.css') !!}
<div id="container" style="position: absolute; width:900px;">
	<div id="topInfo" style="height:300px;">
		<div id="header" style="padding-left: 350px;">
			<h1> {{ trans('invoice/creditgen.Translate1') }} </h1>
		</div>

		<div id="topLeft" style="position:absolute; left:0px; width: 400px;">
			<div id="markinfo" style="width: 200px;">
				{!! Html::image('img/markmaster_ny_logo_2.png', 'MM Logo', array('width'=>'100%'))!!}
			</div>

			<br>
			<br>

			<div id="customerinfo">
				<div style="padding-left: 30px;">
					@if($faktura->billingorgname)
					{!!$faktura->billingorgname !!} <br>
					@endif
					{!! $faktura->billingfirstname . ' ' . $faktura->billinglastname !!} <br>
					{!! $faktura->billingaddress !!} <br>
					@if($faktura->billingaddress2)
					{!! $faktura->billingaddress2 !!} <br>
					@endif
					{!! $faktura->billingzipcode . ' ' !!}
					{!! $faktura->billingcity !!}<br> <br>
				</div>
			</div>
		</div>


		<div id="topRight" style="position:absolute; right:0px; width: 400px; text-align: center;">
			<div id="markinfo">
				{!! 'MARKMASTER AS' !!} <br>
				{!! 'LEGEVEGEN 16' !!} <br>
				{!! '5542 KARMSUND' !!}<br>
				{!! 'Org.nr: NO 979 555 458 MVA' !!} <br>
			</div>
			<div id="fakturainfo">
				<b> {{ trans('invoice/creditgen.Translate2') }} {!! $faktura->id !!}</b><br>
				{{ trans('invoice/creditgen.Translate3') }} {!! $faktura->faktura_date !!} <br>
				{{ trans('invoice/creditgen.Translate28') }} {!! $faktura->user_id !!} <br>
			</div>

		</div>
	</div>
	<!--br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br-->
	<div id="bodyInfo" style="">
		<div id="stickerinfo" style="text-align: center; padding-left: 30px; padding-top: 40px;">

			<p style="text-align: left;"><b>{!!$faktura->note!!}</b> </p><br><br>

		<div id="mvaInfo">
			 <table border="1" id="tableStyle">
				<tr>
					<th>{{ trans('invoice/creditgen.Translate20') }}</th>
					<th>{{ trans('invoice/creditgen.Translate21') }}</th>
					<th>{{ trans('invoice/creditgen.Translate22') }}</th>
					<th>{{ trans('invoice/creditgen.Translate23') }} </th>

				</tr>
				<tr>
					<td> {!!$faktura->priceeksmva!!}</td>
					<td> {!!$faktura->priceeksmva!!}</td>
					<td> {!!$faktura->priceinkmva - $faktura->priceeksmva!!}</td>
					<td> {!!$faktura->priceinkmva!!}</td>

				</tr>
			</table>
		</div>

		<!--br><br><br><br><br><br><br><br><br><br><br><br-->

		<div id="header" style="text-align: center; ">
			<h1> Referanse av Originalfaktura, fakturanr. {!!$faktura->faktura_id!!} </h1>
		</div>


				<table border="1" id="tableStyle">
						<tr>
							<th>{{ trans('invoice/creditgen.Translate4') }}</th>
							<th>{{ trans('invoice/creditgen.Translate5') }}</th>
							<th>{{ trans('invoice/creditgen.Translate6') }}</th>
							<th>{{ trans('invoice/creditgen.Translate7') }}</th>
							<th>{{ trans('invoice/creditgen.Translate8') }}</th>
							<th>{{ trans('invoice/creditgen.Translate9') }} </th>
						</tr>

						@foreach($stickers as $sticker)
						<tr>
							<td>
								{!! $sticker->order_id!!} <br>
								{!! $sticker->id!!}
							</td>
							<td>
								@if($sticker->stickertype == 1)
									{!! '1' !!} <br>
									{{ trans('invoice/creditgen.Translate10') }}
								@elseif($sticker->stickertype == 2)
									{!! '2' !!} <br>
									{{ trans('invoice/creditgen.Translate11') }}
								@elseif($sticker->stickertype == 3)
									{!! '3' !!} <br>
									{{ trans('invoice/creditgen.Translate12') }}
								@elseif($sticker->stickertype == 4)
									{!! '4' !!} <br>
									{{ trans('invoice/creditgen.Translate13') }}
								@elseif($sticker->stickertype == 5)
									{!! '5' !!} <br>
									{{ trans('invoice/creditgen.Translate14') }}
								@elseif($sticker->stickertype == 6)
									{!! '6' !!} <br>
									{{ trans('invoice/creditgen.Translate15') }}
								@elseif($sticker->stickertype == 7)
									{!! '7' !!} <br>
									{{ trans('invoice/creditgen.Translate16') }}
								@elseif($sticker->stickertype == 8)
									{!! '8' !!} <br>
									{{ trans('invoice/creditgen.Translate17') }}
								@elseif($sticker->stickertype == 9)
									{!! '9' !!} <br>
									{{ trans('invoice/creditgen.Translate25') }}
								@endif
							</td>

							<td>
								<?php
								$imgsrc = $sticker->thumbnail;
								$imgsrc = str_replace('http://79.161.166.153', '', $imgsrc);
								$imgsrc = str_replace('http://markmaster.no', '', $imgsrc);
								$imgsrc = str_replace('http://192.168.1.50', '', $imgsrc);
								echo '<img src="' . $imgsrc . '" class="thumbnailpic" alt="' . $sticker->thumbnail . '1">';
								?>
								<!--{!! Html::image($sticker->thumbnail, $sticker->stickertype, array('class'=>'thumbnailpic')) !!}-->
							</td>

							<td>
								{!! '' . $sticker->width . ' x ' . $sticker->height !!}
							</td>

							<td>
								{!! $sticker->quantity !!}
							</td>

							<td>
								@if($faktura->currency == 0)
									{!! number_format ($sticker->price , 2 ) . ' nok' !!}
								@elseif($faktura->currency == 1)
									{!! '$ ' . number_format ($sticker->price , 2 ) !!}
								@elseif($faktura->currency == 2)
									{!! number_format ($sticker->price , 2 ) . ' sek' !!}
								@elseif($faktura->currency == 3)
									{!! number_format ($sticker->price , 2 ) . ' dkk' !!}
								@elseif($faktura->currency == 4)
									{!! '€ ' . number_format ($sticker->price , 2 ) !!}
								@endif
							</td>
						</tr>
						@endforeach
						<tr>
							<td>

							</td>
							<td>
								{!! 'Porto og ekspedisjonsgebyr'!!}
							</td>

							<td>

							</td>

							<td>

							</td>

							<td>

							</td>

							<td>
								@if($faktura->currency == 0)
									{!! number_format ($faktura->fee , 2 ) . ' nok' !!}
								@elseif($faktura->currency == 1)
									{!! '$ ' . number_format ($faktura->fee , 2 ) !!}
								@elseif($faktura->currency == 2)
									{!! number_format ($faktura->fee , 2 ) . ' sek' !!}
								@elseif($faktura->currency == 3)
									{!! number_format ($faktura->fee , 2 ) . ' dkk' !!}
								@elseif($faktura->currency == 4)
									{!! '€ ' . number_format ($faktura->fee , 2 ) !!}
								@endif
							</td>

						</tr>
				</table>
		</div>
	</div>

</div>
