
<?php
	if(app()->getLocale() == 'en') {
		$langcode = "en_GB";
	} else if (app()->getLocale() == 'se') {
		$langcode = "sv_SE";
	} else if (app()->getLocale() == 'dk') {
		$langcode = "da_DK";
	} else {
		$langcode = "no_NO";
	}

	$token = "&token=" . "5p_SsQ2-";
	$orderNumber = "&orderNumber=" . $order->id;
	$subAmount = 100 * $order->amount;
	$subAmount = round($subAmount, 0, PHP_ROUND_HALF_DOWN);
	$amount = "&amount=" . $subAmount;
	$language = "&language=" . $langcode;
	if ($order->currency == 0) {
		$currencyCode = "&CurrencyCode=" . "NOK";
	} else if ($order->currency == 1) {
		$currencyCode = "&CurrencyCode=" . "USD";
	} else if ($order->currency == 2) {
		$currencyCode = "&CurrencyCode=" . "SEK";
	} else if ($order->currency == 3) {
		$currencyCode = "&CurrencyCode=" . "DKK";
	} else if ($order->currency == 4) {
		$currencyCode = "&CurrencyCode=" . "EUR";
	}

	$redirectUrl = "&redirectUrl=" . env('SERVER_PATH') . "/" . app()->getLocale() ."/store/status?orderId=". $order->id;

	$url = "https://epayment.nets.eu/Netaxept/Register.aspx?MerchantId=545558";
	$register_url = $url . $token . $orderNumber . $amount . $language . $currencyCode . $redirectUrl;

	$xml = simplexml_load_file($register_url);
	$transaction_id = $xml->TransactionId;
	$transaction_id = "&transactionid=" . $transaction_id;

	$payment_url = 	"https://epayment.nets.eu/Terminal/default.aspx?merchantId=545558";
	$payment_url = $payment_url . $transaction_id;

	// print_r($xml);
	// echo '<br />amount ' . $amount;
	// echo '<br />transaction id: ' . $transaction_id;
	// echo '<br />order id: ' . $order->id;
	// echo '<br />register url: ' . $register_url;
	header("Location: " . $payment_url);
	exit;
?>
