@extends('layouts.new')

@section('title')
	{{ trans('store/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/index.Translate2') }}">
@stop

@section('head-keyword')
	<meta name="keywords" content="{{ trans('store/index.keywords') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}">
@stop

@section('promo')
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<!--<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol> -->

		@if(app()->getLocale() == "no")
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<a href="/no/merkelapper/klistremerker?dvos=1&id=69136"> <img src="/img/promos/{{ app()->getLocale() }}/postkasseetikett.png" alt="La barna lage sin egen postkasse etikett!" style="width:60%;margin:auto;"> </a>
				</div>
				<div class="item">
					<a href="/no/navnelapper/strykefrie"> <img src="/img/promos/{{ app()->getLocale() }}/strykefrie_navnelapper.png" alt="Strykefrie navnelapper! Kjøp billige strykefrie navnelapper her!" style="width:60%;margin:auto;"> </a>
				</div>
			</div>

		@else
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<a href="http://www.facebook.com/markmasteras" target="_blank"> <img src="/img/promos/{{ app()->getLocale() }}/facebook.png" alt="Markmaster er nå på facebook! Følg oss!" style="width:60%;margin:auto;"> </a>
				</div>
			</div>
		@endif

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

@stop

@section('content')



@stop
