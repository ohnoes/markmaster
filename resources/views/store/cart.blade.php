@extends('layouts.main')

@section('title')
	{{ trans('store/cart.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/cart.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/store/cart">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	@if(count($errors) > 0)
		<div class="alert alert-danger">
			<p> {{ trans('store/cart.Errors') }} </p>
			<ul>
				@foreach($errors->all() as $error)
					<li> {!! $error !!}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<?php
		$currency = Currency::find(1);

		$discountTotal = 0;
		$discountTotalSEK = 0;
		$discountTotalDKK = 0;
		$discountTotalUSD = 0;
		$discountTotalEUR = 0;
		$coupon = Session::get('coupon', 'NA');
		if ($coupon != 'NA') {
			$couponEffect 	= Session::get('coupon.effect');
			$couponSticker 	= Session::get('coupon.sticker');
			$couponAmount 	= Session::get('coupon.amount');
		}

	?>
	<input type="hidden" name="redirect" value="/{{ app()->getLocale() }}/store/checkout" id="redirect">
	{!! Form::hidden('usd', $currency->usd, array('id'=>'usd')) !!}
	{!! Form::hidden('sek', $currency->sek, array('id'=>'sek')) !!}
	{!! Form::hidden('dkk', $currency->dkk, array('id'=>'dkk')) !!}
		@if(Auth::user())
			<?php $theUser = markmaster\Models\User::find(Auth::user()->id); ?>
			@if($theUser->paymenttype == 1)
				{!! Form::hidden('total', Cart::total()+129, array('id'=>'total')) !!}
			@else
				{!! Form::hidden('total', Cart::total()+29, array('id'=>'total')) !!}
			@endif
		@else
			<?php $theUser = "void"; ?>
		@endif

	<div class="container ">
			<h1>{{ trans('store/cart.ShoppingCart') }}</h1>

			<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<div class="table-responsive">
				<table class="table table-hover table-bordered " id="myTable">
					<tr>
						<th> {{ trans('store/cart.URL') }} </th>
						<th> {{ trans('store/cart.Stickertype') }}  </th>
						<th> {{ trans('store/cart.ProductType') }} </th>
						<th> {{ trans('store/cart.Size') }} </th>
						<th> {{ trans('store/cart.Quantity') }} </th>
						<th> {{ trans('store/cart.Price') }}  </th>
						<th> </th>
					</tr>
					{!! Form::hidden('array', $products, array('id'=>'array')) !!}
					@foreach($products as $product)
						@if(Auth::user())
							@if($product->name != 'couponcode')
								<?php
									$orderitem = Orderitem::find($product->id);
									$orderitem->user_id = Auth::user()->id;
									$orderitem->thumbnail = $product->image;
									$orderitem->url = $product->URL . Auth::user()->id;
									$orderitem->edit = $product->edit;
									$orderitem->save();
								?>
							@endif
						@else
							@if($product->name != 'couponcode')
								<?php
									$orderitem = Orderitem::find($product->id);
									$orderitem->thumbnail = $product->image;
									$orderitem->edit = $product->edit;
									$orderitem->save();
								?>
							@endif
						@endif
					@endforeach

					@foreach($products as $product)
					<tr style="background-color: rgb(247, 247, 247);">
						<td>
							@if($product->name == 'couponcode')
								{!! 'N/A' !!}
							@else
								@if (Auth::user())
									@if($product->name == 1)
									 	<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksticker') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
										{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merksticker') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 2)
								 		<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironc') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
										{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkironc') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 3)
									 	<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkgold') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
										{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkgold') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 4)
									 	<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksolv') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
									 	{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merksolv') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 5)
								 		<a href="/{{ app()->getLocale() }}/{{ trans('routes.merktrans') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
									 	{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merktrans') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 6)
									 	<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkcheap') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
									 	{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkcheap') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 7)
								 		<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkiron') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
								 		{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkiron') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 8)
								 		<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkreflection') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
								 		{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkreflection') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @elseif($product->name == 9)
								 		<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironfree') }}?id={!! $product->id !!}&dvos={!! Auth::user()->id !!}">
								 		{!! 'www.markmaster.com' . '/' . app()->getLocale() . '/' . trans('routes.merkironfree') . '?id=' . $product->id . '&dvos=' . Auth::user()->id !!} </a>
								 @endif
								@else
								{!! 'N/A' !!}
								@endif
							@endif
						</td>

						<td>
							@if($product->name == 'couponcode')
								{!!	$product->code !!}
							@else

								<img src="<?php echo $product->image ?>?v=<?php echo Date("Y.m.d.G.i.s")?>" alt="<?php echo $product->name ?>" class="thumbnailpic">

							@endif
						</td>
						<td>
							@if($product->name == 1)
								{{ trans('store/cart.Sticker') }}
							@elseif($product->name == 2)
								{{ trans('store/cart.Ironon') }}
							@elseif($product->name == 3)
								{{ trans('store/cart.Gold') }}
							@elseif($product->name == 4)
								{{ trans('store/cart.Silver') }}
							@elseif($product->name == 5)
								{{ trans('store/cart.Transparent') }}
							@elseif($product->name == 6)
								{{ trans('store/cart.StickerBW') }}
							@elseif($product->name == 7)
								{{ trans('store/cart.IrononBW') }}
							@elseif($product->name == 8)
								{{ trans('store/cart.Reflection') }}
							@elseif($product->name == 9)
								{{ trans('store/cart.Fabricsticker') }}
							@elseif($product->name == 'couponcode')
								{{ trans('store/cart.CouponCode') }}
							@endif
						</td>
						<td>
							@if($product->name == 'couponcode')
								{!! ' - ' !!}
							@else
								{!! '' . $product->width . ' x ' . $product->height !!}
							@endif
						</td>
						<td>
							{!! $product->antall !!}
							<?php
								if($coupon != 'NA'){
									if($couponEffect == 2) {
										echo "(x2)";
									}
								}
							?>
						</td>
						<td>
						<?php

							$regularprice = $product->price;
							$discountPrice = $regularprice;
							if ($coupon != 'NA') {
								if($couponEffect == 1) {
									if($couponSticker == 99 || $couponSticker == $product->name) {
										$discountPrice = floor($product->price *((100 - $couponAmount)/100));
									}
								}
							}
							$discountPriceUS = $discountPrice/$currency->usd;
							$discountPriceUS = round($discountPriceUS, 2, PHP_ROUND_HALF_DOWN);
							$discountPriceSE = $discountPrice/$currency->sek;
							$discountPriceSE = floor($discountPriceSE);
							$discountPriceDK = $discountPrice/$currency->dkk;
							$discountPriceDK = floor($discountPriceDK);
							$discountPriceEU = $discountPrice/$currency->eur;
							$discountPriceEU = round($discountPriceEU, 2, PHP_ROUND_HALF_DOWN);

							$displayPriceUS = $product->price/$currency->usd;
							$displayPriceUS = round($displayPriceUS, 2, PHP_ROUND_HALF_DOWN);
							$displayPriceSE = $product->price/$currency->sek;
							$displayPriceSE = floor($displayPriceSE);
							$displayPriceDK = $product->price/$currency->dkk;
							$displayPriceDK = floor($displayPriceDK);
							$displayPriceEU = $product->price/$currency->eur;
							$displayPriceEU = round($displayPriceEU, 2, PHP_ROUND_HALF_DOWN);

						?>
							@if(Auth::check())
								@if(Auth::user()->currency == 0)
									@if($discountPrice != $regularprice)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $regularprice . ',00' . ' -' . $couponAmount . '%' . ' = ' . $discountPrice . ',00' !!} </label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $product->price  . ',00' !!}</label>
									@endif
								@elseif(Auth::user()->currency == 1)
									@if($discountPriceUS != $displayPriceUS)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! '$' . $displayPriceUS  . ' -' . $couponAmount . '%' . ' = ' . '$' . $discountPriceUS !!}</label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! '$' . $displayPriceUS  !!}</label>
									@endif
								@elseif(Auth::user()->currency == 2)
									@if($discountPriceSE != $displayPriceSE)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceSE . ',00' . ' -' . $couponAmount . '%' . ' = ' . $discountPriceSE . ',00' !!} </label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceSE  . ',00' !!}</label>
									@endif
								@elseif(Auth::user()->currency == 3)
									@if($discountPriceDK != $displayPriceDK)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceDK . ',00' . ' -' . $couponAmount . '%' . ' = ' . $discountPriceDK . ',00' !!} </label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceDK  . ',00' !!}</label>
									@endif
								@elseif(Auth::user()->currency == 4)
									@if($discountPriceEU != $displayPriceEU)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! '€' . $displayPriceEU  . ' -' . $couponAmount . '%' . ' = ' . '€' . $discountPriceEU !!}</label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! '€' . $displayPriceEU  !!}</label>
									@endif
								@endif
							@else
								@if(Session::get('theCurrency', 0) == 0)
									@if($discountPrice != $regularprice)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $regularprice . ',00' . ' -' . $couponAmount . '%' . ' = ' . $discountPrice . ',00' !!} </label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $product->price  . ',00' !!}</label>
									@endif
								@elseif(Session::get('theCurrency') == 1)
									@if($discountPriceUS != $displayPriceUS)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! '$' . $displayPriceUS  . ' -' . $couponAmount . '%' . ' = ' . '$' . $discountPriceUS !!}</label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! '$' . $displayPriceUS  !!}</label>
									@endif
								@elseif(Session::get('theCurrency') == 2)
									@if($discountPriceSE != $displayPriceSE)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceSE . ',00' . ' -' . $couponAmount . '%' . ' = ' . $discountPriceSE . ',00' !!} </label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceSE  . ',00' !!}</label>
									@endif
								@elseif(Session::get('theCurrency') == 3)
									@if($discountPriceDK != $displayPriceDK)
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceDK . ',00' . ' -' . $couponAmount . '%' . ' = ' . $discountPriceDK . ',00' !!} </label>
									@else
										<label id="price<?php echo $product->id ?>" style="float: right"> {!! $displayPriceDK  . ',00' !!}</label>
									@endif
								@elseif(Session::get('theCurrency') == 4)
									@if($discountPriceEU != $displayPriceEU)
										<label id="price<?php echo $product->id ?>"> {!! '€' . $displayPriceEU  . ' -' . $couponAmount . '%' . ' = ' . '€' . $discountPriceEU !!}</label>
									@else
										<label id="price<?php echo $product->id ?>"> {!! '€' . $displayPriceEU  !!}</label>
									@endif
								@endif
							@endif
						</td>
						<td>

							@if (Auth::user())
								@if ($product->itemtype != 'coupon' )
									@if($product->name == 1)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksticker') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@elseif($product->name == 2)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironc') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }}</a>
									@elseif($product->name == 3)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkgold') }}?edit=<?php echo $product->id ?>"class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@elseif($product->name == 4)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merksolv') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@elseif($product->name == 5)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merktrans') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@elseif($product->name == 6)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkcheap') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@elseif($product->name == 7)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkiron') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@elseif($product->name == 8)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkreflection') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@elseif($product->name == 9)
										<a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironfree') }}?edit=<?php echo $product->id ?>" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">{{ trans('store/cart.EditButton') }} </a>
									@endif
								@endif
							@endif
							<a href="/{{ app()->getLocale() }}/store/removeitem/<?php echo $product->id ?>" class="btn btn-danger" style="color: #FFF;"> {{ trans('store/cart.DeleteButton') }} </a>
						</td>
					</tr>

					<?php
						if ($coupon != 'NA') {
							if($couponEffect == 1) {
								if($couponSticker == 99 || $couponSticker == $product->name) {
									$discountTotal += floor($product->price *((100 - $couponAmount)/100));
									$discountTotalSEK += floor(($product->price/$currency->sek) *((100 - $couponAmount)/100));
									$discountTotalDKK += floor(($product->price/$currency->dkk) *((100 - $couponAmount)/100));
									//$discountTotalUSD += floor(($product->price/$currency->usd) *((100 - $couponAmount)/100));
									//$discountTotalEUR += floor(($product->price/$currency->eur) *((100 - $couponAmount)/100));
								} else {
									$discountTotal += floor($product->price);
									$discountTotalSEK += floor(($product->price/$currency->sek));
									$discountTotalDKK += floor(($product->price/$currency->dkk));
									//$discountTotalUSD += floor($product->price);
									//$discountTotalEUR += floor($product->price);
								}
							} else {
								$discountTotal += floor($product->price);
								$discountTotalSEK += floor(($product->price/$currency->sek));
								$discountTotalDKK += floor(($product->price/$currency->dkk));
								//$discountTotalUSD += floor($product->price);
								//$discountTotalEUR += floor($product->price);
							}
						} else {
							$discountTotal += floor($product->price);
							$discountTotalSEK += floor(($product->price/$currency->sek));
							$discountTotalDKK += floor(($product->price/$currency->dkk));
							//$discountTotalUSD += floor($product->price);
							//$discountTotalEUR += floor($product->price);
						}

					?>

					@endforeach
					<tr id="shippingcost">
						<td colspan="1">
							{{ trans('store/cart.Shipping') }}
						</td>
						<td>
						</td>
						<td>
						</td>
						<td>
						</td>
						<td>
						</td>
						<td colspan="1">
						<?php
								$vatUS = 29/$currency->usd;
								$vatUS = round($vatUS, 2, PHP_ROUND_HALF_DOWN);
								$vatSE = 29/$currency->sek;
								$vatSE = floor($vatSE);
								$vatDK = 29/$currency->dkk;
								$vatDK = floor($vatDK);
								$vatEU = 29/$currency->eur;
								$vatEU = round($vatEU, 2, PHP_ROUND_HALF_DOWN);
								$fakUS = 129/$currency->usd;
								$fakUS = round($fakUS, 2, PHP_ROUND_HALF_DOWN);
								$fakSE = 129/$currency->sek;
								$fakSE = floor($fakSE);
								$fakDK = 129/$currency->dkk;
								$fakDK = floor($fakDK);
								$fakEU = 129/$currency->eur;
								$fakEU = round($fakEU, 2, PHP_ROUND_HALF_DOWN);
							?>
						@if(Auth::user())
							<?php
							$theUser = markmaster\Models\User::find(Auth::user()->id);
							?>
							@if($theUser->paymenttype == 1)
								@if(Auth::check())
									@if(Auth::user()->currency == 0)
										<label style="float: right"> {!!  '129,00' !!}
									@elseif(Auth::user()->currency == 1)
										<label style="float: right"> {!! '$' . $fakUS  !!}
									@elseif(Auth::user()->currency == 2)
										<label style="float: right"> {!! $fakSE  . ',00' !!}
									@elseif(Auth::user()->currency == 3)
										<label style="float: right"> {!! $fakDK  . ',00' !!}
									@elseif(Auth::user()->currency == 4)
										<label style="float: right"> {!! '€' . $fakEU  !!}
									@endif
								@else
									@if(Session::get('theCurrency', 0) == 0)
										<label style="float: right"> {!!  '129,00' !!}
									@elseif(Session::get('theCurrency') == 1)
										<label style="float: right"> {!! '$' . $fakUS  !!}
									@elseif(Session::get('theCurrency') == 2)
										<label style="float: right"> {!! $fakSE  . ',00' !!}
									@elseif(Session::get('theCurrency') == 3)
										<label style="float: right"> {!! $fakDK  . ',00' !!}
									@elseif(Session::get('theCurrency') == 4)
										<label style="float: right"> {!! '€' . $fakEU  !!}
									@endif
								@endif
							@else
								@if(Auth::check())
									@if($discountTotal >= 145)
										@if(Auth::user()->currency == 0)
											<label style="float: right; text-dec"> {!!  '29,00' !!}
										@elseif(Auth::user()->currency == 1)
											<label style="float: right"> {!! '$' . $vatUS  !!}
										@elseif(Auth::user()->currency == 2)
											<label style="float: right"> {!! $vatSE  . ',00' !!}
										@elseif(Auth::user()->currency == 3)
											<label style="float: right"> {!! $vatDK  . ',00' !!}
										@elseif(Auth::user()->currency == 4)
											<label style="float: right"> {!! '€' . $vatEU  !!}
										@endif
									@else
										@if(Auth::user()->currency == 0)
											<label style="float: right"> {!!  '29,00' !!}
										@elseif(Auth::user()->currency == 1)
											<label style="float: right"> {!! '$' . $vatUS  !!}
										@elseif(Auth::user()->currency == 2)
											<label style="float: right"> {!! $vatSE  . ',00' !!}
										@elseif(Auth::user()->currency == 3)
											<label style="float: right"> {!! $vatDK  . ',00' !!}
										@elseif(Auth::user()->currency == 4)
											<label style="float: right"> {!! '€' . $vatEU  !!}
										@endif
									@endif
								@else
									@if(Session::get('theCurrency', 0) == 0)
										<label style="float: right"> {!!  '29,00' !!}
									@elseif(Session::get('theCurrency') == 1)
										<label style="float: right"> {!! '$' . $vatUS  !!}
									@elseif(Session::get('theCurrency') == 2)
										<label style="float: right"> {!! $vatSE  . ',00' !!}
									@elseif(Session::get('theCurrency') == 3)
										<label style="float: right"> {!! $vatDK  . ',00' !!}
									@elseif(Session::get('theCurrency') == 4)
										<label style="float: right"> {!! '€' . $vatEU  !!}
									@endif
								@endif
							@endif
						@else
							@if(Session::get('theCurrency', 0) == 0)
								<label style="float: right"> {!!  '29,00' !!}
							@elseif(Session::get('theCurrency') == 1)
								<label style="float: right"> {!! '$' . $vatUS  !!}
							@elseif(Session::get('theCurrency') == 2)
								<label style="float: right"> {!! $vatSE  . ',00' !!}
							@elseif(Session::get('theCurrency') == 3)
								<label style="float: right"> {!! $vatDK  . ',00' !!}
							@elseif(Session::get('theCurrency') == 4)
								<label style="float: right"> {!! '€' . $vatEU  !!}
							@endif
						@endif
						</td>
						<td>
						</td>
					</tr>

					<?php
						$invoicecustomer = false;
						if($discountTotal >= 145) {
							$strikeout = true;
						} else {
							$strikeout = false;
						}
						if($theUser != 'void') {
							if($theUser->paymenttype == 1){
								$invoicecustomer = true;
								$strikeout = false;
							}
						}
					 ?>
					 {!! Form::hidden('invoicecustomer', $invoicecustomer, array('id'=>'invoicecustomer')) !!}
					 {!! Form::hidden('strikeout', $strikeout, array('id'=>'strikeout')) !!}
					 {!! Form::hidden('totalBeforeShipping', $discountTotal, array('id'=>'totalBeforeShipping')) !!}
					<tr class="total">
						<td colspan ="1"> <b> {!! 'TOTAL' !!} </td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td colspan="1">
							<?php
								if($discountTotal >= 145) {
									$totalNO = $discountTotal;
									$totalNO = floor($totalNO);
									$totalUS = ($discountTotal)/$currency->usd;
									$totalUS = round($totalUS, 2, PHP_ROUND_HALF_DOWN);
									$totalSE = floor($discountTotalSEK);
									$totalSE = floor($totalSE);
									$totalDK = floor($discountTotalDKK);
									$totalDK = floor($totalDK);
									$totalEU = ($discountTotal)/$currency->eur;
									$totalEU = round($totalEU, 2, PHP_ROUND_HALF_DOWN);
									$totalfakNO = ($discountTotal+129);
									$totalfakNO = floor($totalfakNO);
									$totalfakUS = ($discountTotal+129)/$currency->usd;
									$totalfakUS = round($totalfakUS, 2, PHP_ROUND_HALF_DOWN);
									$totalfakSE = floor($discountTotalSEK) + floor(129/$currency->sek);
									$totalfakSE = floor($totalfakSE);
									$totalfakDK = floor($discountTotalDKK) + floor(129/$currency->dkk);
									$totalfakDK = floor($totalfakDK);
									$totalfakEU = ($discountTotal+129)/$currency->eur;
									$totalfakEU = round($totalfakEU, 2, PHP_ROUND_HALF_DOWN);
								} else {
									$totalNO = $discountTotal+29;
									$totalNO = floor($totalNO);
									$totalUS = ($discountTotal+29)/$currency->usd;
									$totalUS = round($totalUS, 2, PHP_ROUND_HALF_DOWN);
									$totalSE = floor($discountTotalSEK) + floor(29/$currency->sek);
									$totalSE = floor($totalSE);
									$totalDK = floor($discountTotalDKK) + floor(29/$currency->dkk);
									$totalDK = floor($totalDK);
									$totalEU = ($discountTotal+29)/$currency->eur;
									$totalEU = round($totalEU, 2, PHP_ROUND_HALF_DOWN);
									$totalfakNO = ($discountTotal+129);
									$totalfakNO = floor($totalfakNO);
									$totalfakUS = ($discountTotal+129)/$currency->usd;
									$totalfakUS = round($totalfakUS, 2, PHP_ROUND_HALF_DOWN);
									$totalfakSE = floor($discountTotalSEK) + floor(129/$currency->sek);
									$totalfakSE = floor($totalfakSE);
									$totalfakDK = floor($discountTotalDKK) + floor(129/$currency->dkk);
									$totalfakDK = floor($totalfakDK);
									$totalfakEU = ($discountTotal+129)/$currency->eur;
									$totalfakEU = round($totalfakEU, 2, PHP_ROUND_HALF_DOWN);
								}
								?>
							@if(Auth::user())
								<?php
									$theUser = markmaster\Models\User::find(Auth::user()->id);
								?>
								@if($theUser->paymenttype == 1)
									@if(Auth::check())
										@if(Auth::user()->currency == 0)
											<label style="float: right"> {!! $totalfakNO  . ',00' !!} </label>
										@elseif(Auth::user()->currency == 1)
											<label style="float: right"> {!! '$' . $totalfakUS  !!}
										@elseif(Auth::user()->currency == 2)
											<label style="float: right"> {!! $totalfakSE  . ',00' !!}
										@elseif(Auth::user()->currency == 3)
											<label style="float: right"> {!! $totalfakDK  . ',00' !!}
										@elseif(Auth::user()->currency == 4)
											<label style="float: right"> {!! '€' . $totalfakEU  !!}
										@endif
									@else
										@if(Session::get('theCurrency', 0) == 0)
											<label style="float: right"> {!! $totalfakNO  . ',00' !!}
										@elseif(Session::get('theCurrency') == 1)
											<label style="float: right"> {!! '$' . $totalfakUS  !!}
										@elseif(Session::get('theCurrency') == 2)
											<label style="float: right"> {!! $totalfakSE  . ',00' !!}
										@elseif(Session::get('theCurrency') == 3)
											<label style="float: right"> {!! $totalfakDK  . ',00' !!}
										@elseif(Session::get('theCurrency') == 4)
											<label style="float: right"> {!! '€' . $totalfakEU  !!}
										@endif
									@endif
								<br />
								@else
									@if(Auth::check())
										@if(Auth::user()->currency == 0)
											<label style="float: right"> {!! $totalNO  . ',00' !!}
										@elseif(Auth::user()->currency == 1)
											<label style="float: right"> {!! '$' . $totalUS  !!}
										@elseif(Auth::user()->currency == 2)
											<label style="float: right"> {!! $totalSE  . ',00' !!}
										@elseif(Auth::user()->currency == 3)
											<label style="float: right"> {!! $totalDK  . ',00' !!}
										@elseif(Auth::user()->currency == 4)
											<label style="float: right"> {!! '€' . $totalEU  !!}
										@endif
									@else
										@if(Session::get('theCurrency', 0) == 0)
											<label style="float: right"> {!! $totalNO  . ',00' !!}
										@elseif(Session::get('theCurrency') == 1)
											<label style="float: right"> {!! '$' . $totalUS  !!}
										@elseif(Session::get('theCurrency') == 2)
											<label style="float: right"> {!! $totalSE  . ',00' !!}
										@elseif(Session::get('theCurrency') == 3)
											<label style="float: right"> {!! $totalDK  . ',00' !!}
										@elseif(Session::get('theCurrency') == 4)
											<label style="float: right"> {!! '€' . $totalEU  !!}
										@endif
										<br/>
									@endif
								@endif
							@else
								@if(Session::get('theCurrency', 0) == 0)
									<label style="float: right"> {!! $totalNO  . ',00' !!}
								@elseif(Session::get('theCurrency') == 1)
									<label style="float: right"> {!! '$' . $totalUS  !!}
								@elseif(Session::get('theCurrency') == 2)
									<label style="float: right"> {!! $totalSE  . ',00' !!}
								@elseif(Session::get('theCurrency') == 3)
									<label style="float: right"> {!! $totalDK  . ',00' !!}
								@elseif(Session::get('theCurrency') == 4)
									<label style="float: right"> {!! '€' . $totalEU  !!}
								@endif
							@endif
							<td>
								@if(Auth::check())
									@if(Auth::user()->currency == 0)
										<label> NOK </label>
									@elseif(Auth::user()->currency == 1)

									@elseif(Auth::user()->currency == 2)
										<label> SEK </label>
									@elseif(Auth::user()->currency == 3)
										<label> DKK </label>
									@elseif(Auth::user()->currency == 4)

									@endif
								@else
									@if(Session::get('theCurrency', 0) == 0)
										<label> NOK </label>
									@elseif(Session::get('theCurrency') == 1)

									@elseif(Session::get('theCurrency') == 2)
										<label> SEK </label>
									@elseif(Session::get('theCurrency') == 3)
										<label> DKK </label>
									@elseif(Session::get('theCurrency') == 4)

									@endif
								@endif
							</td>
						</td>
					</tr>
				</table>
				<a href="/{{ app()->getLocale() }}/" class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;"> {{ trans('store/cart.StoreButton') }} </a>
				@if(Cart::count() > 0)
					<a class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;" onclick="checkShipping()"> {{ trans('store/cart.CheckoutButton') }} </a> </br></br>
				@endif
			</div>
				{{ trans('store/cart.SigninMemo') }}
			</form>

			<table>
				<tr>
					@if(Auth::check())
						<?php
							$theUser = markmaster\Models\User::find(Auth::user()->id);
						?>
						@if($theUser->id == 70 || $theUser->id == 57)

						@else
							<td>
							<form method="POST" action="/{{ app()->getLocale() }}/store/coupon" accept-charset="UTF-8" name="inputs" data-toggle="validator">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">

							 <label for="coupon">{{ trans('store/cart.coupon') }}</label>
							{!! Form::text('coupon') !!}

							<input type="submit" value="{{ trans('store/cart.couponButton') }}" class="btn btn-danger" onclick="formValid(event, inputs)"
							style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">
							{!! Form::close() !!}
							</td>
						@endif
					@else
					td>
					<form method="POST" action="/{{ app()->getLocale() }}/store/coupon" accept-charset="UTF-8" name="inputs" data-toggle="validator">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

					 <label for="coupon">{{ trans('store/cart.coupon') }}</label>
					{!! Form::text('coupon') !!}

					<input type="submit" value="{{ trans('store/cart.couponButton') }}" class="btn btn-danger" onclick="formValid(event, inputs)"
					style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;">
					{!! Form::close() !!}
					</td>
					@endif
				</tr>
			</table>
	</div>
	@include('store.shippingpopup')
	{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
	{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
	<script>
    $(document).ready(function() {
			var strike = document.getElementById('strikeout').value;
			if(strike)
      	document.getElementById('shippingcost').classList.add("strikeout");
      });

			function checkShipping(){
				var amount = document.getElementById('totalBeforeShipping').value;
				var diff = 145 - amount;
				document.getElementById('orderAmount').innerHTML = amount + ' NOK. ';
				document.getElementById('orderDiff').innerHTML = diff + ' NOK';
				var userType = document.getElementById('invoicecustomer').value;
				console.log("THE AMOUNT: " + amount);
				if(amount >= 145 || userType) {
					redirectToCheckout();
				} else {
					popBoxShow();
				}

					//{{ app()->getLocale() }}/store/checkout
			}
  </script>

@stop
