@extends('layouts.main')
@section('title')
	{{ trans('store/status.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/status.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/store/status/">
@stop

@section('content')
	<!-- Google Code for Sticker sale Conversion Page -->
	<script type="text/javascript">
    	/* <![CDATA[ */
    	var google_conversion_id = 1012450158;
    	var google_conversion_language = "en";
    	var google_conversion_format = "3";
    	var google_conversion_color = "ffffff";
    	var google_conversion_label = "lldoCMuB9G8Q7obj4gM";
    	if ({!! $order->amount !!}) {
    		var google_conversion_value = {!! $order->amount !!};
    	}
    	var google_conversion_currency = "NOK";
    	var google_remarketing_only = false;
    	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
	<noscript>
	<div style="display:inline;">
	   <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1012450158/?value={!! $order->amount !!}&amp;currency_code=NOK&amp;label=lldoCMuB9G8Q7obj4gM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

	@if($memo == "newuser")
		<!-- Giver User a message that the order is complete/placed -->
		<div id="statusMessage">
			{{ trans('message.newaccountmade') }} <br/>
			<b> User: </b> <?php echo $useremail ?><br/>
			<b> Password: </b> <?php echo $userfirstname ?> <br/>
			{{ trans('message.newaccountmade2') }} <br/>
			<h2> {{ trans('store/status.Translate5') }} </h2>
		</div>
	@else
		<!-- Giver User a message that the order is complete/placed -->
		<div id="statusMessage">
			<h2> {{ trans('store/status.Translate5') }} </h2>
		</div>
	@endif

	<!-- Give User a overview of the order -->
	<!-- ================================================= -->
	<!-- || Order#  || Sticker || Amount || Order Status || [Button]  -->
	<!-- ================================================= -->

<table border="1">
    <tr>
        <th> {{ trans('store/status.Translate6') }} </th>
        <th> {{ trans('store/status.Translate7') }} </th>
        <th> {{ trans('store/status.Translate8') }} </th>
        <th> {{ trans('store/status.Translate9') }} </th>
				<th> {{ trans('store/status.Translate10')}} </th>
    </tr>
    <tr>
				<td>
					{!! $order->id !!}
				</td>
        <td>
					@if($order->currency == 0)
						{!! number_format ($order->amount , 2 ) . ' nok ' !!}
					@elseif($order->currency == 1)
						{!! '$ ' . number_format ($order->amount , 2 ) . ' ' !!}
					@elseif($order->currency == 2)
						{!! number_format ($order->amount , 2 ) . ' sek ' !!}
					@elseif($order->currency == 3)
						{!! number_format ($order->amount , 2 ) . ' dkk ' !!}
					@elseif($order->currency == 4)
						{!! '€ ' . number_format ($order->amount , 2 ) . ' ' !!}
					@endif
        </td>
        <td>
					@if ($order->order_status == 0)
						{{ trans('store/status.Translate11') }}
					@elseif ($order->order_status == 1)
						{{ trans('store/status.Translate12') }}
					@elseif ($order->order_status == 2)
						{{ trans('store/status.Translate13') }}
					@elseif ($order->order_status == 3)
						{{ trans('store/status.Translate14') }}
					@elseif ($order->order_status == 6)
						{{ trans('store/status.Translate18') }}
					@else
						{{ trans('store/status.Translate15') }}
					@endif
				</td>
				<td>
					@if($order->user_id == 0)
					@else
							<a href="/{{ app()->getLocale() }}/order/details/<?php echo $order->id ?>" class="buttonBox">{{ trans('store/status.Translate16') }} </a>
					@endif
				</td>
				<td>
					<a href="/{{ app()->getLocale() }}/invoice/invoice/<?php echo $order->id ?>" class="buttonBox" target="popup" onclick="window.open('/invoice/invoice/' . $order->id ,'name','width=600,height=400')">{{ trans('store/status.Translate17') }} </a>
				</td>
      </tr>
    </table>

@stop
