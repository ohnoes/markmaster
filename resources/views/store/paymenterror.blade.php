@extends('layouts.main')
@section('title')
	{{ trans('store/status.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/status.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/store/status/">
@stop

@section('content')

	@if($memo == "newuser")
		<!-- Giver User a message that the order is complete/placed -->
		<div id="statusMessage">
			{{ trans('message.newaccountmade') }} <br/>
			<b> User: </b> <?php echo $useremail ?><br/>
			<b> Password: </b> <?php echo $userfirstname ?> <br/>
			{{ trans('message.newaccountmade2') }} <br/>
		</div>
	@endif

  @if($errorType == "auth")
    <h1> {{ trans('store/status.Translate3') }} </h1>
  @else
    <h1> {{ trans('store/status.Translate4') }} </h1>
  @endif

@stop
