
<div class="popBox popupBox" id="imageBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="popBoxHide()">X</button>
    <h4 class="popupTitle"> {{ trans('store/popup.Title') }} </h4>
  </div>
  <div >
    <h4 style="padding: 10px;"> {{ trans('store/popup.Part1') }} </h4>
    <h4 style="padding: 10px;"> {{ trans('store/popup.Part2') }} <label id="orderAmount"></label> <label id="orderDiff"></label>{{ trans('store/popup.Part3') }}</h4>
    <h4 style="padding: 10px;"> {{ trans('store/popup.Part4') }} </h4>
    <div class="padding: 10px;">
      <a class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;" onclick="redirectToCheckout()"> {{ trans('store/popup.Button-confirm') }} </a>
      <a class="btn btn-danger" style="background-color: rgb(40, 170, 225);border-color: rgb(40, 170, 225);color: #FFF;" onclick="popBoxHide()"> {{ trans('store/popup.Button-negative') }} </a>
    </div>
  </div>

</div>

<script>
function redirectToCheckout(){
  var redlink = document.getElementById('redirect').value;
  window.location = redlink;
}
</script>

{!! Html::script('js/shippingpopup.js') !!}
