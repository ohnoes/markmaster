@extends('layouts.main')
@section('title')
	{{ trans('store/status.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/status.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/store/status/">
@stop
@section('content')

<?php

	if(\Input::has('orderId')){
		$order_id = \Input::get('orderId');
		$order = \Order::find($order_id);
		$memo = $order->memo;
		$order->memo = "";
		$order->save();
	}


	$theuser = markmaster\Models\User::find($order->user_id);
	if($order->user_id == 0) {
		$orderPaymenttype = 0;
	} else {
		$orderPaymenttype = $theuser->paymenttype;
	}

	$userfirstname = $theuser->firstname;
	$useremail = $theuser->email;


	$token = "&token=" . "5p_SsQ2-";
	$transaction_id = '&transactionId=' . \Input::get('transactionId');

	$process_auth_url = 'https://epayment.nets.eu/Netaxept/Process.aspx?MerchantId=545558&operation=AUTH';
	$process_auth_url = $process_auth_url . $token . $transaction_id;

	$auth_xml = simplexml_load_file($process_auth_url);
	$auth_response_code = $auth_xml->ResponseCode;
	//echo 'response code: ' . $auth_response_code;
	echo '<br /> <br />';


	// messages to output for error1 and error2
	$errorMessage = "";
	$errorResponseText = "";

	// cases
	$error1 = false;
	$error2 = false;
	$success = false;

	// if auth fails
	if ($auth_response_code != 'OK')
	{
		// echo 'Error: ';
		$errorXml = $auth_xml->Error;
		$message = $errorXml->Message;
		$responseText = $errorXml->Result->ResponseText;
		// echo '<p>Message: ' . $message . '</p>';
		// echo '<p>Response Text: ' . $responseText . '</p>';
		$error1 = true;
		$errorMessage = $message;
		$errorResponseText = $responseText;

	}
	else // if auth succeeds
	{

		// echo 'response code ok';


		// echo '<br /> <br />';

		// print_r($auth_xml);
		// echo '<br /> <br />';

		$amount = '&amount=' . $order->amount;
		$process_capture_url = 'https://epayment.nets.eu/Netaxept/Process.aspx?merchantId=545558&operation=CAPTURE';
		$process_capture_url = $process_capture_url . $token . $transaction_id . $amount;

		$capture_xml = simplexml_load_file($process_capture_url);


		$capture_response_code = $capture_xml->ResponseCode;

		// echo 'response code: ' . $capture_response_code;
		// echo '<br /> <br />';

		// if capture fails
		if ($capture_response_code != 'OK')
		{

			// echo 'Error: ';
			$errorXml = $capture_xml->Error;
			$message = $errorXml->Message;
			$responseText = $errorXml->Result->ResponseText;
			// echo '<p>Message: ' . $message . '</p>';
			// echo '<p>Response Text: ' . $responseText . '</p>';

			$error2 = true;
			$errorMessage = $message;
			$errorResponseText = $responseText;
		}
		else // if auth successful
		{
			$success = true;
			// echo '<h3>Congratulations!</h3><p>You just paid ' . $order->amount . ' NOK for stickers!</p>';
		}
		// print_r($capture_xml);
	}
	\Cart::clear();
?>


@if($error1 && $orderPaymenttype != 1) <!-- Auth fail -->
	@if($memo == "newuser")
		<!-- Giver User a message that the order is complete/placed -->
		<div id="statusMessage">
			{{ trans('message.newaccountmade') }} <br/>
			<b> User: </b> <?php echo $useremail ?><br/>
			<b> Password: </b> <?php echo $userfirstname ?> <br/>
			{{ trans('message.newaccountmade2') }} onerror=""<br/>
			{{ trans('store/status.Translate5') }}
		</div>
	@else
		<!-- Give the user information that the order failed because of an error -->
		{{ trans('store/status.Translate3') }}
	@endif
@elseif($error2  && $orderPaymenttype != 1) <!-- Capture fail -->
	@if($memo == "newuser")
		<!-- Giver User a message that the order is complete/placed -->
		<div id="statusMessage">
			{{ trans('message.newaccountmade') }} <br/>
			<b> User: </b> <?php echo $useremail ?><br/>
			<b> Password: </b> <?php echo $userfirstname ?> <br/>
			{{ trans('message.newaccountmade2') }} onerror=""<br/>
			{{ trans('store/status.Translate5') }}
		</div>
	@else
		<!-- Give the user information that the order failed because of an error -->
		<div id="statusMessage">
			{{ trans('store/status.Translate4') }}
		</div>
	@endif
@elseif($orderPaymenttype == 1)
<!--  Faktura kunde her -->
	<?php
		$order->order_status = 6;
		$order_date = new DateTime;

		$order->payment_date = $order_date->format('Y-m-d H:i:s');
		$order->save();

		$data = array(
			'customernr'=> $order->user_id,
			'ordernr'=> $order->id);


		\Mail::send('emails.orderconfirmation', $data, function($message) use ($order)
		{
			$message->to($order->email, $order->firstname . ' ' . $order->lastname)->subject('Ordrebekreftelse');
		});

	?>

	<!-- Giver User a message that the order is complete/placed -->
	<div id="statusMessage">
		{{ trans('store/status.Translate5') }}
	</div>
@else <!-- The order was successful. -->

	<!-- Set order status to 1 and create a faktura-->
	<?php
		$order = \Order::find($order_id);
		$order->order_status = 1;
		$order_date = new DateTime;

		$order->payment_date =  $order_date->format('Y-m-d H:i:s');
		$order->save();

		$faktura = new Faktura;
		$faktura->user_id = $order->user_id;
		$faktura->order_id = $order->id;
		$faktura->currency = $order->currency;
		$faktura->fee = $order->fee;
		$faktura->faktura_date = $order->payment_date;
		$nomvaprice = $order->amount / 125 * 100;
		$faktura->priceeksmva = $nomvaprice;
		$faktura->priceinkmva = $order->amount;
		if($order->billingorgname) {
			$faktura->billingorgname = $order->billingorgname;
		}
		$faktura->billingfirstname = $order->billingfirstname;
		$faktura->billinglastname = $order->billinglastname;
		$faktura->billingaddress = $order->billingaddress;
		if($order->billingaddress2){
			$faktura->billingaddress2 = $order->billingaddress2;
		}
		$faktura->billingzipcode = $order->billingzipcode;
		$faktura->billingcity = $order->billingcity;
		$faktura->billingstate = $order->billingstate;
		$faktura->billingcountry = $order->billingcountry;
		$faktura->save();


		$data = array(
			'customernr'=> $order->user_id,
			'ordernr'=> $order_id);


		\Mail::send('emails.orderconfirmation', $data, function($message) use ($order)
		{
			$message->to($order->email, $order->firstname . ' ' . $order->lastname)->subject('Ordrebekreftelse');
		});

	?>
	<!-- Google Code for Sticker sale Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1012450158;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "lldoCMuB9G8Q7obj4gM";
	var google_conversion_value = 0.00;
	if (<? echo $order->amount ?>) {
		var google_conversion_value = <? echo $order->amount ?>;
	}
	var google_conversion_currency = "NOK";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1012450158/?value=<? echo $order->amount ?>&amp;currency_code=NOK&amp;label=lldoCMuB9G8Q7obj4gM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

	@if($memo == "newuser")
		<!-- Giver User a message that the order is complete/placed -->
		<div id="statusMessage">
			{{ trans('message.newaccountmade') }} <br/>
			<b> User: </b> <?php echo $useremail ?><br/>
			<b> Password: </b> <?php echo $userfirstname ?> <br/>
			{{ trans('message.newaccountmade2') }} <br/>
			{{ trans('store/status.Translate5') }}
		</div>
	@else
		<!-- Giver User a message that the order is complete/placed -->
		<div id="statusMessage">
			{{ trans('store/status.Translate5') }}
		</div>
	@endif

	<!-- Give User a overview of the order -->
	<!-- ================================================= -->
	<!-- || Order#  || Amount || Order Status || [Button]  -->
	<!-- ================================================= -->



@endif

<table border="1">
                <tr>
                    <th> {{ trans('store/status.Translate6') }} </th>
                    <th> {{ trans('store/status.Translate7') }}</th>
                    <th> {{ trans('store/status.Translate8') }} </th>
                    <th> {{ trans('store/status.Translate9') }}  </th>
					<th> {{ trans('store/status.Translate10') }} </th>
                </tr>
                <tr>
					<td>
						{!! $order->id !!}
					</td>

                    <td>
						@if($order->currency == 0)
							{!! number_format ($order->amount , 2 ) . ' nok ' !!}
						@elseif($order->currency == 1)
							{!! '$ ' . number_format ($order->amount , 2 ) . ' ' !!}
						@elseif($order->currency == 2)
							{!! number_format ($order->amount , 2 ) . ' sek ' !!}
						@elseif($order->currency == 3)
							{!! number_format ($order->amount , 2 ) . ' dkk ' !!}
						@elseif($order->currency == 4)
							{!! '€ ' . number_format ($order->amount , 2 ) . ' ' !!}
						@endif
                    </td>
                    <td>
						@if ($order->order_status == 0)
							{{ trans('store/status.Translate11') }}
						@elseif ($order->order_status == 1)
							{{ trans('store/status.Translate12') }}
						@elseif ($order->order_status == 2)
							{{ trans('store/status.Translate13') }}
						@elseif ($order->order_status == 3)
							{{ trans('store/status.Translate14') }}
						@elseif ($order->order_status == 6)
							{{ trans('store/status.Translate18') }}
						@else
							{{ trans('store/status.Translate15') }}
						@endif
					</td>
					<td>
						@if($order->user_id == 0)

						@else
							<a href="/{{ app()->getLocale() }}/order/details/<?php echo $order->id ?>" class="buttonBox">{{ trans('store/status.Translate16') }} </a>
						@endif
					</td>
					<td>
						<a href="/{{ app()->getLocale() }}/invoice/invoice/<?php echo $order->id ?>" class="buttonBox" target="popup" onclick="window.open('/invoice/invoice/' . $order->id ,'name','width=600,height=400')">{{ trans('store/status.Translate17') }} </a>
					</td>
                </tr>
    </table>

@stop
