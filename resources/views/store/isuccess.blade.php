@extends('layouts.main')
@section('title')
	{{ trans('store/status.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/status.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/store/status/">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<?php
		$order->order_status = 6;
		$order_date = new DateTime;

		$order->payment_date = $order_date->format('Y-m-d H:i:s');
		$order->save();

		$data = array(
			'customernr'=> $order->user_id,
			'ordernr'=> $order->id);


		\Mail::send('emails.orderconfirmation', $data, function($message) use ($order)
		{
			$message->to($order->email, $order->firstname . ' ' . $order->lastname)->subject('Ordrebekreftelse');
		});

	?>

	<!-- Giver User a message that the order is complete/placed -->
	<div id="statusMessage">
		<h2> {{ trans('store/status.Translate5') }} </h2>
	</div>

	<!-- Give User a overview of the order -->
	<!-- ================================================= -->
	<!-- || Order#  || Sticker || Amount || Order Status || [Button]  -->
	<!-- ================================================= -->

<table border="1">
    <tr>
        <th> {{ trans('store/status.Translate6') }} </th>
        <th> {{ trans('store/status.Translate7') }} </th>
        <th> {{ trans('store/status.Translate8') }} </th>
        <th> {{ trans('store/status.Translate9') }} </th>
				<th> {{ trans('store/status.Translate10')}} </th>
    </tr>
    <tr>
				<td>
					{!! $order->id !!}
				</td>
        <td>
					@if($order->currency == 0)
						{!! number_format ($order->amount , 2 ) . ' nok ' !!}
					@elseif($order->currency == 1)
						{!! '$ ' . number_format ($order->amount , 2 ) . ' ' !!}
					@elseif($order->currency == 2)
						{!! number_format ($order->amount , 2 ) . ' sek ' !!}
					@elseif($order->currency == 3)
						{!! number_format ($order->amount , 2 ) . ' dkk ' !!}
					@elseif($order->currency == 4)
						{!! '€ ' . number_format ($order->amount , 2 ) . ' ' !!}
					@endif
        </td>
        <td>
					@if ($order->order_status == 0)
						{{ trans('store/status.Translate11') }}
					@elseif ($order->order_status == 1)
						{{ trans('store/status.Translate12') }}
					@elseif ($order->order_status == 2)
						{{ trans('store/status.Translate13') }}
					@elseif ($order->order_status == 3)
						{{ trans('store/status.Translate14') }}
					@elseif ($order->order_status == 6)
						{{ trans('store/status.Translate18') }}
					@else
						{{ trans('store/status.Translate15') }}
					@endif
				</td>
				<td>
					@if($order->user_id == 0)
					@else
							<a href="/{{ app()->getLocale() }}/order/details/<?php echo $order->id ?>" class="buttonBox">{{ trans('store/status.Translate16') }} </a>
					@endif
				</td>
				<td>

				</td>
      </tr>
    </table>

@stop
