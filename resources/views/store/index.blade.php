@extends('layouts.main')

@section('title')
	{{ trans('store/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('store/index.Translate2') }}">
@stop

@section('head-keyword')
	<meta name="keywords" content="{{ trans('store/index.keywords') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('promo')
<div class="promo-wrapper">
	<div id="carousel1" class="carousel slide" data-ride="carousel" data-pg-collapsed>
			<!-- Indicators
			<ol class="carousel-indicators">
					<li data-target="#carousel1" data-slide-to="0" class="active"></li>
					<li data-target="#carousel1" data-slide-to="1"></li>
					<li data-target="#carousel1" data-slide-to="2"></li>
			</ol>-->
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<!--
					<div class="item active imageitem">
							<img src="/img/views/kontroll/banner-1.png" alt="" />
							<div class="carousel-caption">
									<h3>Slide 1 title</h3>
									<p>Slide 1 description.</p>
							</div>
					</div> -->
					<div class="item active imageitem">
							<img src="/img/views/store/index/banner-1.png" alt="" />
							<div class="carousel-caption">
									<!--<h3>Slide 1 title</h3>
									<p>Slide 1 description.</p> -->
							</div>
					</div>
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#carousel1" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
			<a class="right carousel-control" href="#carousel1" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
	</div>
</div>
@stop

@section('content')

<div class="content-wrapper">

	<div class="row row-heading" data-pg-collapsed>
			<div class="col-xs-12">
					<h3 class="text-center">{{ trans('store/index.Usage') }}</h3>
			</div>
	</div>

	<div class="row no-margin" data-pg-collapsed>
			<div class="col-sm-6 box-padding">
				<a href="/{{ app()->getLocale() }}/{{ trans('routes.catstick') }}">
					<div class="row no-margin">
						<div class="col-sm-12 col-xs-4 no-padding">
							<img src="/img/views/store/index/klistremerker.png" width="100%" />
						</div>
						<div class="col-sm-12 col-xs-8 textbox">
							<h3>{{ trans('store/index.Stickers') }}</h3>
							<p>{{ trans('store/index.Stickers-info') }}</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 box-padding">
				<a href="/{{ app()->getLocale() }}/{{ trans('routes.catfabric') }}">
					<div class="row no-margin">
						<div class="col-sm-12 col-xs-4 no-padding">
							<img src="/img/views/store/index/toymerker.png" width="100%" />
						</div>
						<div class="col-sm-12 col-xs-8 textbox">
							<h3>{{ trans('store/index.Fabric') }}</h3>
							<p>{{ trans('store/index.Fabric-info') }}</p>
						</div>
					</div>
				</a>
			</div>
	</div>

</div>
@stop
