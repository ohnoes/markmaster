@extends('layouts.main')

@section('title')
   {{ trans('users/editaccount.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('users/editaccount.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/users/editaccount/">
@stop


@section('content')

	<div class="container">
		@if(count($errors) > 0)
		<div class="alert alert-danger">
			<p> {{ trans('users/editaccount.Translate3') }}</p>
			<ul>
				@foreach($errors->all() as $error)
					<li> {!! $error !!}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<h1 style="text-align: center;">{{ trans('users/editaccount.Translate4') }}</h1> <br>
		<p style="text-align: center;">
			{{ trans('users/editaccount.Translate5') }}<br>
			{{ trans('users/editaccount.Translate6') }}
		</p>

		<div id="userlinks">
			<p style="text-align: center;">

				<a href="/{{ app()->getLocale() }}/users/editshipping/<?php echo Auth::user()->id ?>" class="btn btn-danger">{{ trans('users/editaccount.Translate7') }} </a>
				<a href="/{{ app()->getLocale() }}/users/editbilling/<?php echo Auth::user()->id ?>" class="btn btn-danger">{{ trans('users/editaccount.Translate8') }} </a>
				<a href="/{{ app()->getLocale() }}/users/editpassword/<?php echo Auth::user()->id ?>" class="btn btn-danger">{{ trans('users/editaccount.Translate9') }} </a>
			</p>
		</div>
	</div>

@stop
