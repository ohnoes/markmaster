@extends('layouts.main')

@section('title')
	{{ trans('users/signin.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('users/signin.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/users/signin/">
@stop

@section('content')

	@if (Session::has('messageError'))
					 <div class="alert alert-danger" role="alert"> {!! Session::get('messageError') !!} </div>
	@endif

	<div class="container">
      <h2>{{ trans('users/signin.Translate3') }}</h2>
	<div class="alert alert-warning" role="alert"> {{ trans('users/signin.Translate4') }}  </div>
		<form action="/{{ app()->getLocale() }}/users/signin" method="POST" accept-charset="UTF-8">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<form role="form">
			<div class="form-group">
			  <label for="email">{{ trans('users/signin.Translate5') }}:</label>
			  <div class="input-group">
				<span class="input-group-addon">@</span>
				<input type="email" class="form-control" name="email" placeholder="{{ trans('users/signin.Translate6') }}">
			  </div>
			</div>
			<div class="form-group">
			  <label for="pwd">{{ trans('users/signin.Translate7') }}:</label>
				<input type="password" class="form-control" name="password" placeholder="{{ trans('users/signin.Translate8') }}">
			</div>
			<button type="submit" class="btn btn-success">{{ trans('users/signin.Translate9') }}</button>
		</form>
		</br>
	   <a href="/{{ app()->getLocale() }}/password/remind/"> <button type="button" class="btn btn-danger"> {{ trans('users/signin.Translate10') }} </button> </a>
    </div>

	<br><br>

	<div class="container">
      <h2> {{ trans('users/signin.Translate11') }} </h2>
	  <h3>{{ trans('users/signin.Translate12') }}</h3>

	  <a href="/{{ app()->getLocale() }}/users/newaccount"> <button type="button" class="btn btn-danger"> {{ trans('users/signin.Translate13') }} </button> </a>
	 </div>
	 <br>

@stop
