@extends('layouts.main')

@section('title')
	{{ trans('users/editpassword.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('users/editpassword.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/users/editpassword/">
@stop

@section('content')

	<div class="container">

		<h1>{{ trans('users/editpassword.Translate3') }}</h1>

		@if(count($errors) > 0)
		<div class="alert alert-danger">
			<p> {{ trans('users/editpassword.Translate4') }}</p>
			<ul>
				@foreach($errors->all() as $error)
					<li> {!! $error !!}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<form method="POST" action="/{{ app()->getLocale() }}/users/editpassword" accept-charset="UTF-8" name="inputs" data-toggle="validator">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<?php $user = markmaster\Models\User::find(Auth::user()->id)?>
			<div class="form-group">
				  <label for="oldpassword">{{ trans('users/editpassword.Translate5') }}</label>
					<input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="{{ trans('users/editpassword.Translate6') }}" required>
					<div class="help-block with-errors"></div>
			</div>

			<div class="form-group">
			  <label for="password">{{ trans('users/editpassword.Translate7') }}</label>
				<input type="password" class="form-control" name="password" id="password" placeholder="{{ trans('users/editpassword.Translate8') }}" required>
				<div class="help-block with-errors"></div>
			</div>
			<div class="form-group">
			  <label for="password_confirmation">{{ trans('users/editpassword.Translate9') }}</label>
				<input type="password" class="form-control" name="password_confirmation" placeholder="{{ trans('users/editpassword.Translate10') }}" data-match="#password" data-match-error="OPS! Passordet stemmer ikke" required>
				<div class="help-block with-errors"></div>
			</div>

		<button type="submit" class="btn btn-danger">{{ trans('users/editpassword.Translate11') }}</button>
		{!! Form::close() !!}

	</div>

@stop
