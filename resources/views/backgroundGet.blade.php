<?php

if (Input::has('getBackground') && Input::get('getBackground') == 1 && Input::has('category') && Auth::check())
{
	if($stickerType == 6 || $stickerType == 7) {
		getBackground(Input::get('category'), 0);
	} else {
		getBackground(Input::get('category'), $stickerType);
	}
}
else if (Input::has('uploadBackground') && Input::get('uploadBackground') == 1 && Auth::check())
{
	$file = '/var/www/html/markmaster/log.txt';
	$content = 'GOT INTO FILE!';
	$bytesWritten = File::append($file, $content);

	$stickerType = 2;
	if (Input::has('stickerType')) {
		$stickerType = Input::get('stickerType');
	}
	uploadBackground($stickerType);
}


// called when user submits background to be uploaded and saved to database
function uploadBackground($colorValue)
{
	$validator = Validator::make(Input::all(), Background::$rules);
	if ($validator->fails())
	{
		echo 'failed';
		foreach($validator->getMessageBag()->toArray()['image'] as $msg)
		{
			echo $msg;
		}
		echo 'failed';
		// echo 'validator failed';
		// return;
		return Redirect::to('storeTest')
		->with('message', 'Failed to create clipart.');

		// return Response::json(array('success' => false,
									// 'errors' => $validator->getMessageBag()->toArray()
		// ), 400);
	}

	// ensure valid data
	if ($validator->passes()) {
		$background = new Background;
		$background->title = 'uploadedBackground';


		// if user logged in
		if (Auth::check())
		{
			// save user id with clipart
			$background->user_id = Auth::user()->getId();


			$image = Input::file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$destination = public_path() . '/img/background';
			Image::make($image->getRealPath())->save($destination .'/'. $filename); // Fix the resizing to keep original format here
			$background->image = 'img/background/'.$filename;

			if($colorValue == 3 || $colorValue == 4 || $colorValue == 5) { //grayscale image
				$background->color = 1;
				$file_name = $destination .'/'. $filename;
				$conversion = "convert " . $file_name . " -set colorspace gray -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_name;   //konverterer bilden til grayscale transp
				exec($conversion);

			} else if ($colorValue == 6 || $colorValue == 7) { //black and white image
				$background->color = 0;
				$file_name = $destination .'/'. $filename;
				$conversion = "convert " . $file_name . " -threshold 50% -negate -background black -alpha shape -background black -alpha background PNG32:" . $file_name;      //konverterer bilden til sort/hvit transp
				exec($conversion);

			} else { //full color
				$background->color = 2;
			}


			$background->save();


			echo $background->image;
			echo 'success';
			 // return Redirect::to('storeTest')
			 	// ->with('message', 'Background Created');
		}
	}
}

// echos out backgrounds styled with the class 'backgroundImg', and the onclick event 'addBackground'
function getBackground($category, $clipartType)
{
	if (!Auth::user())
		$category = 0;


	foreach(Background::orderBy('sortorder', 'ASC')->paginate(9999999) as $background)
	{
		if ($background->color <= $clipartType)
		{

			$name = '../../' . $background->image;
			$onclick = 'addBackground(\'' . $name . '\')';
			if ($category == 0) // show public clipart
			{
				if ($background->public == 1)
				{
					echo '<img src="', $name, '" name="', $name, '" class="backgroundImg" onclick="', $onclick, '">';
				}
			}
			else if ($category == 1) // show user's cilpart
			{
				if ($background->user_id==Auth::user()->getId() )
				{
					echo '<img src="', $name, '" name="', $name, '" class="backgroundImg" onclick="', $onclick, '">';
				}
			}
		}
	}
}

?>
