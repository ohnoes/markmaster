<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

      <title>@yield('title')</title>
		  @yield('head-meta')
      @yield('head-extra')
		  @yield('head-extra-styles')
		  @yield('head-keyword')

		  <meta name="google-site-verification" content="ZJ4s88xlf750cFbNDK636w-Ysw9WHwPTYWchddFtLI0" />
		  <meta name="google-site-verification" content="RrZwUO3WvBriUADm2Z8eJrMx0FAivm5c9w_Cu6Vns5Y" />
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  		{!! Html::script('js/jquery-1.11.0.min.js') !!}
  		{!! Html::style('css/bootstrap.css') !!}
  		{!! Html::style('css/markmaster.css') !!}
  		{!! Html::style('css/jquery.simplecolorpicker.css') !!}
      {!! Html::style('css/jquery.contextmenu.css') !!}
      {!! Html::style('css/fonts.css') !!}
  		{!! Html::style('css/fontselector.css') !!}
      {!! Html::style('css/fontload.css') !!}
      {!! Html::style('css/style.css') !!}
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  		<script>
  		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  		  ga('create', 'UA-854456-2', 'auto');
  		  ga('send', 'pageview');
  		</script>
    </head>
    <body>
      <!--[if lt IE 7]>
          <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
      <![endif]-->
      <nav class="navbar navbar-default navbar-inner" role="navigation">
          <div class="container-fluid">
              <div class="col-sm-8 col-sm-push-2 no-padding">
                  <div class="navbar-header" data-pg-collapsed>
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand navbar-logo" href="/{{ app()->getLocale() }}/">
                          <img src="/img/logo_ny2.png" alt="Markmaster AS">
                      </a>
                  </div>
                  <div class="collapse navbar-collapse no-padding" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right" data-pg-collapsed>
                        <li><a href="/{{ app()->getLocale() }}/">{{ trans('layouts/main.Homepage') }}</a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> {{ trans('layouts/main.Products') }} <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempsticker') }}"> {{ trans('layouts/main.Sticker') }} </a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempcheap') }}"> {{ trans('layouts/main.StickerBW') }} </a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempironc') }}"> {{ trans('layouts/main.Ironon') }} </a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempiron') }}"> {{ trans('layouts/main.IrononBW') }}</a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempgold') }}"> {{ trans('layouts/main.Gold') }} </a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempsolv') }}"> {{ trans('layouts/main.Silver') }} </a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.temptrans') }}"> {{ trans('layouts/main.Transparent') }} </a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempreflection') }}"> {{ trans('layouts/main.Reflection') }} </a> </li> <li class="divider"></li>
                          <li> <a href="/{{ app()->getLocale() }}/{{ trans('routes.tempironfree') }}"> {{ trans('layouts/main.Fabricsticker') }} </a>
                          </ul>
                        </li>

                        @if(Auth::check())
                					<li class="dropdown">
                					  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="fa fa-fw fa-lg fa-user"></b> {{ trans('layouts/main.User') }} <b class="caret"></b></a>
                					  <ul class="dropdown-menu" role="menu">
                						 @if(Auth::user()->admin == 1)
                							<li><a href="/{{ app()->getLocale() }}/admin/cliparts"> Upload Clipart </a></li>
                							<li><a href="/{{ app()->getLocale() }}/designs/tempgen"> Make Template </a></li>
                							<li><a href="/{{ app()->getLocale() }}/admin"> Admin Page </a></li> <li class="divider"></li>
                						 @endif
                							<li> <a href="/{{ app()->getLocale() }}/stickers/"> {{ trans('layouts/main.UserStickers') }} </a></li> <li class="divider"></li>
                							<li> <a href="/{{ app()->getLocale() }}/order/"> {{ trans('layouts/main.UserOrders') }} </a></li> <li class="divider"></li>
                							<li> <a href="/{{ app()->getLocale() }}/users/editaccount"> {{ trans('layouts/main.UserAccount') }} </a></li>
                							<li> <a href="/{{ app()->getLocale() }}/users/signout"> {{ trans('layouts/main.Logout') }} </a></li>
                					  </ul>
                					</li>
                				@else
                					<li class="dropdown">
                						<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-user"></b> {{ trans('layouts/main.Login') }}<b class="caret"></b></a>
                						<ul class="dropdown-menu">
                							<li> <a href="/{{ app()->getLocale() }}/users/signin"> {{ trans('layouts/main.UserLogin') }} </a></li> <li class="divider"></li>
                							<li> <a href="/{{ app()->getLocale() }}/users/newaccount"> {{ trans('layouts/main.NewAccount') }} </a></li>
                						</ul>
                					</li>
                				@endif

                        <!-- TODO FIX CART HERE -->
                        <li><a href="/{{ app()->getLocale() }}/store/cart"><b class="fa fa-fw fa-lg fa-shopping-basket"></b> {{ trans('layouts/main.ShoppingCart') }}</a></li>

                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-globe"></b> Language <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                              <?php $arr = config('translatable.locales'); $numItems = count($arr); $i = 0; ?>
                                @foreach (config('translatable.locales') as $lang => $language)
                                    @if ($lang != app()->getLocale())
                                        <li>
                                            <a href="{{ route('lang.switch', $lang) }}" rel="nofollow">
                                                {{ $language }}
                                            </a> <?php if(++$i === $numItems-1) { echo "</li>"; } else { echo "</li> <li class='divider'></li>"; }?>
                                    @endif
                                @endforeach
                            </ul>
                        </li>

                        <li class="dropdown">
                        @if(Auth::check())
                          @if(Auth::user()->currency == 0)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " NOK  " ?><b class="caret"></b></a>
                          @elseif(Auth::user()->currency == 1)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " USD  " ?><b class="caret"></b></a>
                          @elseif(Auth::user()->currency == 2)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " SEK  " ?><b class="caret"></b></a>
                          @elseif(Auth::user()->currency == 3)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " DKK  " ?><b class="caret"></b></a>
                          @elseif(Auth::user()->currency == 4)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " EUR  " ?><b class="caret"></b></a>
                          @endif
                        @else
                          @if(Session::get('theCurrency', 0) == 0)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " NOK  " ?><b class="caret"></b></a>
                          @elseif(Session::get('theCurrency') == 1)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " USD  " ?><b class="caret"></b></a>
                          @elseif(Session::get('theCurrency') == 2)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " SEK  " ?><b class="caret"></b></a>
                          @elseif(Session::get('theCurrency') == 3)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " DKK  " ?><b class="caret"></b></a>
                          @elseif(Session::get('theCurrency') == 4)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <b class="fa fa-fw fa-lg fa-money"></b> <?php echo " EUR  " ?><b class="caret"></b></a>
                          @endif
                        @endif
                            <ul class="dropdown-menu" role="menu">
                                <li> <a rel="nofollow" href="/{{ app()->getLocale() }}/store/currency/0"> <img src="/img/flag/no.png" alt="flag" class="thumbnailpic" height="20px"> <?php echo " Norwegian Kroner " ?> </a> </li> <li class="divider"></li>
                                <li> <a rel="nofollow" href="/{{ app()->getLocale() }}/store/currency/1"> <img src="/img/flag/en.png" alt="flag" class="thumbnailpic" height="20px"> <?php echo " US Dollar " ?> </a> </li> <li class="divider"></li>
                                <li> <a rel="nofollow" href="/{{ app()->getLocale() }}/store/currency/2"> <img src="/img/flag/se.png" alt="flag" class="thumbnailpic" height="20px"> <?php echo " Swedish Kroner " ?> </a> </li> <li class="divider"></li>
                                <li> <a rel="nofollow" href="/{{ app()->getLocale() }}/store/currency/3"> <img src="/img/flag/dk.png" alt="flag" class="thumbnailpic" height="20px"> <?php echo " Danish Kroner " ?> </a> </li> <li class="divider"></li>
                                <li> <a rel="nofollow" href="/{{ app()->getLocale() }}/store/currency/4"> <img src="/img/flag/euro.png" alt="flag" class="thumbnailpic" height="20px"> <?php echo " Euros " ?> </a> </li>
                            </ul>
                        </li>
                      </ul>
                  </div>
              </div>
          </div>
      </nav>
        <div class="wrapper">
            <ul id="fontLoad" class="dropdown">
                <li id="boogaloottf" class="selected">Boogaloo</li>
                <li id="boogaloottfb" >BoogalooB</li>
                <li id="boogaloottfi" >BoogalooI</li>
                <li id="boogaloottfbi" >BoogalooBI</li>
                <li id="crimsontextttf">Crimson Text</li>
                <li id="crimsontextttfb">Crimson TextB</li>
                <li id="crimsontextttfi">Crimson TextI</li>
                <li id="crimsontextttfbi">Crimson TextBI</li>
                <li id="englandttf">England</li>
                <li id="englandttfb">EnglandB</li>
                <li id="englandttfi">EnglandI</li>
                <li id="englandttfbi">EnglandBI</li>
                <li id="felipattf">Felipa</li>
                <li id="felipattfb">FelipaB</li>
                <li id="felipattfi">FelipaI</li>
                <li id="felipattfbi">FelipaBI</li>
                <li id="goblinonettf"> Goblin One </li>
                <li id="gravitasonettf">Gravitas One</li>
                <li id="gravitasonettfb">Gravitas OneB</li>
                <li id="gravitasonettfi">Gravitas OneI</li>
                <li id="gravitasonettfbi">Gravitas OneBI</li>
                <li id="greatvibesttf">Great Vibes</li>
                <li id="greatvibesttfb">Great VibesB</li>
                <li id="greatvibesttfi">Great VibesI</li>
                <li id="greatvibesttfbi">Great VibesBI</li>
                <li id="hammersmithonettf">Hammersmith One</li>
                <li id="hammersmithonettfb">Hammersmith OneB</li>
                <li id="hammersmithonettfi">Hammersmith OneI</li>
                <li id="hammersmithonettfbi">Hammersmith OneBI</li>
                <li id="hennepennyttf"> Henny Penny </li>
                <li id="hennepennyttfb"> Henny PennyB </li>
                <li id="hennepennyttfi"> Henny PennyI </li>
                <li id="hennepennyttfbi"> Henny PennyBI </li>
                <li id="kaushanscriptttf"> KaushanScript </li>
                <li id="kaushanscriptttfb"> KaushanScriptB </li>
                <li id="kaushanscriptttfi"> KaushanScriptI </li>
                <li id="kaushanscriptttfbi"> KaushanScriptBI </li>
                <li id="leaguegothicttf"> Leaguegothic </li>
                <li id="leaguegothicttfb"> Leaguegothic </li>
                <li id="leaguegothicttfi"> Leaguegothic </li>
                <li id="leaguegothicttfbi"> Leaguegothic </li>
                <li id="limelightttf"> Limelight </li>
                <li id="limelightttfb"> Limelight </li>
                <li id="limelightttfi"> Limelight </li>
                <li id="limelightttfbi"> Limelight </li>
                <li id="lobstertwottf"> Lobster Two </li>
                <li id="lobstertwottfb"> Lobster Two </li>
                <li id="lobstertwottfi"> Lobster Two </li>
                <li id="lobstertwottfbi"> Lobster Two </li>
                <li id="maidenoragettf"> Maidenorange </li>
                <li id="maidenoragettfb"> Maidenorange </li>
                <li id="maidenoragettfi"> Maidenorange </li>
                <li id="maidenoragettfbi"> Maidenorange </li>
                <li id="nunitottf"> Nunito </li>
                <li id="nunitottfb"> Nunito </li>
                <li id="nunitottfi"> Nunito </li>
                <li id="nunitottfbi"> Nunito </li>
                <li id="robotottf"> Roboto </li>
                <li id="robotottfb"> Roboto </li>
                <li id="robotottfi"> Roboto </li>
                <li id="robotottfbi"> Roboto </li>
                <li id="robotocondensedttf"> Roboto Condensed </li>
                <li id="robotocondensedttfb"> Roboto Condensed </li>
                <li id="robotocondensedttfi"> Roboto Condensed </li>
                <li id="robotocondensedttfbi"> Roboto Condensed </li>
            </ul>

    		<?php
    				$ip = \Request::getClientIp();

    				//$thestring = 'http://api.wipmania.com/' . $ip . '?' . 'markmaster.com';
    				//$country = file_get_contents($thestring);

    				$country = 'DE';
    				//echo $country;
    		?>
        @if (Auth::check())
      		<?php $id = Auth::user()->id; $currentuser = markmaster\Models\User::find($id); ?>
      		{{ trans('layouts/main.loggedinas') }}
      		<b>{!! $currentuser->email !!}</b>
      		@if(Auth::user()->admin == 1)
      		  <a href="/{{ app()->getLocale() }}/admin/preview"> Preview Link</a>
      		@endif
		    @endif
        <section id="main-content" class="clearfix">
  				<span id="messageSpan">
  				   @if (Session::has('messageLoginSuccess'))
  					   <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
  				   @endif
             @if (Session::has('message'))
               <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
             @endif
			    </span>
  				@yield('promo')
  				@yield('search-keyword')
          @yield('content')
        </section><!-- end main-content -->
        @yield('pagination')
        </div><!-- end wrapper -->

        <div class="row bottom-row" data-pg-collapsed>
            <div class="col-sm-8 col-sm-push-2">
                <div class="col-sm-3">
                    <h3>MARKMASTER AS</h3>
                    <p>
                    {{ trans('layouts/main.orgnumber') }} 97955 458 </br>
                    {{ trans('layouts/main.address') }} </br>
                    {{ trans('layouts/main.address2') }} </br>
                    {{ trans('layouts/main.country') }} </br>
                    {{ trans('layouts/main.phone') }}</br>
                    post@markmaster.com</p>
                    <p id="store-copy">&copy; 2014 Markmaster</p>
                </div>
                <div class="col-sm-3">
                    <h3>{{ trans('layouts/main.Social') }}</h3>
                    <p>
                    <a href="http://www.facebook.com/markmasteras" target="_blank" style="color: black;"> {{ trans('layouts/main.Facebook') }} </a> <br>
                    <a href="https://twitter.com/MarkmasterAS" target="_blank" style="color: black;"> {{ trans('layouts/main.Twitter') }} </a>  <br>
                    Instagram <br></p>
                </div>
                <div class="col-sm-3">
                    <h3> {{ trans('layouts/main.Help') }} </h3>
                    <p>
                    <a href="/{{ app()->getLocale() }}/help/faq" style="color: black;"> {{ trans('layouts/main.FAQ') }} </a> <br>
                    <a href="/{{ app()->getLocale() }}/help/video" style="color: black;"> {{ trans('layouts/main.Video') }} </a>  <br>
                    <a href="/{{ app()->getLocale() }}/help/design" style="color: black;"> {{ trans('layouts/main.CommercialAgents') }} </a> <br>
                    <a href="/{{ app()->getLocale() }}/help/contact" style="color: black;"> {{ trans('layouts/main.Contact') }} </a> <br>
                    <a href="/{{ app()->getLocale() }}/help/conditions" style="color: black;"> {{ trans('layouts/main.Terms') }} </a> <br></p>
                </div>
                <div class="col-sm-3">
                    <h3>{{ trans('layouts/main.Payment') }}</h3>
                    <img src="/img/payment-methods.gif" alt="">
                </div>
            </div>
        </div>

		{!! Html::script('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js') !!}
    {!! Html::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js') !!}
    {!! Html::script('js/plugins.js') !!}
    {!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
		{!! Html::script('js/bootstrap.js') !!}
		{!! Html::script('js/validator.min.js')!!}
    {!! Html::script('js/ie10-viewport-bug-workaround.js')!!}
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <!-- script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script -->
    </body>
</html>
