@extends('layouts.main')

@section('title')
    {{ trans('help/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('help/index.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/help/">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<div class="container">
		<h2 align="center"> {{ trans('help/index.Translate3') }} </h2>

		<div class="row-fluid">
			<div class="col-sm-6 col-md-2 col-lg-2">
				<a href="/{{ app()->getLocale() }}/help/faq"> <button type="button" class="btn btn-danger help-btn">{{ trans('help/index.Translate4') }}</button> </a>
			</div>
			<div class="col-sm-6 col-md-2 col-lg-2">
				<a href="/{{ app()->getLocale() }}/help/video"> <button type="button" class="btn btn-danger help-btn">{{ trans('help/index.Translate5') }}</button> </a>
			</div>
			<div class="col-sm-6 col-md-2 col-lg-2">
				<a href="/{{ app()->getLocale() }}/help/design"> <button type="button" class="btn btn-danger help-btn">{{ trans('help/index.Translate6') }}</button> </a>
			</div>
			<div class="col-sm-6 col-md-2 col-lg-2">
				<a href="/{{ app()->getLocale() }}/conditions"> <button type="button" class="btn btn-danger help-btn">{{ trans('help/index.Translate7') }}</button> </a>
			</div>
			<div class="col-sm-6 col-md-2 col-lg-2">
				<a href="/{{ app()->getLocale() }}/help/contact"> <button type="button" class="btn btn-danger help-btn">{{ trans('help/index.Translate8') }}</button> </a>
			</div>
		</div>
	</div>

@stop
