@extends('layouts.main')

@section('title')
    {{ trans('help/design.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('help/design.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/help/design">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')
	<span class="hidden-xs">
		<div class="container">
				<h2>{{ trans('help/design.Translate2') }} </h2>
		</div>
	</span>

	<span class="visible-xs">
		<div class="container">
				<h2>{{ trans('help/design.Translate3') }} </h2>
		</div>
	</span>
	</br>

		@foreach($agents as $agent)

		<span class="hidden-xs">
			<div class="container">
				<div class="row-fluid">
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->name !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->address !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->contactname !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! ' - ' !!}
					</div>

				</div>
				<div class="row-fluid">
					<div class="col-sm-6 col-md-3 col-lg-3">
						<?php
							echo "<a href='http://$agent->url'> $agent->url </a>"
						?>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->zip !!} {!! ' ' . $agent->city !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! 'Tlf: ' . $agent->contacttelephone !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->contactmail !!}
					</div>
				</div>
			</div>

		</span>
		<span class="visible-xs">
			 <div class="container">
				<button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#<?php echo $agent->id ?>"> {!! $agent->name !!} <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button>
			 <!-- <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"> {!! $agent->name !!} </button> -->
			  <div id="<?php echo $agent->id ?>" class="collapse">
				</br>
				<div class="row-fluid">
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->name !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						<?php
						echo "<a href='http://$agent->url'> $agent->url </a>"
						?>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->contactname !!}
					</div>

				</div>
				<div class="row-fluid">
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->address !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->zip !!} {!! ' ' . $agent->city !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! 'Tlf: ' . $agent->contacttelephone !!}
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3">
						{!! $agent->contactmail !!}
					</div>
				</div>
			  </div>
			</div>
		</span>
		<hr>
		@endforeach

@stop
