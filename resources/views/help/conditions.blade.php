@extends('layouts.main')

@section('title')
    {{ trans('help/conditions.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('help/conditions.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/help/conditions">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<div class="container">

		<h3>{{ trans('help/conditions.Translate3') }}</h3>
		{{ trans('help/conditions.Translate4') }}</p>

		<p>
		<h3>{{ trans('help/conditions.Translate5') }}</h3>
		{{ trans('help/conditions.Translate6') }}.</p>

		<p>
		<h3>{{ trans('help/conditions.Translate7') }}</h3>
		{{ trans('help/conditions.Translate8') }}</p>

		<p>
		<h3>{{ trans('help/conditions.Translate9') }}</h3>
		{{ trans('help/conditions.Translate10') }}</p>

		<p>
		<h3>{{ trans('help/conditions.Translate11') }}</h3>
		{{ trans('help/conditions.Translate12') }}
		</p>

	</div>

@stop
