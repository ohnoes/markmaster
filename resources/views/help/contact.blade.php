@extends('layouts.main')

@section('title')
	{{ trans('help/contact.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('help/contact.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/help/contact">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<div class="container">
		<h2 align="center"> {{ trans('help/contact.Translate3') }} </h2>
		<p align="center"> {{ trans('help/contact.Translate4') }}</p>
		<div class="row-fluid">
			<div class="col-sm-12 col-md-12 col-lg-12 contact">
				<p class="contact-text"> {{ trans('help/contact.Translate5') }} <a href="mailto:post@markmaster.com" class="contact-link"> post@markmaster.com </a> </p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-sm-12 col-md-12 col-lg-12 contact">
				<p class="contact-text"> {{ trans('help/contact.Translate6') }} </p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-sm-12 col-md-12 col-lg-12 contact">
				<p class="contact-text"> Facebook: <a href="http://www.facebook.com/markmasteras" target="_blank" class="contact-link">Markmaster Facebook</a> </p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-sm-12 col-md-12 col-lg-12 contact">
				<p class="contact-text"> Twitter: <a href="https://twitter.com/MarkmasterAS" target="_blank" class="contact-link"> Markmaster Twitter </a> </p>
			</div>
		</div>
	</div>
@stop
