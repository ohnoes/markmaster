@extends('layouts.main')

@section('title')
    {{ trans('help/video.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('help/video.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/help/video">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<div class="container">
		<p align="center">
			<h1> {{ trans('help/video.Translate3') }} </h1>
		</p>

		<div class="row-fluid">
		  <div class="col-sm-12 col-md-12 col-lg-12">
			<div class="flex-video widescreen"><iframe src="//www.youtube.com/embed/He8cUzrw9YU" frameborder="0" allowfullscreen=""></iframe></div>
		  </div>
		</div>
	</div>
@stop
