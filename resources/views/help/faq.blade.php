@extends('layouts.main')

@section('title')
    {{ trans('help/faq.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('help/faq.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/help/faq">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<div class="container">

	<h1>{{ trans('help/faq.Translate3') }} </h1>
	<br>
	<p>
		<i>	{{ trans('help/faq.Translate4') }} </i><br>
			{{ trans('help/faq.Translate5') }}

	</p>
	<p>
		<i>	{{ trans('help/faq.Translate6') }} </i><br>
			{{ trans('help/faq.Translate7') }}

	</p>
	<p>
		<i> {{ trans('help/faq.Translate8') }}  </i><br>
			{{ trans('help/faq.Translate9') }}

	</p>
	<p>
		<i> {{ trans('help/faq.Translate10') }}  </i><br>
			{{ trans('help/faq.Translate11') }}

	</p>
	<p>
		<i> {{ trans('help/faq.Translate12') }}  </i><br>
			{{ trans('help/faq.Translate13') }}

	</p>
	<p>
		<i> {{ trans('help/faq.Translate14') }}  </i><br>
			{{ trans('help/faq.Translate15') }}

	</p>
	<p>
		<i> {{ trans('help/faq.Translate16') }}  </i><br>
			{{ trans('help/faq.Translate17') }}

	</p>
	<p>
		<i> {{ trans('help/faq.Translate18') }}  </i><br>
			{{ trans('help/faq.Translate19') }}

	</p>
	<p>
		<i> {{ trans('help/faq.Translate20') }}  </i><br>
			{{ trans('help/faq.Translate21') }}

	</p>


	</div>

@stop
