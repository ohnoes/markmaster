@extends('layouts.main')

@section('content')

<!--   @if (!Auth::user())
     {!! Redirect::to('users/signin')->with('message', 'Du må være innlogget om du ønsker å laste opp bakgrunner eller bilder') !!}
  @endif -->

<div class="container"> <!-- Main container -->
	<h1 class="tempHeader">Klistreetikett med laminatforsterkning</h1>
	<div id="container"> <!-- Grid container -->
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 design-tools-canvas"> <!-- Canvas block -->
			<!-- Canvas section -->
				<div id="container">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div id="canvasDiv">
								<canvas id="myCanvas" width="600" height="600"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 design-tools"> <!-- Design controll block -->
				<!-- Tab section (left side) -->
				<ul id="tabs" class="nav nav-tabs nav-justified nav-margin" data-tabs="tabs">
					<li class="active"><a href="#sizeAndShapeControl" data-toggle="tab">Størrelse og form</a></li>
					<li><a href="#backgroundColorControl" data-toggle="tab">Bakgrunn og farge</a></li>
					<li><a href="#textControl" data-toggle="tab">Tekst </a></li>
					<li><a href="#clipartControl" data-toggle="tab">Symboler og bilder</a></li>
					@if($sticker->stickertype == 1)
						<li onclick="rdycheckout(1)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@elseif($sticker->stickertype == 2)
						<li onclick="rdycheckout(2)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@elseif($sticker->stickertype == 3)
						<li onclick="rdycheckout(3)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@elseif($sticker->stickertype == 4)
						<li onclick="rdycheckout(4)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@elseif($sticker->stickertype == 5)
						<li onclick="rdycheckout(5)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@elseif($sticker->stickertype == 6)
						<li onclick="rdycheckout(6)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@elseif($sticker->stickertype == 7)
						<li onclick="rdycheckout(7)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@elseif($sticker->stickertype == 8)
						<li onclick="rdycheckout(8)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
						@elseif($sticker->stickertype == 9)
						<li onclick="rdycheckout(9)"><a href="#cartControl" data-toggle="tab">Legg i Handlevogn</a></li>
					@endif
				</ul>
				<div id="my-tab-content" class="tab-content tab-padding">
					<div class="tab-pane active" id="sizeAndShapeControl"> <!-- Size&Shape tab -->
						<?php
							$type = $sticker->stickertype;
						?>

						{!! Form::hidden('size', 'size', array('id'=>'3716')) !!}
						{!! Form::hidden('size', 'size', array('id'=>'6026')) !!}
						{!! Form::hidden('size', 'size', array('id'=>'3010')) !!}
						<input type="hidden" name="stickertype" id="stickertype" value="<?php echo $type ?>">

						<p>
						  {!! Form::radio('shape','','1',array('id'=>'isRect')) !!}
						  {!! Html::image('img/rektangel.png', 'Rektangel', array('onclick'=>'makeRect()' )) !!}
						  {!! Form::label('isRect','Rektangel') !!}
						</p>
						<p>
						  {!! Form::radio('shape','','',array('id'=>'isEllipse')) !!}
						  {!! Html::image('img/elipse.png', 'Elipse', array('onclick'=>'makeElipse()' )) !!}
						  {!! Form::label('isElipse','Ellipse') !!}
						</p>
						<p>

								{!! Form::text('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
								{!! Form::label('bredde mm') !!}
								<br/>
								{!! Form::text('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
								{!! Form::label('høyde mm') !!}


						<br/>
						  *Bredde og høyde må være mellom 6-300mm
						</p>
						<p>

						@if($sticker->stickertype == 6 || $sticker->stickertype == 7)
							{!! Form::hidden('shSticker', 'true', array('id'=>'shSticker', 'value'=>'true')) !!}
							{!! Form::hidden('quantity',$sticker->quantity, array('id' => 'quantity', )) !!}
							<p>
								<select name="Antall" id="Antall">
									  <option value="1">1 stk.</option>
									  <option value="5">5 stk.</option>
									  <option value="10">10 stk.</option>
									  <option value="20">20 stk.</option>
									  <option value="50">50 stk.</option>
									  <option value="75">75 stk.</option>
									  <option value="100">100 stk.</option>
									  <option value="250">250 stk.</option>
									  <option value="500">500 stk.</option>
									  <option value="1000">1000 stk.</option>
								</select>
							</p>
						@else
							{!! Form::hidden('shSticker', 'false', array('id'=>'shSticker', 'value'=>'false')) !!}
							{!! Form::text('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}


							{!! Form::label('Antall') !!}
						@endif

					   <br/><br/>
						  {!! Form::label('thePrice', 'Pris: ', array('id'=>'thePrice')) !!}
						</p>
					</div>
					<div class="tab-pane" id="backgroundColorControl"> <!-- Background tab -->
						<p>
							{!! Form::label('color', 'Velg farge fra tabell:') !!}
						</p>
						<span id="backgroundPicker">
							{!! Form::select('backgroundColorPicker', array(
								'#000000' => 'Black',
								'#ffffff' => 'White',
								'#E6E6E6' => '10% Black',
								'#B1B1B1' => '40% Black',
								'#888887' => '60% Black',
								'#5C5C5B' => '80% Black',
								'#EBB5C3' => 'PANTONE 182 C',
								'#C8112E' => 'PANTONE 185 C',
								'#B01D2B' => 'PANTONE 1797 C',
								'#871630' => 'PANTONE 201 C',
								'#E6D5A8' => 'PANTONE 155 C',
								'#E9954A' => 'PANTONE 804 C',
								'#E64A00' => 'PANTONE Orange 021 C',
								'#EAEBBC' => 'PANTONE 607 C',
								'#EFED84' => 'PANTONE 100 C',
								'#EFE032' => 'PANTONE Yellow C',
								'#C9D8E7' => 'PANTONE 290 C',
								'#8ACBE5' => 'PANTONE 305 C',
								'#1A35A8' => 'PANTONE 286 C',
								'#0F2867' => 'PANTONE 281 C',
								'#549AA3' => 'PANTONE 320 C',
								'#EACDCF' => 'PANTONE 698 C',
								'#E8A3D0' => 'PANTONE 230 C',
								'#B50970' => 'PANTONE 226 C',
								'#D7CAE3' => 'PANTONE 263 C',
								'#9E70C1' => 'PANTONE 528 C',
								'#680E92' => 'PANTONE 527 C',
								'#BC8F70' => 'PANTONE 7515 C',
								'#9E520F' => 'PANTONE 471 C',
								'#B6DD8E' => 'PANTONE 358 C',
								'#A4D426' => 'PANTONE 375 C',
								'#61AE56' => 'PANTONE 354 C',
								'#4A7229' => 'PANTONE 364 C',
								'#cfb53b' => 'Gold-like',
								'#d7d8d8' => 'Silver-like'),
								'#000000',
								array('id' => 'backgroundColor' ))
							!!}
						</span>
						<br/><br/>
						{!! 'eller velg/last opp en bakgrunn' !!}
						<br>

						{!! Form::select('backCategory', array(
							'0' => 'Bakgrunner',
							'1' => 'Mine Bakgrunner'),
							'0',
							array('id' => 'backCategory'))
						!!}
						 <span id="backgroundShow">
						  <?php include '../app/views/adminbackgroundGet.blade.php'; getBackground(0); ?>
						</span>
						<br/><br/>
						{!! Form::label('uploadLine', 'Last opp egen bakgrunn') !!}
						<br/><br/>
						<span class="uploadControls">
						<span class="uploadControls">


							{!! Form::open(array('files' => true, 'id' => 'adminbackgroundForm')) !!}
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
							@if (Auth::user())
								<div class="form-group">
									{!! Form::file('image', array('id' => 'backgroundFileSelect')) !!}
								</div>
								<div class="form-group">
									{!! Form::submit('Last opp Bakgrunn', array('class'=>'payBox')) !!}
								</div>
							@else
								<div class="form-group">
									{!! Form::file('image', array('disabled' => '', 'id' => 'backgroundFileSelect')) !!}
								</div>
								<div class="form-group">
									{!! Form::submit('Last opp Bakgrunn', array('disabled' => '', 'class'=>'payBox')) !!}
								</div>
							@endif
							{!! Form::close() !!}
						</span>
						<br/><br/><br/>
					</div>
					<div class="tab-pane" id="textControl"> <!-- Text tab -->
						<div class="row"> <!-- text box + add and delete buttons -->
							<div class="col-sm-9 col-md-9 col-lg-9">
								{!! Form::textarea('stickerText','Legg inn tekst her!', array('id' => 'stickerText', 'oninput' => 'setText()')) !!}
							</div>
							<div class="col-sm-3 col-md-3 col-lg-3">
									{!! form::button('Legg til', array('onclick' => 'setTextInput()', 'id' => 'addTextButton' )) !!}
									{!! form::button('Slett', array('onclick' => 'deleteText()', 'id' => 'deleteTextButton' )) !!}
							</div>
						</div>
						<div class="row"> <!-- Text formatting controlls & font -->
							<div class="col-sm-12 col-md-12 col-lg-12">
								{!! Form::label('textAlign', 'Tekst Format', array('id' => 'textAlign')) !!}
								<span class="glyphicon glyphicon-bold textIconBox" aria-hidden="true" onclick="setTextBold()"></span>
								<span class="glyphicon glyphicon-italic textIconBox" aria-hidden="true" onclick="setTextItalic()"></span>

								<span class="glyphicon glyphicon-fire textIconBox" onclick="setTextUnderline()"></span> <!-- Custom glyph-icon for underline -->

								<span class="glyphicon glyphicon-align-left textIconBox" aria-hidden="true" onclick="setTextAlign('left')"></span>
								<span class="glyphicon glyphicon-align-center textIconBox" aria-hidden="true" onclick="setTextAlign('center')"></span>
								<span class="glyphicon glyphicon-align-right textIconBox" aria-hidden="true" onclick="setTextAlign('right')"></span>

								<span id="color">
									{!! Form::label('color', 'Farge') !!}
									{!! Form::select('colorpickerRaise', array(
									   '#000000' => 'Black',
										'#ffffff' => 'White',
										'#E6E6E6' => '10% Black',
										'#B1B1B1' => '40% Black',
										'#888887' => '60% Black',
										'#5C5C5B' => '80% Black',
										'#EBB5C3' => 'PANTONE 182 C',
										'#C8112E' => 'PANTONE 185 C',
										'#B01D2B' => 'PANTONE 1797 C',
										'#871630' => 'PANTONE 201 C',
										'#E6D5A8' => 'PANTONE 155 C',
										'#E9954A' => 'PANTONE 804 C',
										'#E64A00' => 'PANTONE Orange 021 C',
										'#EAEBBC' => 'PANTONE 607 C',
										'#EFED84' => 'PANTONE 100 C',
										'#EFE032' => 'PANTONE Yellow C',
										'#C9D8E7' => 'PANTONE 290 C',
										'#8ACBE5' => 'PANTONE 305 C',
										'#1A35A8' => 'PANTONE 286 C',
										'#0F2867' => 'PANTONE 281 C',
										'#549AA3' => 'PANTONE 320 C',
										'#EACDCF' => 'PANTONE 698 C',
										'#E8A3D0' => 'PANTONE 230 C',
										'#B50970' => 'PANTONE 226 C',
										'#D7CAE3' => 'PANTONE 263 C',
										'#9E70C1' => 'PANTONE 528 C',
										'#680E92' => 'PANTONE 527 C',
										'#BC8F70' => 'PANTONE 7515 C',
										'#9E520F' => 'PANTONE 471 C',
										'#B6DD8E' => 'PANTONE 358 C',
										'#A4D426' => 'PANTONE 375 C',
										'#61AE56' => 'PANTONE 354 C',
										'#4A7229' => 'PANTONE 364 C',
										'#cfb53b' => 'Gold-like',
										'#d7d8d8' => 'Silver-like'),
										'#000000',
										array('id' => 'fill'))
									!!}
								</span>

								<div class="fontReset">

									<ul id="fontList" class="dropdown">
										<li id="boogaloottf" class="selected">Boogaloo</li>
										<li id="crimsontextttf">Crimson Text</li>
										<li id="englandttf">England</li>
										<li id="felipattf">Felipa</li>
										<li id="goblinonettf"> Goblin One </li>
										<li id="gravitasonettf">Gravitas One</li>
										<li id="greatvibesttf">Great Vibes</li>
										<li id="hammersmithonettf">Hammersmith One</li>
										<li id="hennepennyttf"> Henny Penny </li>
										<li id="kaushanscriptttf"> KaushanScript </li>
										<li id="leaguegothicttf"> Leaguegothic </li>
										<li id="limelightttf"> Limelight </li>
										<li id="lobstertwottf"> Lobster Two </li>
										<li id="maidenoragettf"> Maidenorange </li>
										<li id="nunitottf"> Nunito </li>
										<li id="robotottf"> Roboto </li>
										<li id="robotocondensedttf"> Roboto Condensed </li>
									</ul>
								</div>

								{!! Form::label('fontFamily', ' ') !!}

								{!! Form::select('fontFamily', array(
										  'boogaloottf' => 'boogaloottf',
										  'crimsontextttf' => 'crimsontextttf',
										  'englandttf' => 'englandttf',
										  'felipattf' => 'felipattf',
										  'goblinonettf' => 'goblinonettf',
										  'gravitasonettf' => 'gravitasonettf',
										  'greatvibesttf' => 'greatvibesttf',
										  'hammersmithonettf' => 'hammersmithonettf',
										  'hennepennyttf' => 'hennepennyttf',
										  'kaushanscriptttf' => 'kaushanscriptttf',
										  'leaguegothicttf' => 'leaguegothicttf',
										  'limelightttf' => 'limelightttf',
										  'lobstertwottf' => 'lobstertwottf',
										  'maidenoragettf' => 'maidenoragettf',
										  'nunitottf' => 'nunitottf',
										  'robotottf' => 'robotottf',
										  'robotocondensedttf' => 'robotocondensedttf')) !!}
							</div>
						</div>
						<div class="row"> <!-- Size, linedistance, rotation & position -->
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center">
								<div class="row">
									{!! Form::label('fontSize', 'Størrelse')!!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-resize-full textIconBoxControll" aria-hidden="true"
									onclick="increaseFont()"
									onmousedown="inter=setInterval(increaseFont, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-plus" aria-hidden="true" >
										  </span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('fontSizeTextbox', '10', array('id' => 'fontSizeTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-resize-small textIconBoxControll" aria-hidden="true"
									onclick="decreaseFont()"
									onmousedown="inter=setInterval(decreaseFont, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-minus" aria-hidden="true">  </span>
									</span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center">
								<div class="row">
									{!! Form::label('lineHeight', 'Linje avstand')!!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-gift textIconBoxControll" aria-hidden="true"
									onclick="increaseLineHeight()"
									onmousedown="inter=setInterval(increaseLineHeight, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('lineHeightTextbox', '0.95', array('id' => 'lineHeightTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-leaf textIconBoxControll" aria-hidden="true"
									onclick="decreaseLineHeight()"
									onmousedown="inter=setInterval(decreaseLineHeight, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-minus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center">
								<div class="row">
									{!! Form::label('lineRotate', 'Rotasjon')!!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-repeat textIconBoxControll"
									onclick="increaseTextRotation()"
									onmousedown="inter=setInterval(increaseTextRotation, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('textAngleTextbox', '0', array('id' => 'textAngleTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-minus textIconBoxControll" id="flipped-icon"
									onclick="decreaseTextRotation()"
									onmousedown="inter=setInterval(decreaseTextRotation, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-repeat" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center">
								<div class="row">
									Posisjon
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-top textIconBox" aria-hidden="true"
									onclick="moveObjectTop()"
									onmousedown="inter=setInterval(moveObjectTop, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
									</span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-left textIconBox" aria-hidden="true"
									onclick="moveObjectLeft()"
									onmousedown="inter=setInterval(moveObjectLeft, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span></div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-right textIconBox" aria-hidden="true"
									onclick="moveObjectRight()"
									onmousedown="inter=setInterval(moveObjectRight, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span> </div>
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-bottom textIconBox" aria-hidden="true"
									onclick="moveObjectDown()"
									onmousedown="inter=setInterval(moveObjectDown, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
								</div>
							</div>
						</div>
						<div class="row"> <!-- Center,Layer & line thickness -->
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-breathingroom" style="text-align: center">
								<div id="row">
									{!! Form::label('centerObject', 'Sentrer Teksten') !!}
								</div>
								<div id="row">
									<span class="glyphicon glyphicon-eye-open textIconBox" aria-hidden="true" onclick="centerObjectV()"></span>
									<span class="glyphicon glyphicon-eye-close textIconBox" aria-hidden="true" onclick="centerObjectH()"></span>
								</div>
								<div id="row">
									{!! Form::label('layerControl', 'Layer Kontroll') !!}
								</div>
								<div id="row">
									<!--<span class="glyphicon glyphicon-fast-backward textIconBox" aria-hidden="true" onclick="sendToBack()"></span>-->
									<span class="glyphicon glyphicon-step-backward textIconBox" aria-hidden="true" onclick="moveDown()"></span>
									<span class="glyphicon glyphicon-step-forward textIconBox" aria-hidden="true" onclick="moveUp()"></span>
									<!--<span class="glyphicon glyphicon-fast-forward textIconBox" aria-hidden="true" onclick="bringToFront()"></span>-->
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-breathingroom" style="text-align: center">
								<div id="row">
									{!! Form::label('strokeWidth', 'Tykkelse')!!}
								</div>
								<div id="row">
									<span class="glyphicon glyphicon-plus textIconBoxControll" aria-hidden="true"
									onclick="increaseTextStroke()"
									onmousedown="inter=setInterval(increaseTextStroke, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span>
								</div>
								<div id="row">
									{!! Form::text('strokeWidthTextbox', '0.01', array('id' => 'strokeWidthTextbox')) !!}
								</div>
								<div id="row">
									<span class="glyphicon glyphicon-minus textIconBoxControll" aria-hidden="true"
									onclick="decreaseTextStroke()"
									onmousedown="inter=setInterval(decreaseTextStroke, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span>
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-breathingroom" style="text-align: center">
								<div id="row">
									{!! Form::label('stroke', 'Kantlinje tekst')!!}
								</div>
								<div id="row">
									{!! Form::select('colorpickerStroke', array(
										'#000000' => 'Black',
										'#ffffff' => 'White',
										'#E6E6E6' => '10% Black',
										'#B1B1B1' => '40% Black',
										'#888887' => '60% Black',
										'#5C5C5B' => '80% Black',
										'#EBB5C3' => 'PANTONE 182 C',
										'#C8112E' => 'PANTONE 185 C',
										'#B01D2B' => 'PANTONE 1797 C',
										'#871630' => 'PANTONE 201 C',
										'#E6D5A8' => 'PANTONE 155 C',
										'#E9954A' => 'PANTONE 804 C',
										'#E64A00' => 'PANTONE Orange 021 C',
										'#EAEBBC' => 'PANTONE 607 C',
										'#EFED84' => 'PANTONE 100 C',
										'#EFE032' => 'PANTONE Yellow C',
										'#C9D8E7' => 'PANTONE 290 C',
										'#8ACBE5' => 'PANTONE 305 C',
										'#1A35A8' => 'PANTONE 286 C',
										'#0F2867' => 'PANTONE 281 C',
										'#549AA3' => 'PANTONE 320 C',
										'#EACDCF' => 'PANTONE 698 C',
										'#E8A3D0' => 'PANTONE 230 C',
										'#B50970' => 'PANTONE 226 C',
										'#D7CAE3' => 'PANTONE 263 C',
										'#9E70C1' => 'PANTONE 528 C',
										'#680E92' => 'PANTONE 527 C',
										'#BC8F70' => 'PANTONE 7515 C',
										'#9E520F' => 'PANTONE 471 C',
										'#B6DD8E' => 'PANTONE 358 C',
										'#A4D426' => 'PANTONE 375 C',
										'#61AE56' => 'PANTONE 354 C',
										'#4A7229' => 'PANTONE 364 C',
										'#cfb53b' => 'Gold-like',
										'#d7d8d8' => 'Silver-like'),
										'#000000',
										array('id' => 'stroke'))
									!!}
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="clipartControl"> <!-- Clipart tab -->
						<div class="row"> <!-- Clipart View Box -->
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="row">

									<?php

									  foreach(Category::all() as $cat)
									  {
										$catID = $cat->id;
										$catName = $cat->name;
										echo '<option value="', $catID, '">', $catName, '</option>';
									  }
									?>


									{!! Form::select('category', array(
										'0' => 'Alle Clipart',
										'1' => 'Mine Clipart',
										'2' => 'Mest Brukt',
										'3' => 'Dyr',
										'4' => 'Stjernetegn',
										'5' => 'Figurer',
										'6' => 'Symboler',
										'7' => 'Sport',
										'8' => 'Maskiner',
										'9' => 'Former',
										'10'=>'Vaskesymboler'),
										'0',
										array('id' => 'category'))
									!!}
									{!! Form::text('categorySearchBox', '', array('id' => 'categorySearchBox', 'placeholder' => 'Søk i denne kategorien', 'width' => '50')) !!}
									{!! Form::label('clipLoadingProgress', ' ', array('id' => 'clipLoadingProgress'))!!}
								</div>
								<div class="row">
									<span id="clipartShow">
										<?php include '../app/views/admin/clipartget.blade.php'; echoClipart(0, 5); ?>
									</span>
								</div>
							</div>
						</div>
						<div class="row"><!-- Flip, color & delete -->
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									{!! Form::label('flipArt', 'Vend bilde') !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-warning-sign textIconBox" aria-hidden="true" onclick="flipXImg()"></span>
									<span class="glyphicon glyphicon-plane textIconBox" aria-hidden="true" onclick="flipYImg()"></span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									{!! Form::label('centerArt', 'Sentrer bilde') !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-eye-close textIconBox" aria-hidden="true" onclick="centerObjectH()"></span>
									<span class="glyphicon glyphicon-eye-open textIconBox" aria-hidden="true" onclick="centerObjectV()"></span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									@if(true)
										<!-- Fix so this is disabled when the clipart can't change color -->
										{!! Form::hidden('clipartType', '2', array('id'=>'clipartType')) !!}
										{!! Form::label('clipcolor', 'Bilde Farge')!!}
										{!! Form::select('colorpickerClipart', array(
											'#000000' => 'Black',
											'#ffffff' => 'White',
											'#E6E6E6' => '10% Black',
											'#B1B1B1' => '40% Black',
											'#888887' => '60% Black',
											'#5C5C5B' => '80% Black',
											'#EBB5C3' => 'PANTONE 182 C',
											'#C8112E' => 'PANTONE 185 C',
											'#B01D2B' => 'PANTONE 1797 C',
											'#871630' => 'PANTONE 201 C',
											'#E6D5A8' => 'PANTONE 155 C',
											'#E9954A' => 'PANTONE 804 C',
											'#E64A00' => 'PANTONE Orange 021 C',
											'#EAEBBC' => 'PANTONE 607 C',
											'#EFED84' => 'PANTONE 100 C',
											'#EFE032' => 'PANTONE Yellow C',
											'#C9D8E7' => 'PANTONE 290 C',
											'#8ACBE5' => 'PANTONE 305 C',
											'#1A35A8' => 'PANTONE 286 C',
											'#0F2867' => 'PANTONE 281 C',
											'#549AA3' => 'PANTONE 320 C',
											'#EACDCF' => 'PANTONE 698 C',
											'#E8A3D0' => 'PANTONE 230 C',
											'#B50970' => 'PANTONE 226 C',
											'#D7CAE3' => 'PANTONE 263 C',
											'#9E70C1' => 'PANTONE 528 C',
											'#680E92' => 'PANTONE 527 C',
											'#BC8F70' => 'PANTONE 7515 C',
											'#9E520F' => 'PANTONE 471 C',
											'#B6DD8E' => 'PANTONE 358 C',
											'#A4D426' => 'PANTONE 375 C',
											'#61AE56' => 'PANTONE 354 C',
											'#4A7229' => 'PANTONE 364 C',
											'#cfb53b' => 'Gold-like',
											'#d7d8d8' => 'Silver-like'),
											'#000000',
											array('id' => 'clipcolor', 'onchange' => 'clipColor()'))
										!!}
									@endif
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									{!! form::button('Slett', array('onclick' => 'deleteClipart()', 'id' => 'deleteClipartButton' )) !!}
								</div>
							</div>
						</div>
						<div class="row grid-breathingroom"><!-- Size, rotation & layer -->
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									{!! Form::label('clipartScale', 'Størrelse')!!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-resize-full textIconBoxControll" aria-hidden="true"
									onclick="increaseObjectScale()"
									onmousedown="inter=setInterval(increaseObjectScale, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('clipartScaleTextbox', '1', array('id' => 'clipartScaleTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-resize-small textIconBoxControll" aria-hidden="true"
									onclick="decreaseObjectScale()"
									onmousedown="inter=setInterval(decreaseObjectScale, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-minus" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div class="row">
									{!! Form::label('clipartRotate', 'Rotasjon')!!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-repeat textIconBoxControll">
										<span class="glyphicon glyphicon-plus" aria-hidden="true"
										onclick="increaseObjectRotation()"
										onmousedown="inter=setInterval(increaseObjectRotation, 50);"
										onmouseup="clearInterval(inter);"
										onmouseout="clearInterval(inter);">	</span>
									</span>
								</div>
								<div class="row">
									{!! Form::text('clipartAngleTextbox', '0', array('id' => 'clipartAngleTextbox')) !!}
								</div>
								<div class="row">
									<span class="glyphicon glyphicon-minus textIconBoxControll" id="flipped-icon"
									onclick="decreaseObjectRotation()"
									onmousedown="inter=setInterval(decreaseObjectRotation, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
										<span class="glyphicon glyphicon-repeat" aria-hidden="true" onclick="">  </span>
									</span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
								<div id="row">
									{!! Form::label('layerControl', 'Layer Kontroll') !!}
								</div>
								<div id="row">
									<!--<span class="glyphicon glyphicon-fast-backward textIconBox" aria-hidden="true" onclick="sendToBack()"></span>-->
									<span class="glyphicon glyphicon-step-backward textIconBox" aria-hidden="true" onclick="moveDown()"></span>
									<span class="glyphicon glyphicon-step-forward textIconBox" aria-hidden="true" onclick="moveUp()"></span>
									<!--<span class="glyphicon glyphicon-fast-forward textIconBox" aria-hidden="true" onclick="bringToFront()"></span>-->
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center">
								<div class="row">
									Posisjon
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-top textIconBox" aria-hidden="true"
									onclick="moveObjectTop()"
									onmousedown="inter=setInterval(moveObjectTop, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);">
									</span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-left textIconBox" aria-hidden="true"
									onclick="moveObjectLeft()"
									onmousedown="inter=setInterval(moveObjectLeft, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-right textIconBox" aria-hidden="true"
									onclick="moveObjectRight()"
									onmousedown="inter=setInterval(moveObjectRight, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span> </div>
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon glyphicon-triangle-bottom textIconBox" aria-hidden="true"
									onclick="moveObjectDown()"
									onmousedown="inter=setInterval(moveObjectDown, 50);"
									onmouseup="clearInterval(inter);"
									onmouseout="clearInterval(inter);"></span> </div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
								</div>
							</div>
						</div>
						<div class="row grid-breathingroom"><!-- Upload controlls -->
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<span class="uploadControls">
									{!! Form::open(array('files' => true, 'id' => 'admincilpartForm')) !!}
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
									@if (Auth::user())
										<div class="form-group">
											{!! Form::file('image', array('id' => 'clipartFileSelect')) !!}
										</div>
										<div class="form-group">
											{!! Form::submit('Last opp bilde', array('class'=>'payBox')) !!}
										</div>
									@else
										<div class="form-group">
											{!! Form::file('image', array('disabled' => '', 'id' => 'clipartFileSelect')) !!}
										</div>
										<div class="form-group">
											{!! Form::submit('Last opp bilde', array('disabled' => '', 'class'=>'payBox')) !!}
										</div>
									@endif
									{!! Form::close() !!}
								</span>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="cartControl"> <!-- Cart tab -->
						@if(Auth::user() && Auth::user()->admin == 1)
							{!! Form::button('-test- Preview Sticker', array('onclick' => 'submitSticker()')) !!}
							{!! '<span id="stickerPreview">
							</span>' !!}
						@endif

						{!! Form::open(array('url'=>'admin/updatesticker')) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
						{!! Form::hidden('stickerid', $sticker->id) !!}
						{!! Form::hidden('userid', Auth::user()->id) !!}
						{!! Form::hidden('stickertype', $sticker->stickertype) !!}
						{!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}

						@if($sticker->stickertype == 6 || $sticker->stickertype == 7)

						<p>
							<select name="Antall2" id="Antall2">
							  <option value="1">1 stk.</option>
							  <option value="5">5 stk.</option>
							  <option value="10">10 stk.</option>
							  <option value="20">20 stk.</option>
							  <option value="50">50 stk.</option>
							  <option value="75">75 stk.</option>
							  <option value="100">100 stk.</option>
							  <option value="250">250 stk.</option>
							  <option value="500">500 stk.</option>
							  <option value="1000">1000 stk.</option>
							</select>
						</p>

						@else
							{!! Form::text('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
							{!! Form::label('Antall') !!}
						@endif
						{!! Form::hidden('jsquantity', $sticker->quantity, array('id'=>'jsquantity')) !!}
						{!! Form::hidden('jstype', $sticker->stickertype, array('id'=>'jstype')) !!}
						{!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
						{!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
						{!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
						{!! Form::label('thePrice', 'Price: ', array('id'=>'thePrice2')) !!}
						{!! Form::submit('Oppdater etikett', array('class'=>'buttonBox')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<font color="red" size="5px">*</font>Du må være innlogget om du ønsker å laste opp bakgrunner eller bilder. Gyldig filformat: png, gif, eller jpg.<br>
				Bakgrunner og clipart blir personlige og ikke tilgjengelig for andre enn kontohaver
			</div>
		</div>
	</div>
</div> <!-- container -->


<div id='mask'></div>
<div id='popup'>

    <p>Laster opp fil</p>
	<span id='popupSpin'></span>
</div>

<?php // the following span is needed by store.js to work properly with this specific page
	  // (adminsticker.blade.php)  it allows store.js to check what page it is, and get the
	  // correct url for retrieving cliparts / (accessing clipartget, otherwise it's one level too deep
?><span id="adminstickerTag"></span>
<!--canvas id="myCanvas" width="200" height="100"></canvas-->
{!! Html::style('css/uploadingSpin.css') !!}
{!! Html::style('css/stickerMeasureBar.css') !!}
{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/fabric/dist/fabric.min.js') !!}
{!! Html::script('js/tabulous.min.js') !!}
{!! Html::script('js/jquery.simplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/spin.min.js') !!}
{!! Html::script('js/store.js') !!}


 <?php

	$jsonstring = $sticker->json;
	$antall = $sticker->quantity;

  ?>

  <script>
    $(document).ready(function() {
      loadfromJson(<?php echo $jsonstring ?>);

		if (document.getElementById('jstype').value == 6 ||  document.getElementById('jstype').value == 7) {
			var element = document.getElementById('Antall');
			element.value = document.getElementById('jsquantity').value;
			var element2 = document.getElementById('Antall2');
			element2.value = document.getElementById('jsquantity').value;
		}




      });
  </script>


@stop
