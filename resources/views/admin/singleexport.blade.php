@extends('layouts.main')

@section('content')

<?php
		ini_set('memory_limit', '1024M');
    $typeofsticker = 1;
?>

<div class="">

	</br></br>
  @include('designer.hiddeninput')
  {!! Form::hidden('isRect', '1', array('id'=>'isRect')) !!}
  {!! Form::hidden('isEllipse', '0', array('id'=>'isEllipse')) !!}
  {!! Form::hidden('isGeneratedCutline', '0', array('id'=>'isGeneratedCutline')) !!}
  {!! Form::hidden('width', '37', array('id'=>'width')) !!}
  {!! Form::hidden('height', '16', array('id'=>'height')) !!}
  {!! Form::hidden('fontFamily', '', array('id'=>'fontFamily')) !!}
  {!! Form::hidden('fill', '#000000', array('id'=>'fill')) !!}
  {!! Form::hidden('strokeWidthTextbox', '0.1', array('id'=>'strokeWidthTextbox')) !!}
  {!! Form::hidden('stroke', '#000000', array('id'=>'stroke')) !!}
  {!! Form::hidden('quantity', '1', array('id'=>'quantity')) !!}
  {!! Form::hidden('priceHolder', '1', array('id'=>'priceHolder')) !!}
  {!! Form::hidden('postprice', '1', array('id'=>'postprice')) !!}
  {!! Form::hidden('stickerpriceholder', '1', array('id'=>'stickerpriceholder')) !!}
  {!! Form::hidden('thePrice', '1', array('id'=>'thePrice')) !!}
  {!! Form::hidden('thePrice2', '1', array('id'=>'thePrice2')) !!}
  {!! Form::hidden('cilpartForm', 'cilpartForm', array('id'=>'cilpartForm')) !!}
  {!! Form::hidden('clipartFileSelect', 'clipartFileSelect', array('id'=>'clipartFileSelect')) !!}
  {!! Form::hidden('backgroundForm', 'backgroundForm', array('id'=>'backgroundForm')) !!}
  {!! Form::hidden('backgroundFileSelect', 'backgroundFileSelect', array('id'=>'backgroundFileSelect')) !!}
	<div class="table-responsive">
	<table class="table table-hover table-bordered ">

			<tr>
				<th> Bestillingsnummer</th>
				<th> Bestillingsdato</th>
				<th> Leverings adresse</th>
				<th> Status </th>
				<th> Pris </th>
        <th> Single Export </th>
			</tr>


			@foreach($stickers as $sticker)
  			<tr>
  				<td> {!! $sticker->id !!} </td>
  				<td> {!! $sticker->created_at !!} </td>
  				<td> {!! Html::image($sticker->thumbnail, $sticker->thumbnail, array('class'=>'thumbnailpic')) !!} </td>
  				<td>
  					@if ($sticker->status == 0)
  						{!! 'Klar for betaling' !!}
  					@elseif ($sticker->status == 1)
  						{!! 'klar for expo' !!}
  					@elseif ($sticker->status == 2)
  						{!! 'Eksporert' !!}
  					@elseif ($sticker->status == 3)
  						{!! 'Kansellert' !!}
  					@elseif ($sticker->status == 6)
  						{!! 'Fakturakunde' !!}
  					@else
  						{!! 'N/A' !!}
  					@endif
  				</td>
  				<td> {!! $sticker->price . ',-' !!} </td>
          <td>
          <button type="button" class="mybtn" onclick="stickerRender(<?php echo $sticker->id ?>)">Export</button>
          {!! Form::hidden($sticker->id, $sticker->json, array('id'=>$sticker->id)) !!}
          <input type="hidden" id="type-<?php echo $sticker->id?>" name="type=<?php echo $sticker->id?>" value="<?php echo $sticker->stickertype ?>">
          </td>
  			</tr>
      @endforeach
		</table>
	</div>
</div>

<div class="col-sm-12 col-md-12 col-lg-12 design-tools-canvas"> <!-- Canvas block -->
<!-- Canvas section -->
				<div id="canvasDiv">
					<canvas id="myCanvas" width="900" height="450"></canvas>
				</div>
</div>

{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/jquery.fontselector.js') !!}
<!-- {!! Html::script('js/fabric.js-master/dist/fabric.min.js') !!} -->
{!! Html::script('js/fabric.js-master/dist/fabric.js') !!}
{!! Html::script('js/jquery.simplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/spin.min.js') !!}
{!! Html::script('js/customiseControls.min.js') !!}
{!! Html::script('js/outline-2.js') !!}
{!! Html::script('js/background-2.js') !!}
{!! Html::script('js/clipart-2.js') !!}
{!! Html::script('js/price-2.js') !!}
{!! Html::script('js/text-2.js') !!}
{!! Html::script('js/bootstrap-colorpicker.js') !!}
{!! Html::script('js/upload-2.js') !!}
{!! Html::script('js/store-2.js') !!}
{!! Html::script('js/canvasToImage-2.js') !!}
{!! Html::script('js/jsonToPDF.js') !!}
{!! Html::script('js/textbuttons-2.js') !!}

<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script>
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' &&
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' ||
                 d.type.toUpperCase() === 'FILE' ||
                 d.type.toUpperCase() === 'SEARCH' ||
                 d.type.toUpperCase() === 'EMAIL' ||
                 d.type.toUpperCase() === 'NUMBER' ||
                 d.type.toUpperCase() === 'DATE' )
             ) ||
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});

</script>

@stop
