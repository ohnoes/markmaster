@extends('layouts.main')

@section('content')

<?php
	include '../resources/views/admin/adminutil.php';
?>

<div class="">

	<h1>Customers</h1>
	{!! Form::label('userSearch', 'Search Customers: ') !!}
	{!! Form::text('userSearch', '', array('id' => 'userSearchBox')) !!}
	<br/>
	{!! Form::label('any', 'All: ') !!}
	{!! Form::radio('searchField', 'any', true) !!}
	{!! Form::button('Search Button', array('onclick' => 'getUserSearch()')) !!}

	<div class="table-responsive">
	<table class="table table-hover table-bordered " id="usersTable">
		<tr id="tableHeader">
			<th>Customer #	{!! Form::radio('searchField', 'id', false) !!} 			</th>
			<th>Orgname		{!! Form::radio('searchField', 'orgname', false) !!}		</th>
			<th>Name		{!! Form::radio('searchField', 'name', false) !!} 		</th>
			<th>Phone		{!! Form::radio('searchField', 'telephone', false) !!} 	</th>
			<th>email		{!! Form::radio('searchField', 'email', false) !!} 		</th>
			<th>Address		{!! Form::radio('searchField', 'address', false) !!} 		</th>
			<th>City		{!! Form::radio('searchField', 'city', false) !!} 		</th>
			<th>State		{!! Form::radio('searchField', 'state', false) !!} 		</th>
			<th>Zipcode		{!! Form::radio('searchField', 'zipcode', false) !!} 		</th>
			<th>Country		{!! Form::radio('searchField', 'country', false) !!}		</th>
			<th>Edit		</th>
			<th>Delete		</th>
		</tr>
		<span id="ordersData">
			<?php foreach ($users as $user): ?>
				<?php echo getUserAsTableRow($user); ?>
			<?php endforeach; ?>
		</span>
	</table>
	</div>
	<div id="pagination" align="center">
		{!! $users->render() !!}
	</div>
</div>

@stop


<script>

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getUserSearch() {
  var field = $('input:radio[name="searchField"]:checked').val()
  var search = $('#userSearchBox').val();
  var page = getParameterByName('page');
  if (page == '') page = '1';
  console.log(page);
  var table = $('#usersTable');
  var tableHeader = $('#tableHeader');

  var data = {'field': field, 'search': search};
  if (search == '')
  {
	data['page'] = page;
  }

  $.ajax({
      type: 'get',
      url: 'usersearch',
      cache: false,
      dataType: 'html',
      data: data,
      success: function(data) {
		  console.log(data);
		  table.html(tableHeader);
		  table.append(data);
      },
      error: function(xhr, textStatus, thrownError) {
		  console.error("search failed" + xhr + textStatus + thrownError);
          // alert('Something went to wrong.Please Try again later...');
      }
  });
}

</script>
