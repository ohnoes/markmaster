@extends('layouts.main')

@section('content')

	{!! 'Order ID: ' !!} {!! $order->id !!} <br>

	{!! 'Faktura ID: ' !!} {!! $faktura->id !!} <br>

	{!! 'Total sum: ' !!} {!! $order->amount !!}

	<div id="stickerinfo" style="text-align: center; padding-left: 30px; padding-top: 40px;">
				<table border="1" id="tableStyle">
						<tr>
							<th>Produksjonskode</th>
							<th>Varegruppe</th>
							<th>Etikett</th>
							<th>Størrelse mm</th>
							<th>Antall</th>
							<th>Pris inkl. mva </th>
						</tr>

						@foreach($stickers as $sticker)
						<tr>
							<td>
								{!! $sticker->order_id!!} <br>
								{!! $sticker->id!!}
							</td>
							<td>
								@if($sticker->stickertype == 1)
									{!! '1' !!} <br>
									{!! 'Klisteretikett farge, med laminat' !!}
								@elseif($sticker->stickertype == 2)
									{!! '2' !!} <br>
									{!! 'Strykfastetikett farge' !!}
								@elseif($sticker->stickertype == 3)
									{!! '3' !!} <br>
									{!! 'Klistreetikett i gullfarget vinyl' !!}
								@elseif($sticker->stickertype == 4)
									{!! '4' !!} <br>
									{!! 'Klistreetikett i sølvfarget vinyl' !!}
								@elseif($sticker->stickertype == 5)
									{!! '5' !!} <br>
									{!! 'Klistreetikett i transparent vinyl' !!}
								@elseif($sticker->stickertype == 6)
									{!! '6' !!} <br>
									{!! 'Klistreetikett i sort/hvit' !!}
								@elseif($sticker->stickertype == 7)
									{!! '7' !!} <br>
									{!! 'Strykfastetikett' !!}
								@elseif($sticker->stickertype == 8)
									{!! '8' !!} <br>
									{!! 'Klistreetikett i refleks' !!}
								@endif
							</td>

							<td>
								<?php
								$imgsrc = $sticker->thumbnail;
								$imgsrc = str_replace('http://79.161.166.153', '', $imgsrc);
								$imgsrc = str_replace('http://markmaster.no', '', $imgsrc);
								$imgsrc = str_replace('http://192.168.1.50', '', $imgsrc);
								echo '<img src="' . $imgsrc . '" class="thumbnailpic" alt="' . $sticker->thumbnail . '1">';
								?>
								<!--{!! Html::image($sticker->thumbnail, $sticker->stickertype, array('class'=>'thumbnailpic')) !!}-->
							</td>

							<td>
								{!! '' . $sticker->width . ' x ' . $sticker->height !!}
							</td>

							<td>
								{!! $sticker->quantity !!}
							</td>

							<td>
								{!! 'kr ' . $sticker->price . ',00' !!}
							</td>
						</tr>
						@endforeach
						<tr>
							<td>

							</td>
							<td>
								{!! 'Porto og ekspedisjonsgebyr'!!}
							</td>

							<td>

							</td>

							<td>

							</td>

							<td>

							</td>

							<td>
								{!! 'kr ' . '29,00' !!}
							</td>

						</tr>
				</table>
		</div>

		<form action="/{{ app()->getLocale() }}/admin/credit/{{$faktura->id}}" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<br><br>
			{!! Form::label('note', 'Kreditinformasjon: ') !!}
			{!! Form::textarea('note') !!} <br><br>
			{!! Form::label('amount', 'Kredit Amount: ') !!}
			{!! Form::text('amount') !!}
			{!! Form::hidden('faktura_date', $faktura->faktura_date) !!}
			{!! Form::hidden('user_id', $order->user_id)!!}
			{!! Form::hidden('order_id', $order->id)!!}
			{!! Form::hidden('faktura_id', $faktura->id)!!}
			{!! Form::hidden('billingorgname', $faktura->billingorgname)!!}
			{!! Form::hidden('billingfirstname', $faktura->billingfirstname)!!}
			{!! Form::hidden('billinglastname', $faktura->billinglastname)!!}
			{!! Form::hidden('billingaddress', $faktura->billingaddress)!!}
			{!! Form::hidden('billingaddress2', $faktura->billingaddress2)!!}
			{!! Form::hidden('billingzipcode', $faktura->billingzipcode)!!}
			{!! Form::hidden('billingcity', $faktura->billingcity)!!}
			{!! Form::hidden('billingstate', $faktura->billingstate)!!}
			{!! Form::hidden('billingcountry', $faktura->billingcountry)!!}


			<br><br>
		{!! Form::submit('Krediter bestilling', array('class'=>'buttonBox')) !!}
		{!! Form::close() !!}

@stop
