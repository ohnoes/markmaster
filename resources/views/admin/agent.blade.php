@extends('layouts.main')

@section('content')

	<div id="new-account">

		
		<form action="/{{ app()->getLocale() }}/admin/agent" method="POST" name="inputs">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<p>
			{!! Form::label('Navn') !!}
			{!! Form::text('name') !!}
		</p>
		<p>
			{!! Form::label('Adresse') !!}
			{!! Form::text('address') !!}
		</p>
		<p>
			{!! Form::label('Postnummer') !!}
			{!! Form::text('zip') !!}
		</p>
		<p>
			{!! Form::label('Sted') !!}
			{!! Form::text('city') !!}
		</p>
		<p>
			{!! Form::label('telefon') !!}
			{!! Form::text('telephone') !!}
		</p>
		<p>
			{!! Form::label('webside') !!}
			{!! Form::text('url') !!}
		</p>
		<p>
			{!! Form::label('Kontakt Navn') !!}
			{!! Form::text('contactname') !!}
		</p>
		<p>
			{!! Form::label('Kontakt Telefon') !!}
			{!! Form::text('contacttelephone') !!}
		</p>
		<p>
			{!! Form::label('E-post') !!}
			{!! Form::text('contactmail') !!}
		</p>
		<p>
		{!! Form::submit('Lag ny Reklame agent', array('class'=>'buttonBox', 'onclick'=>'formValid(event, inputs)')) !!}
		{!! Form::close() !!}
		</p>
		<br><br>
	</div>

@stop
