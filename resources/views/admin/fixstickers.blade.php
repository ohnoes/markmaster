@extends('layouts.main')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php
		ini_set('memory_limit', '2048M');

      $templates = DB::table('templates')->get();
      foreach ($templates as $temp) {
        $tmp = Template::find($temp->id);
				if($tmp->updated == 0) {
					$json = json_decode($tmp->json);
					//print_r($someArray);        // Dump all data of the Array
					//echo $json->width; // Access Array data
					$typeofsticker = 1;

					$ratio = $json->width/$json->height;
					if(($json->width/2) > $json->height) {
						$aWidth = 900 - 2*40;
						$aHeight = $aWidth / $ratio;
					} else {
						$aHeight = 450 - 2*40;
						$aWidth = $ratio * $aHeight;
					}

					if($json->shape == 'rect') {
						$scale = $aWidth/$json->outlineInfo->width;
					} else {
						$newRx = $aWidth/2;
						$scale = $newRx/$json->outlineInfo->rx;
					}
					foreach($json->canvas->objects as $obj) {
						$obj->top = 225 + (($obj->top-300)*$scale);
						$obj->left = 450 + (($obj->left-300)*$scale);
						$obj->scaleX = $obj->scaleX * $scale;
						$obj->scaleY = $obj->scaleY * $scale;
					}

					$tmp->json = json_encode($json);
					$tmp->updated = 1;
					$tmp->save();
				}

      }

			$stickers = DB::table('orderitems')->where('updated', 0)->orderBy('id', 'DESC')->get();
      foreach ($stickers as $stick) {
        $tmp = Orderitem::find($stick->id);
				if($tmp->updated == 0) {
	        $json = json_decode($tmp->json);
	        //print_r($someArray);        // Dump all data of the Array
	        //echo $json->width; // Access Array data
	        $typeofsticker = 1;
					if( isset($json->width) && isset($json->height) && isset($json->canvas->objects) ){
								$ratio = $json->width/$json->height;
	 		         if(($json->width/2) > $json->height) {
	 		           $aWidth = 900 - 2*40;
	 		           $aHeight = $aWidth / $ratio;
	 		         } else {
	 		           $aHeight = 450 - 2*40;
	 		           $aWidth = $ratio * $aHeight;
	 		         }

	 		         if($json->shape == 'rect') {
	 		           $scale = $aWidth/$json->outlineInfo->width;
	 		         } else {
	 		           $newRx = $aWidth/2;
	 		           $scale = $newRx/$json->outlineInfo->rx;
	 		         }
	 		         foreach($json->canvas->objects as $obj) {
	 		           $obj->top = 225 + (($obj->top-300)*$scale);
	 		           $obj->left = 450 + (($obj->left-300)*$scale);
	 		           $obj->scaleX = $obj->scaleX * $scale;
	 		           $obj->scaleY = $obj->scaleY * $scale;
	 		         }

	 		         $tmp->json = json_encode($json);
							 $tmp->updated = 1;
	 		         $tmp->save();
					}
				}

      }

?>

  <button type="button" class="mybtn" onclick="fixTmp()">FIX IT</button>

<script>
  function fixTmp(){
    template = <?php echo json_encode($tmp) ?>;
    console.log("TEMPLATE: " + template);
    //console.log("JSON: " + template.json);

    var server_path = 'http://markmaster.app/';

    var json = JSON.parse(template.json);
    console.log("JSON: " + json);
    console.log("JSON info: " + json.width);
    var outlinePadding = 40;
    var width = json["width"];
    var height = json.height;
    var ratio = width / height;
    var cWidth = 900;
    var cHeight = 450;
    var centerX = cWidth / 2;
    var centerY = cHeight / 2;
    var aWidth;
    var aHeight;
    var cornerRadius = 2;
    if ((width/2) > height)
    {
      aWidth = cWidth - 2*outlinePadding;
      aHeight = aWidth / ratio;
    }
    else
    {
      aHeight = cHeight - 2*outlinePadding;
      aWidth = ratio * aHeight;
    }

    console.log("shape: " + json.shape);
    if(json.shape == "rect") {

    } else {
      var newRx = aWidth/2;
      console.log("width: " + json.width);
      console.log("left: " + json.outlineInfo.left);
      console.log("rx: " + json.outlineInfo.rx);
      console.log("newRx: " + newRx);
      console.log("the scale: " + newRx/json.outlineInfo.rx);

    }
  }
</script>

{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/jquery.fontselector.js') !!}
{!! Html::script('js/fabric.js-master/dist/fabric.js') !!}
{!! Html::script('js/jquery.simplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/spin.min.js') !!}
{!! Html::script('js/customiseControls.min.js') !!}


{!! Html::script('js/bootstrap-colorpicker.js') !!}


@stop
