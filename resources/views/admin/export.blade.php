@extends('layouts.main')

@section('content')

<?php

		$icast1 = 70;
		$icast2 = 57;
		$orders = Order::where("order_status", 1)->where(function ($query) use ($icast1, $icast2) {
				$query->where("user_id", '!=', $icast1)
						->where("user_id", '!=', $icast2);
			})->orWhere("order_status", 6)
			->where(function ($query) use ($icast1, $icast2) {
				$query->where("user_id", '!=', $icast1)
						->where("user_id", '!=', $icast2);
			})->get(['*']);

?>

<div class="">
	</br></br>

	<a href="/{{ app()->getLocale() }}/admin/processexports/"> <button type="button" class="btn btn-danger">Export</button> </a>
	</br></br></br></br>

	<div class="table-responsive">
	<table class="table table-hover table-bordered ">

			<tr>
				<th> Bestillingsnummer</th>
				<th> Bestillingsdato</th>
				<th> Leverings adresse</th>
				<th> Status </th>
				<th> Pris </th>
			</tr>

			@foreach($orders as $order)
			<tr>
				<td> {!! $order->id !!} </td>
				<td> {!! $order->order_date !!} </td>
				<td> {!! $order->address, ' ', $order->address2, ' ', $order->zipcode, ' ', $order->city !!} </td>
				<td>
					@if ($order->order_status == 0)
						{!! 'Klar for betaling' !!}
					@elseif ($order->order_status == 1)
						{!! 'Betalt' !!}
					@elseif ($order->order_status == 2)
						{!! 'Eksporert' !!}
					@elseif ($order->order_status == 3)
						{!! 'Kansellert' !!}
					@elseif ($order->order_status == 6)
						{!! 'Fakturakunde' !!}
					@else
						{!! 'N/A' !!}
					@endif
				</td>
				<td> {!! $order->amount . ',-' !!} </td>
			</tr>
			@endforeach

		</table>
	</div>
</div>

@stop
