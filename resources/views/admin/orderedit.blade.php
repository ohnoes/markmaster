@extends('layouts.main')

@section('content')

	<div class="">

		@if(count($errors) > 0)
		<div class="alert alert-danger">
			<p> Følgende feil må rettes:</p>
			<ul>
				@foreach($errors->all() as $error)
					<li> {!! $error !!}</li>
				@endforeach
			</ul>
		</div>
		@endif

	<h1>Orders</h1>


	<form action="/{{ app()->getLocale() }}/admin/updateorder/{{$order->id}}" method="POST" name="inputs" accept-charset="UTF-8" data-toggle="validator">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<label>Order ID: <?php echo $order->id ?></label>
		</div>
		<div class="form-group">
			<label>User ID: <?php echo $order->user_id ?></label>
		</div>

		<?php
			$user = markmaster\Models\User::find($order->user_id);
		?>

		<div class="form-group">
			<label> {!! $order->email !!} </label>
		</div>

		<div class="form-group">
			@if($order->user_id == 0)
				<label> Ureg Bruker </label>
			@else
				<label> {!! $user->telephone !!} </label>
			@endif
		</div>

		<div class="form-group">
			<label for="amount"> Order Amount: </label>
			{!! Form::text('amount', round($order->amount, 2, PHP_ROUND_HALF_DOWN), array('class'=>'form-control')) !!}
		</div>
		<div class="form-group">
			<label for="order_status"> Order Status: </label>
			{!! Form::text('order_status', $order->order_status, array('class'=>'form-control')) !!}
			{!! '0 = klar for betaling, 1 = Betalt, 2 = exportert, 3 = kansellert, 4 = Hold, 5 = Kreditert, 6 = Fakturakunde' !!}
		</div>

		<div class="form-group">
			<label for="order_memo"> Order memo: </label>
			{!! Form::textarea('order_memo', $order->memo, array('class'=>'form-control')) !!}
		</div>


		</br>

		<div class="">
			<div class="row-fluid">
				<div class="col-sm-12 col-md-6 col-lg-6 admin-form-box"> <!-- Shipping-box -->
				{!! Form::label('shipping', 'Shipping Information', array('id'=>'shippinglabel')) !!}
					<div class="form-group">
						<label for="orgname"> Organisasjonsnavn </label>
						{!! Form::text('orgname', $order->orgname, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="firstname"> First Name </label>
						{!! Form::text('firstname', $order->firstname, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="lastname"> Last Name </label>
						{!! Form::text('lastname', $order->lastname, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="address"> Address </label>
						{!! Form::text('address', $order->address, array('class'=>'form-control')) !!}
						{!! Form::text('address2', $order->address2, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="zipcode"> ZIP </label>
						{!! Form::text('zipcode', $order->zipcode, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="city"> City </label>
						{!! Form::text('city', $order->city, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="state"> State </label>
						{!! Form::text('state', $order->state, array('class'=>'form-control')) !!}
					</div>

					{!! Form::hidden('userCountry', $order->country, array('id'=>'userCountry')) !!}
					<div class="form-group">
						<label for="country">Land: </label>
						<select class="form-control" name="country" id="country">
							<option value="Afghanistan">Afghanistan</option>
							<option value="Åland Islands">Åland Islands</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="American Samoa">American Samoa</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option>
							<option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="Brunei Darussalam">Brunei Darussalam</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Cote D'ivoire">Cote D'ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Denmark">Denmark</option>
							<option value="Djibouti">Djibouti</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="French Southern Territories">French Southern Territories</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadeloupe">Guadeloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guernsey">Guernsey</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-bissau">Guinea-bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
							<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Isle of Man">Isle of Man</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jersey">Jersey</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
							<option value="Korea, Republic of">Korea, Republic of</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macao">Macao</option>
							<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Martinique">Martinique</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
							<option value="Moldova, Republic of">Moldova, Republic of</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Montenegro">Montenegro</option>
							<option value="Montserrat">Montserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option>
							<option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn">Pitcairn</option>
							<option value="Poland">Poland</option>
							<option value="Portugal">Portugal</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russian Federation">Russian Federation</option>
							<option value="Rwanda">Rwanda</option>
							<option value="Saint Helena">Saint Helena</option>
							<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
							<option value="Samoa">Samoa</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Serbia">Serbia</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakia">Slovakia</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syrian Arab Republic">Syrian Arab Republic</option>
							<option value="Taiwan, Province of China">Taiwan, Province of China</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
							<option value="Thailand">Thailand</option>
							<option value="Timor-leste">Timor-leste</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkey">Turkey</option>
							<option value="Turkmenistan">Turkmenistan</option>
							<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Arab Emirates">United Arab Emirates</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="United States">United States</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="Uruguay">Uruguay</option>
							<option value="Uzbekistan">Uzbekistan</option>
							<option value="Vanuatu">Vanuatu</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Viet Nam">Viet Nam</option>
							<option value="Virgin Islands, British">Virgin Islands, British</option>
							<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6 admin-form-box"> <!-- Billing-box -->
					{!! Form::label('billing', 'Billing Information',array('id'=>'billinglabel')) !!}
						<div class="form-group">
						<label for="billingorgname"> Organisasjonsnavn </label>
						{!! Form::text('billingorgname', $order->billingorgname, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="billingfirstname"> First Name </label>
						{!! Form::text('billingfirstname', $order->billingfirstname, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="billinglastname"> Last Name </label>
						{!! Form::text('billinglastname', $order->billinglastname, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="billingaddress"> Address </label>
						{!! Form::text('billingaddress', $order->billingaddress, array('class'=>'form-control')) !!}
						{!! Form::text('billingaddress2', $order->billingaddress2, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="billingzipcode"> ZIP </label>
						{!! Form::text('billingzipcode', $order->billingzipcode, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="billingcity"> City </label>
						{!! Form::text('billingcity', $order->billingcity, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						<label for="billingstate"> State </label>
						{!! Form::text('billingstate', $order->billingstate, array('class'=>'form-control')) !!}
					</div>

					{!! Form::hidden('userBilling', $order->billingcountry, array('id'=>'userBilling')) !!}

					<div class="form-group">
						<label for="billingcountry">Land: </label>
						<select class="form-control" name="billingcountry" id="billingcountry">
							<option value="Afghanistan">Afghanistan</option>
							<option value="Åland Islands">Åland Islands</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="American Samoa">American Samoa</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option>
							<option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="Brunei Darussalam">Brunei Darussalam</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Cote D'ivoire">Cote D'ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Denmark">Denmark</option>
							<option value="Djibouti">Djibouti</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="French Southern Territories">French Southern Territories</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadeloupe">Guadeloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guernsey">Guernsey</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-bissau">Guinea-bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
							<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Isle of Man">Isle of Man</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jersey">Jersey</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
							<option value="Korea, Republic of">Korea, Republic of</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macao">Macao</option>
							<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Martinique">Martinique</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
							<option value="Moldova, Republic of">Moldova, Republic of</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Montenegro">Montenegro</option>
							<option value="Montserrat">Montserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option>
							<option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn">Pitcairn</option>
							<option value="Poland">Poland</option>
							<option value="Portugal">Portugal</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russian Federation">Russian Federation</option>
							<option value="Rwanda">Rwanda</option>
							<option value="Saint Helena">Saint Helena</option>
							<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
							<option value="Samoa">Samoa</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Serbia">Serbia</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakia">Slovakia</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syrian Arab Republic">Syrian Arab Republic</option>
							<option value="Taiwan, Province of China">Taiwan, Province of China</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
							<option value="Thailand">Thailand</option>
							<option value="Timor-leste">Timor-leste</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkey">Turkey</option>
							<option value="Turkmenistan">Turkmenistan</option>
							<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Arab Emirates">United Arab Emirates</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="United States">United States</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="Uruguay">Uruguay</option>
							<option value="Uzbekistan">Uzbekistan</option>
							<option value="Vanuatu">Vanuatu</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Viet Nam">Viet Nam</option>
							<option value="Virgin Islands, British">Virgin Islands, British</option>
							<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<br>
		{!! Form::submit('Oppdater Bestilling', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
	<br>
	<div class="">
		<div class="table-responsive">
		<table class="table table-hover table-bordered ">
                <tr>
                    <th>URL link til etikettdesign</th>
                    <th>Etikett (Klikk på bilde for å endre)</th>
                    <th>Type</th>
                    <th>Størrelse mm</th>
                    <th>Antall</th>
										<th>Exp Antall </th>
					<th>Pris inkl. mva </th>
					<th>Slett</th>
					<th>Edit</th>
					<th>Status</th>
					<th> </th>
                </tr>

                @foreach($stickers as $sticker)
                <tr>
					<td>
						{!! Html::link($sticker->url , 'Link', array('class'=>'buttonBox')) !!}
					</td>

                    <td>
                       {!! Html::image($sticker->thumbnail, $sticker->name, array('class'=>'thumbnailpic')) !!}
                    </td>
                    <td>
						@if($sticker->stickertype == 1)
							{!! 'Klisteretikett farge, med laminat' !!}
						@elseif($sticker->stickertype == 2)
							{!! 'Strykfastetikett farge' !!}
						@elseif($sticker->stickertype == 3)
							{!! 'Klistreetikett i gullfarget vinyl' !!}
						@elseif($sticker->stickertype == 4)
							{!! 'Klistreetikett i sølvfarget vinyl' !!}
						@elseif($sticker->stickertype == 5)
							{!! 'Klistreetikett i transparent vinyl' !!}
						@elseif($sticker->stickertype == 6)
							{!! 'Klistreetikett i sort/hvit' !!}
						@elseif($sticker->stickertype == 7)
							{!! 'Strykfastetikett' !!}
						@elseif($sticker->stickertype == 8)
							{!! 'Refleksetikett' !!}
						@elseif($sticker->stickertype == 9)
							{!! 'Tøyklistremerke' !!}
						@endif
					</td>
					<td>
						{!! '' . $sticker->width . ' x ' . $sticker->height !!}
					</td>
                    <td>
                       {!! $sticker->quantity !!}
                    </td>
										<td>
                       {!! $sticker->quantexp !!}
                    </td>
                    <td>
                        @if($order->currency == 0)
							{!! number_format ($sticker->price , 2 ) . ' nok' !!}
						@elseif($order->currency == 1)
							{!! '$ ' . number_format ($sticker->price , 2 ) !!}
						@elseif($order->currency == 2)
							{!! number_format ($sticker->price , 2 ) . ' sek' !!}
						@elseif($order->currency == 3)
							{!! number_format ($sticker->price , 2 ) . ' dkk' !!}
						@elseif($order->currency == 4)
							{!! '€ ' . number_format ($sticker->price , 2 ) !!}
						@endif
					</td>
					<td>
							<a href="/{{ app()->getLocale() }}/admin/removeitem/{{ $sticker->id }}" onclick="return confirm('Er du sikker?')"> <button type="button" class="btn btn-danger">Delete</button> </a>
                    </td>
					<td>
						@if($sticker->stickertype == 1)
							<a href="/{{ app()->getLocale() }}/merkelapper/klistremerker?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 2)
							<a href="/{{ app()->getLocale() }}/merkelapper/strykfast-farge?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 3)
							<a href="/{{ app()->getLocale() }}/merkelapper/klistremerker-gull?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 4)
							<a href="/{{ app()->getLocale() }}/merkelapper/klistremerker-solv?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 5)
							<a href="/{{ app()->getLocale() }}/merkelapper/klistremerker-transparent?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 6)
							<a href="/{{ app()->getLocale() }}/merkelapper/billige?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 7)
							<a href="/{{ app()->getLocale() }}/merkelapper/strykfast?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 8)
							<a href="/{{ app()->getLocale() }}/merkelapper/refleksmerker?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@elseif($sticker->stickertype == 9)
							<a href="/{{ app()->getLocale() }}/merkelapper/strykefrie?adminedit=<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger">Edit</button> </a>
						@endif

					</td>
						<form action="/{{ app()->getLocale() }}/admin/updatestickerstatus/{{$sticker->id}}" method="POST" name="inputs" accept-charset="UTF-8" data-toggle="validator">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<td>
						{!! Form::text('stickerstatus', $sticker->status, array('class'=>'form-control')) !!}
					</td>
					<td>
						{!! Form::submit('Oppdater status', array('class'=>'btn btn-danger')) !!}
						{!! Form::close() !!}
					</td>
                </tr>
                @endforeach
            </table>
			</div>
			<a href="/{{ app()->getLocale() }}/admin/credit/{{ $order->id }}"> <button type="button" class="btn btn-danger">Credit</button> </a>
	</div>
	{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
	{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}



	<script>


			$(document).ready(function() {
			if(document.getElementById('userBilling').value != null) {
				var billingValue = document.getElementById('userBilling').value;
				var billingElement = document.getElementById('billingcountry');
				billingElement.value = billingValue;
			} else {
				var billingElement = document.getElementById('billingcountry');
				billingElement.value = 'Norway';
			}

			if(document.getElementById('userCountry').value != null) {
				var userValue = document.getElementById('userCountry').value;
				var element = document.getElementById('country');
				element.value = userValue;
			} else {
				var element = document.getElementById('country');
				element.value = 'Norway';
			}

		});

	</script>



@stop
