<?php
if(Input::has('stickerType')){
  if(Input::get('stickerType') == 6 || Input::get('stickerType') == 7) {
    $fileExt = '.png';
  } else {
    $fileExt = '.pdf';
  }

  $type = Input::get('stickerType');
  $stickid = Input::get('stickerid');
  $completeorder = Input::get('completeorder');
  exportSticker($type, $stickid, $completeorder, $fileExt);
}

function exportSticker($type, $stickid, $completeorder, $fileExt) {
  $orderitem = Orderitem::find($stickid);

  // save image or pdf in newexport
  // if($type == 6 || $type == 7) {
  //   $image = $data;
  //   $extension = 'png';
  //   $filenameNoExt = $orderitem->id;
  //   $filename = $filenameNoExt . '.' . $extension;
  //   $destination = public_path() . '/newexport';
  //   $base=base64_decode($image);
  //   Image::make($base)->save($destination .'/'. $filename); // Fix the resizing to keep original format here
  // } else {
  //   $pdfdata = base64_decode($data);
  //   $extension = 'pdf';
  //   $filenameNoExt = $orderitem->id;
  //   $filename = $filenameNoExt . '.' . $extension;
  //   $destination = public_path() . '/newexport';
  //   file_put_contents($destination . $filename, $pdfdata);
    // $file = fopen($destination . $filename, 'w'); // open the file path
    // fwrite($file, $pdfdata); //save data
    // fclose($file);
//  }
  // write to txt file and update sticker status
  if(file_exists(public_path() . '/newexport/orderLines.txt')) { // append to file
    $my_file = public_path() . '/newexport/orderLines.txt';
    $handle = fopen($my_file, 'a') or die('Cannot open file:  '.$my_file); //implicitly creates file
    $data = "\n" . $orderitem->user_id . '|' . $orderitem->order_id . '|' . $orderitem->id . '|'
    . $orderitem->quantexp . '|' . $orderitem->price . '|' . $orderitem->stickertype . '|' . $orderitem->width . '|' . $orderitem->height;
    fwrite($handle, $data);
    fclose($handle);
  } else { // create the file
    $my_file = public_path() . '/newexport/orderLines.txt';
    $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file); //implicitly creates file
    $data = 'User ID|Order ID|Sticker|quantity|price|stickerType|width|height';
    fwrite($handle, $data);
    $new_data = "\n" . $orderitem->user_id . '|' . $orderitem->order_id . '|' . $orderitem->id . '|'
    . $orderitem->quantexp . '|' . $orderitem->price . '|' . $orderitem->stickertype . '|' . $orderitem->width . '|' . $orderitem->height;
    fwrite($handle, $new_data);
    fclose($handle);
  }
  $orderitem->status = 2;
  $orderitem->save();

  // if order is done, write to txt file and update order status
  if($completeorder == 1) {
      $order = Order::find($orderitem->order_id);
      // write to txt file and update sticker status
      if(file_exists(public_path() . '/newexport/orderCustomers.txt')) { // append to file
        $my_file = public_path() . '/newexport/orderCustomers.txt';
        $handle = fopen($my_file, 'a') or die('Cannot open file:  '.$my_file); //implicitly creates file
        $data = "\n" . $order->user_id . '|' . $order->id . '|' . $order->created_at . '|'
        . $order->amount . '|' . $order->orgname . '|' . $order->firstname . '|' . $order->lastname . '|' . $order->address . '|'
        . $order->address2 . '|' . $order->zipcode . '|' . $order->city . '|' . $order->state . '|' . $order->country . '|'
        . $order->billingorgname . '|' . $order->billingfirstname . '|' . $order->billinglastname . '|' . $order->billingaddress . '|'
        . $order->billingaddress2 . '|' . $order->billingzipcode . '|' . $order->billingcity . '|' . $order->billingstate . '|' . $order->billingcountry;
        fwrite($handle, $data);
        fclose($handle);
      } else { // create the file
        $my_file = public_path() . '/newexport/orderCustomers.txt';
        $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file); //implicitly creates file
        $data = 'User ID|Order ID|Order date|price|organisation|firstname|lastname|address|address2|zipcode|city|state|country|billingorganisation|billingfirstname|billinglastname|billingaddress|billingaddress2|billingzipcode|billingcity|billingstate|billingcountry';
        fwrite($handle, $data);
        $new_data = "\n" . $order->user_id . '|' . $order->id . '|' . $order->created_at . '|'
        . $order->amount . '|' . $order->orgname . '|' . $order->firstname . '|' . $order->lastname . '|' . $order->address . '|'
        . $order->address2 . '|' . $order->zipcode . '|' . $order->city . '|' . $order->state . '|' . $order->country . '|'
        . $order->billingorgname . '|' . $order->billingfirstname . '|' . $order->billinglastname . '|' . $order->billingaddress . '|'
        . $order->billingaddress2 . '|' . $order->billingzipcode . '|' . $order->billingcity . '|' . $order->billingstate . '|' . $order->billingcountry;
        fwrite($handle, $new_data);
        fclose($handle);
      }
      $order->order_status = 2;
      $order_date_stamp = new DateTime;
	    $order->export_date = $order_date_stamp->format('Y-m-d H:i:s');
      $order->save();
  }
  // echo success
  echo 'success';
}
?>
