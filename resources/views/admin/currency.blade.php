@extends('layouts.main')

@section('content')

	<div class="">

		<form action="/{{ app()->getLocale() }}/admin/currency" method="POST">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<label for="usdollar">  USD </label>
					{!! Form::text('usd', $currency->usd, array('class'=>'form-control')) !!}
				</div>
				<div class="form-group">
					<label for="billinglastname"> SEK </label>
					{!! Form::text('sek', $currency->sek, array('class'=>'form-control')) !!}
				</div>
				<div class="form-group">
					<label for="billinglastname"> DKK </label>
					{!! Form::text('dkk', $currency->dkk, array('class'=>'form-control')) !!}
				</div>
				<div class="form-group">
					<label for="billinglastname"> EUR </label>
					{!! Form::text('eur', $currency->eur, array('class'=>'form-control')) !!}
				</div>
				<button type="submit" class="btn btn-success"> Update currency</button>
		{!! Form::close() !!}

	</div>
@stop
