
<?php
	function getUserAsTableRow($user)
	{
		$userString =
			'<tr>' .
				'<td>' . $user->id . '</td>' .
				'<td>' . $user->orgname . '</td>' .
				'<td>' . $user->firstname . ' ' . $user->lastname . '</td>' .
				'<td>' . $user->telephone . '</td>' .
				'<td>' . $user->email . '</td>' .
				'<td>' . $user->address . ' ' . $user->address2 . '</td>' .
				'<td>' . $user->city . '</td>' .
				'<td>' . $user->state . '</td>' .
				'<td>' . $user->zipcode . '</td>' .
				'<td>' . $user->country . '</td>' .
				'<td>' . '<a href="customeredit/' . $user->id . '"  class="btn btn-danger">Edit</a>' .
				'<td>' . '<a href="customerdelete/' . $user->id . '"  class="btn btn-danger" onclick="return confirm(\'Er du sikker?\')">Delete</a>' .
			'</tr>';

		return $userString;
	}

	function getOrderStatusString($order_status)
	{
		if ($order_status == 0)
			return 'Klar for betaling';
		if ($order_status == 1)
			return 'Betalt';
		if ($order_status == 2)
			return 'Eksportert';
		if ($order_status == 3)
			return 'Kansellert';
		if ($order_status == 4)
			return 'On Hold';
		if ($order_status == 5)
			return 'Kreditert';
		if ($order_status == 6)
			return 'Fakturakunde';

		return 'invalid status!';
	}

	function getOrderAsTableRow($order)
	{
		$theUser = markmaster\Models\User::find($order->user_id);
		if($order->user_id == 0) {
			$orderPaymenttype = 0;
		} else {
			$orderPaymenttype = $theUser->paymenttype;
		}
		if($order->currency  == 0) {
				$orderamountstring = number_format($order->amount , 2) . ' nok';
			}else if($order->currency == 1) {
				$orderamountstring =  '$ ' . number_format($order->amount , 2);
			}else if($order->currency == 2) {
				$orderamountstring = number_format($order->amount , 2) . ' sek';
			}else if($order->currency == 3) {
				$orderamountstring = number_format($order->amount , 2) . ' dkk';
			}else if($order->currency == 4) {
				$orderamountstring =  '€ ' . number_format($order->amount , 2);
			}
		if($order->order_status == 0) {
			$orderString =
					'<tr>' .
						'<td>' . $order->id . '</td>' .
						'<td>' . $order->orgname . '</td>' .
						'<td>' . $order->firstname . ' ' . $order->lastname . '</td>' .
						'<td>' . $order->user_id . '</td>' .
						'<td>' . $order->order_date . '</td>' .
						'<td>' . $orderamountstring . '</td>' .
						'<td>' . getOrderStatusString($order->order_status) . '</td>' .
						'<td>' . $order->export_date . '</td>' .
						'<td>' . $order->credit_date . '</td>' .
						'<td>' . '<a href="orderedit/' . $order->id . '"  class="btn btn-danger">Edit</a>' . '</td>' .
						'<td>' . '<a href="singleexport/' . $order->id . '"  class="mybtn">Export</a>' . '</td>' .
						'<td>' . '<a href="orderdelete/' . $order->id . '"  class="btn btn-danger" onclick="return confirm(\'Er du sikker?\')">Delete</a>' .
					'</tr>';
		} elseif ($order->order_status == 6) {
			$orderString =
					'<tr>' .
						'<td>' . $order->id . '</td>' .
						'<td>' . $order->orgname . '</td>' .
						'<td>' . $order->firstname . ' ' . $order->lastname . '</td>' .
						'<td>' . $order->user_id . '</td>' .
						'<td>' . $order->order_date . '</td>' .
						'<td>' . $orderamountstring . '</td>' .
						'<td>' . getOrderStatusString($order->order_status) . '</td>' .
						'<td>' . $order->export_date . '</td>' .
						'<td>' . $order->credit_date . '</td>' .
						'<td>' . '<a href="orderedit/' . $order->id . '"  class="btn btn-danger">Edit</a>' . '</td>' .
						'<td>' . '<a href="singleexport/' . $order->id . '"  class="mybtn">Export</a>' . '</td>' .

						'<td>' . '<a href="../invoice/pakkseddel/' . $order->id . '" class="btn btn-danger">pakkseddel</a>' . '</td>' .

						'<td>' . '' . '</td>' .
					'</tr>';
		}elseif ($order->order_status == 5) {
			$orderString =
					'<tr>' .
						'<td>' . $order->id . '</td>' .
						'<td>' . $order->orgname . '</td>' .
						'<td>' . $order->firstname . ' ' . $order->lastname . '</td>' .
						'<td>' . $order->user_id . '</td>' .
						'<td>' . $order->order_date . '</td>' .
						'<td>' . $orderamountstring . '</td>' .
						'<td>' . getOrderStatusString($order->order_status) . '</td>' .
						'<td>' . $order->export_date . '</td>' .
						'<td>' . $order->credit_date . '</td>' .
						'<td>' . '<a href="orderedit/' . $order->id . '"  class="btn btn-danger">Edit</a>' . '</td>' .
						'<td>' . '<a href="singleexport/' . $order->id . '"  class="mybtn">Export</a>' . '</td>' .

						'<td>' . '<a href="../invoice/credit/' . $order->id . '"  class="btn btn-danger">Credit</a>' . '</td>' .

						'<td>' . '<a href="../invoice/invoice/' . $order->id . '" class="btn btn-danger">Faktura</a>' . '</td>' .
					'</tr>';
		}elseif ($orderPaymenttype && $order->order_status == 5) {
			$orderString =
					'<tr>' .
						'<td>' . $order->id . '</td>' .
						'<td>' . $order->orgname . '</td>' .
						'<td>' . $order->firstname . ' ' . $order->lastname . '</td>' .
						'<td>' . $order->user_id . '</td>' .
						'<td>' . $order->order_date . '</td>' .
						'<td>' . $orderamountstring . '</td>' .
						'<td>' . getOrderStatusString($order->order_status) . '</td>' .
						'<td>' . $order->export_date . '</td>' .
						'<td>' . $order->credit_date . '</td>' .
						'<td>' . '<a href="orderedit/' . $order->id . '"  class="btn btn-danger">Edit</a>' .  '</td>'.
						'<td>' . '<a href="singleexport/' . $order->id . '"  class="mybtn">Export</a>' . '</td>' .

						'<td>' . '<a href="../invoice/pakkseddel/' . $order->id . '" class="btn btn-danger">Exportert pakkseddel</a>' . '</td>' .

						'<td>' . '<a href="../invoice/credit/' . $order->id . '"  class="btn btn-danger">Credit</a>' . '</td>' .

						'<td>' . '<a href="orderreset/' . $order->id . '"  class="btn btn-danger" onclick="return confirm(\'Er du sikker?\')">Reset</a>' . '</td>' .
					'</tr>';
		}elseif ($orderPaymenttype == 1) {
			$orderString =
					'<tr>' .
						'<td>' . $order->id . '</td>' .
						'<td>' . $order->orgname . '</td>' .
						'<td>' . $order->firstname . ' ' . $order->lastname . '</td>' .
						'<td>' . $order->user_id . '</td>' .
						'<td>' . $order->order_date . '</td>' .
						'<td>' . $orderamountstring . '</td>' .
						'<td>' . getOrderStatusString($order->order_status) . '</td>' .
						'<td>' . $order->export_date . '</td>' .
						'<td>' . $order->credit_date . '</td>' .
						'<td>' . '<a href="orderedit/' . $order->id . '"  class="btn btn-danger">Edit</a>' . '</td>' .
						'<td>' . '<a href="singleexport/' . $order->id . '"  class="mybtn">Export</a>' . '</td>' .

						'<td>' . '<a href="../invoice/pakkseddel/' . $order->id . '" class="btn btn-danger">Exportert pakkseddel</a>' . '</td>' .

						'<td>' . '<a href="orderreset/' . $order->id . '" class="btn btn-danger">Reset</a>' . '</td>' .

					'</tr>';
		} elseif ($order->order_status == 1) {

			$orderString =
					'<tr>' .
						'<td>' . $order->id . '</td>' .
						'<td>' . $order->orgname . '</td>' .
						'<td>' . $order->firstname . ' ' . $order->lastname . '</td>' .
						'<td>' . $order->user_id . '</td>' .
						'<td>' . $order->order_date . '</td>' .

						'<td>' . $orderamountstring . '</td>' .
						'<td>' . getOrderStatusString($order->order_status) . '</td>' .
						'<td>' . $order->export_date . '</td>' .
						'<td>' . $order->credit_date . '</td>' .
						'<td>' . '<a href="orderedit/' . $order->id . '"  class="btn btn-danger">Edit</a>' . '</td>' .
						'<td>' . '<a href="singleexport/' . $order->id . '"  class="mybtn">Export</a>' . '</td>' .

						'<td>' . '<a href="../invoice/invoice/' . $order->id . '" class="btn btn-danger">Faktura</a>' . '</td>' .

					'</tr>';

		} else {

			$orderString =
					'<tr>' .
						'<td>' . $order->id . '</td>' .
						'<td>' . $order->orgname . '</td>' .
						'<td>' . $order->firstname . ' ' . $order->lastname . '</td>' .
						'<td>' . $order->user_id . '</td>' .
						'<td>' . $order->order_date . '</td>' .
						'<td>' . $orderamountstring . '</td>' .
						'<td>' . getOrderStatusString($order->order_status) . '</td>' .
						'<td>' . $order->export_date . '</td>' .
						'<td>' . $order->credit_date . '</td>' .
						'<td>' . '<a href="orderedit/' . $order->id . '"  class="btn btn-danger">Edit</a>' . '</td>' .
						'<td>' . '<a href="singleexport/' . $order->id . '"  class="mybtn">Export</a>' . '</td>' .
						'<td>' . '<a href="../invoice/invoice/' . $order->id . '" class="btn btn-danger">Faktura</a>' . '</td>' .
						'<td>' . '<a href="orderreset/' . $order->id . '"  class="btn btn-danger" onclick="return confirm(\'Er du sikker?\')">Reset</a>' . '</td>' .
					'</tr>';

		}


		return $orderString;
	}
?>
