@extends('layouts.main')

@section('content')

	<div class="">
		<p>
			<a href="/{{ app()->getLocale() }}/admin/orders"> <button type="button" class="btn btn-danger">View Orders</button> </a>
			<a href="/{{ app()->getLocale() }}/admin/customers"> <button type="button" class="btn btn-danger">View Customers</button> </a>
			<a href="/{{ app()->getLocale() }}/admin/export"> <button type="button" class="btn btn-danger">Export Stickers</button> </a>
			<a href="/{{ app()->getLocale() }}/admin/icast"> <button type="button" class="btn btn-danger">Export Icast</button> </a>
		</p>
		<p>
			<a href="/{{ app()->getLocale() }}/admin/currency"> <button type="button" class="btn btn-danger">Currency</button> </a>
		</p>
		<p>
		<br>
		<a href="/{{ app()->getLocale() }}/admin/newexport"> <button type="button" class="mybtn">Export Stickers</button> </a>
		<a href="/{{ app()->getLocale() }}/admin/newiexport"> <button type="button" class="mybtn">Export Icast</button> </a>
		<br>
		<br>
		<br>
			<a href="/{{ app()->getLocale() }}/admin/agent"> <button type="button" class="btn btn-danger">Create new Commerical Agent</button> </a>
		</p>
	</div>
@stop
