@extends('layouts.main')

@section('content')

<?php
	include '../resources/views/admin/adminutil.php';
?>

<div class="">

	<h1>Orders</h1>
	{!! Form::label('orderSearch', 'Search Orders: ') !!}
	{!! Form::text('orderSearch', '', array('id' => 'orderSearchBox')) !!}
	<br/>
	{!! Form::label('any', 'All: ') !!}
	{!! Form::radio('searchField', 'any', true) !!}
	{!! Form::button('Search Button', array('onclick' => 'getOrderSearch()')) !!}

	<div class="table-responsive">
	<table class="table table-hover table-bordered " id="ordersTable">
		<tr id="tableHeader">
			<th>Order #			{!! Form::radio('searchField', 'id', false) !!} 			</th>
			<th>Org Name 		{!! Form::radio('searchField', 'orgname', false) !!}		</th>
			<th>Name			{!! Form::radio('searchField', 'name', false) !!} 		</th>
			<th>Customer ID		{!! Form::radio('searchField', 'user_id', false) !!} 		</th>
			<th>Order Date		{!! Form::radio('searchField', 'order_date', false) !!} 	</th>
			<th>Order Amount 	{!! Form::radio('searchField', 'amount', false) !!} 		</th>
			<th>Order Status	{!! Form::radio('searchField', 'order_status', false) !!} </th>
			<th>Export Date		{!! Form::radio('searchField', 'export_date', false) !!} 	</th>
			<th>Credit Date		{!! Form::radio('searchField', 'credit_date', false) !!} 	</th>
			<th>			</th>
			<th>			</th>
			<th>			</th>
			<th>			</th>
		</tr>
		<span id="ordersData">
			<?php foreach ($orders as $order): ?>
				<?php echo 	getOrderAsTableRow($order); ?>
			<?php endforeach; ?>
		</span>
	</table>

	<div id="pagination" align="center">
		{!! $orders->render() !!}
	</div>
</div>

@stop

<script>

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getOrderSearch() {
  var field = $('input:radio[name="searchField"]:checked').val()
  var search = $('#orderSearchBox').val();
  var page = getParameterByName('page');
  if (page == '') page = '1';
  console.log(page);
  var table = $('#ordersTable');
  var tableHeader = $('#tableHeader');

  var data = {'field': field, 'search': search};
  if (search == '')
  {
	data['page'] = page;
  }

  $.ajax({
      type: 'get',
      url: 'ordersearch',
      cache: false,
      dataType: 'html',
      data: data,
      success: function(data) {
		  console.log(data);
		  table.html(tableHeader);
		  table.append(data);
      },
      error: function(xhr, textStatus, thrownError) {
		  console.error("search failed" + xhr + textStatus + thrownError);
          // alert('Something went to wrong.Please Try again later...');
      }
  });
}

</script>
