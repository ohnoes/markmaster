@extends('layouts.main')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php
		ini_set('memory_limit', '1024M');
		$icast1 = 70;
		$icast2 = 57;
		$orders = Order::where("order_status", 1)->where(function ($query) use ($icast1, $icast2) {
				$query->where("user_id", '!=', $icast1)
						->where("user_id", '!=', $icast2);
			})->orWhere("order_status", 6)
			->where(function ($query) use ($icast1, $icast2) {
				$query->where("user_id", '!=', $icast1)
						->where("user_id", '!=', $icast2);
			})->orderBy('id', 'DESC')->get(['*']);

			$stickers = Orderitem::where("status", 1)
				->where(function ($query) use ($icast1, $icast2) {
					$query->where("user_id", '!=', $icast1)
							->where("user_id", '!=', $icast2);
				})->get(['*']);
    $typeofsticker = 1;
?>

<div class="">
	</br></br>
		{!! Form::open(array('files' => true, 'id' => 'handleexport')) !!}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button type="button" class="mybtn" onclick="renderAll(0,0)">Export All</button>
	</br></br></br></br>

  @include('designer.hiddeninput')
  {!! Form::hidden('isRect', '1', array('id'=>'isRect')) !!}
  {!! Form::hidden('isEllipse', '0', array('id'=>'isEllipse')) !!}
  {!! Form::hidden('isGeneratedCutline', '0', array('id'=>'isGeneratedCutline')) !!}
  {!! Form::hidden('width', '37', array('id'=>'width')) !!}
  {!! Form::hidden('height', '16', array('id'=>'height')) !!}
  {!! Form::hidden('fontFamily', '', array('id'=>'fontFamily')) !!}
  {!! Form::hidden('fill', '#000000', array('id'=>'fill')) !!}
  {!! Form::hidden('strokeWidthTextbox', '0.1', array('id'=>'strokeWidthTextbox')) !!}
  {!! Form::hidden('stroke', '#000000', array('id'=>'stroke')) !!}
  {!! Form::hidden('quantity', '1', array('id'=>'quantity')) !!}
  {!! Form::hidden('priceHolder', '1', array('id'=>'priceHolder')) !!}
  {!! Form::hidden('postprice', '1', array('id'=>'postprice')) !!}
  {!! Form::hidden('stickerpriceholder', '1', array('id'=>'stickerpriceholder')) !!}
  {!! Form::hidden('thePrice', '1', array('id'=>'thePrice')) !!}
  {!! Form::hidden('thePrice2', '1', array('id'=>'thePrice2')) !!}
  {!! Form::hidden('cilpartForm', 'cilpartForm', array('id'=>'cilpartForm')) !!}
  {!! Form::hidden('clipartFileSelect', 'clipartFileSelect', array('id'=>'clipartFileSelect')) !!}
  {!! Form::hidden('backgroundForm', 'backgroundForm', array('id'=>'backgroundForm')) !!}
  {!! Form::hidden('backgroundFileSelect', 'backgroundFileSelect', array('id'=>'backgroundFileSelect')) !!}
	<input type="hidden" name="redirect" value="{{ app()->getLocale() }}/admin" id="redirect">
	<div class="table-responsive">
	<table class="table table-hover table-bordered ">

			<tr>
				<th> Bestillingsnummer</th>
				<th> Bestillingsdato</th>
				<th> Leverings adresse</th>
				<th> Status </th>
				<th> Pris </th>
        <th> Single Export </th>
			</tr>

      @foreach($orders as $order)
				@foreach($stickers as $sticker)
					@if($sticker->order_id == $order->id)
						{!! Form::hidden($sticker->id, $sticker->json, array('id'=>$sticker->id)) !!}
						<input type="hidden" id="type-<?php echo $sticker->id?>" name="type=<?php echo $sticker->id?>" value="<?php echo $sticker->stickertype ?>">
					@endif
				@endforeach
				<tr>
					<td> {!! $order->id !!} </td>
					<td> {!! $order->order_date !!} </td>
					<td> {!! $order->address, ' ', $order->address2, ' ', $order->zipcode, ' ', $order->city !!} </td>
					<td>
						@if ($order->order_status == 0)
							{!! 'Klar for betaling' !!}
						@elseif ($order->order_status == 1)
							{!! 'Betalt' !!}
						@elseif ($order->order_status == 2)
							{!! 'Eksporert' !!}
						@elseif ($order->order_status == 3)
							{!! 'Kansellert' !!}
						@elseif ($order->order_status == 6)
							{!! 'Fakturakunde' !!}
						@else
							{!! 'N/A' !!}
						@endif
					</td>
					<td> {!! $order->amount . ',-' !!} </td>
	        <td> <a href="/{{ app()->getLocale() }}/admin/singleexport/<?php echo $order->id?>"> <button type="button" class="mybtn">Export</button> </a> </td>
				</tr>
			@endforeach
		</table>
	</div>
</div>

<div class="col-sm-12 col-md-12 col-lg-12 design-tools-canvas"> <!-- Canvas block -->
<!-- Canvas section -->
				<div id="canvasDiv">
					<canvas id="myCanvas" width="900" height="450"></canvas>
				</div>
</div>

{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/jquery.fontselector.js') !!}
<!-- {!! Html::script('js/fabric.js-master/dist/fabric.min.js') !!} -->
{!! Html::script('js/fabric.js-master/dist/fabric.js') !!}
{!! Html::script('js/jquery.simplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/spin.min.js') !!}
{!! Html::script('js/customiseControls.min.js') !!}
{!! Html::script('js/outline-2.js') !!}
{!! Html::script('js/background-2.js') !!}
{!! Html::script('js/clipart-2.js') !!}
{!! Html::script('js/price-2.js') !!}
{!! Html::script('js/text-2.js') !!}
{!! Html::script('js/bootstrap-colorpicker.js') !!}
{!! Html::script('js/upload-2.js') !!}
{!! Html::script('js/store-2.js') !!}
{!! Html::script('js/canvasToImage-2.js') !!}
{!! Html::script('js/jsonToPDF.js') !!}
{!! Html::script('js/textbuttons-2.js') !!}

<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script>
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' &&
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' ||
                 d.type.toUpperCase() === 'FILE' ||
                 d.type.toUpperCase() === 'SEARCH' ||
                 d.type.toUpperCase() === 'EMAIL' ||
                 d.type.toUpperCase() === 'NUMBER' ||
                 d.type.toUpperCase() === 'DATE' )
             ) ||
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});


function renderAll(orderIndex, stickerNumber) {
	//sleep(1000);
	orderArray = <?php echo json_encode($orders) ?>;
	stickerArray = <?php echo json_encode($stickers) ?>;

	var stickersInOrder = 0;
	stickersInOrder = 0;
	for(var i = 0; i < stickerArray.length; i++) {
		 if(stickerArray[i].order_id == orderArray[orderIndex].id) {
			 if(stickerNumber == stickersInOrder) {
				 var jsonToLoad = JSON.parse(document.getElementById(stickerArray[i].id).value);
				 var sticktype = document.getElementById("type-"+stickerArray[i].id).value;
				 var stickid = stickerArray[i].id;
			 }
			 stickersInOrder++;
		 }
	 }

	 if(stickersInOrder == 0) {
		 renderAll(orderIndex+1,0);
	 } else {

		 	// orderIndex = 0, the first iteration of orders
		 	// orderIndex = orderArray.length-1 last iteration
		 	// if stickerNumber = stickersInOrder-1, last sticker of the order, move to next order

			var completeorder;
			console.log("Stickers in order: " + stickersInOrder);
			console.log("stickerNumber: " + stickerNumber);
			if(stickerNumber == stickersInOrder-1) {
				completeorder = 1;
			} else {
				completeorder = 2;
			}
			var THE_TOKEN = $('meta[name="csrf-token"]').attr('content');
			loadfromJsonExp(jsonToLoad, stickid, sticktype);
		 	var formData = new FormData();
		 	formData.append('stickerType', sticktype);
		 	formData.append('stickerid', stickid);
			formData.append('completeorder', completeorder);
		 	$.ajax({
		 			type: 'post',
		 			url: 'handleexport',
		 			beforeSend: function (xhr) {
		 				var token = $('meta[name="csrf-token"]').attr('content');
		 				if (token) {
		 							return xhr.setRequestHeader('X-CSRF-TOKEN', token);
		 				}
		 			 },
		 			cache: false,
		 			contentType: false,
		 			processData: false,
		 			dataType: 'html',
		 			data: formData,
		 			beforeSend: function() {
		 					showPopup();
		 					$("#validation-errors").hide().empty();
		 			},
		 			success: function(data) {
		 				moreToLoad = true;
		 				hidePopup();
		 				// console.log(data);
		 				test1=data;
		 				// console.log(data.length);
		 				if (data.search("failed") > -1)
		 				{

		 				putMessage(data.split("failed")[1]);
		 				}
		 				else {
		 					// SUCCESS MOVE TO NEXT STEP
							stickerNumber++;
							if(stickerNumber == stickersInOrder) {
								orderIndex++;
							}
							if(orderIndex == orderArray.length && stickerNumber == stickersInOrder) { // we are done, redirect
								alert("WE ARE DONE!");
							} else if(stickerNumber == stickersInOrder) { // reset stickers
								stickerNumber = 0;
								renderAll(orderIndex, stickerNumber);
							} else {
								renderAll(orderIndex, stickerNumber);
							}
		 				}
		 			},
		 			error: function(xhr, textStatus, thrownError) {
		 			hidePopup();
		 					console.error('AJAX error Something went to wrong.Please Try again later...');
		 					console.error(xhr);
		 					console.error(textStatus);
		 					console.error(thrownError);
		 			}
		 	});
	 }
}

</script>

@stop
