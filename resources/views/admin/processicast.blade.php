<?php

	$icast1 = 70;
	$icast2 = 57;
	$orders = Order::where("order_status", 6)
		->where(function ($query) use ($icast1, $icast2) {
			$query->where("user_id", '=', $icast1)
					->orwhere("user_id", '=', $icast2);
		})->get(['*']);

	$stickers = Orderitem::where("status", 1)
		->where(function ($query) use ($icast1, $icast2) {
			$query->where("user_id", '=', $icast1)
					->orwhere("user_id", '=', $icast2);
		})->get(['*']);


	//$filePath = "/var/www/html/markmaster/public/exports/";
	// $filePath = "../../public/export/";
	$filePath = "../exports/";
	$orderCustomersFileName = $filePath . "orderCustomers.txt";
	$orderLinesFileName = $filePath . "orderLines.txt";


	include '../resources/views/stickerSubmit.blade.php';
	$badStickers = array();
	$badOrders = array();
	$firstError = true;
	foreach($orders as $order)
	{
		foreach ($stickers as $sticker)
		{
			if ($sticker->order_id == $order->id)
			{
				// print_r($orderitem);
				try
				{
					renderFullsizeSticker($sticker->json, $sticker->id, $sticker->stickertype);
					$sticker->status = 2;
					$sticker->save();
				}
				catch(Exception $e)
				{
					$badStickers[$sticker->id] = 1;
					$badOrders[$sticker->order_id] = 1;
					$stickerErrorsFileName = $filePath . "stickerErrors.txt";
					if (!file_exists($stickerErrorsFileName))
					{
						$stickerErrors = fopen($stickerErrorsFileName, "x"); // information about the order
					}
					else
					{
						$stickerErrors = fopen($stickerErrorsFileName, "a");
					}
					if ($firstError)
					{
						$firstError = false;
						fwrite($stickerErrors, "---------------------------New Errors---------------------------");
					}
					fwrite($stickerErrors, "\nError encounted while rendering sticker (id: " . $sticker->id . ", order-id: " . $sticker->order_id . ")");
					echo "<p>Error encounted while rendering sticker (id: " . $sticker->id . ", order-id: " . $sticker->order_id . ")</p>";
				}
			}
		}
	}




	// if orderCustomers.txt doesn't exist, then create and write header to it.
	if (!file_exists($orderCustomersFileName))
	{
		$orderCustomers = fopen($orderCustomersFileName, "x"); // information about the order
		$orderCustomersFormat = "User ID|Order ID|Order date|price|organisation|firstname|lastname" .
								"|address|address2|zipcode|city|state|country|" .
								"|billingorganisation|billingfirstname|billinglastname|billingaddress|billingaddress2" .
								"|billingzipcode|billingcity|billingstate|billingcountry";
		fwrite($orderCustomers, $orderCustomersFormat);
		fclose($orderCustomers);
	}

	$orderCustomers = fopen($orderCustomersFileName, "a");



	// if orderLines.txt doesn't exist, then create and write header to it.
	if (!file_exists($orderLinesFileName))
	{
		$orderLines = fopen($orderLinesFileName, "x"); // information about the stickers
		$orderLinesFormat = 	"User ID|Order ID|Sticker|quantity|price" .
								"|stickerType|width|height";
		fwrite($orderLines, $orderLinesFormat);
		fclose($orderLines);
	}

	$orderLines = fopen($orderLinesFileName, "a"); // information about the stickers

	foreach ($orders as $order)
	{
		// if problem found with order, skip over it
		if (array_key_exists($order->id, $badOrders))
		{
			continue;
		}
		// orderCustomers.txt information about order that's being exported (sort by customer ID?)
		fwrite($orderCustomers, "\n" . $order->user_id . "|" . $order->id . "|" . $order->order_date . "|" . $order->amount . "|" . $order->orgname . "|" . $order->firstname . "|" . $order->lastname);
		fwrite($orderCustomers, "|" . $order->address . "|" . $order->address2 . "|" . $order->zipcode . "|" . $order->city . "|" . $order->state . "|" . $order->country);
		fwrite($orderCustomers, "|" . $order->billingorgname . "|" . $order->billingfirstname . "|" . $order->billinglastname . "|" . $order->billingaddress . "|" . $order->billingaddress2);
		fwrite($orderCustomers, "|" . $order->billingzipcode . "|" . $order->billingcity . "|" . $order->billingstate . "|" . $order->billingcountry);

	}

	foreach ($stickers as $sticker)
	{
		// if problem found with sticker, skip over it
		if($sticker->order_id) {

			$order = Order::find($sticker->order_id);
			if (array_key_exists($order->id, $badOrders))
			{
				continue;
			}
			if($order->order_status == 6) {
				// orderLines.txt information about each sticker that's being exported (sort by customer ID?)
				fwrite($orderLines, "\n" . $sticker->user_id . "|" . $sticker->order_id . "|" . $sticker->id . "|" . $sticker->quantity . "|" . $sticker->price . "|" . $sticker->stickertype);
				fwrite($orderLines, "|" . $sticker->width . "|" . $sticker->height);
			}
		}

	}

	fclose($orderCustomers);
	fclose($orderLines);


	foreach ($orders as $order)
	{
		// if bad order / unable to render sticker within order
		if (array_key_exists($order->id, $badOrders))
		{

		}
		else // order render successful
		{
			$order->order_status = 2;

			$order_export_date = new DateTime;
			$order->export_date =  $order_export_date->format('Y-m-d H:i:s');

			$order->save();
		}
	}

?>

{!! Redirect::to('/no/admin/icast')->with('message', 'Export Complete') !!}
