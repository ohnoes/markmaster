@extends('layouts.main')

@section('title')
  {{ trans('order/details.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('order/details.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/order/details/">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	<!-- Display the detail about the sticker, very much like the shopping cart -->
	<div class="container ">

	<div class="table-responsive">
	<table class="table table-hover table-bordered ">
	<meta charset="utf-8">
                <tr>
                    <th>{{ trans('order/details.Translate3') }}</th>
                    <th>{{ trans('order/details.Translate4') }}</th>
                    <th>{{ trans('order/details.Translate5') }}</th>
                    <th>{{ trans('order/details.Translate6') }}</th>
                    <th>{{ trans('order/details.Translate7') }}</th>
					<th>{{ trans('order/details.Translate8') }}</th>
					<th> </th>
                </tr>

                @foreach($stickers as $sticker)
                <tr>
					<td>
            @if($sticker->stickertype == 1)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merksticker') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 2)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironc') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 3)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkgold') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 4)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merksolv') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 5)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merktrans') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 6)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkcheap') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 7)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkiron') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 8)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkreflection') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @elseif($sticker->stickertype == 9)
              <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironfree') }}?id={!! $sticker->id !!}&dvos={!! Auth::user()->id !!}">
              <button type="button" class="btn btn-danger">{{ trans('order/details.Translate9') }}</button> </a>
           @endif

					</td>

                    <td>
                       {!! Html::image($sticker->thumbnail, $sticker->stickertype, array('class'=>'thumbnailpic')) !!}
                    </td>
                    <td>
						@if($sticker->stickertype == 1)
							{{ trans('order/details.Translate10') }}
						@elseif($sticker->stickertype == 2)
							{{ trans('order/details.Translate11') }}
						@elseif($sticker->stickertype == 3)
							{{ trans('order/details.Translate12') }}
						@elseif($sticker->stickertype == 4)
							{{ trans('order/details.Translate13') }}
						@elseif($sticker->stickertype == 5)
							{{ trans('order/details.Translate14') }}
						@elseif($sticker->stickertype == 6)
							{{ trans('order/details.Translate15') }}
						@elseif($sticker->stickertype == 7)
							{{ trans('order/details.Translate16') }}
						@elseif($sticker->stickertype == 8)
							{{ trans('order/details.Translate17') }}
						@elseif($sticker->stickertype == 9)
							{{ trans('order/details.Translate21') }}
						@endif
					</td>
					<td>
						{!! '' . $sticker->width . ' x ' . $sticker->height !!}
					</td>
                    <td>
                       {!! $sticker->quantity !!}
                    </td>
					<?php $order = Order::find($sticker->order_id)?>
                    <td>
						@if($order->currency == 0)
							{!! number_format ($sticker->price , 2 ) . ' nok' !!}
						@elseif($order->currency == 1)
							{!! '$ ' . number_format ($sticker->price , 2 ) !!}
						@elseif($order->currency == 2)
							{!! number_format ($sticker->price , 2 ) . ' sek' !!}
						@elseif($order->currency == 3)
							{!! number_format ($sticker->price , 2 ) . ' dkk' !!}
						@elseif($order->currency == 4)
							{!! '€ ' . number_format ($sticker->price , 2 ) !!}
						@endif
					</td>
					<td>
						@if($sticker->status == 0 || $sticker->status == 1)
                @if($sticker->stickertype == 1)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merksticker') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 2)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironc') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 3)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkgold') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 4)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merksolv') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 5)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merktrans') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 6)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkcheap') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 7)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkiron') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 8)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkreflection') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @elseif($sticker->stickertype == 9)
                  <a href="/{{ app()->getLocale() }}/{{ trans('routes.merkironfree') }}?edit={!! $sticker->id !!}">
                  <button type="button" class="btn btn-danger">{{ trans('order/details.Translate18') }}</button> </a>
               @endif
						@endif
						<?php $order = Order::find($sticker->order_id)?>
						@if($order->order_status == 0)
							<a href="/{{ app()->getLocale() }}/order/deletesticker/<?php echo $sticker->id ?>"> <button type="button" class="btn btn-danger" onclick="return confirm(trans('order/details.Translate20'));">{{ trans('order/details.Translate19') }}</button> </a>
						@endif

                    </td>
                </tr>
                @endforeach
            </table>

	</div>
	</div>

@stop
