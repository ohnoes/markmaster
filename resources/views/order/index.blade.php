@extends('layouts.main')

@section('title')
    {{ trans('order/index.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('order/index.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/order/">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')

	@if(count($errors) > 0)
		<div class="alert alert-danger">
			<p> {{ trans('order/index.Translate3') }}</p>
			<ul>
				@foreach($errors->all() as $error)
					<li> {!! $error !!}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="container ">

		<h1>{{ trans('order/index.Translate4') }}</h1><hr>

		<div class="table-responsive">
		<table class="table table-hover table-bordered ">
			<tr>
				<th> {{ trans('order/index.Translate5') }}</th>
				<th> {{ trans('order/index.Translate6') }}</th>
				<th> {{ trans('order/index.Translate7') }}</th>
				<th> {{ trans('order/index.Translate8') }} </th>
				<th> {{ trans('order/index.Translate9') }} </th>
				<th> </th>
			</tr>

			@foreach($orders as $order)
			<tr>
				<td> {!! $order->id !!} </td>
				<td> {!! $order->order_date !!} </td>
				<td> {!! $order->address, ' ', $order->address2, ' ', $order->zipcode, ' ', $order->city !!} </td>
				<td>
					@if ($order->order_status == 0)
						{{ trans('order/index.Translate10') }}
					@elseif ($order->order_status == 1)
						{{ trans('order/index.Translate11') }}
					@elseif ($order->order_status == 2)
						{{ trans('order/index.Translate12') }}
					@elseif ($order->order_status == 3)
						{{ trans('order/index.Translate13') }}
					@elseif ($order->order_status == 4)
						{{ trans('order/index.Translate14') }}
					@elseif ($order->order_status == 6)
						{{ trans('order/index.Translate21') }}
					@else
						{{ trans('order/index.Translate15') }}
					@endif
				</td>
				<td>
					@if($order->currency == 0)
						{!! number_format ($order->amount , 2 ) . ' nok' !!}
					@elseif($order->currency == 1)
						{!! '$ ' . number_format ($order->amount , 2 ) !!}
					@elseif($order->currency == 2)
						{!! number_format ($order->amount , 2 ) . ' sek' !!}
					@elseif($order->currency == 3)
						{!! number_format ($order->amount , 2 ) . ' dkk' !!}
					@elseif($order->currency == 4)
							{!! '€ ' . number_format ($order->amount , 2 ) !!}
					@endif


				</td>
				<td>
					<a href="/{{ app()->getLocale() }}/order/details/<?php echo $order->id ?>"> <button type="button" class="btn btn-danger">{{ trans('order/index.Translate16') }}</button> </a>
					 @if($order->order_status == 0)
						<a href="/{{ app()->getLocale() }}/order/pay/<?php echo $order->id ?>"> <button type="button" class="btn btn-danger">{{ trans('order/index.Translate17') }}</button> </a>
					 @endif
					 @foreach($fakturas as $faktura)
						@if($faktura->order_id == $order->id)
							<a href="/{{ app()->getLocale() }}/invoice/invoice/<?php echo $order->id ?>"> <button type="button" class="btn btn-danger" target="popup" onclick="window.open('/invoice/invoice/'<?php echo $order->id?>  ,'name','width=600,height=400')">{{ trans('order/index.Translate18') }}</button> </a>
						@endif
					 @endforeach
					 @if($order->order_status == 0)
						<a href="/{{ app()->getLocale() }}/order/delete/<?php echo $order->id ?>"> <button type="button" class="btn btn-danger" onclick="return confirm({{ trans('order/index.Translate20') }});">{{ trans('order/index.Translate19') }}</button> </a>
					 @endif
				</td>
			</tr>
			@endforeach

		</table>
		</div>

	</div><!-- end orders -->

	<div id="pagination">
		{!! $orders->render() !!}
	</div>

@stop
