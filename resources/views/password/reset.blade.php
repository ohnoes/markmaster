@extends('layouts.main')

@section('content')
	<div class="container">

		<form action="{!! action('RemindersController@postReset') !!}" method="POST">
			<input type="hidden" name="token" value="{!! $token !!}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<div class="form-group">
				<label for="email">{{ trans('password/reset.Translate1') }}</label>
				{!! Form::email('email', '', array('class'=>'form-control')) !!}
			</div>
			<div class="form-group">
				<label for="password"> {{ trans('password/reset.Translate2') }} </label>
				<input type="password" name="password" class="form-control">
			</div>
			<div class="form-group">
				<label for="password_confirmation"> {{ trans('password/reset.Translate3') }} </label>
				<input type="password" name="password_confirmation" class="form-control"></br></br>
			</div>
			<input type="submit" value="Reset Password" class="btn btn-danger">
		</form>

	</div>
@stop
