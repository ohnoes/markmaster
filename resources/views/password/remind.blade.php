@extends('layouts.main')

@section('title')
    Password reminder
@stop

@section('head-meta')
	<meta name="description" content="Password reminder">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/password/remind">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('message')
<span id="messageSpan">
	 @if (Session::has('messageLoginSuccess'))
		 <div class="alert alert-success" role="alert"> {{ trans('layouts/main.loginmsg') }} </div>
	 @endif
	 @if (Session::has('message'))
		 <div class="alert alert-info" role="alert"> {!! Session::get('message') !!} </div>
	 @endif
</span>
@stop

@section('content')
	<div class="container">

		@if (session('status'))
						<div class="alert alert-success">
							{!! session('status') !!}
						</div>
		@endif

		<form action="{!! action('RemindersController@postRemind') !!}" method="POST">

			<div class="form-group">
				<label for="email"> Epost: </label>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
				{!! Form::email('email', '', array('class'=>'form-control')) !!}
				</br>
				<input type="submit" value="Send Passord" class="btn btn-danger">
			</div>
		</form>

	</div>
@stop
