<?php
if (Input::has('getClipart') && Input::get('getClipart') == 1 && Input::has('category'))// && Auth::check())
{
	$search = '';
	if (Input::has('search'))
	{
		$search = Input::get('search');
	}
	$clipIndex = 0;
	if (Input::has('clipIndex'))
	{
		$clipIndex = Input::get('clipIndex');
	}
	$numToLoad = 20;
	if (Input::has('numToLoad'))
	{
		$numToLoad = Input::get('numToLoad');
	}
	getClipart(Input::get('category'), $search, $clipIndex, $numToLoad);
}
else if (Input::has('uploadClipart') && Input::get('uploadClipart') == 1 && Input::has('category') && Auth::check())
{
	 // return Redirect::to('storeTest')
		// ->with('message', 'Failed to create clipart.');
		
		// return Response::json(array('success' => false,
									// 'errors' => $validator->getMessageBag()->toArray()
		// ), 400);
		
	return uploadClipart();
	// getClipart(Input::get('category'), '', 0, 20);
	echoClipart(Input::get('category'), 20);
}
else
{
	// echo 'error';
	 // return Redirect::to('storeTest')
		// ->with('message', 'Failed to create clipart.');
}


// called when user submits clipart to be uploaded and saved to database
function uploadClipart()
{
	$validator = Validator::make(Input::all(), Clipart::$rules);
	if ($validator->fails())
	{
	
		return Redirect::to('storeTest')
		->with('message', 'Failed to create clipart.');
	
		// return Response::json(array('success' => false,
									// 'errors' => $validator->getMessageBag()->toArray()
		// ), 400);
	}

	// ensure valid data
	if ($validator->passes()) {
		$clipart = new Clipart;
		if (Input::has('title')) {
			$clipart->title = Input::get('title');
		} else {
			$clipart->title = 'userClipart';
		}
		


		// if user logged in
		if (Auth::check())
		{
			// save user id with clipart
			$clipart->user_id = Auth::user()->getId();


			$image = Input::file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$destination = public_path() . '/img/clipart';
			Image::make($image->getRealPath())->save($destination .'/'. $filename); // Fix the resizing to keep original format here
			$clipart->image = 'img/clipart/'.$filename;
			$clipart->save();
			
			
			 // return Redirect::to('storeTest')
			 	// ->with('message', 'Clipart Created');
		}
	}
	
	 // return Redirect::to('storeTest')
		// ->with('message', 'Failed to create clipart.');
}


// echos json object containing the urls to clipart matching the category, and subcategories matching $search
// 
function getClipart($category, $search, $startIndex, $numToLoad)
{
	// build array containing id of every category matching (partial or full) $search
	// $matches = array();
	// if ($search !== '')
	// {
	// 	foreach(Category::all() as $cat)
	// 	{
	// 		if (strpos($cat->name, $search) !== false)
	// 		{
	// 			array_push($matches, $cat->id);
	// 		}
	// 	}
	// }
	
	if (!Auth::user() && $category == 1)
	{
		$data = array('clipIndex' => 0,
				  'numLoaded' => 0,
				  'names'     => 0,
				  'user'	  => false);
		echo json_encode($data);
	}
	else
	{

		$names = array();
		$numLoaded = 0;
		$clipIndex = 0;
		foreach(Clipart::all() as $clip)
		{
			$subcat1 = $clip->subcat1;
			$subcat2 = $clip->subcat2;
			$name = '../' . $clip->image;
			$onclick = 'addClipart(\'' . $name . '\')';
			if ($category == 0) // show all
			{
				if ($clip->public == 1 || (Auth::user() && $clip->user_id==Auth::user()->getId()) )
				{
					// if searching for subcategory and match not found, skip clipart
					if ($search !== '' && strpos($clip->searchwords, $search) === false)
						continue;

					$clipIndex++;
					if ($clipIndex <= $startIndex)
						continue;
					array_push($names, $name);
					// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
					// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" data-subcat1="', $subcat1, '" data-subcat2="', $subcat2, '" onclick="', $onclick, '">';
					$numLoaded++;
					if ($numLoaded >= $numToLoad)
						break;
				}
			}
			else if ($category == 1) // show user's cilpart
			{
				if ($clip->user_id==Auth::user()->getId() )
				{
					if ($search !== '' && strpos($clip->searchwords, $search) === false)
						continue;
					$clipIndex++;
					if ($clipIndex <= $startIndex)
						continue;
					array_push($names, $name);
					// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
					$numLoaded++;
					if ($numLoaded >= $numToLoad)
						break;
				}
			}
			else // show requested category clipart
			{
				if ($clip->category == $category || $clip->subcat1 == $category || $clip->subcat2 == $category)
				{
					if ($search !== '' && strpos($clip->searchwords, $search) === false)
						continue;
					$clipIndex++;
					if ($clipIndex <= $startIndex)
						continue;
					array_push($names, $name);
					// echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
					$numLoaded++;
					if ($numLoaded >= $numToLoad)
						break;
				}
			}
		}
		$data = array('clipIndex' => $clipIndex,
					  'numLoaded' => $numLoaded,
					  'names'     => $names,
					  'user'	  => true);
		echo json_encode($data);
	}
}

// echos out $numToLoad many html clipart matching the category as images, styled with the class 'clipartImg', and the onclick event 'addClipart'
function echoClipart($category, $numToLoad)
{
	$numToLoad = intval($numToLoad);
	$numLoaded = 0;
	foreach(Clipart::all() as $clip)
	{
		$subcat1 = $clip->subcat1;
		$subcat2 = $clip->subcat2;
		$name = '../' . $clip->image;
		$onclick = 'addClipart(\'' . $name . '\')';
		if ($category == 0) // show all
		{
			if ($clip->public == 1 || (Auth::user() && $clip->user_id==Auth::user()->getId()) )
			{
				echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
				$numLoaded++;
				if ($numLoaded >= $numToLoad)
					break;
			}
	    }
	  	else if ($category == 1) // show user's cilpart
	  	{
			if ($clip->user_id==Auth::user()->getId() )
			{
				echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
				$numLoaded++;
				if ($numLoaded >= $numToLoad)
					break;
			}
	    }
	    else // show requested category clipart
	    {
	    	if ($clip->category == $category || $clip->subcat1 == $category || $clip->subcat2 == $category)
	    	{
				echo '<img src="', $name, '" name="', $name, '" class="clipartImg" onclick="', $onclick, '">';
				$numLoaded++;
				if ($numLoaded >= $numToLoad)
					break;
			}
	    }
	}
}

?>