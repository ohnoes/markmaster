@extends('layouts.main')

@section('title')
	{{ trans('templates/reflection.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('templates/reflection.Translate2') }}">
	<meta name="keywords" content="{{ trans('templates/reflection.keywords') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/{{ trans('routes.tempreflection') }}">
@stop

@section('wrapper')
<div class="row main-row">
	<div class="wrapper col-sm-8 col-sm-push-2" data-pg-collapsed>
@stop

@section('promo')
<!--
<div class="promo-wrapper">
	<div id="carousel1" class="carousel slide" data-ride="carousel" data-pg-collapsed>
			<div class="carousel-inner">
					<div class="item active imageitem">
							<img src="/img/views/template/strykefri_navnelapp_template_banner.png" alt="{{ trans('templates/fabricsticker.imagealt') }}" id="klistrebilde">
					</div>
			</div>
	</div>
</div> -->
@stop

@section('content')

<div class="content-wrapper">

	<div class="row row-heading" data-pg-collapsed>
			<div class="col-xs-12">
					<h3 class="text-center">{{ trans('templates/klisterfarge.Header') }}</h3>
			</div>
	</div>
	<?php
		$counter = 0;
	 ?>
			@foreach($templates as $template)
				@if($template->product_type == 7)
				<?php
						$tmp = "/" . app()->getLocale() . "/" . trans('routes.merkreflection') . "?tmp=".$template->id;
					?>

					@if(true)
					@if($counter % 4 == 0)
						<div class="row no-margin" style="margin: 15px 7.5px; background-color: #e7e7e7;" data-pg-collapsed>
					@endif
					<div class="col-sm-3 col-md-3 col-lg-3 box-padding">
						<a href="<?php echo $tmp ?>">
								<div class="row no-margin">
									<div class="col-sm-12 col-xs-4 no-padding" style="background-color: white; border: 1px solid;">
										<img src="/<?php echo $template->image ?>" alt="<?php echo $template->title ?>" width="100%">
									</div>
									<div class="col-sm-12 col-xs-8 textbox" style="text-align: center;">
										<h3> {!! $template->title !!} </h3>
										<p> {!! $template->description !!} </p>
									</div>
								</div>
						</a>
					</div>
					@if($counter % 4 == 3)
						</div>
					@endif
					<?php
						$counter++;
					 ?>
					@elseif(app()->getLocale() == 'en')
					<div class="col-sm-6 col-md-4 col-lg-4 box-padding">
						<a href="<?php echo $tmp ?>">
								<div class="row no-margin">
									<div class="col-sm-12 col-xs-4 no-padding">
										<img src="/<?php echo $template->image ?>" alt="<?php echo $template->title ?>" width="100%">
									</div>
									<div class="col-sm-12 col-xs-8 textbox">
										<h3> {!! $template->titleEN !!} </h3>
										<p> {!! $template->descriptionEN !!} </p>
									</div>
								</div>
						</a>
					</div>
					@elseif(app()->getLocale() == 'se')
					<div class="col-sm-6 col-md-4 col-lg-4 box-padding">
						<a href="<?php echo $tmp ?>">
								<div class="row no-margin">
									<div class="col-sm-12 col-xs-4 no-padding">
										<img src="/<?php echo $template->image ?>" alt="<?php echo $template->title ?>" width="100%">
									</div>
									<div class="col-sm-12 col-xs-8 textbox">
										<h3> {!! $template->titleSE !!} </h3>
										<p> {!! $template->descriptionSE !!} </p>
									</div>
								</div>
						</a>
					</div>
					@elseif(app()->getLocale() == 'dk')
					<div class="col-sm-6 col-md-4 col-lg-4 box-padding">
						<a href="<?php echo $tmp ?>">
								<div class="row no-margin">
									<div class="col-sm-12 col-xs-4 no-padding">
										<img src="/<?php echo $template->image ?>" alt="<?php echo $template->title ?>" width="100%">
									</div>
									<div class="col-sm-12 col-xs-8 textbox">
										<h3> {!! $template->titleDK !!} </h3>
										<p> {!! $template->descriptionDK !!} </p>
									</div>
								</div>
						</a>
					</div>
					@endif
				@endif
			@endforeach
		</div>
	</div>
</div>
@stop
