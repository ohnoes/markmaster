@extends('layouts.main')

@section('title')
	{{ trans('templates/goldsilver.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('templates/goldsilver.Translate1') }}">
	<meta name="keywords" content="{{ trans('templates/goldsilver.keywords') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/{{ trans('routes.goldorsolv') }}">
@stop

@section('content')

	<div class="container">
		<!-- Boxes -->
		<div class="container">
			<h1 class="tmp-top-heading">{{ trans('templates/goldsilver.Translate3') }}</h1>
			</div>
			<span class="hidden-xs">
				<div class="row-fluid">
					<div class="col-sm-6 col-md-6 col-lg-6 testcol">
						<img src="/img/views/template/gull_silver_template.png" alt="{{ trans('templates/goldsilver.imagealt') }}" id="klistrebilde" class="img-responsive">
					</div>
					<div class="col-sm-6 col-md-6 col-lg-6 testcol template-top-p">
					<p>
						{{ trans('templates/goldsilver.Translate4') }}
					</p>
					<p>
						{{ trans('templates/goldsilver.Translate5') }}
					</p>
					<p>
						{{ trans('templates/goldsilver.Translate6') }}
					</p>
					<p>
						{{ trans('templates/goldsilver.Translate7') }}
					</p>
					<p>
						{{ trans('templates/goldsilver.Translate8') }}
					</p>
					</div>
				</div>
			</span>
			<p class="template-top-newdesign">
				{{ trans('templates/goldsilver.Translate9') }}
			</p>



		<div class="row-fluid">
		<a href="/{{ app()->getLocale() }}/{{ trans('routes.tempgold') }}">
				<div class="col-sm-6 col-md-3 col-lg-3 product-box">
					<div class="row-fluid">
						<div class="col-xs-9 col-sm-12 col-lg-12 product-box-heading"> <h4>{{ trans('templates/goldsilver.Translate13') }}</h4> </div>
						<div class="col-xs-9 col-sm-12 col-lg-12 product-box-paragraph"> {{ trans('templates/goldsilver.Translate14') }}</div>
						<div class="col-xs-3 col-sm-12 col-lg-12 product-box-image"> <img src="/img/views/store/test/gulletikett2.png" class="img-responsive"> </div>
					</div>
				</div>
			</a>
			<a href="/{{ app()->getLocale() }}/{{ trans('routes.tempsolv') }}">
				<div class="col-sm-6 col-md-3 col-lg-3 product-box">
					<div class="row-fluid">
						<div class="col-xs-9 col-sm-12 col-lg-12 product-box-heading"> <h4>{{ trans('templates/goldsilver.Translate15') }}</h4> </div>
						<div class="col-xs-9 col-sm-12 col-lg-12 product-box-paragraph"> {{ trans('templates/goldsilver.Translate16') }}</div>
						<div class="col-xs-3 col-sm-12 col-lg-12 product-box-image"> <img src="/img/views/store/test/silveretikett2.png" class="img-responsive"> </div>
					</div>
				</div>
			</a>
		</div>
	</div>

@stop
