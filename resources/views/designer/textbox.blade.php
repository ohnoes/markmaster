
<div class="textBox popupBox" id="textBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="textBoxHide()">X</button>
    <h4 class="popupTitle"> TEXT </h4>
  </div>
  <div class="popupBody">
    @if($typeofsticker == 1 || $typeofsticker == 2 || $typeofsticker == 8 || $typeofsticker == 9)
        <div class="col-xs-12 no-padding textadding"> <!-- Size, linedistance, rotation & position -->
            {!! Form::textarea('text-input', '' , array('id'=>'text-input', 'class'=>'text-input', 'oninput'=>'setText()', 'placeholder'=>'text', 'style'=>'text-align: center;')) !!}
        </div>

        <div class="col-xs-12 no-padding">
            <div id="fontSelect" class="fontSelect" style="margin-left: 0px">
                <div class="arrow-down"></div>
            </div>
        </div>
        <div class="col-xs-12 padding-line"></div>
        <div id="text-left-align" class="col-xs-4 no-padding designbtn" onclick="setTextAlign('left')">
          <b class="fa fa-fw fa-lg fa-align-left"></b>
        </div>
        <div id="text-center-align" class="col-xs-4 no-padding designbtn designbtnselected" onclick="setTextAlign('center')">
          <b class="fa fa-fw fa-lg fa-align-center"></b>
        </div>
        <div id="text-right-align" class="col-xs-4 no-padding designbtn" onclick="setTextAlign('right')">
          <b class="fa fa-fw fa-lg fa-align-right"></b>
        </div>
        <div class="col-xs-12 padding-line"></div>
        <div id="text-bold" class="col-xs-4 no-padding designbtn" onclick="setTextBold()">
          <b class="fa fa-fw fa-lg fa-bold"></b>
        </div>
        <div id="text-italic" class="col-xs-4 no-padding designbtn" onclick="setTextItalic()">
          <b class="fa fa-fw fa-lg fa-italic"></b>
        </div>
        <div id="text-underline" class="col-xs-4 no-padding designbtn" onclick="setTextUnderline()">
          <b class="fa fa-fw fa-lg fa-underline"></b>
        </div>
        <div class="col-xs-12 padding-line"></div>
        <style>
            .colorpicker-2x .colorpicker-saturation {
                width: 200px;
                height: 200px;
            }

            .colorpicker-2x .colorpicker-hue {
                width: 30px;
                height: 200px;
            }


            .colorpicker-2x .colorpicker-alpha {
                width: 0px;
                height: 0px;
            }

            .colorpicker-2x .colorpicker-color,
            .colorpicker-2x .colorpicker-color div {
                height: 30px;
            }

            .colorpicker-2x .colorpicker-visible {
              width: 220px;
            }

            .colorpicker-2x .colorpicker-selectors-color {
              width: 25px;
              height: 25px;
              margin: 2px;
              border: 1px solid #cecece;
              border-radius: 2px;
            }

            .selectedcolorbox i{
              width: 23px !important;
              height: 23px !important;
              border: 1px solid #cecece;
              border-radius: 4px;
            }

            .selectedcolorbox {
              padding: 4px 8px;
            }
        </style>

        <div class="col-xs-3 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-center-v') }}</label>
          <div class="col-xs-12 no-padding designbtn" onclick="centerObjectV()">
            <b class="glyphicon glyphicon-eye-open" style="font-size: 20px; line-height: 30px;"></b>
          </div>
        </div>

        <div class="col-xs-3 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-center-h') }}</label>
          <div class="col-xs-12 no-padding designbtn" onclick="centerObjectH()">
            <b class="glyphicon glyphicon-eye-close" style="font-size: 20px; line-height: 30px;"></b>
          </div>
        </div>

        <div class="col-xs-6 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-color') }}</label>
          <div id="cp8" data-format="alias" class="input-group colorpicker-component">
              <input type="text" value="primary" class="form-control" id="fill"/>
              <span class="input-group-addon selectedcolorbox"><i id="fillcolor"></i></span>
          </div>
        </div>
        <div class="col-xs-12 padding-line"></div>
        <div class="col-xs-4 no-padding">
            <label for="fontSize" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-size') }}</label>
            {!! Form::number('fontSizeTextbox', '2.5', array('id' => 'fontSizeTextbox', 'class' => 'fontSizeTextbox',  'step'=>'0.1',
            'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
        </div>
        <div class="col-xs-4 no-padding">
            <label for="lineHeight" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-line-distance') }}</label>
            {!! Form::number('lineHeightTextbox', '0.95', array('id' => 'lineHeightTextbox', 'step'=>'0.01',
            'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
        </div>
        <div class="col-xs-4 no-padding" >
            <label for="lineRotate" style="width: 100%; text-align: center;"> {{ trans('designs/designer.Label-rotation') }} </label>
            {!! Form::number('textAngleTextbox', '0', array('id' => 'textAngleTextbox',
            'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
        </div>

        <div class="col-xs-12 padding-line"></div>

        <div class="col-xs-4 no-padding" >
          <label for="strokeWidthTextbox" style="width: 100%; text-align: center;"> {{ trans('designs/designer.Label-thickness') }} </label>
          {!! Form::number('strokeWidthTextbox', '0.01', array('id' => 'strokeWidthTextbox', 'step'=>'0.01',
          'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
        </div>

        <div class="col-xs-6 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-line-edge') }}</label>
          <div id="cp9" data-format="alias" class="input-group colorpicker-component">
              <input type="text" value="primary" class="form-control" id="stroke"/>
              <span class="input-group-addon selectedcolorbox"><i id="strokecolor"></i></span>
          </div>
        </div>


        {!! Form::label('fontFamily', ' ') !!}
        {!! Form::select('fontFamily', array(
                  'boogaloottf' => 'boogaloottf',
                  'crimsontextttf' => 'crimsontextttf',
                  'englandttf' => 'englandttf',
                  'felipattf' => 'felipattf',
                  'goblinonettf' => 'goblinonettf',
                  'gravitasonettf' => 'gravitasonettf',
                  'greatvibesttf' => 'greatvibesttf',
                  'hammersmithonettf' => 'hammersmithonettf',
                  'hennepennyttf' => 'hennepennyttf',
                  'kaushanscriptttf' => 'kaushanscriptttf',
                  'leaguegothicttf' => 'leaguegothicttf',
                  'limelightttf' => 'limelightttf',
                  'lobstertwottf' => 'lobstertwottf',
                  'maidenoragettf' => 'maidenoragettf',
                  'nunitottf' => 'nunitottf',
                  'robotottf' => 'robotottf',
                  'robotocondensedttf' => 'robotocondensedttf')) !!}

    <!-- Only black and white text -->
    @else
    <div class="col-xs-12 no-padding textadding"> <!-- Size, linedistance, rotation & position -->
        {!! Form::textarea('text-input', '' , array('id'=>'text-input', 'class'=>'text-input', 'oninput'=>'setText()', 'placeholder'=>'text', 'style'=>'text-align: center;')) !!}
    </div>

    <div class="col-xs-12 no-padding">
        <div id="fontSelect" class="fontSelect" style="margin-left: 0px">
            <div class="arrow-down"></div>
        </div>
    </div>
    <div class="col-xs-12 padding-line"></div>
    <div id="text-left-align" class="col-xs-4 no-padding designbtn" onclick="setTextAlign('left')">
      <b class="fa fa-fw fa-lg fa-align-left"></b>
    </div>
    <div id="text-center-align" class="col-xs-4 no-padding designbtn designbtnselected" onclick="setTextAlign('center')">
      <b class="fa fa-fw fa-lg fa-align-center"></b>
    </div>
    <div id="text-right-align" class="col-xs-4 no-padding designbtn" onclick="setTextAlign('right')">
      <b class="fa fa-fw fa-lg fa-align-right"></b>
    </div>
    <div class="col-xs-12 padding-line"></div>
    <div id="text-bold" class="col-xs-4 no-padding designbtn" onclick="setTextBold()">
      <b class="fa fa-fw fa-lg fa-bold"></b>
    </div>
    <div id="text-italic" class="col-xs-4 no-padding designbtn" onclick="setTextItalic()">
      <b class="fa fa-fw fa-lg fa-italic"></b>
    </div>
    <div id="text-underline" class="col-xs-4 no-padding designbtn" onclick="setTextUnderline()">
      <b class="fa fa-fw fa-lg fa-underline"></b>
    </div>
    <div class="col-xs-12 padding-line"></div>

    <div class="col-xs-3 no-padding">
      <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-center-v') }}</label>
      <div class="col-xs-12 no-padding designbtn" onclick="centerObjectV()">
        <b class="glyphicon glyphicon-eye-open" style="font-size: 20px; line-height: 30px;"></b>
      </div>
    </div>

    <div class="col-xs-3 no-padding">
      <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-center-h') }}</label>
      <div class="col-xs-12 no-padding designbtn" onclick="centerObjectH()">
        <b class="glyphicon glyphicon-eye-close" style="font-size: 20px; line-height: 30px;"></b>
      </div>
    </div>

    <div class="col-xs-12 padding-line"></div>
    <div class="col-xs-4 no-padding">
        <label for="fontSize" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-size') }}</label>
        {!! Form::number('fontSizeTextbox', '2.5', array('id' => 'fontSizeTextbox', 'class' => 'fontSizeTextbox',  'step'=>'0.1',
        'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
    </div>
    <div class="col-xs-4 no-padding">
        <label for="lineHeight" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-line-distance') }}</label>
        {!! Form::number('lineHeightTextbox', '0.95', array('id' => 'lineHeightTextbox', 'step'=>'0.01',
        'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
    </div>
    <div class="col-xs-4 no-padding" >
        <label for="lineRotate" style="width: 100%; text-align: center;"> {{ trans('designs/designer.Label-rotation') }} </label>
        {!! Form::number('textAngleTextbox', '0', array('id' => 'textAngleTextbox',
        'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
    </div>

    <div class="col-xs-12 padding-line"></div>


    {!! Form::label('fontFamily', ' ') !!}
    {!! Form::select('fontFamily', array(
              'boogaloottf' => 'boogaloottf',
              'crimsontextttf' => 'crimsontextttf',
              'englandttf' => 'englandttf',
              'felipattf' => 'felipattf',
              'goblinonettf' => 'goblinonettf',
              'gravitasonettf' => 'gravitasonettf',
              'greatvibesttf' => 'greatvibesttf',
              'hammersmithonettf' => 'hammersmithonettf',
              'hennepennyttf' => 'hennepennyttf',
              'kaushanscriptttf' => 'kaushanscriptttf',
              'leaguegothicttf' => 'leaguegothicttf',
              'limelightttf' => 'limelightttf',
              'lobstertwottf' => 'lobstertwottf',
              'maidenoragettf' => 'maidenoragettf',
              'nunitottf' => 'nunitottf',
              'robotottf' => 'robotottf',
              'robotocondensedttf' => 'robotocondensedttf')) !!}

      <!-- Only black and white text -->
      {!! Form::hidden('strokeWidthTextbox', '0.01', array('id' => 'strokeWidthTextbox', 'step'=>'0.01')) !!}
      {!! Form::hidden('stroke', '#000000', array('id' => 'stroke')) !!}
      {!! Form::hidden('fill', '#000000', array('id' => 'fill')) !!}

    @endif
  </div>
	<br style="clear:both;"/>
</div>

{!! Html::script('js/textbox.js') !!}
