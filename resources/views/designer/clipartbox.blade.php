
<div class="imageBox popupBox" id="imageBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="imageBoxHide()">X</button>
    <h4 class="popupTitle"> CLIPART </h4>
  </div>
  <div >
            <div class="col-xs-12 topbox" style="padding: 10px;">
                    <label> {{ trans('designs/designer.Label-add') }} </label>
                    <div style="width: 60px; height: 45px; background-image: url('/img/add_image2.png'); display:block; cursor: pointer; cursor: hand;" onclick="clipartBoxShow()"/></div>
            </div>
    <div class="row no-margin"><!-- Upload controlls -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px;">
            <label> {{ trans('designs/designer.Label-upload-clipart') }} </label>

            @if (Auth::user())
            <span class="uploadControls">
                {!! Form::open(array('files' => true, 'id' => 'cilpartForm')) !!}
                <!-- {!! Form::label('title', 'Name your clipart') !!}
                {!! Form::text('title') !!} -->
                    <div class="form-group">
                        {!! Form::file('image', array('id' => 'clipartFileSelect')) !!}
                    </div>
                    <div class="form-group">
                        <input type="submit" value="{{ trans('designs/designer.Label-upload-clipart') }}" class="payBox">
                    </div>

                @else
                <label style="font-style: italic; font-weight: lighter;"> {{ trans('designs/designer.Label-login-upload') }} </label>
                  {!! Form::hidden('cilpartForm', 'cilpartForm', array('id'=>'cilpartForm')) !!}
                  {!! Form::hidden('clipartFileSelect', 'clipartFileSelect', array('id'=>'clipartFileSelect')) !!}
                @endif
                {!! Form::close() !!}
            </span>
        </div>
    </div>

  </div>
</div>

{!! Html::script('js/clipartbox.js') !!}
