{!! Form::hidden('size', 'size', array('id'=>'3716')) !!}
{!! Form::hidden('size', 'size', array('id'=>'6026')) !!}
{!! Form::hidden('size', 'size', array('id'=>'3010')) !!}
@if(Input::has('edit') && Orderitem::find(Input::get('edit')))
<?php
    $sticker = Orderitem::find(Input::get('edit'));
?>
@endif

@if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
  @if (Auth::user() && Auth::user()->admin == 1 )
    {!! Form::hidden('adminupdate', 1, array('id'=>'adminupdate')) !!}
  @else
    {!! Form::hidden('adminupdate', 0, array('id'=>'adminupdate')) !!}
  @endif
@else
{!! Form::hidden('adminupdate', 0, array('id'=>'adminupdate')) !!}
@endif

@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
  @if (Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 0) ) <!-- update cart -->
    {!! Form::hidden('updatestickercart', 1, array('id'=>'updatestickercart')) !!}
    {!! Form::hidden('updatesticker', 0, array('id'=>'updatesticker')) !!}
    {!! Form::hidden('newstickercart', 0, array('id'=>'newstickercart')) !!}
  @elseif(Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 1) ) <!-- update sticker -->
  {!! Form::hidden('updatestickercart', 0, array('id'=>'updatestickercart')) !!}
  {!! Form::hidden('updatesticker', 1, array('id'=>'updatesticker')) !!}
  {!! Form::hidden('newstickercart', 0, array('id'=>'newstickercart')) !!}
  @elseif(Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 2) ) <!-- add NEW sticker to cart -->
  {!! Form::hidden('updatestickercart', 0, array('id'=>'updatestickercart')) !!}
  {!! Form::hidden('updatesticker', 0, array('id'=>'updatesticker')) !!}
  {!! Form::hidden('newstickercart', 1, array('id'=>'newstickercart')) !!}
  @else <!-- add NEW sticker to cart -->
  {!! Form::hidden('updatestickercart', 0, array('id'=>'updatestickercart')) !!}
  {!! Form::hidden('updatesticker', 0, array('id'=>'updatesticker')) !!}
  {!! Form::hidden('newstickercart', 1, array('id'=>'newstickercart')) !!}
  @endif
@else
  {!! Form::hidden('updatestickercart', 0, array('id'=>'updatestickercart')) !!}
  {!! Form::hidden('updatesticker', 0, array('id'=>'updatesticker')) !!}
  {!! Form::hidden('newstickercart', 1, array('id'=>'newstickercart')) !!}
@endif

<?php
    $thecur = Currency::find(1);
    if(Auth::check()) {
        $usercur = Auth::user()->currency;
        if ($usercur == 0) {
            $currencyValue = 1;
        } else if ($usercur == 1) {
            $currencyValue = $thecur->usd;
        } else if ($usercur == 2) {
            $currencyValue = $thecur->sek;
        } else if ($usercur == 3) {
            $currencyValue = $thecur->dkk;
        }else if ($usercur == 4) {
            $currencyValue = $thecur->eur;
        }
    } else {
        $usercur = Session::get('theCurrency', 0);
        if ($usercur == 0) {
            $currencyValue = 1;
        } else if ($usercur == 1) {
            $currencyValue = $thecur->usd;
        } else if ($usercur == 2) {
            $currencyValue = $thecur->sek;
        } else if ($usercur == 3) {
            $currencyValue = $thecur->dkk;
        }else if ($usercur == 4) {
            $currencyValue = $thecur->eur;
        }
    }
?>
{!! Form::hidden('thecurrency', $usercur, array('id'=>'thecurrency')) !!}
{!! Form::hidden('currencycheck', $currencyValue, array('id'=>'currencycheck')) !!}
{!! Form::hidden('priceHolder', 'Price: ', array('id'=>'priceHolder')) !!}
{!! Form::hidden('thePrice', 'Price: ', array('id'=>'thePrice2')) !!}
@if($typeofsticker == 1 || $typeofsticker == 2 || $typeofsticker == 8 || $typeofsticker == 9)
  {!! Form::hidden('clipartType', '2', array('id'=>'clipartType')) !!}
@elseif($typeofsticker == 3 || $typeofsticker == 4 || $typeofsticker == 5)
  {!! Form::hidden('clipartType', '1', array('id'=>'clipartType')) !!}
@else
  {!! Form::hidden('clipartType', '0', array('id'=>'clipartType')) !!}
@endif

@if($typeofsticker == 1)
    {!! Form::hidden('stickertype', '1', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 2)
    {!! Form::hidden('stickertype', '2', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 3)
    {!! Form::hidden('stickertype', '3', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 4)
    {!! Form::hidden('stickertype', '4', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 5)
    {!! Form::hidden('stickertype', '5', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 6)
    {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 7)
    {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 8)
    {!! Form::hidden('stickertype', '8', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 9)
    {!! Form::hidden('stickertype', '9', array('id'=>'stickertype')) !!}
@endif

{!! Form::hidden('stickerText', trans('designs/designer.Label-your-text') , array('id'=>'stickerText')) !!}

@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
    <?php
        $sticker = Orderitem::find(Input::get('edit'));
    ?>
    @if (Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 0 || $sticker->status == 1) )
        {!! Form::hidden('edit', true, array('id'=>'edit')) !!}
        {!! Form::hidden('stickerid', $sticker->id, array('id'=>'stickerid')) !!}
    @else
        {!! Form::hidden('edit', false, array('id'=>'edit')) !!}
        {!! Form::hidden('stickerid', 0, array('id'=>'stickerid')) !!}
    @endif
@elseif(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')) && Auth::user() && Auth::user()->admin == 1)
<?php
    $sticker = Orderitem::find(Input::get('adminedit'));
?>
{!! Form::hidden('edit', true, array('id'=>'edit')) !!}
{!! Form::hidden('stickerid', $sticker->id, array('id'=>'stickerid')) !!}
@else
  {!! Form::hidden('edit', false, array('id'=>'edit')) !!}
  {!! Form::hidden('stickerid', 0, array('id'=>'stickerid')) !!}
@endif

<input type="hidden" name="redirect" value="{{ app()->getLocale() }}/store/cart" id="redirect">

@if($typeofsticker != 6 && $typeofsticker != 7)
    @if(Auth::user() && Auth::user()->admin == 1)
                {!! Form::hidden('thePrice', 'Price: ', array('id'=>'thePrice2')) !!}
                @include('designer.stickertype')
    @endif
    @if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
        <?php
            $sticker = Orderitem::find(Input::get('adminedit'));
        ?>
        @if (Auth::user() && Auth::user()->admin == 1 && ($sticker->status == 0 || $sticker->status == 1) )
            <form method="POST" action="/{{ app()->getLocale() }}/admin/updatesticker" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::hidden('stickerid', $sticker->id) !!}
            {!! Form::hidden('userid', Auth::user()->id) !!}
            @include('designer.stickertype')
            {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
            @if($sticker->status == 1)
                {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
            @endif
            {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
            {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
            {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
            {!! Form::close() !!}
        @endif
    @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
        <?php
            $sticker = Orderitem::find(Input::get('edit'));
        ?>
        @if (Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 0 || $sticker->status == 1) )
            <form method="POST" action="/{{ app()->getLocale() }}/store/updatecart" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::hidden('stickerid', $sticker->id) !!}
            {!! Form::hidden('userid', Auth::user()->id) !!}
            @include('designer.stickertype')
            {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
            @if($sticker->status == 1)
                {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
            @else

            @endif
            {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
            {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
            {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
            {!! Form::close() !!}
        @endif
    @else
              <form method="POST" action="/{{ app()->getLocale() }}/store/addtocart" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              @if(Auth::check())
                {!! Form::hidden('userid', Auth::user()->id)!!}
              @endif
              @include('designer.stickertype')
              {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
              {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
              {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
              {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
              {!! Form::close() !!}
    @endif
@elseif($typeofsticker == 6)
    {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
    @if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
        <?php
            $sticker = Orderitem::find(Input::get('adminedit'));
        ?>
        @if (Auth::user() && Auth::user()->admin == 1)
            <form method="POST" action="/{{ app()->getLocale() }}/admin/updatesticker" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::hidden('stickerid', $sticker->id) !!}
            {!! Form::hidden('userid', Auth::user()->id) !!}
            {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
            {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
            @if($sticker->status == 1)
                {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}
            @else
                {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
            @endif
            {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
            {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
            {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
            {!! Form::close() !!}
        @endif
    @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
        <?php
            $sticker = Orderitem::find(Input::get('edit'));
        ?>
        @if (Auth::user() && $sticker->user_id == Auth::user()->id && ($sticker->status == 0 || $sticker->status == 1) )
            <form method="POST" action="/{{ app()->getLocale() }}/store/updatecart" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::hidden('stickerid', $sticker->id) !!}
            {!! Form::hidden('userid', Auth::user()->id) !!}
            {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
            {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
            @if($sticker->status == 1)
                {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}

            @else

            @endif
            {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
            {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
            {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
            {!! Form::close() !!}
        @endif
    @else
        <form method="POST" action="/{{ app()->getLocale() }}/store/addtocart" accept-charset="UTF-8">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @if(Auth::check())
            {!! Form::hidden('userid', Auth::user()->id)!!}
        @endif
        {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
        {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
        {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
        {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
        {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
        {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}

        {!! Form::close() !!}
    @endif
@else
    @if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
        <?php
            $sticker = Orderitem::find(Input::get('adminedit'));
        ?>
        @if (Auth::user() && Auth::user()->admin == 1 )
            <form method="POST" action="/{{ app()->getLocale() }}/admin/updatesticker" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::hidden('stickerid', $sticker->id) !!}
            {!! Form::hidden('userid', Auth::user()->id) !!}
            {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
            {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}

            @if($sticker->status == 1)
                {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}

            @else

            @endif
            {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
            {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
            {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
            {!! Form::close() !!}
        @endif
    @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
        <?php
            $sticker = Orderitem::find(Input::get('edit'));
        ?>
        @if (Auth::user() && $sticker->user_id == Auth::user()->id && ($sticker->status == 0 || $sticker->status == 1) )
            <form method="POST" action="/{{ app()->getLocale() }}/store/updatecart" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::hidden('stickerid', $sticker->id) !!}
            {!! Form::hidden('userid', Auth::user()->id) !!}
            {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
            {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}

            @if($sticker->status == 1)
                {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}

            @else

            @endif
            {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
            {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
            {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
            {!! Form::close() !!}
        @endif
    @else
        <p>
        <form method="POST" action="/{{ app()->getLocale() }}/store/addtocart" accept-charset="UTF-8">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @if(Auth::check())
            {!! Form::hidden('userid', Auth::user()->id)!!}
        @endif
        {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
        {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
        {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
        {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
        {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
        {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}

        {!! Form::close() !!}
    </p>
    @endif
@endif
