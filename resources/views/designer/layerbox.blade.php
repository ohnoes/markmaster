<script>
var start_pos;
var index;

$(function() {
    $('#sortable').sortable({
      start: function(event, ui) {
        ui.item.startPos = ui.item.index();
      },
      stop: function(event, ui) {
        var movingObject = canvas.getObjects();
        var objSize = movingObject.length-1;
        var startLocation = (objSize - ui.item.startPos);
        var endLocation = (objSize - ui.item.index());
        movingObject[startLocation].moveTo(endLocation);
        canvas.renderAll();
      }
    });
});

</script>

<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0; padding-left: 1.5em; font-size: 1.4em; height: 40px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }
</style>

<div class="layerBox popupBox" id="layerBox" style="display: none;">
    <div class="popupHeader">
      <button type="button" name="button" class="popupClose" onclick="layerBoxHide()">X</button>
      <h4 class="popupTitle"> LAYERS </h4>
    </div>
    <div class="popupBody">
      <ul id="sortable">

      </ul>
    </div>
  	<br style="clear:both;"/>
</div>
{!! Html::script('js/layerbox.js') !!}

<script>
  function toggleLock(id) {
    //console.log("toggle got id: " + id);
    var idT = '#'+id;
    if($(idT).hasClass( "fa-lock" )) {
      document.getElementById(id).classList.remove("fa-lock");
      document.getElementById(id).classList.add("fa-unlock-alt");

      var theParent = document.getElementById(id).parentNode;
      var parentId = theParent.getAttribute("id");
      var theIndex = $( '#'+parentId ).index();
      var theIndex = theIndex;
      var unlockObject = canvas.getObjects();
      var objSize = unlockObject.length-1;
      var unlockIndex = (objSize - theIndex);
      unlockObject[unlockIndex].selectable = true;

    } else {
      document.getElementById(id).classList.add("fa-lock");
      document.getElementById(id).classList.remove("fa-unlock-alt");

      var theParent = document.getElementById(id).parentNode;
      var parentId = theParent.getAttribute("id");
      var theIndex = $( '#'+parentId ).index();
      var theIndex = theIndex;
      var unlockObject = canvas.getObjects();
      var objSize = unlockObject.length-1;
      var unlockIndex = (objSize - theIndex);
      unlockObject[unlockIndex].selectable = false;

    }
  }

  function layerUp(id) {
    var theParent = document.getElementById(id).parentNode;
    var parentId = theParent.getAttribute("id");
    var theIndex = $( '#'+parentId ).index();

    if(theIndex > 0) {
      $el = jQuery('#'+parentId);
      $el.insertBefore($el.prev());


      var movingObject = canvas.getObjects();
      var objSize = movingObject.length-1;
      var startLocation = (objSize - theIndex);
      var endLocation = (objSize - (theIndex-1));
      movingObject[startLocation].moveTo(endLocation);
      canvas.renderAll();
    }

  }

  function layerDown(id) {
    var theParent = document.getElementById(id).parentNode;
    var parentId = theParent.getAttribute("id");
    var theIndex = $( '#'+parentId ).index();
    var movingObject = canvas.getObjects();
    var objSize = movingObject.length-1;

    if(theIndex < objSize) {
      $el = jQuery('#'+parentId);
      $el.insertAfter($el.next());



      var startLocation = (objSize - theIndex);
      var endLocation = (objSize - (theIndex+1));
      movingObject[startLocation].moveTo(endLocation);
      canvas.renderAll();
    }

    // check if layer is already at bottom
  }
</script>
