
@if($typeofsticker == 1)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-sticker-color') }} ">
@elseif($typeofsticker == 2)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-iron-color') }} ">
@elseif($typeofsticker == 3)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-gold') }} ">
@elseif($typeofsticker == 4)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-silver') }} ">
@elseif($typeofsticker == 5)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-transparent') }} ">
@elseif($typeofsticker == 6)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-sticker') }} ">
@elseif($typeofsticker == 7)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-iron') }} ">
@elseif($typeofsticker == 8)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-reflection') }} ">
@elseif($typeofsticker == 9)
    <meta name="description" content="{{ trans('designs/designer.Page-Description-sticker-fabric') }} ">
@endif
<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/{{ trans('routes.merkbase') }}" />
