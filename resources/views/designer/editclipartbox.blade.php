
<div class="imageEditBox popupBox" id="imageEditBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="imageEditBoxHide()">X</button>
    <h4 class="popupTitle"> EDIT CLIPART </h4>
  </div>
  <div >
      <div class="col-xs-12 topbox" style="padding: 10px;">
           <div class="col-xs-6" style="text-align: center">
                    <label for="clipartScale" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-size') }}</label><br>
                    {!! Form::number('clipartScaleTextbox', '1', array('id' => 'clipartScaleTextbox', 'step'=>'0.1',
                    'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
            </div>

            <div class="col-xs-6" style="text-align: center">
                    <label for="clipartRotate" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-rotation') }}</label><br>
                    {!! Form::number('clipartAngleTextbox', '0', array('id' => 'clipartAngleTextbox',
                    'style'=>'width: 100%; height: 34px; padding: 6px 12px;')) !!}
            </div>
      </div>
      <div class="col-xs-12 topbox" style="padding: 10px;">

        <div class="col-xs-3 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-flip-x') }}</label>
          <div class="col-xs-12 no-padding designbtn" onclick="flipXImg()">
            <b class="glyphicon glyphicon-warning-sign" style="font-size: 20px; line-height: 30px;"></b>
          </div>
        </div>

        <div class="col-xs-3 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-flip-y') }}</label>
          <div class="col-xs-12 no-padding designbtn" onclick="flipYImg()">
            <b class="glyphicon glyphicon-plane" style="font-size: 20px; line-height: 30px;"></b>
          </div>
        </div>

        <div class="col-xs-3 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-center-v') }}</label>
          <div class="col-xs-12 no-padding designbtn" onclick="centerObjectV()">
            <b class="glyphicon glyphicon-eye-open" style="font-size: 20px; line-height: 30px;"></b>
          </div>
        </div>

        <div class="col-xs-3 no-padding">
          <label for="color" style="width: 100%; text-align: center;">{{ trans('designs/designer.Label-center-h') }}</label>
          <div class="col-xs-12 no-padding designbtn" onclick="centerObjectH()">
            <b class="glyphicon glyphicon-eye-close" style="font-size: 20px; line-height: 30px;"></b>
          </div>
        </div>
      </div>
      <div class="col-xs-12 topbox" style="padding: 10px;">


      <div class="col-xs-6" style="text-align: center;">
        <button id="deleteClipartButton" onclick="deleteClipart()" class="mybtn">{{ trans('designs/designer.Label-delete') }}</button>
      </div>
      <div class="col-xs-6 no-padding">
        @if($typeofsticker == 1 || $typeofsticker == 2 || $typeofsticker == 8 || $typeofsticker == 9)
        {!! Form::hidden('clipartType', '2', array('id'=>'clipartType')) !!}
        <div id="cp11" data-format="alias" class="input-group colorpicker-component">
            <input type="text" value="primary" class="form-control" id="clipcolor" onchange='clipColor()'/>
            <span class="input-group-addon selectedcolorbox"><i id="clipcolorr"></i></span>
        </div>
        @endif
      </div>
    </div>

  </div>
</div>

{!! Html::script('js/cliparteditbox.js') !!}
