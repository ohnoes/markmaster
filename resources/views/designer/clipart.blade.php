<div class="tab-pane" id="clipartControl"> <!-- Clipart tab -->
    <div class="row"> <!-- Clipart View Box -->
            <div class="col-xs-1 col-sm-1 col-md-0 col-lg-0">
            </div>
            <div class="col-xs-5 col-sm-5 col-md-2 col-lg-2">
                <div class="row">
                    <label> {{ trans('designs/designer.Label-add') }} </label>
                </div>
                <div class="row">
                    <div style="width: 60px; height: 45px; background-image: url('/img/add_image2.png'); display:block; cursor: pointer; cursor: hand;" onclick="clipartBoxShow()"/></div>
                </div>
                <div class="row" style="padding-top: 10px;">
                    <button id="deleteClipartButton" onclick="deleteClipart()">{{ trans('designs/designer.Label-delete') }}</button>
                </div>
            </div>
            <div class="col-xs-5 col-sm-5 col-md-2 col-lg-2" style="text-align: center">
                <div class="row">
                    <label for="position"> {{ trans('designs/designer.Label-position') }} </label>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-top textIconBox" aria-hidden="true"
                    onclick="moveObjectTop()"
                    onmousedown="inter=setInterval(moveObjectTop, 50);"
                    onmouseup="clearInterval(inter);"
                    onmouseout="clearInterval(inter);">
                    </span> </div>
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-left textIconBox" aria-hidden="true"
                    onclick="moveObjectLeft()"
                    onmousedown="inter=setInterval(moveObjectLeft, 50);"
                    onmouseup="clearInterval(inter);"
                    onmouseout="clearInterval(inter);"></span></div>
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-right textIconBox" aria-hidden="true"
                    onclick="moveObjectRight()"
                    onmousedown="inter=setInterval(moveObjectRight, 50);"
                    onmouseup="clearInterval(inter);"
                    onmouseout="clearInterval(inter);"></span> </div>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon glyphicon-triangle-bottom textIconBox" aria-hidden="true"
                    onclick="moveObjectDown()"
                    onmousedown="inter=setInterval(moveObjectDown, 50);"
                    onmouseup="clearInterval(inter);"
                    onmouseout="clearInterval(inter);"></span> </div>
                    <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 posBox"> <span class="glyphicon emptyBox" aria-hidden="true"></span> </div>
                </div>
            </div>
                <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1" style="text-align: center">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" style="text-align: center">
                <div class="row">
                    <label for="clipartScale">{{ trans('designs/designer.Label-size') }}</label>
                </div>
                <div class="row">
                    <span class="glyphicon glyphicon-resize-full textIconBoxControll" aria-hidden="true"
                    onclick="increaseObjectScale()"
                    onmousedown="inter=setInterval(increaseObjectScale, 50);"
                    onmouseup="clearInterval(inter);"
                    onmouseout="clearInterval(inter);">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="">  </span>
                    </span>
                </div>
                <div class="row">
                    {!! Form::text('clipartScaleTextbox', '1', array('id' => 'clipartScaleTextbox')) !!}
                </div>
                <div class="row">
                    <span class="glyphicon glyphicon-resize-small textIconBoxControll" aria-hidden="true"
                    onclick="decreaseObjectScale()"
                    onmousedown="inter=setInterval(decreaseObjectScale, 50);"
                    onmouseup="clearInterval(inter);"
                    onmouseout="clearInterval(inter);">
                        <span class="glyphicon glyphicon-minus" aria-hidden="true" onclick="">  </span>
                    </span>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" style="text-align: center">
                <div class="row">
                    <label for="clipartRotate">{{ trans('designs/designer.Label-rotation') }}</label>
                </div>
                <div class="row">
                    <span class="glyphicon glyphicon-repeat textIconBoxControll">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"
                        onclick="increaseObjectRotation()"
                        onmousedown="inter=setInterval(increaseObjectRotation, 50);"
                        onmouseup="clearInterval(inter);"
                        onmouseout="clearInterval(inter);">	</span>
                    </span>
                </div>
                <div class="row">
                    {!! Form::text('clipartAngleTextbox', '0', array('id' => 'clipartAngleTextbox')) !!}
                </div>
                <div class="row">
                    <span class="glyphicon glyphicon-minus textIconBoxControll" id="flipped-icon"
                    onclick="decreaseObjectRotation()"
                    onmousedown="inter=setInterval(decreaseObjectRotation, 50);"
                    onmouseup="clearInterval(inter);"
                    onmouseout="clearInterval(inter);">
                        <span class="glyphicon glyphicon-repeat" aria-hidden="true" onclick="">  </span>
                    </span>
                </div>
            </div>
        </div>


    <div class="row"><!-- Flip, color & delete -->
        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
            <div class="row">
                <label for="flipArt">{{ trans('designs/designer.Label-flip-clipart') }}</label>
            </div>
            <div class="row">
                <span class="glyphicon glyphicon-warning-sign textIconBox" aria-hidden="true" onclick="flipXImg()"></span>
                <span class="glyphicon glyphicon-plane textIconBox" aria-hidden="true" onclick="flipYImg()"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
            <div class="row">
                <label for="centerArt">{{ trans('designs/designer.Label-center-clipart') }}</label>
            </div>
            <div class="row">
                <span class="glyphicon glyphicon-eye-close textIconBox" aria-hidden="true" onclick="centerObjectH()"></span>
                <span class="glyphicon glyphicon-eye-open textIconBox" aria-hidden="true" onclick="centerObjectV()"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
            <div class="row">
                @if($typeofsticker == 1 || $typeofsticker == 2 || $typeofsticker == 8 || $typeofsticker == 9)
                    <!-- Fix so this is disabled when the clipart can't change color -->
                    {!! Form::hidden('clipartType', '2', array('id'=>'clipartType')) !!}
                    <label for="clipcolor">{{ trans('designs/designer.Label-clipart-color') }}</label>
                    {!! Form::select('colorpickerClipart', array(
                        '#000000' => 'Black',
                        '#ffffff' => 'White',
                        '#E6E6E6' => '10% Black',
                        '#B1B1B1' => '40% Black',
                        '#888887' => '60% Black',
                        '#5C5C5B' => '80% Black',
                        '#EBB5C3' => 'PANTONE 182 C',
                        '#C8112E' => 'PANTONE 185 C',
                        '#B01D2B' => 'PANTONE 1797 C',
                        '#871630' => 'PANTONE 201 C',
                        '#E6D5A8' => 'PANTONE 155 C',
                        '#E9954A' => 'PANTONE 804 C',
                        '#E64A00' => 'PANTONE Orange 021 C',
                        '#EAEBBC' => 'PANTONE 607 C',
                        '#EFED84' => 'PANTONE 100 C',
                        '#EFE032' => 'PANTONE Yellow C',
                        '#C9D8E7' => 'PANTONE 290 C',
                        '#8ACBE5' => 'PANTONE 305 C',
                        '#1A35A8' => 'PANTONE 286 C',
                        '#0F2867' => 'PANTONE 281 C',
                        '#549AA3' => 'PANTONE 320 C',
                        '#EACDCF' => 'PANTONE 698 C',
                        '#E8A3D0' => 'PANTONE 230 C',
                        '#B50970' => 'PANTONE 226 C',
                        '#D7CAE3' => 'PANTONE 263 C',
                        '#9E70C1' => 'PANTONE 528 C',
                        '#680E92' => 'PANTONE 527 C',
                        '#BC8F70' => 'PANTONE 7515 C',
                        '#9E520F' => 'PANTONE 471 C',
                        '#B6DD8E' => 'PANTONE 358 C',
                        '#A4D426' => 'PANTONE 375 C',
                        '#61AE56' => 'PANTONE 354 C',
                        '#4A7229' => 'PANTONE 364 C',
                        '#cfb53b' => 'Gold-like',
                        '#d7d8d8' => 'Silver-like'),
                        '#000000',
                        array('id' => 'clipcolor', 'onchange' => 'clipColor()'))
                    !!}
                @endif
            </div>
        </div>
    </div>
    <div class="row grid-breathingroom"><!-- Size, rotation & layer -->
        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="text-align: center;">
            <div id="row">
                <label for="layerControl">{{ trans('designs/designer.Label-layer') }}</label>
            </div>
            <div id="row">
                <!--<span class="glyphicon glyphicon-fast-backward textIconBox" aria-hidden="true" onclick="sendToBack()"></span>-->
                <span class="glyphicon glyphicon-step-backward textIconBox" aria-hidden="true" onclick="moveDown()"></span>
                <span class="glyphicon glyphicon-step-forward textIconBox" aria-hidden="true" onclick="moveUp()"></span>
                <!--<span class="glyphicon glyphicon-fast-forward textIconBox" aria-hidden="true" onclick="bringToFront()"></span>-->
            </div>
        </div>
    </div>
    <div class="row grid-breathingroom"><!-- Upload controlls -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <span class="uploadControls">
                {!! Form::open(array('files' => true, 'id' => 'cilpartForm')) !!}
                <!-- {!! Form::label('title', 'Name your clipart') !!}
                {!! Form::text('title') !!} -->
                @if (Auth::user())
                    <div class="form-group">
                        {!! Form::file('image', array('id' => 'clipartFileSelect')) !!}
                    </div>
                    <div class="form-group">
                        <input type="submit" value="{{ trans('designs/designer.Label-upload-clipart') }}" class="payBox">
                    </div>
                @else
                    <div class="form-group">
                        {!! Form::file('image', array('disabled' => '', 'id' => 'clipartFileSelect')) !!}
                    </div>
                    <div class="form-group">
                        <input type="submit" value="{{ trans('designs/designer.Label-upload-clipart') }}" class="payBox">
                    </div>
                @endif
                {!! Form::close() !!}
            </span>
        </div>
    </div>
</div>
