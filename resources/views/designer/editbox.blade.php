
<div class="editBox" id="editBox" onclick="editOnClick()" style="display: none;">
  <div style="padding: 5px;">
    <b class="fa fa-fw fa-lg fa-edit"></b> <br>
    EDIT
  </div>
	<br style="clear:both;"/>
</div>

{!! Html::script('js/editbox.js') !!}
