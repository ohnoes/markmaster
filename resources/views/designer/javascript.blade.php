{!! Html::script('js/vendor/modernizr-2.6.2.min.js') !!}
{!! Html::script('//code.jquery.com/jquery-1.11.0.min.js') !!}
{!! Html::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') !!}
{!! Html::script('js/jquery.fontselector.js') !!}
{!! Html::script('js/fabric.js-master/dist/fabric.min.js') !!}
<!--{!! Html::script('js/fabric.js-master/dist/fabric.js') !!} -->
{!! Html::script('js/jquery.simplecolorpicker.js') !!}
{!! Html::script('js/jquery.contextmenu.js') !!}
{!! Html::script('js/spin.min.js') !!}
{!! Html::script('js/customiseControls.min.js') !!}
{!! Html::script('js/paper-core.min.js') !!}
{!! Html::script('js/marchingSquares.min.js') !!}
{!! Html::script('js/outline.js') !!}
<!-- {!! Html::script('js/outline-2.js') !!} -->
{!! Html::script('js/background-2.js') !!}
{!! Html::script('js/clipart-2.js') !!}
{!! Html::script('js/price-2.js') !!}
{!! Html::script('js/text-2.js') !!}
{!! Html::script('js/bootstrap-colorpicker.js') !!}
<script>
 $('#fontSelect').fontSelector({
    'hide_fallbacks' : true,
    'initial' : 'Boogaloo, boogaloottf, Courier, monospace',
    'fonts' : [
        'Boogaloo, boogaloottf, Helvetica, sans-serif',
        'Crimson Text, crimsontextttf, Gadget, sans-serif',
        'England, englandttf, cursive',
        'Felipa, felipattf, Courier, monospace',
        'Gravitas One, gravitasonettf, sans-serif',
        'Great Vibes, greatvibesttf, monospace',
        'Hammersmith One, hammersmithonettf, sans-serif',
        'Henny Penny, hennepennyttf, Palatino,serif',
        'KaushanScript, kaushanscriptttf, sans-serif',
        'Leaguegothic, leaguegothicttf, serif',
        'Limelight, limelightttf, sans-serif',
        'Lobster Two, lobstertwottf, sans-serif',
        'Maidenorange, maidenoragettf, sans-serif',
        'Nunito, nunitottf, sans-serif',
        'Roboto, robotottf, sans-serif',
        'Roboto Condensed, robotocondensedttf, sans-serif',
        ]
});

$(function() {
        $('#cp8').colorpicker({
            customClass: 'colorpicker-2x',
            sliders: {
                saturation: {
                    maxLeft: 200,
                    maxTop: 200
                },
                hue: {
                    maxTop: 200
                }
            },
            colorSelectors: {
              '#000000': '#000000',
              '#5C5C5B': '#5C5C5B',
              '#888887': '#888887',
              '#B1B1B1': '#B1B1B1',
              '#E6E6E6': '#E6E6E6',
              '#FFFFFF': '#FFFFFF',
              '#EBB5C3': '#EBB5C3',
              '#C8112E': '#C8112E',
              '#B01D2B': '#B01D2B',
              '#871630': '#871630',
              '#E6D5A8': '#E6D5A8',
              '#E9954A': '#E9954A',
              '#E64A00': '#E64A00',
              '#EAEBBC': '#EAEBBC',
              '#EFED84': '#EFED84',
              '#EFE032': '#EFE032',
              '#C9D8E7': '#C9D8E7',
              '#8ACBE5': '#8ACBE5',
              '#1A35A8': '#1A35A8',
              '#0F2867': '#0F2867',
              '#549AA3': '#549AA3',
              '#EACDCF': '#EACDCF',
              '#E8A3D0': '#E8A3D0',
              '#B50970': '#B50970',
              '#D7CAE3': '#D7CAE3',
              '#9E70C1': '#9E70C1',
              '#680E92': '#680E92',
              '#BC8F70': '#BC8F70',
              '#9E520F': '#9E520F',
              '#B6DD8E': '#B6DD8E',
              '#A4D426': '#A4D426',
              '#61AE56': '#61AE56',
              '#4A7229': '#4A7229',
              '#CFB53B': '#CFB53B',
              '#D7D8D8': '#D7D8D8'

            }
        });

        $('#cp10').colorpicker({
            customClass: 'colorpicker-2x',
            color: '#FFFFFF',
            sliders: {
                saturation: {
                    maxLeft: 200,
                    maxTop: 200
                },
                hue: {
                    maxTop: 200
                }
            },
            colorSelectors: {
              '#000000': '#000000',
              '#5C5C5B': '#5C5C5B',
              '#888887': '#888887',
              '#B1B1B1': '#B1B1B1',
              '#E6E6E6': '#E6E6E6',
              '#FFFFFF': '#FFFFFF',
              '#EBB5C3': '#EBB5C3',
              '#C8112E': '#C8112E',
              '#B01D2B': '#B01D2B',
              '#871630': '#871630',
              '#E6D5A8': '#E6D5A8',
              '#E9954A': '#E9954A',
              '#E64A00': '#E64A00',
              '#EAEBBC': '#EAEBBC',
              '#EFED84': '#EFED84',
              '#EFE032': '#EFE032',
              '#C9D8E7': '#C9D8E7',
              '#8ACBE5': '#8ACBE5',
              '#1A35A8': '#1A35A8',
              '#0F2867': '#0F2867',
              '#549AA3': '#549AA3',
              '#EACDCF': '#EACDCF',
              '#E8A3D0': '#E8A3D0',
              '#B50970': '#B50970',
              '#D7CAE3': '#D7CAE3',
              '#9E70C1': '#9E70C1',
              '#680E92': '#680E92',
              '#BC8F70': '#BC8F70',
              '#9E520F': '#9E520F',
              '#B6DD8E': '#B6DD8E',
              '#A4D426': '#A4D426',
              '#61AE56': '#61AE56',
              '#4A7229': '#4A7229',
              '#CFB53B': '#CFB53B',
              '#D7D8D8': '#D7D8D8'

            }
        });

        $('#cp11').colorpicker({
            customClass: 'colorpicker-2x',
            sliders: {
                saturation: {
                    maxLeft: 200,
                    maxTop: 200
                },
                hue: {
                    maxTop: 200
                }
            },
            colorSelectors: {
              '#000000': '#000000',
              '#5C5C5B': '#5C5C5B',
              '#888887': '#888887',
              '#B1B1B1': '#B1B1B1',
              '#E6E6E6': '#E6E6E6',
              '#FFFFFF': '#FFFFFF',
              '#EBB5C3': '#EBB5C3',
              '#C8112E': '#C8112E',
              '#B01D2B': '#B01D2B',
              '#871630': '#871630',
              '#E6D5A8': '#E6D5A8',
              '#E9954A': '#E9954A',
              '#E64A00': '#E64A00',
              '#EAEBBC': '#EAEBBC',
              '#EFED84': '#EFED84',
              '#EFE032': '#EFE032',
              '#C9D8E7': '#C9D8E7',
              '#8ACBE5': '#8ACBE5',
              '#1A35A8': '#1A35A8',
              '#0F2867': '#0F2867',
              '#549AA3': '#549AA3',
              '#EACDCF': '#EACDCF',
              '#E8A3D0': '#E8A3D0',
              '#B50970': '#B50970',
              '#D7CAE3': '#D7CAE3',
              '#9E70C1': '#9E70C1',
              '#680E92': '#680E92',
              '#BC8F70': '#BC8F70',
              '#9E520F': '#9E520F',
              '#B6DD8E': '#B6DD8E',
              '#A4D426': '#A4D426',
              '#61AE56': '#61AE56',
              '#4A7229': '#4A7229',
              '#CFB53B': '#CFB53B',
              '#D7D8D8': '#D7D8D8'

            }
        });

        $('#cp9').colorpicker({
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            }
        },
        colorSelectors: {
          '#000000': '#000000',
          '#5C5C5B': '#5C5C5B',
          '#888887': '#888887',
          '#B1B1B1': '#B1B1B1',
          '#E6E6E6': '#E6E6E6',
          '#FFFFFF': '#FFFFFF',
          '#EBB5C3': '#EBB5C3',
          '#C8112E': '#C8112E',
          '#B01D2B': '#B01D2B',
          '#871630': '#871630',
          '#E6D5A8': '#E6D5A8',
          '#E9954A': '#E9954A',
          '#E64A00': '#E64A00',
          '#EAEBBC': '#EAEBBC',
          '#EFED84': '#EFED84',
          '#EFE032': '#EFE032',
          '#C9D8E7': '#C9D8E7',
          '#8ACBE5': '#8ACBE5',
          '#1A35A8': '#1A35A8',
          '#0F2867': '#0F2867',
          '#549AA3': '#549AA3',
          '#EACDCF': '#EACDCF',
          '#E8A3D0': '#E8A3D0',
          '#B50970': '#B50970',
          '#D7CAE3': '#D7CAE3',
          '#9E70C1': '#9E70C1',
          '#680E92': '#680E92',
          '#BC8F70': '#BC8F70',
          '#9E520F': '#9E520F',
          '#B6DD8E': '#B6DD8E',
          '#A4D426': '#A4D426',
          '#61AE56': '#61AE56',
          '#4A7229': '#4A7229',
          '#CFB53B': '#CFB53B',
          '#D7D8D8': '#D7D8D8'

        }
    });
});
</script>

{!! Html::script('js/upload-2.js') !!}
<!-- {!! Html::script('js/store-2.js') !!} -->
{!! Html::script('js/store.js') !!}
{!! Html::script('js/canvasToImage-2.js') !!}
{!! Html::script('js/textbuttons-2.js') !!}

<script>
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' &&
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' ||
                 d.type.toUpperCase() === 'FILE' ||
                 d.type.toUpperCase() === 'SEARCH' ||
                 d.type.toUpperCase() === 'EMAIL' ||
                 d.type.toUpperCase() === 'NUMBER' ||
                 d.type.toUpperCase() === 'DATE' )
             ) ||
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});
</script>

@if (Input::has('id') && Orderitem::find(Input::get('id')))

  <?php
  $sticker = Orderitem::find(Input::get('id'));
    if(Input::has('id') && Input::has('dvos') && $sticker->user_id == Input::get('dvos')) {
      $jsonstring = $sticker->json;
    } else {
      $jsonstring = 'missmatch';
    }
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson(<?php echo $jsonstring ?>);
	  setPrice();
      });
  </script>

  @if (Input::get('id') == "69136")
      <script>
          document.getElementById('quantity').value = 1;
          document.getElementById('quantity2').value = 1;
          setPrice();
      </script>
  @endif
@elseif (Input::has('id') && Input::has('dvos'))
  <?php
    $jsonstring = 'missmatch';
  ?>
  <script>
    $(document).ready(function() {
      loadfromJson(<?php echo $jsonstring ?>);
	  setPrice();
      });
  </script>
@endif

@if (Input::has('tmp') && Template::find(Input::get('tmp')))
  <?php
    $sticker = Template::find(Input::get('tmp'));
    $jsonstring = $sticker->json;
	  $tmpType = $sticker->product_type;
	  $tmpQuantity = $sticker->quantity;
    $tmpType += 1;
  ?>
  @if($tmpType == $typeofsticker)
	  <script>
		$(document).ready(function() {

			var tempAntall = <?php echo $tmpQuantity ?>;
			document.getElementById('quantity').value = tempAntall;
            if(document.getElementById('quantity2') != null){
                document.getElementById('quantity2').value = tempAntall;
            }

		  loadfromJson(<?php echo $jsonstring ?>);
		  setPrice();
		  });
	  </script>
	 @endif
@endif

@if (Input::has('edit') && Orderitem::find(Input::get('edit')))
	<?php
	$sticker = Orderitem::find(Input::get('edit'));
	$jsonstring = $sticker->json;
	?>
	@if (Auth::user() && $sticker->user_id == Auth::user()->id)
		<script>
			$(document).ready(function() {
        $('#quantity').val(<?php echo $sticker->quantity ?>);
        $('#Antall').val(<?php echo $sticker->quantity ?>);
			  loadfromJson(<?php echo $jsonstring ?>);
			  setPrice();
			  });
		</script>

	@endif
@endif

@if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
	<?php
	$sticker = Orderitem::find(Input::get('adminedit'));
	$jsonstring = $sticker->json;
	?>
	@if (Auth::user() && Auth::user()->admin == 1)
		<script>
      $('#quantity').val(<?php echo $sticker->quantity ?>);
      $('#Antall').val(<?php echo $sticker->quantity ?>);
			$(document).ready(function() {
			  loadfromJson(<?php echo $jsonstring ?>);

        var quantity = document.getElementById('quantity').value;
				var element = document.getElementById('Antall2');

        setPrice();
			  });
		</script>

	@endif
@endif
