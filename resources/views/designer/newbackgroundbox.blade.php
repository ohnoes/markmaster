<?php
	include '../resources/views/designs/newbackgroundget.blade.php';
	$background = getAllBackground();
?>

<script>
	var background = <?php echo json_encode($background); ?>;
</script>

<div class="nbackgroundBox popupBox" style="display: none;">
	<div class="popupHeader">
		<button type="button" name="button" class="popupClose" onclick="nbackgroundBoxHide()">X</button>
		<h4 class="popupTitle" style="margin-bottom: 0px;"> PICK BACKGROUND </h4>
	</div>
	<div class="clipartCategoryBox" >
      <a onclick="backgroundCategorySelect(1)" class="clipartCategoryText">{{ trans('designs/designer.Label-backgrounds') }}</a>		  <br />
			<a onclick="backgroundCategorySelect(0)" class="clipartCategoryText">{{ trans('designs/designer.Label-mybackgrounds') }}</a>		<br />
	</div>
	<div class="backgroundImageBox" >
	</div>
	<br style="clear:both;"/>
</div>

{!! Html::script('js/background.new.js') !!}
