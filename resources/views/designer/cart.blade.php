<div class="tab-pane" id="cartControl">
        @if($typeofsticker != 6 && $typeofsticker != 7)
            @if(Auth::user() && Auth::user()->admin == 1)
                        {!! Form::hidden('thePrice', 'Price: ', array('id'=>'thePrice2')) !!}
                        @include('designer.stickertype')
                        {!! Form::button('-test- Preview Sticker', array('onclick' => 'submitSticker()')) !!}
                        {!! '<span id="stickerPreview">
                        </span>' !!}
            @endif
            @if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                <?php
                    $sticker = Orderitem::find(Input::get('adminedit'));
                ?>
                @if (Auth::user() && Auth::user()->admin == 1 && ($sticker->status == 0 || $sticker->status == 1) )
                    <form method="POST" action="/{{ app()->getLocale() }}/admin/updatesticker" accept-charset="UTF-8">
                			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('stickerid', $sticker->id) !!}
                    {!! Form::hidden('userid', Auth::user()->id) !!}
                    @include('designer.stickertype')
                    {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                    @if($sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @else
                        {!! Form::number('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    @endif
                    <label>{{ trans('designs/designer.Label-quantity') }}</label>
                    {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                    {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                    {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                    <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label><br/><br/>
                    <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="buttonBox">
                    {!! Form::close() !!}
                @endif
            @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                <?php
                    $sticker = Orderitem::find(Input::get('edit'));
                ?>
                @if (Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 0 || $sticker->status == 1) )
                    <form method="POST" action="/{{ app()->getLocale() }}/store/updatecart" accept-charset="UTF-8">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('stickerid', $sticker->id) !!}
                    {!! Form::hidden('userid', Auth::user()->id) !!}
                    @include('designer.stickertype')
                    {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                    @if($sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @else
                        {!! Form::number('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    @endif
                    <label>{{ trans('designs/designer.Label-quantity') }}</label>
                    {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                    {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                    {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                    <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label><br/><br/>
                    <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="buttonBox">
                    {!! Form::close() !!}
                @endif
            @else
                <p>
                      <form method="POST" action="/{{ app()->getLocale() }}/store/addtocart" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      @if(Auth::check())
                        {!! Form::hidden('userid', Auth::user()->id)!!}
                      @endif
                      @include('designer.stickertype')
                      {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                      {!! Form::number('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                      <label>{{ trans('designs/designer.Label-pick-quantity') }}</label>
                   <br/><br/>
                      {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                      {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                      {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                      <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label><br/><br/>
                      <input type="submit" value="{{ trans('designs/designer.Label-order') }}" class="buttonBox">
                      {!! Form::close() !!}
                  </p>
            @endif
        @elseif($typeofsticker == 6)
            {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
            @if(Auth::user() && Auth::user()->admin == 1)
                {!! Form::button('-test- Preview Sticker', array('onclick' => 'submitSticker()')) !!}
                {!! '<span id="stickerPreview">
                    </span>' !!}
            @endif


            @if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                <?php
                    $sticker = Orderitem::find(Input::get('adminedit'));
                ?>
                @if (Auth::user() && Auth::user()->admin == 1)
                    <form method="POST" action="/{{ app()->getLocale() }}/admin/updatesticker" accept-charset="UTF-8">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('stickerid', $sticker->id) !!}
                    {!! Form::hidden('userid', Auth::user()->id) !!}
                    {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
                    {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                    <label> {{ trans('designs/designer.Label-quantity') }}</label>
                    @if($sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                        {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @else
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                        <select id="Antall2" name="Antall2" onchange="setPrice()">
                            <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                            <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                            <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                            <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                            <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                            <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                            <option value="100">{{ trans('designs/designer.Label-100') }}</option>
                            <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                            <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                            <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                        </select>

                    @endif
                    {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                    {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                    {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                    <br><br>
                    <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label>
                    <br><br>
                    <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="buttonBox">
                    {!! Form::close() !!}
                @endif
            @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                <?php
                    $sticker = Orderitem::find(Input::get('edit'));
                ?>
                @if (Auth::user() && $sticker->user_id == Auth::user()->id && ($sticker->status == 0 || $sticker->status == 1) )
                    <form method="POST" action="/{{ app()->getLocale() }}/store/updatecart" accept-charset="UTF-8">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('stickerid', $sticker->id) !!}
                    {!! Form::hidden('userid', Auth::user()->id) !!}
                    {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
                    {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                    <label> {{ trans('designs/designer.Label-quantity') }}</label>
                    @if($sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                        {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @else
                        <select id="Antall2" name="Antall2" onchange="setPrice()">
                            <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                            <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                            <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                            <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                            <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                            <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                            <option value="100">{{ trans('designs/designer.Label-100') }}</option>
                            <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                            <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                            <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                        </select>
                    @endif
                    {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                    {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                    {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                    <br><br>
                    <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label>
                    <br><br>
                    <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="buttonBox">
                    {!! Form::close() !!}
                @endif
            @else
                <p>
                <form method="POST" action="/{{ app()->getLocale() }}/store/addtocart" accept-charset="UTF-8">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if(Auth::check())
                    {!! Form::hidden('userid', Auth::user()->id)!!}
                @endif
                {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
                {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}

                <label> {{ trans('designs/designer.Label-quantity') }}</label>

                <select id="Antall2" name="Antall2" onchange="setPrice()">
                    <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                    <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                    <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                    <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                    <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                    <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                    <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
                    <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                    <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                    <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                </select>

                <br/><br/>
                {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label>

                <br/><br/>
                <input type="submit" value="{{ trans('designs/designer.Label-order') }}" class="buttonBox">
                {!! Form::close() !!}
            </p>
            @endif
        @else
            @if(Auth::user() && Auth::user()->admin == 1)
                {!! Form::button('-test- Preview Sticker', array('onclick' => 'submitSticker()')) !!}
                {!! '<span id="stickerPreview">
                    </span>' !!}
            @endif

            @if (Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                <?php
                    $sticker = Orderitem::find(Input::get('adminedit'));
                ?>
                @if (Auth::user() && Auth::user()->admin == 1 )
                    <form method="POST" action="/{{ app()->getLocale() }}/admin/updatesticker" accept-charset="UTF-8">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('stickerid', $sticker->id) !!}
                    {!! Form::hidden('userid', Auth::user()->id) !!}
                    {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
                    {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                    <label> {{ trans('designs/designer.Label-quantity') }}</label>
                    @if($sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                        {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @else

                        <select id="Antall2" name="Antall2" onchange="setPrice()">
                            <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                            <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                            <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                            <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                            <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                            <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                            <option value="100">{{ trans('designs/designer.Label-100') }}</option>
                            <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                            <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                            <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                        </select>

                    @endif
                    {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                    {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                    {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                    <br><br>
                    <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label>
                    <br><br>
                    <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="buttonBox">
                    {!! Form::close() !!}
                @endif
            @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                <?php
                    $sticker = Orderitem::find(Input::get('edit'));
                ?>
                @if (Auth::user() && $sticker->user_id == Auth::user()->id && ($sticker->status == 0 || $sticker->status == 1) )
                    <form method="POST" action="/{{ app()->getLocale() }}/store/updatecart" accept-charset="UTF-8">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('stickerid', $sticker->id) !!}
                    {!! Form::hidden('userid', Auth::user()->id) !!}
                    {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
                    {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                    <label> {{ trans('designs/designer.Label-quantity') }}</label>
                    @if($sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                        {!! Form::hidden('Antall2', $sticker->quantity, array('id'=>'Antall2')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @else
                        <select id="Antall2" name="Antall2" onchange="setPrice()">
                            <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                            <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                            <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                            <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                            <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                            <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                            <option value="100">{{ trans('designs/designer.Label-100') }}</option>
                            <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                            <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                            <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                        </select>
                    @endif
                    {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                    {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                    {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                    <br><br>
                    <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label>
                    <br><br>
                    <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="buttonBox">
                    {!! Form::close() !!}
                @endif
            @else
                <p>
                <form method="POST" action="/{{ app()->getLocale() }}/store/addtocart" accept-charset="UTF-8">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if(Auth::check())
                    {!! Form::hidden('userid', Auth::user()->id)!!}
                @endif
                {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
                {!! Form::hidden('stickerjson', 'stickerjson', array('id'=>'stickerjson')) !!}
                {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}

                <label> {{ trans('designs/designer.Label-quantity') }}</label>

                <select id="Antall2" name="Antall2" onchange="setPrice()">
                    <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                    <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                    <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                    <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                    <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                    <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                    <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
                    <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                    <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                    <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                </select>

                <br/><br/>
                {!! Form::hidden('stickerprice', 'stickerprice', array('id'=>'stickerprice'))!!}
                {!! Form::hidden('stickerwidth', 'stickerwidth', array('id'=>'stickerwidth')) !!}
                {!! Form::hidden('stickerheight', 'stickerheight', array('id'=>'stickerheight')) !!}
                <label for="thePrice" id="thePrice2"> {{ trans('designs/designer.Label-price') }}</label>

                <br/><br/>
                <input type="submit" value="{{ trans('designs/designer.Label-order') }}" class="buttonBox">
                {!! Form::close() !!}
            </p>
            @endif
        @endif
</div>
