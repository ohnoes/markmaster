<div class="tab-pane" id="backgroundColorControl">

    <!-- Sticker with Color -->
    @if($typeofsticker == 1 || $typeofsticker == 2 || $typeofsticker == 8 || $typeofsticker == 9)
        {!! Form::hidden('clipartType', '2', array('id'=>'clipartType')) !!}
        <p>
            <label for="color" >{{ trans('designs/designer.Label-color-select') }}</label>
        </p>
        <span id="backgroundPicker">
            {!! Form::select('backgroundColorPicker', array(
                '#000000' => 'Black',
                '#ffffff' => 'White',
                '#E6E6E6' => '10% Black',
                '#B1B1B1' => '40% Black',
                '#888887' => '60% Black',
                '#5C5C5B' => '80% Black',
                '#EBB5C3' => 'PANTONE 182 C',
                '#C8112E' => 'PANTONE 185 C',
                '#B01D2B' => 'PANTONE 1797 C',
                '#871630' => 'PANTONE 201 C',
                '#E6D5A8' => 'PANTONE 155 C',
                '#E9954A' => 'PANTONE 804 C',
                '#E64A00' => 'PANTONE Orange 021 C',
                '#EAEBBC' => 'PANTONE 607 C',
                '#EFED84' => 'PANTONE 100 C',
                '#EFE032' => 'PANTONE Yellow C',
                '#C9D8E7' => 'PANTONE 290 C',
                '#8ACBE5' => 'PANTONE 305 C',
                '#1A35A8' => 'PANTONE 286 C',
                '#0F2867' => 'PANTONE 281 C',
                '#549AA3' => 'PANTONE 320 C',
                '#EACDCF' => 'PANTONE 698 C',
                '#E8A3D0' => 'PANTONE 230 C',
                '#B50970' => 'PANTONE 226 C',
                '#D7CAE3' => 'PANTONE 263 C',
                '#9E70C1' => 'PANTONE 528 C',
                '#680E92' => 'PANTONE 527 C',
                '#BC8F70' => 'PANTONE 7515 C',
                '#9E520F' => 'PANTONE 471 C',
                '#B6DD8E' => 'PANTONE 358 C',
                '#A4D426' => 'PANTONE 375 C',
                '#61AE56' => 'PANTONE 354 C',
                '#4A7229' => 'PANTONE 364 C',
                '#cfb53b' => 'Gold-like',
                '#d7d8d8' => 'Silver-like'),
                '#000000',
                array('id' => 'backgroundColor' ))
            !!}
        </span>
        <br/><br/>
        <label>{{ trans('designs/designer.Label-upload-select-background') }}</label>
        <br>

         <select id="backCategory">
            <option value="0" selected>{{ trans('designs/designer.Label-backgrounds') }}</option>
            <option value="1">{{ trans('designs/designer.Label-mybackgrounds') }}</option>
         </select>

        <span id="backgroundShow">
            <?php include '../resources/views/backgroundGet.blade.php'; getBackground(0, 2); ?>
        </span>
        <br/><br/>
        <label>{{ trans('designs/designer.Label-upload-mybackground') }}</label>
        <br/><br/>
        <span class="uploadControls">
            {!! Form::open(array('files' => true, 'id' => 'backgroundForm')) !!}
            @if (Auth::user())
                <div class="form-group">
                    {!! Form::file('image', array('id' => 'backgroundFileSelect')) !!}
                </div>
                <div class="form-group">
                    <input type="submit" value="{{ trans('designs/designer.Label-upload-background') }}" class="payBox">
                </div>
            @else
                <div class="form-group">
                    {!! Form::file('image', array('disabled' => '', 'id' => 'backgroundFileSelect')) !!}
                </div>
                <div class="form-group">
                    <input type="submit" value="{{ trans('designs/designer.Label-upload-background') }}" class="payBox">
                </div>
            @endif
            {!! Form::close() !!}
        </span>
        <br/><br/><br/>
    <!-- Gold, Silver, Transparent -->
    @elseif($typeofsticker == 3 || $typeofsticker == 4 || $typeofsticker == 5)
        {!! Form::hidden('clipartType', '1', array('id'=>'clipartType')) !!}
        <div class="form-group">
            <label for="color" >{{ trans('designs/designer.Label-background-nonchangeable') }}</label>
        </div>

         <span class="uploadControls">
            {!! Form::open(array('files' => true, 'id' => 'backgroundForm')) !!}
             <!-- {!! Form::label('title', 'Name your background') !!}
            {!! Form::text('title') !!} -->

              {!! Form::hidden('image','image', array('id' => 'backgroundFileSelect')) !!}

            {!! Form::close() !!}
          </span>
    <!-- Black and White labels/stickers -->
    @else
    {!! Form::hidden('clipartType', '0', array('id'=>'clipartType')) !!}
        <div class="form-group">
            <label>{{ trans('designs/designer.Label-upload-select-background') }}</label>
        </div>
        <div class="form-group">
            <select id="backCategory">
                <option value="0" selected>{{ trans('designs/designer.Label-backgrounds') }}</option>
                <option value="1">{{ trans('designs/designer.Label-mybackgrounds') }}</option>
             </select>
            <span id="backgroundShow">
              <?php include '../resources/views/backgroundGet.blade.php'; getBackground(0, 0); ?>
            </span>
        </div>

        <div class="form-group">
            <label for="uploadLine">{{ trans('designs/designer.Label-upload-mybackground') }}</label>
        </div>

        <span class="uploadControls">
            {!! Form::open(array('files' => true, 'id' => 'backgroundForm')) !!}
            @if (Auth::user())
                <div class="form-group">
                    {!! Form::file('image', array('id' => 'backgroundFileSelect')) !!}
                </div>
                <div class="form-group">
                    <input type="submit" value="{{ trans('designs/designer.Label-upload-background') }}" class="payBox">
                </div>
            @else
                <div class="form-group">
                    {!! Form::file('image', array('disabled' => '', 'id' => 'backgroundFileSelect')) !!}
                </div>
                <div class="form-group">
                    <input type="submit" value="{{ trans('designs/designer.Label-upload-background') }}" class="payBox">
                </div>
            @endif
            {!! Form::close() !!}
        </span>
    @endif

</div>



@if($typeofsticker == 3)
    <script>
    	$(document).ready(function() {
    		var bgColor = '#cfb53b';
    		setBackgroundColor(bgColor);
    	});
    </script>
@elseif($typeofsticker == 4)
    <script>
        $(document).ready(function() {
            var bgColor = '#d7d8d8';
            setBackgroundColor(bgColor);
        });
    </script>
@endif
