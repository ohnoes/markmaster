<div class="tab-pane active" id="sizeAndShapeControl">
        <!-- Label holders -->
            <!-- Stickers -->
            @if($typeofsticker != 6 && $typeofsticker != 7)
                {!! Form::hidden('size', 'size', array('id'=>'3716')) !!}
                {!! Form::hidden('size', 'size', array('id'=>'6026')) !!}
                {!! Form::hidden('size', 'size', array('id'=>'3010')) !!}
                <?php
                    $thecur = Currency::find(1);
                    if(Auth::check()) {
                        $usercur = Auth::user()->currency;
                        if ($usercur == 0) {
                            $currencyValue = 1;
                        } else if ($usercur == 1) {
                            $currencyValue = $thecur->usd;
                        } else if ($usercur == 2) {
                            $currencyValue = $thecur->sek;
                        } else if ($usercur == 3) {
                            $currencyValue = $thecur->dkk;
                        }else if ($usercur == 4) {
                            $currencyValue = $thecur->eur;
                        }
                    } else {
                        $usercur = Session::get('theCurrency', 0);
                        if ($usercur == 0) {
                            $currencyValue = 1;
                        } else if ($usercur == 1) {
                            $currencyValue = $thecur->usd;
                        } else if ($usercur == 2) {
                            $currencyValue = $thecur->sek;
                        } else if ($usercur == 3) {
                            $currencyValue = $thecur->dkk;
                        }else if ($usercur == 4) {
                            $currencyValue = $thecur->eur;
                        }
                    }
                ?>
                {!! Form::hidden('thecurrency', $usercur, array('id'=>'thecurrency')) !!}
                {!! Form::hidden('currencycheck', $currencyValue, array('id'=>'currencycheck')) !!}
                {!! Form::hidden('priceHolder', 'Price: ', array('id'=>'priceHolder')) !!}
            <!-- Black and white stickers -->
            @elseif($typeofsticker == 6)
                {!! Form::hidden('shape','1',array('id'=>'isRect')) !!}
                {!! Form::hidden('shape','',array('id'=>'isEllipse')) !!}
                {!! Form::hidden('size', 'size', array('id'=>'3010')) !!}
                <?php
                    $thecur = Currency::find(1);
                    if(Auth::check()) {
                        $usercur = Auth::user()->currency;
                        if ($usercur == 0) {
                            $currencyValue = 1;
                        } else if ($usercur == 1) {
                            $currencyValue = $thecur->usd;
                        } else if ($usercur == 2) {
                            $currencyValue = $thecur->sek;
                        } else if ($usercur == 3) {
                            $currencyValue = $thecur->dkk;
                        }else if ($usercur == 4) {
                            $currencyValue = $thecur->eur;
                        }
                    } else {
                        $usercur = Session::get('theCurrency', 0);
                        if ($usercur == 0) {
                            $currencyValue = 1;
                        } else if ($usercur == 1) {
                            $currencyValue = $thecur->usd;
                        } else if ($usercur == 2) {
                            $currencyValue = $thecur->sek;
                        } else if ($usercur == 3) {
                            $currencyValue = $thecur->dkk;
                        }else if ($usercur == 4) {
                            $currencyValue = $thecur->eur;
                        }
                    }

                ?>
                {!! Form::hidden('thecurrency', $usercur, array('id'=>'thecurrency')) !!}
                {!! Form::hidden('currencycheck', $currencyValue, array('id'=>'currencycheck')) !!}
                {!! Form::hidden('priceHolder', 'Price: ', array('id'=>'priceHolder')) !!}
            <!-- Black and white iron on labels -->
            @else
                {!! Form::hidden('shape','1',array('id'=>'isRect')) !!}
                {!! Form::hidden('shape','',array('id'=>'isEllipse')) !!}
                {!! Form::hidden('size', 'size', array('id'=>'3716')) !!}
                {!! Form::hidden('size', 'size', array('id'=>'6026')) !!}
                <?php
                    $thecur = Currency::find(1);
                    if(Auth::check()) {
                        $usercur = Auth::user()->currency;
                        if ($usercur == 0) {
                            $currencyValue = 1;
                        } else if ($usercur == 1) {
                            $currencyValue = $thecur->usd;
                        } else if ($usercur == 2) {
                            $currencyValue = $thecur->sek;
                        } else if ($usercur == 3) {
                            $currencyValue = $thecur->dkk;
                        }else if ($usercur == 4) {
                            $currencyValue = $thecur->eur;
                        }
                    } else {
                        $usercur = Session::get('theCurrency', 0);
                        if ($usercur == 0) {
                            $currencyValue = 1;
                        } else if ($usercur == 1) {
                            $currencyValue = $thecur->usd;
                        } else if ($usercur == 2) {
                            $currencyValue = $thecur->sek;
                        } else if ($usercur == 3) {
                            $currencyValue = $thecur->dkk;
                        }else if ($usercur == 4) {
                            $currencyValue = $thecur->eur;
                        }
                    }

                ?>
                    {!! Form::hidden('thecurrency', $usercur, array('id'=>'thecurrency')) !!}
                    {!! Form::hidden('currencycheck', $currencyValue, array('id'=>'currencycheck')) !!}
                    {!! Form::hidden('priceHolder', 'Price: ', array('id'=>'priceHolder')) !!}
            @endif

        <!-- Sticker shape -->
        @if($typeofsticker != 6 && $typeofsticker != 7)
            <div class="form-group">
                {!! Form::radio('shape','','1',array('id'=>'isRect')) !!}
                {!! Html::image('img/rektangel.png', 'Rektangel', array('onclick'=>'makeRect()' )) !!}
                <label for="isRect">{{ trans('designs/designer.Label-rectangle') }}</label>
            </div>
            <div class="form-group">
                {!! Form::radio('shape','','',array('id'=>'isEllipse')) !!}
                {!! Html::image('img/elipse.png', 'Elipse', array('onclick'=>'makeElipse()' )) !!}
                <label for="isEllipse">{{ trans('designs/designer.Label-elipse') }}</label>
            </div>
        @endif

        <!-- Sticker Size -->
            <!-- Stickers -->
            @if($typeofsticker != 6 && $typeofsticker != 7)
                @if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                    <?php
                        $sticker = Orderitem::find(Input::get('adminedit'));
                    ?>
                    @if (Auth::user() && Auth::user()->admin == 1 && $sticker->status == 1)
                        <div class="form-group">
                            {!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
                            {!! Form::text('widthtext', $sticker->width, array('disabled')) !!}
                            <label for="widthtext">{{ trans('designs/designer.Label-width-mm') }}</label>
                        </div>
                        <div class="form-group">
                            {!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
                            {!! Form::text('heighttext', $sticker->height, array('disabled')) !!}
                            <label for="heighttext">{{ trans('designs/designer.Label-height-mm') }}</label>
                        </div>
                    @else
                        <div class="form-group">
                            {!! Form::text('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                            <label for="width">{{ trans('designs/designer.Label-width-mm') }}</label>
                        </div>
                        <div class="form-group">
                            {!! Form::text('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                            <label for="height">{{ trans('designs/designer.Label-height-mm') }}</label>
                        </div>
                    @endif
                @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                    <?php
                        $sticker = Orderitem::find(Input::get('edit'));
                    ?>
                    @if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
                        <div class="form-group">
                            {!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
                            {!! Form::text('widthtext', $sticker->width, array('disabled')) !!}
                            <label for="widthtext">{{ trans('designs/designer.Label-width-mm') }}</label>
                        </div>
                        <div class="form-group">
                            {!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
                            {!! Form::text('heighttext', $sticker->height, array('disabled')) !!}
                            <label for="heighttext">{{ trans('designs/designer.Label-height-mm') }}</label>
                        </div>
                    @else
                        <div class="form-group">
                            {!! Form::text('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                            <label for="width">{{ trans('designs/designer.Label-width-mm') }}</label>
                        </div>
                        <div class="form-group">
                            {!! Form::text('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                            <label for="height">{{ trans('designs/designer.Label-height-mm') }}</label>
                        </div>
                    @endif
                @else
                    @if($typeofsticker == 9)
                        <div class="form-group">
                            {!! Form::text('width','30', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                            <label for="width">{{ trans('designs/designer.Label-width-mm') }}</label>
                        </div>
                        <div class="form-group">
                            {!! Form::text('height','13', array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                            <label for="height">{{ trans('designs/designer.Label-height-mm') }}</label>
                        </div>
                    @else
                        <div class="form-group">
                            {!! Form::text('width','37', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                            <label for="width">{{ trans('designs/designer.Label-width-mm') }}</label>
                        </div>
                        <div class="form-group">
                            {!! Form::text('height','16', array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                            <label for="height">{{ trans('designs/designer.Label-height-mm') }}</label>
                        </div>
                    @endif
                @endif

                @if($typeofsticker == 9)
                    {{ trans('designs/designer.Label-width-height-info2') }}
                @else
                    {{ trans('designs/designer.Label-width-height-info') }}
                @endif
            <!-- Black and white stickers -->
            @elseif($typeofsticker == 6)
                <div class="form-group">
                <label for="size">{{ trans('designs/designer.Label-pick-size') }} </label>
                </div>
                <div class="form-group">
                {!! Form::radio('size','','1',array('id'=>'3716')) !!}
                <label for="3716">{{ trans('designs/designer.Label-3716') }} </label>
                </div>
                <div class="form-group">
                {!! Form::radio('size','','',array('id'=>'6026')) !!}
                <label for="6026">{{ trans('designs/designer.Label-6026') }} </label>
                </div>

                @if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                <?php
                    $sticker = Orderitem::find(Input::get('adminedit'));
                ?>
                @if (Auth::user() && Auth::user()->admin == 1 && $sticker->status == 1)
                    {!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
                    {!! Form::hidden('widthtext', $sticker->width, array('disabled')) !!}
                    {!! Form::hidden('bredde mm') !!}

                    {!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
                    {!! Form::hidden('heighttext', $sticker->height, array('disabled')) !!}
                    {!! Form::hidden('høyde mm') !!}
                @else
                    {!! Form::hidden('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                    {!! Form::hidden('bredde mm') !!}

                    {!! Form::hidden('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                    {!! Form::hidden('høyde mm') !!}
                    @endif
                @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                    <?php
                        $sticker = Orderitem::find(Input::get('edit'));
                    ?>
                    @if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
                        {!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
                        {!! Form::hidden('widthtext', $sticker->width, array('disabled')) !!}
                        {!! Form::hidden('bredde mm') !!}

                        {!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
                        {!! Form::hidden('heighttext', $sticker->height, array('disabled')) !!}
                        {!! Form::hidden('høyde mm') !!}
                    @else
                        {!! Form::hidden('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                        {!! Form::hidden('bredde mm') !!}

                        {!! Form::hidden('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                        {!! Form::hidden('høyde mm') !!}
                    @endif
                @else
                  {!! Form::hidden('width','37', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                  {!! Form::hidden('bredde mm') !!}

                  {!! Form::hidden('height','16', array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                  {!! Form::hidden('høyde mm') !!}
                @endif
            <!-- Black and white iron on labels -->
            @else
                    <div class="form-group">
                    <label for="size">{{ trans('designs/designer.Label-pick-size') }}</label>
                    </div>
                    <div class="form-group">
                    {!! Form::radio('size','','1',array('id'=>'3010')) !!}
                    <label for="3010">{{ trans('designs/designer.Label-3010') }}</label>
                    </div>

                @if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                    <?php
                        $sticker = Orderitem::find(Input::get('adminedit'));
                    ?>
                    @if (Auth::user() && Auth::user()->admin == 1 && $sticker->status == 1)
                        {!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
                        {!! Form::hidden('widthtext', $sticker->width, array('disabled')) !!}
                        {!! Form::hidden('bredde mm') !!}

                        {!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
                        {!! Form::hidden('heighttext', $sticker->height, array('disabled')) !!}
                        {!! Form::hidden('høyde mm') !!}
                    @else
                        {!! Form::hidden('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                        {!! Form::hidden('bredde mm') !!}

                        {!! Form::hidden('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                        {!! Form::hidden('høyde mm') !!}
                    @endif
                @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                    <?php
                        $sticker = Orderitem::find(Input::get('edit'));
                    ?>
                    @if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
                        {!! Form::hidden('width', $sticker->width, array('id'=>'width')) !!}
                        {!! Form::hidden('widthtext', $sticker->width, array('disabled')) !!}
                        {!! Form::hidden('bredde mm') !!}

                        {!! Form::hidden('height', $sticker->height, array('id'=>'height')) !!}
                        {!! Form::hidden('heighttext', $sticker->height, array('disabled')) !!}
                        {!! Form::hidden('høyde mm') !!}
                    @else
                        {!! Form::hidden('width',$sticker->width, array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                        {!! Form::hidden('bredde mm') !!}

                        {!! Form::hidden('height',$sticker->height, array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                        {!! Form::hidden('høyde mm') !!}
                    @endif
                @else
                  {!! Form::hidden('width','30', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
                  {!! Form::hidden('bredde mm') !!}
                  <br/>
                  {!! Form::hidden('height','10', array('id' => 'height', 'oninput' => 'setPrice()')) !!}
                  {!! Form::hidden('høyde mm') !!}
                @endif
            @endif

    <!-- Sticker Quantity -->
        <!-- Stickers -->
        @if($typeofsticker != 6 && $typeofsticker != 7)
            <div class="form-group">
                @if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                    <?php
                        $sticker = Orderitem::find(Input::get('adminedit'));
                    ?>
                    @if (Auth::user() && Auth::user()->admin == 1 && $sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity2')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @elseif ($sticker->status == 0 || $sticker->status == 2)
                        {!! Form::number('quantity',$sticker->quantity, array('id' => 'quantity2', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    @endif
                @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                    <?php
                        $sticker = Orderitem::find(Input::get('edit'));
                    ?>
                    @if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
                        {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity2')) !!}
                        {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                    @elseif ($sticker->status == 0 || $sticker->status == 2)
                        {!! Form::number('quantity',$sticker->quantity, array('id' => 'quantity2', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    @endif
                @else
                    {!! Form::number('quantity','100', array('id' => 'quantity2', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                @endif
                <label for="quantity">{{ trans('designs/designer.Label-pick-quantity') }}</label>
            </div>
        <!-- Black and white sticker -->
        @elseif($typeofsticker == 6)
            @if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                <?php
                    $sticker = Orderitem::find(Input::get('adminedit'));
                ?>
                @if (Auth::user() && Auth::user()->admin == 1 )
                <div class="form-group">
                    {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                    {!! Form::hidden('Antall', $sticker->quantity, array('id'=>'Antall')) !!}
                    {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                </div>
                @elseif ($sticker->status == 0 || $sticker->status == 2)
                <div class="form-group">
                    {!! Form::hidden('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    <label> {{ trans('designs/designer.Label-quantity') }} </label>
                    <select id="Antall" onchange="setPrice()">
                        <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                        <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                        <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                        <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                        <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                        <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                        <option value="100">{{ trans('designs/designer.Label-100') }}</option>
                        <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                        <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                        <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                    </select>
                </div>
                @endif
            @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                <?php
                    $sticker = Orderitem::find(Input::get('edit'));
                ?>
                @if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
                <div class="form-group">
                    {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                    {!! Form::hidden('Antall', $sticker->quantity, array('id'=>'Antall')) !!}
                    {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                </div>
                @elseif ($sticker->status == 0 || $sticker->status == 2)
                <div class="form-group">
                    {!! Form::hidden('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    <label> {{ trans('designs/designer.Label-quantity') }} </label>
                    <select id="Antall" onchange="setPrice()">
                        <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                        <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                        <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                        <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                        <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                        <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                        <option value="100">{{ trans('designs/designer.Label-100') }}</option>
                        <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                        <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                        <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                    </select>
                </div>
                @endif
            @else
            <div class="form-group">
             {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
            <label> {{ trans('designs/designer.Label-quantity') }} </label>
            <select id="Antall" onchange="setPrice()">
                <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
                <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
            </select>
            </div>
            @endif
        <!-- Black and white iron on labels -->
        @else
            @if(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')))
                <?php
                    $sticker = Orderitem::find(Input::get('adminedit'));
                ?>
                @if (Auth::user() && Auth::user()->admin == 1 && $sticker->status == 1)
                <div class="form-group">
                    {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                    {!! Form::hidden('Antall', $sticker->quantity, array('id'=>'Antall')) !!}
                    {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                </div>
                @elseif ($sticker->status == 0 || $sticker->status == 2)
                <div class="form-group">
                    {!! Form::hidden('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    <label> {{ trans('designs/designer.Label-quantity') }} </label>

                    <select id="Antall" onchange="setPrice()">
                        <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                        <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                        <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                        <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                        <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                        <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                        <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
                        <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                        <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                        <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                    </select>
                </div>
                @endif
            @elseif (Input::has('edit') && Orderitem::find(Input::get('edit')))
                <?php
                    $sticker = Orderitem::find(Input::get('edit'));
                ?>
                @if (Auth::user() && $sticker->user_id == Auth::user()->id && $sticker->status == 1)
                <div class="form-group">
                    {!! Form::hidden('quantity', $sticker->quantity, array('id'=>'quantity')) !!}
                    {!! Form::hidden('Antall', $sticker->quantity, array('id'=>'Antall')) !!}
                    {!! Form::number('quantitytext', $sticker->quantity, array('disabled')) !!}
                </div>
                @elseif ($sticker->status == 0 || $sticker->status == 2)
                <div class="form-group">
                    {!! Form::hidden('quantity',$sticker->quantity, array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
                    <label for="Antall">{{ trans('designs/ironon.Translate15') }}</label>

                    <select id="Antall" onchange="setPrice()">
                        <option value="1" >{{ trans('designs/ironon.Translate16') }}</option>
                        <option value="5">{{ trans('designs/ironon.Translate17') }}</option>
                        <option value="10">{{ trans('designs/ironon.Translate18') }}</option>
                        <option value="20">{{ trans('designs/ironon.Translate19') }}</option>
                        <option value="50">{{ trans('designs/ironon.Translate20') }}</option>
                        <option value="75">{{ trans('designs/ironon.Translate21') }}</option>
                        <option value="100" selected>{{ trans('designs/ironon.Translate22') }}</option>
                        <option value="250">{{ trans('designs/ironon.Translate23') }}</option>
                        <option value="500">{{ trans('designs/ironon.Translate24') }}</option>
                        <option value="1000">{{ trans('designs/ironon.Translate25') }}</option>
                    </select>
                </div>
                @endif
            @else
            <div class="form-group">
             {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'oninput' => 'setPrice()')) !!}
            <label for="Antall">{{ trans('designs/ironon.Translate15') }}</label>
                    <select id="Antall">
                        <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                        <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                        <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                        <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                        <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                        <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                        <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
                        <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                        <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                        <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
                    </select>
            </div>
            @endif
        @endif

    <!-- Sticker Price -->
    <div class="form-group">
        <label for="thePrice" id="thePrice">{{ trans('designs/designer.Label-price') }}</label>
    </div>

</div>
