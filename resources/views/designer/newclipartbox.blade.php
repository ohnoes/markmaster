<?php
	include '../resources/views/designs/newclipartget.blade.php';
	$clipart = getAllClipart();
?>

<script>
	var clipart = <?php echo json_encode($clipart); ?>;
</script>

<div class="clipartBox popupBox" style="display: none;">
	<div class="popupHeader">
		<button type="button" name="button" class="popupClose" onclick="clipartBoxHide()">X</button>
		<h4 class="popupTitle" style="margin-bottom: 0px;"> PICK CLIPART </h4>
	</div>
	<div class="clipartCategoryBox" >
		<div class="clipartSearchBox">
			<input id="csb" class="clipartSearchBox" type="text" oninput="searchClipart()" placeholder="Search" />
		</div>
      <a onclick="clipartCategorySelect(2)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-used') }}</a>		<br />
			<a onclick="clipartCategorySelect(3)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-animal') }}</a>		<br />
			<a onclick="clipartCategorySelect(4)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-zodiac') }}</a>		<br />
			<a onclick="clipartCategorySelect(5)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-figures') }}</a>		<br />
			<a onclick="clipartCategorySelect(6)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-symbols') }}</a>		<br />
			<a onclick="clipartCategorySelect(7)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-sport') }}</a>		<br />
			<a onclick="clipartCategorySelect(8)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-machines') }}</a>		<br />
			<a onclick="clipartCategorySelect(9)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-shape') }}</a>		<br />
			<a onclick="clipartCategorySelect(10)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-laundry') }}</a>      <br />
      <a onclick="clipartCategorySelect(0)" class="clipartCategoryText">{{ trans('designs/designer.Label-clipart-my') }}</a>       <br />
	</div>
	<div class="clipartImageBox" >
	</div>
	<br style="clear:both;"/>
</div>

{!! Html::script('js/clipart.new.js') !!}
