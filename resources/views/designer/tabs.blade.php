<ul id="tabs" class="nav nav-tabs nav-justified nav-margin" data-tabs="tabs">
    <li class="active"><a href="#sizeAndShapeControl" data-toggle="tab">{{ trans('designs/designer.Tab-size') }}</a></li>
    <li><a href="#backgroundColorControl" data-toggle="tab">{{ trans('designs/designer.Tab-background') }}</a></li>
    <li><a href="#textControl" data-toggle="tab">{{ trans('designs/designer.Tab-text') }} </a></li>
    <li><a href="#clipartControl" data-toggle="tab">{{ trans('designs/designer.Tab-clipart') }}</a></li>
    @if($typeofsticker == 1)
        <li onclick="rdycheckout(1)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @elseif($typeofsticker == 2)
        <li onclick="rdycheckout(2)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @elseif($typeofsticker == 3)
        <li onclick="rdycheckout(3)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @elseif($typeofsticker == 4)
        <li onclick="rdycheckout(4)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @elseif($typeofsticker == 5)
        <li onclick="rdycheckout(5)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @elseif($typeofsticker == 6)
        <li onclick="rdycheckout(6)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @elseif($typeofsticker == 7)
        <li onclick="rdycheckout(7)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @elseif($typeofsticker == 8)
        <li onclick="rdycheckout(8)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @else
        <li onclick="rdycheckout(9)"><a href="#cartControl" data-toggle="tab">{{ trans('designs/designer.Tab-cart') }}</a></li>
    @endif
</ul>
