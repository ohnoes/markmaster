@if($typeofsticker == 1)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-sticker-color') }}</h1>
    {!! Form::hidden('stickertype', '1', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 2)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-iron-color') }}</h1>
    {!! Form::hidden('stickertype', '2', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 3)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-gold') }}</h1>
    {!! Form::hidden('stickertype', '3', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 4)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-silver') }}</h1>
    {!! Form::hidden('stickertype', '4', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 5)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-transparent') }}</h1>
    {!! Form::hidden('stickertype', '5', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 6)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-sticker') }}</h1>
    {!! Form::hidden('stickertype', '6', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 7)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-iron') }}</h1>
    {!! Form::hidden('stickertype', '7', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 8)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-reflection') }}</h1>
    {!! Form::hidden('stickertype', '8', array('id'=>'stickertype')) !!}
@elseif($typeofsticker == 9)
    <h1 class="tempHeader">{{ trans('designs/designer.Header-sticker-fabric') }}</h1>
    {!! Form::hidden('stickertype', '9', array('id'=>'stickertype')) !!}
@endif
<meta name="csrf-token" content="{{ csrf_token() }}" />
