
@if($typeofsticker == 7 || $typeofsticker == 6)
<div class="shapeBox popupBox" id="shapeBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="shapeBoxHide()">X</button>
    <h4 class="popupTitle"> SHAPE </h4>
  </div>
  <div class="popupBody">
    <div class="form-group">
        <div onclick="makeRect()" style="cursor: hand;cursor: pointer;">
          {!! Form::radio('shape','','1',array('id'=>'isRect')) !!}
          {!! Html::image('img/rektangel.png', 'Rektangel') !!}
          <label for="isRect">{{ trans('designs/designer.Label-rectangle') }}</label>
        </div>
    </div>
  </div>
	<br style="clear:both;"/>
</div>
@else
<div class="shapeBox popupBox" id="shapeBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="shapeBoxHide()">X</button>
    <h4 class="popupTitle"> SHAPE </h4>
  </div>
  <div class="popupBody">
    <div class="form-group">
        <div onclick="makeRect()" style="cursor: hand;cursor: pointer;">
          {!! Form::radio('shape','','1',array('id'=>'isRect')) !!}
          {!! Html::image('img/rektangel.png', 'Rektangel') !!}
          <label for="isRect">{{ trans('designs/designer.Label-rectangle') }}</label>
        </div>
    </div>
    <div class="form-group">
      <div onclick="makeElipse()" style="cursor: hand;cursor: pointer;">
        {!! Form::radio('shape','','',array('id'=>'isEllipse')) !!}
        {!! Html::image('img/elipse.png', 'Elipse') !!}
        <label for="isEllipse">{{ trans('designs/designer.Label-elipse') }}</label>
      </div>
    </div>
    <div class="form-group"> {{-- TODO - fix styling for outline button. currently goes outside the box --}}
      <div onclick="makeOutline()" style="cursor: hand;cursor: pointer;"> {{-- TODO - determine where else to add the outline box --}}
        {!! Form::radio('shape','','',array('id'=>'isGeneratedCutline')) !!}
        {!! Html::image('img/elipse.png', 'Elipse') !!} {{-- TODO - get a picture for outline --}}
        <label for="isGeneratedCutline">{{ trans('designs/designer.Label-elipse') }}</label> {{-- TODO - choose a label for outline --}}
      </div>
    </div>
  </div>
	<br style="clear:both;"/>
</div>

@endif

{!! Html::script('js/shapebox.js') !!}
