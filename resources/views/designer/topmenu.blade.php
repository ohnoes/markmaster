<!-- Type of stickers that matter for size: Black N white stickers + ironfree  -->
<!-- Type of stickers that matter for amount: Black N white stickers  -->

<div class="row design-top-row no-margin"> <!-- ORDER ROW -->
  <div class="col-xs-4 left-align top-box right-border same-height"> <!-- HEADER -->
      @include('designer.header')
  </div>

  @if ( Input::has('id') && Orderitem::find(Input::get('id')) && Input::has('dvos') )
    <?php
        $stickerLoaded = false;
        $sticker = Orderitem::find(Input::get('id'));
        if($sticker->user_id == Input::get('dvos')) {
          $stickerLoaded = true;
        }
    ?>
  @elseif(Input::has('edit') && Orderitem::find(Input::get('edit')))
    <?php
        $stickerLoaded = true;
        $sticker = Orderitem::find(Input::get('edit'));
    ?>
  @else
    <?php
        $stickerLoaded = false;
    ?>
  @endif

@if($typeofsticker == 6) <!-- Black N white Sticker -->
  {!! Form::hidden('shape','1',array('id'=>'isRect')) !!}
  {!! Form::hidden('shape','',array('id'=>'isEllipse')) !!}
  {!! Form::hidden('size', 'size', array('id'=>'3010')) !!}
  {!! Form::hidden('width','37', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
  {!! Form::hidden('height','16', array('id' => 'height', 'oninput' => 'setPrice()')) !!}

  @if($stickerLoaded && ($sticker->width == 60))
    @if((Input::has('edit') && Orderitem::find(Input::get('edit'))) && Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 1))
      <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','',array('id'=>'3716', 'disabled'=>'disabled')) !!} </br>
        <label for="3716">{{ trans('designs/designer.Label-3716') }} </label>
        </div>
      </div>
      <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','1',array('id'=>'6026', 'disabled'=>'disabled')) !!} </br>
        <label for="6026">{{ trans('designs/designer.Label-6026') }} </label>
        </div>
      </div>
    @else
      <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','',array('id'=>'3716')) !!} </br>
        <label for="3716">{{ trans('designs/designer.Label-3716') }} </label>
        </div>
      </div>
      <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','1',array('id'=>'6026')) !!} </br>
        <label for="6026">{{ trans('designs/designer.Label-6026') }} </label>
        </div>
      </div>
    @endif
  @else
    @if((Input::has('edit') && Orderitem::find(Input::get('edit'))) && Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 1))
      <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','1',array('id'=>'3716', 'disabled'=>'disabled')) !!} </br>
        <label for="3716">{{ trans('designs/designer.Label-3716') }} </label>
        </div>
      </div>
      <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','',array('id'=>'6026', 'disabled'=>'disabled')) !!} </br>
        <label for="6026">{{ trans('designs/designer.Label-6026') }} </label>
        </div>
      </div>
    @else
      <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','1',array('id'=>'3716')) !!} </br>
        <label for="3716">{{ trans('designs/designer.Label-3716') }} </label>
        </div>
      </div>
      <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
        <div class="form-group">
        {!! Form::radio('size','','',array('id'=>'6026')) !!} </br>
        <label for="6026">{{ trans('designs/designer.Label-6026') }} </label>
        </div>
      </div>
      @endif
  @endif
  <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;"> <!-- QUANTITY -->
    {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'onchange' => 'setPrice()')) !!}
     <label for="Antall">{{ trans('designs/ironon.Translate15') }}</label> </br>
       @if((Input::has('edit') && Orderitem::find(Input::get('edit'))) && Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 1))
         <select id="Antall" disabled>
             <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
             <option value="5">{{ trans('designs/designer.Label-5') }}</option>
             <option value="10">{{ trans('designs/designer.Label-10') }}</option>
             <option value="20">{{ trans('designs/designer.Label-20') }}</option>
             <option value="50">{{ trans('designs/designer.Label-50') }}</option>
             <option value="75">{{ trans('designs/designer.Label-75') }}</option>
             <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
             <option value="250">{{ trans('designs/designer.Label-250') }}</option>
             <option value="500">{{ trans('designs/designer.Label-500') }}</option>
             <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
         </select>
       @else
        <select id="Antall">
            <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
            <option value="5">{{ trans('designs/designer.Label-5') }}</option>
            <option value="10">{{ trans('designs/designer.Label-10') }}</option>
            <option value="20">{{ trans('designs/designer.Label-20') }}</option>
            <option value="50">{{ trans('designs/designer.Label-50') }}</option>
            <option value="75">{{ trans('designs/designer.Label-75') }}</option>
            <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
            <option value="250">{{ trans('designs/designer.Label-250') }}</option>
            <option value="500">{{ trans('designs/designer.Label-500') }}</option>
            <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
        </select>
       @endif
  </div>
@elseif($typeofsticker == 7) <!-- Black N white ironon -->
{!! Form::hidden('shape','1',array('id'=>'isRect')) !!}
{!! Form::hidden('shape','',array('id'=>'isEllipse')) !!}
{!! Form::hidden('size', 'size', array('id'=>'3010')) !!}
{!! Form::hidden('width','30', array('id' => 'width', 'oninput' => 'setPrice()'))  !!}
{!! Form::hidden('height','10', array('id' => 'height', 'oninput' => 'setPrice()')) !!}
<div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
  <div class="form-group">
  {!! Form::radio('size','','1',array('id'=>'3010')) !!} </br>
  <label for="3010">{{ trans('designs/designer.Label-3010') }}</label>
  </div>
</div>

<div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;"> <!-- QUANTITY -->
  {!! Form::hidden('quantity','100', array('id' => 'quantity', 'class' => 'quantity', 'onchange' => 'setPrice()')) !!}
   <label for="Antall">{{ trans('designs/ironon.Translate15') }}</label> </br>
          @if((Input::has('edit') && Orderitem::find(Input::get('edit'))) && Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 1))
            <select id="Antall" disabled>
                <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
                <option value="5">{{ trans('designs/designer.Label-5') }}</option>
                <option value="10">{{ trans('designs/designer.Label-10') }}</option>
                <option value="20">{{ trans('designs/designer.Label-20') }}</option>
                <option value="50">{{ trans('designs/designer.Label-50') }}</option>
                <option value="75">{{ trans('designs/designer.Label-75') }}</option>
                <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
                <option value="250">{{ trans('designs/designer.Label-250') }}</option>
                <option value="500">{{ trans('designs/designer.Label-500') }}</option>
                <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
            </select>
          @else
           <select id="Antall">
               <option value="1" >{{ trans('designs/designer.Label-1') }}</option>
               <option value="5">{{ trans('designs/designer.Label-5') }}</option>
               <option value="10">{{ trans('designs/designer.Label-10') }}</option>
               <option value="20">{{ trans('designs/designer.Label-20') }}</option>
               <option value="50">{{ trans('designs/designer.Label-50') }}</option>
               <option value="75">{{ trans('designs/designer.Label-75') }}</option>
               <option value="100" selected>{{ trans('designs/designer.Label-100') }}</option>
               <option value="250">{{ trans('designs/designer.Label-250') }}</option>
               <option value="500">{{ trans('designs/designer.Label-500') }}</option>
               <option value="1000">{{ trans('designs/designer.Label-1000') }}</option>
           </select>
          @endif
</div>

@elseif($typeofsticker == 9) <!-- iron free -->
  @if((Input::has('edit') && Orderitem::find(Input::get('edit'))) && Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 1))
    <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-width-mm') }} </br>
          {!! Form::number('width', 30, array('id'=>'width', 'oninput'=>'setPrice()', 'disabled'=>'disabled')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-height-mm') }} </br>
          {!! Form::number('height', 13, array('id'=>'height', 'oninput'=>'setPrice()', 'disabled'=>'disabled')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;"> <!-- QUANTITY -->
          {{ trans('designs/designer.Label-quantity') }} </br>
          {!! Form::number('quantity', 100, array('id'=>'quantity', 'onchange'=>'setPrice()', 'disabled'=>'disabled')) !!}
    </div>
  @else
    <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-width-mm') }} </br>
          {!! Form::number('width', 30, array('id'=>'width', 'oninput'=>'setPrice()')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-height-mm') }} </br>
          {!! Form::number('height', 13, array('id'=>'height', 'oninput'=>'setPrice()')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;"> <!-- QUANTITY -->
          {{ trans('designs/designer.Label-quantity') }} </br>
          {!! Form::number('quantity', 100, array('id'=>'quantity', 'onchange'=>'setPrice()')) !!}
    </div>
  @endif
@else
  @if((Input::has('edit') && Orderitem::find(Input::get('edit'))) && Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 1))
    <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-width-mm') }} </br>
          {!! Form::number('width', 37, array('id'=>'width', 'oninput'=>'setPrice()', 'disabled'=>'disabled')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-height-mm') }} </br>
          {!! Form::number('height', 16, array('id'=>'height', 'oninput'=>'setPrice()', 'disabled'=>'disabled')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;"> <!-- QUANTITY -->
          {{ trans('designs/designer.Label-quantity') }} </br>
          {!! Form::number('quantity', 100, array('id'=>'quantity', 'onchange'=>'setPrice()', 'disabled'=>'disabled')) !!}
    </div>
  @else
    <div class="col-xs-2 top-box left-align input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-width-mm') }} </br>
          {!! Form::number('width', 37, array('id'=>'width', 'oninput'=>'setPrice()')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;">
          {{ trans('designs/designer.Label-height-mm') }} </br>
          {!! Form::number('height', 16, array('id'=>'height', 'oninput'=>'setPrice()')) !!}
    </div>
    <div class="col-xs-2 top-box left-align right-border input-list style-1 same-height clearfix" style="white-space: nowrap; overflow: hidden;"> <!-- QUANTITY -->
          {{ trans('designs/designer.Label-quantity') }} </br>
          {!! Form::number('quantity', 100, array('id'=>'quantity', 'onchange'=>'setPrice()')) !!}
    </div>
  @endif
@endif
  <div class="col-xs-2 top-box same-height" style="white-space: nowrap; overflow: hidden;"> <!-- PRICE -->
    <span class="text-mid-align">
        <label for="thePrice" id="thePrice">{{ trans('designs/designer.Label-price') }}</label> </br>
        {!! Form::open(array('files' => true, 'id' => 'addToCart')) !!}
        {!! Form::hidden('postjson', '', array('id'=>'postjson')) !!}
        {!! Form::hidden('postquantity', '', array('id'=>'postquantity')) !!}
        {!! Form::hidden('postwidth', '', array('id'=>'postwidth')) !!}
        {!! Form::hidden('postheight', '', array('id'=>'postheight')) !!}
        {!! Form::hidden('postprice', '', array('id'=>'postprice')) !!}
        @if (Input::has('edit') && Orderitem::find(Input::get('edit')))
            <?php
                $sticker = Orderitem::find(Input::get('edit'));
            ?>
            @if (Auth::user() && ($sticker->user_id == Auth::user()->id) && ($sticker->status == 0 || $sticker->status == 1) )
              <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="mybtn">
              @else
              <input type="submit" value="{{ trans('designs/designer.Label-order') }}" class="mybtn">
              @endif
        @elseif(Input::has('adminedit') && Orderitem::find(Input::get('adminedit')) && Auth::user() && Auth::user()->admin == 1)
          <input type="submit" value="{{ trans('designs/designer.Label-update-sticker') }}" class="mybtn">
        @else
        <input type="submit" value="{{ trans('designs/designer.Label-order') }}" class="mybtn">
        @endif
        {!! Form::close() !!}
        </span>
  </div>
</div>
