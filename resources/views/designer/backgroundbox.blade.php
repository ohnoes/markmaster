@if($typeofsticker == 7 || $typeofsticker == 6)
<div class="backgroundBox popupBox" id="backgroundBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="backgroundBoxHide()">X</button>
    <h4 class="popupTitle"> BACKGROUND </h4>
  </div>
  <div >
        {!! Form::hidden('clipartType', '0', array('id'=>'clipartType')) !!}

        <div class="col-xs-12 topbox" style="padding: 10px;">
                <label> {{ trans('designs/designer.Label-add') }} </label>
                <div style="width: 60px; height: 45px; background-image: url('/img/add_image2.png'); display:block; cursor: pointer; cursor: hand;" onclick="nbackgroundBoxShow()"/></div>
        </div>
<div class="row no-margin"><!-- Upload controlls -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px;">
        <label> {{ trans('designs/designer.Label-upload-mybackground') }} </label>
        <span class="uploadControls">
            @if (Auth::user())
            {!! Form::open(array('files' => true, 'id' => 'backgroundForm')) !!}
            <!-- {!! Form::label('title', 'Name your clipart') !!}
            {!! Form::text('title') !!} -->
                <div class="form-group">
                    {!! Form::file('image', array('id' => 'backgroundFileSelect')) !!}
                </div>
                <div class="form-group">
                    <input type="submit" value="{{ trans('designs/designer.Label-upload-clipart') }}" class="payBox">
                </div>

            @else
            <label style="font-style: italic; font-weight: lighter;"> {{ trans('designs/designer.Label-login-upload') }} </label>
              {!! Form::hidden('backgroundForm', 'backgroundForm', array('id'=>'backgroundForm')) !!}
              {!! Form::hidden('backgroundFileSelect', 'backgroundFileSelect', array('id'=>'backgroundFileSelect')) !!}
            @endif
            {!! Form::close() !!}
        </span>
    </div>
</div>
  <br style="clear:both;"/>
</div>
</div>
@elseif($typeofsticker == 3 || $typeofsticker == 4 || $typeofsticker == 5)
<div class="backgroundBox popupBox" id="backgroundBox" style="display: none;">
  <div class="popupHeader">
    <button type="button" name="button" class="popupClose" onclick="backgroundBoxHide()">X</button>
    <h4 class="popupTitle"> BACKGROUND </h4>
  </div>
  <div class="popupBody">
    {!! Form::hidden('clipartType', '1', array('id'=>'clipartType')) !!}
    <div class="form-group">
        <label for="color" >{{ trans('designs/designer.Label-background-nonchangeable') }}</label>
    </div>
    {!! Form::hidden('backgroundForm', 'backgroundForm', array('id'=>'backgroundForm')) !!}
    {!! Form::hidden('backgroundFileSelect', 'backgroundFileSelect', array('id'=>'backgroundFileSelect')) !!}
  </div>
  <br style="clear:both;"/>
</div>
@else
  <div class="backgroundBox popupBox" id="backgroundBox" style="display: none;">
    <div class="popupHeader">
      <button type="button" name="button" class="popupClose" onclick="backgroundBoxHide()">X</button>
      <h4 class="popupTitle"> BACKGROUND </h4>
    </div>
    <div >
          {!! Form::hidden('clipartType', '2', array('id'=>'clipartType')) !!}
          <div class="col-xs-12 topbox" style="padding: 10px;">
            <div class="col-xs-6 no-padding">
              <label for="color" style="width: 100%;">{{ trans('designs/designer.Label-color-select') }}</label>
            </div>
            <div class="col-xs-6 no-padding">
              <div id="cp10" data-format="alias" class="input-group colorpicker-component">
                  <input type="text" value="primary" class="form-control" id="backgroundColorPicker" />
                  <span class="input-group-addon selectedcolorbox"><i id="backgrcolor"></i></span>
              </div>
            </div>
          </div>

          <div class="col-xs-12 topbox" style="padding: 10px;">
                  <label> {{ trans('designs/designer.Label-add') }} </label>
                  <div style="width: 60px; height: 45px; background-image: url('/img/add_image2.png'); display:block; cursor: pointer; cursor: hand;" onclick="nbackgroundBoxShow()"/></div>
          </div>
  <div class="row no-margin"><!-- Upload controlls -->
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px;">
          <label> {{ trans('designs/designer.Label-upload-mybackground') }} </label>
          <span class="uploadControls">
              @if (Auth::user())
              {!! Form::open(array('files' => true, 'id' => 'backgroundForm')) !!}
              <!-- {!! Form::label('title', 'Name your clipart') !!}
              {!! Form::text('title') !!} -->
                  <div class="form-group">
                      {!! Form::file('image', array('id' => 'backgroundFileSelect')) !!}
                  </div>
                  <div class="form-group">
                      <input type="submit" value="{{ trans('designs/designer.Label-upload-clipart') }}" class="payBox">
                  </div>

              @else
              <label style="font-style: italic; font-weight: lighter;"> {{ trans('designs/designer.Label-login-upload') }} </label>
                {!! Form::hidden('backgroundForm', 'backgroundForm', array('id'=>'backgroundForm')) !!}
                {!! Form::hidden('backgroundFileSelect', 'backgroundFileSelect', array('id'=>'backgroundFileSelect')) !!}
              @endif
              {!! Form::close() !!}
          </span>
      </div>
  </div>
  	<br style="clear:both;"/>
  </div>
</div>
@endif
@if($typeofsticker == 3)
    <script>
    	$(document).ready(function() {
    		var bgColor = '#cfb53b';
    		setBackgroundColor(bgColor);
    	});
    </script>
@elseif($typeofsticker == 4)
    <script>
        $(document).ready(function() {
            var bgColor = '#d7d8d8';
            setBackgroundColor(bgColor);
        });
    </script>
@endif
{!! Html::script('js/backgroundbox.js') !!}
