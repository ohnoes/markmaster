<?php

// if (Input::has('test') && Input::get('test') == 1)
// {
	// echo 'start';
	// renderThumbnail('hi', 'dere');
// }


function renderThumbnail($jsonDataString, $stickerName)
{
	$sticker = Orderitem::find($stickerName);
	$stickerType = $sticker->stickertype;
	$url = 'http://127.0.0.1:8000/rendercanvas';
	$data = array('stickerData' => $jsonDataString, 'isThumbnail' => '1', 'stickerID' => $stickerName, 'stickerType' => $stickerType, 'useDevSite' => '0');
	
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data),
		),
	);
	
	$context = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	// echo $result;
	// var_dump($result);
}

function renderFullsizeSticker($jsonDataString, $stickerName, $stickerType)
{
	$url = 'http://127.0.0.1:8000/rendercanvas';
	$data = array('stickerData' => $jsonDataString, 'isThumbnail' => '0', 'stickerID' => $stickerName, 'stickerType' => $stickerType, 'useDevSite' => '0');
	
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data),
		),
	);
	
	$context = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
}

?>
