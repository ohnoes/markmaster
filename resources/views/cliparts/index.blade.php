@extends('layouts.main')

@section('content')

	@if(count($errors) > 0)
	<div id="form-errors">
		<p> Følgende feil må rettes:</p>
			<ul>
				@foreach($errors->all() as $error)
					<li> {!! $error !!}</li>
				@endforeach
			</ul>
	</div><!-- end form-errors -->
	@endif

	<div id="admin">

		<h1>Cliparts Admin Panel</h1><hr>

		<p>Here you can view, delete, and upload new clipart.</p>

		<h2> Upload New Clipart</h2><hr>

		@if(count($errors) > 0)
		<div id="form-errors">
			<p> The following errors have occurred:</p>

			<ul>
				@foreach($errors->all() as $error)
					<li> {!! $error !!}</li>
				@endforeach
			</ul>
		</div><!-- end form-errors -->
		@endif



		<form method="POST" action="/{{ app()->getLocale() }}/admin/cliparts/create" accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<p>

            {!! Form::label('title', 'Name your clipart') !!}
            {!! Form::text('title') !!}
            {!! Form::file('image') !!}
        </br>

	        {!! Form::label('category', 'Pick Category') !!}
            {!! Form::select('category', array(
	          '0' => 'Alle Clipart',
	          '1' => 'Mine Clipart',
	          '2' => 'Mest populære',
	          '3' => 'Dyr',
	          '4' => 'Stjernetegn',
	          '5' => 'Figurer',
	          '6' => 'Symboler',
	          '7' => 'Sport',
	          '8' => 'Maskiner',
	          '9' => 'Former',
			  '10'=> 'Vaske Symboler',
			  '11'=> 'Simple Popular',
			  '12'=> 'Simple Dyr',
			  '13'=> 'Simple Monster',
			  '14'=> 'Simple Sport',
			  '15'=> 'Simple Motor',
			  '16'=> 'Simple Diverse'),
	          '0',
	          array('id' => 'category')) !!}

	        {!! Form::label('subcat1', 'Pick Sub-Category One') !!}
            {!! Form::select('subcat1', array(
	          '0' => 'Alle Clipart',
	          '1' => 'Mine Clipart',
	          '2' => 'Mest populære',
	          '3' => 'Dyr',
	          '4' => 'Stjernetegn',
	          '5' => 'Figurer',
	          '6' => 'Symboler',
	          '7' => 'Sport',
	          '8' => 'Maskiner',
	          '9' => 'Former',
			  '10'=> 'Vaske Symboler',
			 '11'=> 'Simple Popular',
			 '12'=> 'Simple Dyr',
			 '13'=> 'Simple Monster',
			 '14'=> 'Simple Sport',
			 '15'=> 'Simple Motor',
			 '16'=> 'Simple Diverse'),
	          '0',
	          array('id' => 'subcat1')) !!}

	        {!! Form::label('subcat2', 'Pick Sub-Category Two') !!}
            {!! Form::select('subcat2', array(
	          '0' => 'Alle Clipart',
	          '1' => 'Mine Clipart',
	          '2' => 'Mest populære',
	          '3' => 'Dyr',
	          '4' => 'Stjernetegn',
	          '5' => 'Figurer',
	          '6' => 'Symboler',
	          '7' => 'Sport',
	          '8' => 'Maskiner',
	          '9' => 'Former',
			  '10'=> 'Vaske Symboler',
			 '11'=> 'Simple Popular',
			 '12'=> 'Simple Dyr',
			 '13'=> 'Simple Monster',
			 '14'=> 'Simple Sport',
			 '15'=> 'Simple Motor',
			 '16'=> 'Simple Diverse'),
	          '0',
	          array('id' => 'subcat2')) !!}

			<p>
	        	{!! Form::label('sortorder', 'Sort order for the clipart') !!}
				{!! Form::text('sortorder') !!}
	        </p>

	        <p>
	        	{!! Form::textarea('searchwords', 'Add searchable words') !!}
	        </p>
	        <p>
	        	{!! Form::label('colors', 'Farge kategori')!!}
				{!! Form::select('colors', array(
					'0' => 'punktgrafik  og sorte svg',
					'1' => 'gråskale punktgrafik og gråskale svg',
					'2' => 'fullfarge punktgrafik og fullfarge svg'),
					'0',
					array('id' => 'colors')) !!}
	    	</p>
	    	<p>
		        {!! Form::checkbox('strechable') !!}
		        {!! Form::label('strechable', 'Should it be possible to strech the clipart?') !!}
		    </p>



		</p>
		{!! Form::submit('Upload Clipart', array('class'=>'secondary-cart-btn')) !!}
		{!! Form::close() !!}

		</br>

		<h2>Cliparts</h2><hr>

		<ul id="clipartList">
			@foreach($cliparts as $clipart)
				<li id="clipartList">
					{!! Html::image($clipart->image, $clipart->title, array('class'=>'clipartImg', 'width'=>'50px', 'height'=>'50px')) !!}
					{!! $clipart->title !!} -
						{!! 'Public: ' !!}
							@if($clipart->public)
								{!! 'yes' !!} -
							@else
								{!! 'no' !!} -
							@endif
						{!! 'Colortype: ' !!}
							@if($clipart->can_color == 0)
								{!! 'sort/hvit' !!} -
							@elseif($clipart->can_color == 1)
								{!! 'grayscale' !!} -
							@elseif($clipart->can_color == 2)
								{!! 'farge' !!} -
							@endif
						{!! 'Sokeord: ' !!} {!!$clipart->searchwords!!} -

					<form action="/{{ app()->getLocale() }}/admin/cliparts/edit" class ="form-inline" method="POST" accept-charset="UTF-8">
			    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						{!! Form::hidden('id', $clipart->id) !!}
					{!! Form::submit('Edit') !!} -

					<form action="/{{ app()->getLocale() }}/admin/cliparts/destroy" class ="form-inline" method="POST" accept-charset="UTF-8">
			    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						{!! Form::hidden('id', $clipart->id) !!}
					{!! Form::submit('Delete') !!}


					{!! Form::close() !!}
				</li>
			@endforeach
		</ul>

		<div id="pagination" align="center">
			{!! $cliparts->render() !!}
		</div>

	</div><!-- end admin -->

@stop
