@extends('layouts.main')

@section('title')
    {{ trans('help/conditions.Translate1') }}
@stop

@section('head-meta')
	<meta name="description" content="{{ trans('help/conditions.Translate2') }}">
@stop

@section('head-extra')
	<link rel="canonical" href="https://www.markmaster.com/{{ app()->getLocale() }}/help/conditions">
@stop

@section('content')

	<div class="container">

		<h3>{{ trans('help/conditions.Translate3') }}</h3>
		{{ trans('help/conditions.Translate4') }}</p>

		<p>
		<h3>{{ trans('help/conditions.Translate5') }}</h3>
		{{ trans('help/conditions.Translate6') }}.</p>

		<p>
		<h3>{{ trans('help/conditions.Translate7') }}</h3>
		{{ trans('help/conditions.Translate8') }}</p>

		<p>
		<h3>{{ trans('help/conditions.Translate9') }}</h3>
		{{ trans('help/conditions.Translate10') }}</p>

		<p>
		<h3>{{ trans('help/conditions.Translate11') }}</h3>
		{{ trans('help/conditions.Translate12') }}
		</p>

	</div>

@stop
