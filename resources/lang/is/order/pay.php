<?php

return [
	'Translate1' => 'Greiðsla',
	'Translate2' => 'Sendingar og greiðslu heimilisfang fyrir pöntunina þína',
	'Translate3' => 'Eftirfarandi villur þarf að laga:',
	'Translate4' => 'Sendingar heimilifang',
	'Translate5' => 'Org.',
	'Translate6' => 'Fornafn',
	'Translate7' => 'Eftirnafn',
	'Translate8' => 'Heimilisfang',
	'Translate9' => 'Póstnúmer',
	'Translate10' => 'Borg',
	'Translate11' => 'Sýsla',
	'Translate12' => 'Land: ',
	'Translate13' => 'Greiðslu heimilisfang',
	'Translate14' => 'Greiðsluheimilifang er það saman og sendingar heimilsfang',
	'Translate15' => 'ATH með því að ýta á borga, ég staðfesti að hafa samþykkt greiðsluskilmálana.',
	'Translate16' => 'Borga',
	'Translate17' => 'GREIÐSLUSKILMÁLAR',
	'Translate18' => 'Þessir skilmálar snúa að greiðslum viðskiptavina fyrir þjónustu og vörur sem Markmaster AS útvegar',
	'Translate19' => 'VERÐ OG SENDING',
	'Translate20' => 'Sjálfvirk pöntunarstaðfesting verður send um leið og Markmaster AS hefur borist greiðsla. Það er á ábyrð kaupenda að allar upplýsingar (vörutegund, innihald, magn, og verð) séu réttar.  Öll verð eru með VSK og sendingarkostnaði.  Sendingartími fer eftir áfangastað, en gera má ráð fyrir að flestar pantanir berist til kaupenda innan tveggja vikna.',
	'Translate21' => 'SKILARÉTTUR OG BÆTUR',
	'Translate22' => 'Það er skylda Markmaster AS að bæta fyrir glataðar eða gallarðar vörur ef að viðskiptavinurinn láti vita innan skynsamlegs tímaramma. Vörur Markmaster AB eru sérhannaðar fyrir hvern og einn viðskiptavin þannig að það er ekki hægt að krefast endurgreiðslu eftir að varan hefur verið framleidd. Það er á ábyrgð viðskiptavinarins að gang þannig frá pöntun sínum að þær innihaldi ekki stafsetningar eða uppsetningarvillur.  Markmaster AS yfirfer ekki hverja pöntun fyrir sig þar sem pöntunin fer beint frá vef yfir í prentun.',
	'Translate23' => 'HÖFUNDARRÉTTUR',
	'Translate24' => 'Ef viðskiptavinur hleður upp sinni eigin smámynd eða mynd, þá fullvissar viðskiptavinurinn Marmaster AS um að enginn mynd eða texti sé háður höfundarrétti þriðja aðila. Ábyrgðin liggur hjá viðskiptavininum að svo sé. Viðskiptavinurinn leysir Markmaster AS undan allri ábyrgð og kröfum sem varða höfundarvarið efni þriðja aðila. Viðskiptavinurinn er ábyrgur fyrir sektum og skaða sem skapast vegna þessa.',
	'Translate25' => 'FYRIRVARI UM BREYTINGAR',
	'Translate26' => 'Þessir skilmálar geta breyst án viðvörunar.',

	'Translate27' => 'Greiða',

];
