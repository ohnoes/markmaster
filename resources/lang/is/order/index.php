<?php

return [
	'Translate1' => 'Pantanir þínar hjá Markmaster',
	'Translate2' => 'Pantanir þínar',
	'Translate3' => 'Vinsamlega lagaðu þessar villur:',
	'Translate4' => 'Pantanir þínar',
	'Translate5' => 'Pantananúmer',
	'Translate6' => 'Dagsetning pöntunar',
	'Translate7' => 'Sendingarstaður',
	'Translate8' => 'Staða',
	'Translate9' => 'Verð',
	'Translate10' => 'Tilbúið að greiða',
	'Translate11' => 'Greitt',
	'Translate12' => 'Flutt út',
	'Translate13' => 'Hætt við',
	'Translate14' => 'Bíða',
	'Translate15' => 'Upplýsingar ekki tiltæknar',
	'Translate16' => 'Upplýsingar',
	'Translate17' => 'Borga',
	'Translate18' => 'Reikningur',
	'Translate19' => 'Eyða',
	'Translate20' => 'Ertu viss um að þú viljir eyða pöntuninni þinni?',


];
