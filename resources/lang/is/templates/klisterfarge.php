<?php

return [
	'Translate1' => 'Litaðir miðar',
	'Translate2' => 'Gerðu þinn eigin litaðan miða. Byrjaðu á nýrri hönnun eða notaðu einn af hönnunargrunnum Markmaster',
	'Translate3' => 'Plastaðir miðar',
	'Translate4' => 'Miðarnir eru búnir til með mjög sterku lími. Þeir eru líka plastaðir sem gerir þá mjög slitþolna.',
	'Translate5' => 'Miðarnir þola þvott með allt að 60°heitu vatni. Passar vel fyrir verkfæri, vatnsflöskur, nestisbox, myndavélar, tölvur og íþróttavörur. Það er einnig hægt að merkja skó (miðinn fer ofan í skóinn og á innleggið). ',
	'Translate6' => 'Með valkvæðri stærð, magni og lögun.',
	'Translate7' => 'Framleitt á pappírsblað.',
	'Translate8' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate9' => '',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjaðu nýja hönnun',
	'Translate12' => 'Nei',
	'Translate13' => 'Nafnspjald',
	'Translate14' => 'Nafnspjald',
	'Translate15' => 'Byrjaðu nýja hönnun',
	'Translate16' => 'Ný hönnun',
	'Translate17' => 'Byrjaðu nýja hönnun',




];
