<?php

return [
	'Translate1' => 'Svart/hvítir miðar',
	'Translate2' => 'Gerðu þinn eigin miða. Byrjaðu á nýrri hönnun eða notaðu einn af hönnunargrunnum Markmaster',

	'Translate3' => 'Svart/hvítir miðar',
	'Translate4' => 'Hvítir miðar með svörtu letri. Ódýr og góð leið úr náttúrulegum efnum. ',
	'Translate5' => 'Passar vel í að merkja flöskur, bækur, skóladót og snyrtivörur. Þolir60°C þvott',
	'Translate6' => '2 sizes  37x16 mm and 60x26 mm',
	'Translate7' => 'Framleitt og afhent í rúllu.',
	'Translate8' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate9' => '',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjaðu nýja hönnun',
	'Translate12' => 'Nei',
];
