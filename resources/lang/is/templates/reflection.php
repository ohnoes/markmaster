<?php

return [
	'Translate1' => 'Endurskins miði',
	'Translate2' => 'Gerðu þinn eigin litaðan miða. Byrjaðu á nýrri hönnun eða notaðu einn af hönnunargrunnum Markmaster',
	'Translate3' => 'Endurskins miði',
	'Translate4' => 'Þú getur búiði til þína eigin endurskinsmiða með valkvæðum litum.  Þú velur stærðina sjálf/ur. ',
	'Translate5' => 'Sem dæmi, merktu hjólið þitt með persónulegummiða sem sést í myrkri.',
	'Translate6' => 'Með valkvæðri stærð, magni og lögun.',
	'Translate7' => 'Framleitt á pappírsblað.',
	'Translate8' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate9' => '',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjaðu nýja hönnun',
	'Translate12' => 'Nei',



];
