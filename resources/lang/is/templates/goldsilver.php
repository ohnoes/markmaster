<?php

return [
	'Translate1' => 'Gull eða silfur miðar',
	'Translate2' => 'Gerðu þinn eigin gull eða silfur miða. Byrjaðu á nýrri hönnun eða notaðu einn af hönnunargrunnum Markmaster',

	'Translate3' => 'Gull eða silfur miði',
	'Translate4' => 'Miðar með gullnum eða silfruðum bakgrunni og svörtu letri.',

	'Translate5' => 'Notið hönnunar tækin okkar til að hlaða upp myndum af þinni eigin hönnun .',
	'Translate6' => 'Hentar vel til að merkja verðlaunagripi o.s.fr.',
	'Translate7' => 'Með valkvæðri stærð, magni og lögun.',
	'Translate8' => 'Framleitt á pappírslblað.',
	'Translate9' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjið nýja hönnun',
	'Translate12' => 'Nei',
	'Translate13' => 'Gull miði',
	'Translate14' => 'Miðar með gullnum bakgrunni. Pasar vel fyrir verðlaunagripi o.s.fr.',
	'Translate15' => 'Silfur miði',
	'Translate16' => 'Miðar með gullnum bakgrunni. Pasar vel fyrir verðlaunagripi o.s.fr.',


];
