<?php

return [
	'Translate1' => 'Stjaujárns miði í svörtu og hvítu',
	'Translate2' => 'Gerðu þinn eigin gull eða silfur miða. Byrjaðu á nýrri hönnun eða notaðu einn af hönnunargrunnum Markmaster',
	'Translate3' => 'Hvítir straujárnsmiðar með svörtu letri',
	'Translate4' => 'Vinsælasta vara okkar! Merking á allskonar efnum og hlutum. Föt, vettlingar o.s.fr. Auðvelt að festa með gufustraujárni. Þolir allt að 95°C. þvott',
	'Translate5' => 'Stærðin er 30 x 10 mm',
	'Translate6' => 'Framleitt og afhent í rúllu.',
	'Translate7' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate8' => '',
	'Translate9' => '',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjaðu nýja hönnun',
	'Translate12' => 'Nei',



];
