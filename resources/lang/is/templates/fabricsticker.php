<?php

return [
	'Translate1' => 'Efnismiði',
	'Translate2' => 'Hannaðu þinn eigin efnismiða. Byrjaðu nýja hönnun á Markmaster hönnunar grunni',
	'Translate3' => 'Efnis miði',
	'Translate4' => 'eru nafna miðar til sem eru festir án straujárns, á öllum fatnaði með þvottaupplýsingamiða',
	'Translate5' => 'Getur lika verið notað á aðra hluti sem lím á flest efni. ',
	//
	'Translate6' => 'Með valkvæðri stærð, magni og lögun.',
	'Translate7' => 'Framleitt á pappírsmiða.',
	'Translate8' => 'Veldu "Ný hönnun" til að byrja hönnun frá byrjun eða veldu einn af hönnunargrunnunum okkar til að byrja.',
	'Translate9' => '',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrja nýja hönnun',
	'Translate12' => 'Nei',
	//
	'Translate13' => 'Ef miðarnir eru festir á réttan hátt, geta þeir þolað 40° þvott.',
	'Translate14' => 'Festið miða á þvottaupplýsingamiðann. Það er mikilvægt að þrýsta vel á jaðar og kanta. Bíðið í 24 tíma uns varan er þvegin.',
	'Translate15' => 'Fyrir föt sem eru þvegin á hærri hita en 40° eða eru ekki með upplýsingaþvottamiða, við mælum með straujárnsmiðunum okkar.',




];
