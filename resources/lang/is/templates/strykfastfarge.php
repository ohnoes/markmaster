<?php

return [
	'Translate1' => 'Litaðir straujárnsmiðuar',
	'Translate2' => 'Hannaðu þinn egin litaðan straujárnsmiða.  Hannaðu þinn eigin litaðan miða á einum af hönnunargrunnum Markmaster',
	'Translate3' => 'Litaður straujárnsmiði',
	'Translate4' => 'Miðarnir eru farmleiddir í efni sem festist við föt með venjulegu straujárni. ',
	'Translate5' => 'Ef miðarnir eru festir á með réttum hætti þá geta þeir þolað þvott allt að 60°C. Notið hönnunar tækin okkar til að hlaða upp myndum af þinni eigin hönnun.',
	'Translate6' => 'Fullkomið til að merkja föt.',
	'Translate7' => 'Með valkvæðri stærð, magni og lögun.',
	'Translate8' => 'Framleitt á pappír.',
	'Translate9' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjaðu nýja hönnun',
	'Translate12' => 'Nei',



];
