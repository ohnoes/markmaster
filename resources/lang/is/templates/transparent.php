<?php

return [
	'Translate1' => 'Glær miði',
	'Translate2' => 'Hannaðu þinn eigin glæran miða.  Hannaðu þinn eigin litaðan miða á einum af hönnunargrunnum Markmaster',
	'Translate3' => 'Glær miði',
	'Translate4' => 'Miðar með glærum bakgrunni og svörtu letri.',
	'Translate5' => 'Notið hönnunar tækin okkar til að hlaða upp myndum af þinni eigin hönnun .',
	'Translate6' => 'Hentar vel til að merkja verðlanagripi o.s.fr.',
	'Translate7' => 'Með valkvæðri stærð, magni og lögun.',

	'Translate8' => 'Framleitt á pappír.',
	'Translate9' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjaðu nýja hönnun',
	'Translate12' => 'Nei',



];
