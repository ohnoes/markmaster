<?php

return [
	'Translate1' => 'Gull miðar',
	'Translate2' => 'Gerðu þinn eigin gullmiða. Byrjaðu á nýrri hönnun eða notaðu einn af hönnunargrunnum Markmaster',
	'Translate3' => 'Gull miði',
	'Translate4' => 'Miðar með gullnum bakgrunni og svörtu letri.',
	'Translate5' => 'Notið hönnunar tækin okkar til að hlaða upp myndum af þinni eigin hönnun .',
	'Translate6' => 'Hentar vel til að merkja verðlaunagripi o.s.fr.',
	'Translate7' => 'Með valkvæðri stærð, magni og lögun.',
	'Translate8' => 'Framleitt á pappírslblað.',
	'Translate9' => 'Veljið "Ný hönnun" til að hefja nýja hönnun frá byrjun eða notið einn af hönnunargrunnunum okkar sem byrjunarreit.',
	'Translate10' => 'Ný hönnun',
	'Translate11' => 'Byrjið nýja hönnun',
	'Translate12' => 'Nei',



];
