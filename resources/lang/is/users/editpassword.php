<?php

return [
	'Translate1' => 'Uppfæra lykilorð',
	'Translate2' => 'Uppfæra lykilorð hjá Markmaster',
	'Translate3' => 'Uppfæra lykilorð',
	'Translate4' => 'Eftirfarandi villur þaf að lagfæra: ',
	'Translate5' => 'Núverandi lykilorð:',
	'Translate6' => 'Gamla lykilorð',
	'Translate7' => 'Lykilorð:',
	'Translate8' => 'Lykilorð',
	'Translate9' => 'Staðfestið lykilorð:',
	'Translate10' => 'Staðfestið',
	'Translate11' => 'Uppfæra lykilorð',

];
