<?php

return [
	'Translate1' => 'Uppfæra áfangastað sendingar',
	'Translate2' => 'Uppfæra áfangastað sendingar hjá Markmaster',
	'Translate3' => 'Uppfæra áfangastað sendingar',
	'Translate4' => 'Eftirfarandi villur þarf að leiðrétta:',
	'Translate5' => 'Stofnun:',
	'Translate6' => 'Fornafn:',
	'Translate7' => 'Eftirnafn:',
	'Translate8' => 'Heimilisfang:',
	'Translate9' => 'Póstnúmer:',
	'Translate10' => 'Borg:',
	'Translate11' => 'Land:',
	'Translate12' => 'Uppfæra upplýsingar',
	'Translate13' => 'Já,ég vil fá tölvupóst frá Markmaster.no um afslætti og tilboð',
	'Translate14' => 'Email:',
	'Translate15' => 'Phone:',


];
