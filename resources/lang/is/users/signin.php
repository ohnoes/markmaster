<?php

return [
	'Translate1' => 'Innskráning hjá Markmaster',
	'Translate2' => 'Innskráning Markmaster web2print store',
	'Translate3' => 'Núverandi viðskiptavinur',
	'Translate4' => ' ',
	'Translate5' => 'Tölvupóstur',
	'Translate6' => 'Netfangið þitt',
	'Translate7' => 'Lykilorð',
	'Translate8' => 'Sláið inn lykilorð',
	'Translate9' => 'Innskráning',
	'Translate10' => 'Gleymt lykilorð',
	'Translate11' => 'Nýr notandi',
	'Translate12' => 'Það er auðvelt að skapa nýjan reikning',
	'Translate13' => 'Skapa nýjan notenda',

];
