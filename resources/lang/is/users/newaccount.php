<?php

return [
	'Translate1' => 'Stofna nýjan notanda hjá Markmaster',
	'Translate2' => 'Stofna nýjan notanda hjá Markmasters web2print store',
	'Translate3' => 'Nýr notandi',
	'Translate4' => 'Eftirfarandi villur þarf að laga: ',
	'Translate5' => 'Stofnun:',
	'Translate6' => 'Fornafn:',
	'Translate7' => 'Eftirnafn:',
	'Translate8' => 'Heimilisfang:',
	'Translate9' => 'Póstnúmer:',
	'Translate10' => 'Borg:',
	'Translate11' => 'Land:',
	'Translate12' => 'Tölvupóstur',
	'Translate13' => 'Lykilorð',
	'Translate14' => 'Staðfestið lykilorð',
	'Translate15' => 'Staðfestið',
	'Translate16' => 'Sími',
	'Translate17' => 'Já,ég vil fá tölvupóst frá Markmaster.no um afslætti og tilboð',

	'Translate18' => 'Skráður notandi',

];
