<?php

return [
	'Translate1' => 'Uppfæra reikningsupplýsingar',
	'Translate2' => 'Uppfæra reikningsupplýsingar þínar hjá Markmaster',
	'Translate3' => 'Vinsamlega lagið þessar villur:',
	'Translate4' => 'Uppfæra reikningsupplýsingar',
	'Translate5' => 'Uppfæra reikningsupplýsingar þínar hjá Markmaster'
	'Translate6' => 'Vinsamlega veljið upplýsingarnar sem þú villt uppfæra',
	'Translate7' => 'Notenda upplýsingar ',
	'Translate8' => 'Greiðenda upplýsingar',
	'Translate9' => 'Lykilorð',

];
