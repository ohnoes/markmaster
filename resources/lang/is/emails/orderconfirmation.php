<?php

return [
	'Translate1' => 'Heimilisfang sendingar',
	'Translate2' => 'Staðfesting pöntunar',
	'Translate3' => 'Dagsetning',
	'Translate4' => 'Viðskiptanúmer.',
	'Translate5' => 'Pöntunarnúmer.',
	'Translate6' => 'Heimilisfang greiðanda',
	'Translate7' => 'Merki',
	'Translate8' => 'Tegund',
	'Translate9' => 'Stærð í mm',
	'Translate10' => 'Magn',
	'Translate11' => 'Verð með VSK',
	'Translate12' => 'Litaðir miðar, plastaðir',
	'Translate13' => 'Litaðir stjaujárnsmiðar',
	'Translate14' => 'Gulllitaðir miðar',
	'Translate15' => 'Silfurlitaðir miðar',
	'Translate16' => 'Glærir miðar',
	'Translate17' => 'Svartir og hvítir miðar',
	'Translate18' => 'Staujárnsmiðar',
	'Translate22' => 'Endurskins miðar',
	'Translate23' => 'Efnis miðar',
	'Translate19' => 'Verð með VSK',
	'Translate20' => 'Sendingarkostnaður',
	'Translate21' => 'Samtals.',

];
