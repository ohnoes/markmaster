<?php

return [
	'Translate1' => 'Markmasters hjálp',
	'Translate2' => 'Hér er listi afhlutum sem við getum hjálpað þér með',
	'Translate3' => 'Hjálp',
	'Translate4' => 'Spurningar og svör',
	'Translate5' => 'Myndbönd',
	'Translate6' => 'Hönnunarhjálp',
	'Translate7' => 'Kaupskilmálar',
	'Translate8' => 'Hafið samband',

];
