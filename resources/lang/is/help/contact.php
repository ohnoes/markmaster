<?php

return [
	'Translate1' => 'Samband við Markmaster',
	'Translate2' => 'Hafðu samband við Markmaster ef þú hefur spurningar eða villt skilja eftir athugasemdir',
	'Translate3' => 'Hafa samband',
	'Translate4' => 'Hefur þú einhverjar spurningar eða viltu koma einhverju áleiðis?  Hérna eru möguleikarnir til að hafa samband við okkur:',
	'Translate5' => 'Tölvupóstur: ',
	'Translate6' => 'Sími:  0047 77 69 69 61',

];
