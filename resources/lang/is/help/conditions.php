<?php

return [
	'Translate1' => 'Markmasters Skilmálar',
	'Translate2' => 'Hérna eru skilmálar Markmaster',
	'Translate3' => 'SKILMÁLAR',
	'Translate4' => 'Þessir skilmálar snúa að viðskiptum og greiðlsu viðskiptavina Markmeaster sem nota og kaupa þjónustu Markmaster AS',
	'Translate5' => 'VERÐ OG SENDING',
	'Translate6' => 'Sjálfvirk staðfseting mun verða send í tölvupósti um leið og Markmaster hefur tekið við greiðslu, það er á ábyrgð viðskitavina okkar að allar upplýsingar séu (vörutegund, innihald, magn og verð)réttar
			Öll verð eru með VSK.  Útreiknaður sendingartími fyrir allar sendingar fara eftir áfangastað en flestar pantanir ættu að berast innan 2 vikna.',
	'Translate7' => 'SKILARÉTTUR OG BÆTUR',
	'Translate8' => 'Það er skylda Markmaster að bæta pantanir sem eru ófullnægjandi ef að viðskiptavinurinn hefur samband við Markmaster innan skynsamlegs tímaramma.
			Vörur Markmaster AS eru sérsniðnar að kröfum viðskiptavina okkar þannig að endurkröfuréttur eftir að varan hefur verið framleidd.
			Það er á ábyrgð viðskiptavina okkar að pantanir innihaldi ekki stafsetingarvillur.
			Markmaster AS yfirfer ekki hverja pöntun fyrir sig áður en hún er framleidd.  Allar pantani fara beint frá vef í prentun.',
	'Translate9' => 'HÖFUNDARRÉTTUR',
	'Translate10' => 'Ef viðskiptavinur hleður upp sinni eigin mynd eða smámynd, þá verður viðskiptavinurinn að sjá til þess að enginn þeirra mynda sé varður af höfundarrétti þriðja aðila.
			Öll brot á þessu verða á ábyrgð viðskiptavinarins.  Viðskiptavinurinn leysir Markmaster undan öllum kröfum sem þriðji aðili kann að setja fram.
			Viðskiptavinurinn verður ábyrgur fyrir öllum kostnaði sem hlýst af brotum á ofanrituðu.',
	'Translate11' => 'FYRIRVARI UM BREYTINGAR',
	'Translate12' => 'Þessir skilmálar geta breyst án viðvörunar.',

];
