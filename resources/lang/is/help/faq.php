<?php

return [
	'Translate1' => 'Algengar spurningar Markmaster',
	'Translate2' => 'Hér er listi yfir algengar spurninar go svör við þeim',
	'Translate3' => 'Hér er listi yfir algengar spurninar go svör við þeim',
	'Translate4' => 'Má setja föt með straujuðum miða í þurrkara?',
	'Translate5' => 'Já.  Ef miðinn er festur á réttan hátt, hann ætti að þola mikla notkun í þurrkara. Ef miðinn er að detta af, er það vegna þess að of lágur hiti var notaður til að festa miðann á í upphafi. Miðinn á að bráðna lítillega inn í efnið.',
	'Translate6' => 'Get ég notað straujárnsmiða á ull?',
	'Translate7' => 'Já. Þú geturnotað straujárnsmiða á ullarvörur. Ef þú festir straujárnsmiða á sokka eða vettlinga, þá er best að snúa miðanum langsum til að viðhalda teygjanleika.',
	'Translate8' => 'Af hverju fölnar textinn lítilsháttar eftir að miðinn hefur verið festur?',
	'Translate9' => 'Þetta gerist vegna þess að straujunin er ekki rétt eða of hár hiti er notaður. Prufaðu að hafa straujárnið alveg kjurrt og lækkið hitann örlítið.',
	'Translate10' => 'Getur þú fest miða með lægri hita en þriggja punkta þar sem sum föt þola ekki þriggja punkta straujun?',
	'Translate11' => 'Já, miðinn mun festast, en hann gæti dottið af ef þvegið er við háan hita. En á móti kemur að föt sem þola ekki þriggja punkta straujun, ættu ekki að vera þvegin við háan hita. Þetta ætti því ekki að skipta máli.',
	'Translate12' => 'Hvað tekur mikinn tíma að fá sendinguna?',
	'Translate13' => 'Venjulega eru miðarnir framleiddir daginn eftir pöntun. Þá er þeim pakkað og sendinginn fer af stað. Fjarlægð áætlunarstaðar skiptir auðvitað máli varðandi tímann sem það tekur að senda vöruna, en flestar vörur munu skila sér innan tveggja vikna',
	'Translate14' => 'Get ég breytt pöntuninni minni?',
	'Translate15' => 'Magn og stærð er ekki hægt að breyta eftir að greiðsla er ynnt af hendi. En það er hægt að breyta hönnuninni á miðanum uns framleiðslan hefst. ',
	'Translate16' => 'Hvaða greiðsluleiðir eru í boði?',
	'Translate17' => 'Netgreiðslur með kreditkorti. Við tökum á móti greiðslum MasterCard, Maestro, Visa, Diners Club og American Express.',
	'Translate18' => 'Sendið þið vörur utan Noregs?',
	'Translate19' => 'Já, við sendum út um allan heim.',
	'Translate20' => 'Ég er ekki ánægð/ur með miðana mína. Get ég skilað þeim?',
	'Translate21' => 'Ef miðarnir eru gallair eða skemmtir þegar þeir koma frá Markmaster þá eru þeir endurgreiddir að fullu. Ef það eru stafsetningarvillur á miðunum eða uppsetningu ábótavant, þá miðum við endurgreiðslu við í samræmi við lög um persónulega framleiddar vörur, venjulega helmingur af upphaflegu verði',


];
