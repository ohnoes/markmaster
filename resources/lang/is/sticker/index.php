<?php

return [
	'Translate1' => 'Þínir miðar',
	'Translate2' => 'Miðar sem þú hefur hannað. Klikkaðu á miðann til að kaupa fleiri.',
	'Translate3' => 'Þínir miðar',
	'Translate4' => 'Deilingar URL',
	'Translate5' => 'Miði',
	'Translate6' => 'Gerð',
	'Translate7' => 'Litaðir plast miðar',
	'Translate8' => 'Straujárnsmiðar litaðir',
	'Translate9' => 'Gulllitaðir miðar',
	'Translate10' => 'Silfurlitaðir miðar',
	'Translate11' => 'Glærir miðar',
	'Translate12' => 'Miðar í svart/hvítu',
	'Translate13' => 'Straujárnsmiðar',
	'Translate14' => 'Endurskins miðar',
	'Translate15' => 'Stærð',
	'Translate16' => 'Efnismiðar'

];
