<?php

return [

	/////////////////////////////////////////////////////
	//  						Header 						//
	/////////////////////////////////////////////////////
	'Homepage' => 'Heimasíða',
	'Products' => 'Vörur',
	'ShoppingCart' => 'Karfa',
	'Login' => 'Login',
	'User' => 'Notandi',

	/*---- Header dropdown items for Products ----*/
	'Sticker' => 'Litaður miði',
	'StickerBW' => 'Svart/hvítur miði',
	'Ironon' => 'Straujaður miði litaður',
	'IrononBW' => 'Straujaður miði svart/hvítur',
	'Gold' => 'Gulllitaður miði',
	'Silver' => 'Silfurlitaður miði',
	'Transparent' => 'Glær miði',
	'Reflection' => 'Endurskins miði',
	'Fabricsticker' => 'Efnis miði',

	/*---- Header dropdown items for Login ----*/
	'UserLogin' => 'Login',
	'NewAccount' => 'Nýr notandi',

	/*---- Header dropdown items for User ----*/
	'UserStickers' => 'Miðarnir mínir',
	'UserOrders' => 'Mínar pantanir',
	'UserAccount' => 'Minn reikningur',
	'Logout' => 'Skrá út',

	/////////////////////////////////////////////////////
	//  						Footer	 						//
	/////////////////////////////////////////////////////
	'Social' => 'Samfélagsmiðlar',
	'Help' => 'Hjálp',
	'Payment' => 'Geiðslulausnir',

	/*---- Footer Social list items ----*/
	'Twitter' => 'Twitter',
	'Facebook' => 'Facebook',

	/*---- Footer Help list items ----*/
	'FAQ' => 'Spurningar og svör',
	'Video' => 'Myndbönd',
	'CommercialAgents' => 'Hönnunarhjálp',
	'Contact' => 'Hafið samband',
	'Terms' => 'Skilmálar',

	'loggedinas' => 'Þú ert skráð/ur inn sem: ',
	'loginmsg' => 'Þakka þér fyrir að skrá þig inn',

	'orgnumber' => 'org. number',
	'address' => 'Legevegen 16',
	'address2' => '5542 Karmsund',
	'phone' => '+47 77 69 69 61',
	'country' => 'Norway',

];
