<?php

return [

	/////////////////////////////////////////////////////
	//  						Skilaboð 					//
	/////////////////////////////////////////////////////
	'Errors' => 'Vinsamlega lagið þessar villur: ',

	/////////////////////////////////////////////////////
	//  						Fyrirsögn   					//
	/////////////////////////////////////////////////////
	'ShoppingCart' => 'Innkaupakerra',

	/////////////////////////////////////////////////////
	//  					Table headings   			    //
	/////////////////////////////////////////////////////
	'URL' => 'URL hlekkur á miðahönnun*' ,
	'Stickertype' => 'Miði',
	'ProductType' => 'Gerð',
	'Size' => 'Stærð í mm',
	'Quantity' => 'Magn',
	'Price' => 'Verð með VSK',


	/////////////////////////////////////////////////////
	//  					Miða tegund    			    //
	/////////////////////////////////////////////////////
	'Sticker' => 'Litaður plastmiðli ',
	'StickerBW' => 'Svart hvítur miði',
	'Ironon' => 'Straujárnsmiði litaður',
	'IrononBW' => 'Straujárnsmiði',
	'Gold' => 'Gulllitaður miði',
	'Silver' => 'Silfurlitaður miði',
	'Transparent' => 'Glær miði',
	'Reflection' => 'Endurskinsmiði',
	'Fabricsticker' => 'Efnismiði',
	'CouponCode' => 'Coupon code',

	/////////////////////////////////////////////////////
	//  					Takkar			   			    //
	/////////////////////////////////////////////////////
	'EditButton' => 'Breyta',
	'DeleteButton' => 'Eyða',
	'StoreButton' => 'Aftur í verslun',
	'CheckoutButton' => 'Skrá út',

	/////////////////////////////////////////////////////
	//  					Extra Lang			   		    //
	/////////////////////////////////////////////////////
	'Shipping' => 'Sendigarkostnaður',
	'SigninMemo' => '*Þú verður að skrá þig inn til að deilahönnun þinni',
	'DeleteWarning' => 'Ertu viss um að þú viljir eyða miðanum?',

	'Translate1' => 'Innkaupakarfan þin',
	'Translate2' => 'Innkaupakarfan þín í Markmasters web2print store',
	'couponButton' => 'Bættu við afsláttarmiða numeri',
	'coupon' => 'Afsláttarmiða númer',
];
