<?php

return [
	'Translate1' => 'Staða pöntunar',
	'Translate2' => 'Staða þinnar pöntunar',
	'Translate3' => 'Eitthvað fór úrskeiðis. Vinsamlega prufið aftur með öðru korti',
	'Translate4' => 'Eitthvað fór úrskeiðis. Vinsamlega prufið aftur með öðru korti',
	'Translate5' => 'Greiðsla samþykkt. Pöntunin þín verður framleidd og send innan 1-2 virkra daga.',
	'Translate6' => 'Pöntunarnúmer',
	'Translate7' => 'Verð',
	'Translate8' => 'Staða pöntunar',
	'Translate9' => 'Þín pöntun',
	'Translate10' => 'Reikningur',
	'Translate11' => 'Tilbúin til greiðslu',
	'Translate12' => 'Greitt',
	'Translate13' => 'Afgreitt',
	'Translate14' => 'Afpantað',
	'Translate15' => 'Engar upplýsingar',
	'Translate16' => 'Pöntun',
	'Translate17' => 'Reikningur',

];
