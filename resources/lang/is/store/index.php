<?php

return [
	'Translate1' => 'Markmaster',
	'Translate2' => 'Markmasters web2print store. Pantið miða, straujárnsmiða og fleira.',
	'Translate3' => 'Plastmiðar',
	'Translate4' => 'Hágæða plastmiðar með starku lími sem stenst mikið álag.',
	'Translate5' => 'Svart/hvítir miðar',
	'Translate6' => 'Miða má festa á hluti. Sniðug vara með mikla möguleika',
	'Translate7' => 'Litaðir straujárnsmiðar',
	'Translate8' => 'Litaðir miðar sem má strauja á fatnað og efni. Þolir 60°C þvott',
	'Translate9' => 'Straujárnsmiðar í svart/hvítu',
	'Translate10' => 'Háæða miða sem má strauja á fatnað og efni.  Þolir 90°C þvott wash',
	'Translate11' => 'Gull/silfur miðar',
	'Translate12' => 'Miðar með gull eða silfur bakgrunni. Passar vel á verðlaunagripi og þ.h.',
	'Translate13' => 'Efnis miði',
	'Translate14' => 'Miði sem má nota á föt eða efni.  Auðvelt að festa með straujárni.',
	'Translate15' => 'Gegnsær miði',
	'Translate16' => 'Gegnsær miði. Passar vel til að merkja glervörur.',
	'Translate17' => 'Endurskins miði',
	'Translate18' => 'MIði með endurskinsyfirborði. Sést vel í myrkri.',

	];
