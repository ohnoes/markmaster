<?php

return [
	'Translate1' => 'Leveringsadresse',
	'Translate2' => 'Ordrebekreftelse',
	'Translate3' => 'Dato',
	'Translate4' => 'Kundenr.',
	'Translate5' => 'Ordrenr.',
	'Translate6' => 'Fakturaadresse',
	'Translate7' => 'Etikett',
	'Translate8' => 'Type',
	'Translate9' => 'Størrelse mm',
	'Translate10' => 'Antall',
	'Translate11' => 'Pris inkl. mva',
	'Translate12' => 'Klisteretikett farge, med laminat',
	'Translate13' => 'Strykfastetikett farge',
	'Translate14' => 'Klistreetikett i gullfarget vinyl',
	'Translate15' => 'Klistreetikett i sølvfarget vinyl',
	'Translate16' => 'Klistreetikett i transparent viny',
	'Translate17' => 'Klistreetikett i sort/hvit',
	'Translate18' => 'Strykfastetikett',
	'Translate19' => 'Sum. inkl. mva.',
	'Translate20' => 'Porto/eksp.',
	'Translate21' => 'Total sum.',
	
];