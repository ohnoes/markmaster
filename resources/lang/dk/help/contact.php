<?php

return [
	'Translate1' => 'Kontakt Markmaster',
	'Translate2' => 'Her kan du kontakte Mark Master, hvis du har spørgsmål eller gerne vil give os nogle tilbagemeldinger',
	'Translate3' => 'Kontakt os',
	'Translate4' => 'Hvis du har spørgsmål eller gerne vil give os noget feedback? Her er nogle kanaler, hvor du kan kontakte os:',
	'Translate5' => 'E-mail: ',
	'Translate6' => 'Telefon:  77 69 69 61',

];