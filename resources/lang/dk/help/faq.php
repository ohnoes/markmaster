<?php

return [
	'Translate1' => 'Markmaster spørgsmål og svar',
	'Translate2' => 'Her er en liste over ofte stillede spørgsmål og svar',
	'Translate3' => 'Ofte stillede spørgsmål og svar: ',
	'Translate4' => 'Tåler strygemærker brug af tørretumbleren?',
	'Translate5' => 'Ja, korrekt tilpasset, skal kunne modstå tørretumbler. Om de falder av er det et resultat af for lav monteringstemperatur eller for kort 
							tryk tid. Mærket skal blive delvist smeltet ned i stoffet under påstrygning',
	'Translate6' => 'Kan etiketterne stryges på uld?',
	'Translate7' => 'Ja, uld tåler fint påstrygningen temperaturmessig. Påstrygningen på uldsokker eller -handsker gøres bedst ved at montere mærket 
							på siden, så at den ikke hindrer elasticitet af ribben.',
	'Translate8' => 'Hvorfor bliver skriften utydelig efter påstrygning?',
	'Translate9' => 'Oftest skyldes dette en kombination af høj temperatur og bevægelse med strygejernet under tryktiden. 
								Strygejernet skal holdes helt i ro under tryktiden, og prøv gerne med noget lavere temperatur og/ eller kortere tryktid.',
	'Translate10' => 'Må etikettene monteres ved en lavere temperatur end tre prikker på strygejernet da tøjet ikke kan tåle så høje temperaturer?',
	'Translate11' => 'Ja, mærket sitter godt ved to prikker, men den vil da ikke tåle kokvask. Det har dog ingen betydning da tøjet ikke heller tåler det.',
	'Translate12' => 'Hvor lang leveringstid er det på etiketterne?',
	'Translate13' => ' Etiketterne normalt går i produktion dagen efter bestilling og de er derefter pakket og afsendt, så snart de er færdige. 
								De sendes som breve, A-stolpe,og, litt afhengig av postvæsenet, de vil ankomme til kunden 1-2 dage efter 
								levering fra os. HUSK! Det er vigtigt at kontrollere, at du har indtastet den rigtige leveringsadresse på kundeoplysninger.',
	'Translate14' => 'Kan jeg ændre min ordre?',
	'Translate15' => 'Antallet og størrelsen. kan ikke ændres, når ordren er gennemført og betalt, men korrigere stavefejl osv er muligt indtil produktions- start. ',
	'Translate16' => 'Hvilken betaling har du?',
	'Translate17' => 'Vi har online kreditkortbetalinger. Credit (faktura) kan tilbydes kunderne ved kredit check, og som kan modtage EHF faktura. Kontakt os venligst for mere info om dette.',
	'Translate18' => 'Er det muligt at få etiketterne leveret til et andet land end Norge?',
	'Translate19' => 'Ja, det går fint at have leveringsadresse i et andet land end Norge. 
								Men vi har ikke eksportsalg, så vi fratrækker ikke norsk moms når varene skal leveres til udlandet. 
								OBS! OBS! Det kan også afholde omkostninger til fortoldning til nye lande, på kunderne bekostning.',
	'Translate20' => 'Jeg er ikke tilfreds med de etiketter, jeg har modtaget, kan de returneres?',
	'Translate21' => 'Ved udførelsefeil af teknisk karakter erstattes etiketterne fuldt. Hvis der er stavefejl eller ugunstig
layout af kunden, vi vil refundere beløbet i overensstemmelse med gældende regler for personlig
produkter, som regel 50% af købsprisen.',


];