<?php

return [
	'Translate1' => 'Markmasters Kjøpsvilkår',
	'Translate2' => 'Her er Markmasters  kjøpsvilkår',
	'Translate3' => 'VILKÅR',
	'Translate4' => 'Betingelser gælder kundernes brug og køb af alle varer og tjenesteydelser
		leveret av Markmaster AS',
	'Translate5' => 'PRISER OG LEVERING',
	'Translate6' => 'Kunden modtager en automatisk ordrebekræftelse på mail i det Markmaster
			AS har modtaget ordren, og kunden er forpligtet til at kontrollere, at dette
			 er i overholdelse hvad angår produkttype, indhold,
			antal og pris.
			Alle priser er angivet inkl. Moms.
			Varerne vil blive leveret af den angivne leveringstid, normalt 2-5
			arbeidsdage. Herunder regnes ikke weekend- og helligdage.',
	'Translate7' => 'REKLAMASJON OG ANGREFRIST',
	'Translate8' => 'Markmaster AS er forpligtet til at afhjælpe manglen på produktet, hvis
			kunden hurtig giver feedback om dette.
			Markmaster AS produkter er personligt udformet, det er derfor
ingen tilbagetrækning af produkter, efter at de er produceret.
			Kunden er selv ansvarlig for at kontrollere, at deres ordrer ikke
			indeholder stavning, tegnsætning eller grammatiske fejl.
			Markmaster AS undersøger ikke kundernes ordrer for fejl før
			produktet går i produktion, ordren går direkte fra web til
			print.',
	'Translate9' => 'OPPHAVSRETT',
	'Translate10' => 'Hvis kunden har eget design eller på anden måde påvirker
			produktet forsikrer kunden overfor Markmaster AS at tekst og motiv
			er fri fra tredjemands rettigheder.
			Mulig Copyright-, person- eller navnerettihetskrenkelser blir så
			kundens belastning.
			Kunden forsikrer også, at den ved at tilpasse
			produktet ikke krænker tredjemands rettigheder.
			Kunden frigiver Markmaster AS fra alle krav og fordringer der er
			gjordt på grund af krænkelse af sådanne tredjeparts rettigheder,
			forudsat at kunden skal forsvare pligtforsømmelse.
			Kunden erstatter Markmaster AS al opretstående forsvarsomkostinger og
			andre skader.',
	'Translate11' => 'ÆNDRINGER I VILKÅR',
	'Translate12' => 'Markmaster AS forbeholder sig ret til at ændre disse vilkår og betingelser,
			herunder som følge af ændringer i lovgivningen.',

];