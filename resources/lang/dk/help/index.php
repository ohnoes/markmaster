<?php

return [
	'Translate1' => 'Markmasters afdeling for hjælp',
	'Translate2' => 'Her er en liste over ting, vi kan hjælpe dig med',
	'Translate3' => 'Hjælp',
	'Translate4' => 'Spørgsmål og svar',
	'Translate5' => 'Video',
	'Translate6' => 'Design hjælp',
	'Translate7' => 'Betingelser',
	'Translate8' => 'Kontakt os',

];