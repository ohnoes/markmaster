<?php

return [
	'Translate1' => 'Dine ordrer hos Markmaster',
	'Translate2' => 'Dine ordrer',
	'Translate3' => 'Følgende fejl skal rettes:',
	'Translate4' => 'Dine Ordrer',
	'Translate5' => 'Ordrenummer',
	'Translate6' => 'Ordredato',
	'Translate7' => 'Leverings adresse',
	'Translate8' => 'Status',
	'Translate9' => 'Pris',
	'Translate10' => 'Klar til betaling',
	'Translate11' => 'Betalt',
	'Translate12' => 'Eksportert',
	'Translate13' => 'Aflyst',
	'Translate14' => 'Hold',
	'Translate15' => 'N/A',
	'Translate16' => 'Info',
	'Translate17' => 'Betal',
	'Translate18' => 'Faktura',
	'Translate19' => 'Fjerne',
	'Translate20' => 'Er du sikker på du vil fjerne etiketten?',
	
	
];