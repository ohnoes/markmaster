<?php

return [
	'Translate1' => 'Din ordre hos Markmaster',
	'Translate2' => 'Ordreinfo',
	'Translate3' => 'URL link til etiketdesign',
	'Translate4' => 'Etiket',
	'Translate5' => 'Type',
	'Translate6' => 'Størrelse mm',
	'Translate7' => 'Antal',
	'Translate8' => 'Pris inkl. moms',
	'Translate9' => 'Link',
	'Translate10' => 'Selvklæbende etiketter farve, med laminat',
	'Translate11' => 'Strygemærker farve',
	'Translate12' => 'Klistermærker i guldfarvet vinyl',
	'Translate13' => 'Klistermærker i sølvfarvet vinyl',
	'Translate14' => 'Klistermærker i transparent vinyl',
	'Translate15' => 'Klistermærke i sort/hvid',
	'Translate16' => 'Strygemærke',
	'Translate17' => 'Klistermærke i refleks',
	'Translate18' => 'Edit',
	'Translate19' => 'Fjerne',
	'Translate20' => 'Er du sikker på du vil fjerne etiketten?',
	
];