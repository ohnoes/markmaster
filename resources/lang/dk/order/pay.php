<?php

return [
	'Translate1' => 'Betaling',
	'Translate2' => 'Levering og betalingsoplysninger til din ordre',
	'Translate3' => 'Følgende fejl skal rettes:',
	'Translate4' => 'Leveringsinformation',
	'Translate5' => 'Organisationsnavn',
	'Translate6' => 'Fornavn',
	'Translate7' => 'Efternavn',
	'Translate8' => 'Adresse',
	'Translate9' => 'Postkode',
	'Translate10' => 'By',
	'Translate11' => 'Fylke',
	'Translate12' => 'Land: ',
	'Translate13' => 'Fakturainformation',
	'Translate14' => 'Fakturainformation er det samme som leveringsinformation',
	'Translate15' => 'NB! Ved å trykke på Betal bekræfter jeg hermed at jeg har læst og accepteret Vilkår og betingelser',
	'Translate16' => 'Betal',
	'Translate17' => 'BETINGELSER',
	'Translate18' => 'Betingelser gælder kundernes brug og køb af alle varer og tjenesteydelse
		leveret av Markmaster AS',
	'Translate19' => 'PRISER OG LEVERING',
	'Translate20' => 'Kunden modtager en automatisk ordrebekræftelse på mail i det Markmaster
			AS har modtaget ordren, og kunden er forpligtet til at kontrollere at denne
			er i overensstemmelse med ordren hvad angår produkttype, indhold,
			antal og pris.
			Alle priser er angivet inkl. Moms og forsendelse.
			Varerne vil blive leveret ved den angivne leveringstid, normalt 2-5
			arbeidsdager. Herunder anses ikke weekend-og helligdage.',
	'Translate21' => 'KLAGE OG ANGREFRIST',
	'Translate22' => 'Markmaster AS er forpligtet til at afhjælpe manglen på produktet hvis
			kunden hurtig gir feedback om dette.
			Markmaster AS produkter er personligt gjort, det er derfor
ingen tilbagetrækning af produkter, efter at de er produceret.
			Kunden er selv ansvarlig for at kontrollere, at deres ordrer ikke
			indeholder stavning, tegnsætning eller grammatiske fejl.
			Markmaster AS undersøger ikke kundernes ordrer for fejl før
			produkt går i produktion, ordren går direkte fra web til
			print.',
	'Translate23' => 'COPYRIGHT',
	'Translate24' => 'Hvis kunden har eget design eller på anden måde påvirker
			produktet forsikrer kunden overfor Markmaster AS at tekst og motiv
			er fri fra tredjemands rettigheder.
			Mulig Copyright-, person- eller navnerettihetskrenkelser blir så
kundens belastning.
			Kunden forsikrer også, at den ved at tilpasse 
			produktet ikke krænker tredjemands rettigheder.
			Kunden frigiver Markmaster AS fra alle krav og fordringer, der er
			gjordt på grund af krænkelse af sådanne tredjeparts rettigheder,
			forudsat at kunden skal forsvare pligtforsømmelse.
			Kunden erstatter Markmaster AS al opretstående forsvarsomkostinger og
			andre skader.',
	'Translate25' => 'ÆNDRINGER I VILKÅR',
	'Translate26' => 'Mark Master forbeholder sig ret til at ændre disse vilkår og betingelser,
			herunder som følge af ændringer i lovgivningen.',
	'Translate27' => 'Betal',
	
];