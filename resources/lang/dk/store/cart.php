<?php

return [
	
	/////////////////////////////////////////////////////
	//  						Messages 					//
	/////////////////////////////////////////////////////
	'Errors' => 'Venligst ret disse fejl ',
	
	/////////////////////////////////////////////////////
	//  						Heading   					//
	/////////////////////////////////////////////////////
	'ShoppingCart' => 'Indkøbskurv',
	
	/////////////////////////////////////////////////////
	//  					Table headings   			    //
	/////////////////////////////////////////////////////
	'URL' => 'URL link til etiketdesign*' ,
	'Stickertype' => 'Etiket',
	'ProductType' => 'Type',
	'Size' => 'Størrelse mm',
	'Quantity' => 'Antal',
	'Price' => 'Pris inkl. moms',
	
	
	/////////////////////////////////////////////////////
	//  					Product types    			    //
	/////////////////////////////////////////////////////
	'Sticker' => 'Klistermærke farve, med laminat',
	'StickerBW' => 'Klistermærke i sort/hvid',
	'Ironon' => 'Strygemærke farve',
	'IrononBW' => 'Strygemærke',
	'Gold' => 'Klistermærke i guldfarvet vinyl',
	'Silver' => 'Klistermærke i sølvfarvet vinyl',
	'Transparent' => 'Klistermærke i transparent vinyl',
	'Reflection' => 'Klistermærke i refleks',
	'Fabricsticker' => 'Tøjklistermærke',
	'CouponCode' => 'Rabatkode',

	


	
	/////////////////////////////////////////////////////
	//  					Buttons			   			    //
	/////////////////////////////////////////////////////
	'EditButton' => 'Edit',
	'DeleteButton' => 'Fjerne',
	'StoreButton' => 'Tilbage til butik',
	'CheckoutButton' => 'Gå til betaling',
	
	/////////////////////////////////////////////////////
	//  					Extra Lang			   		    //
	/////////////////////////////////////////////////////
	'Shipping' => 'Porto og Ekspeditionsgebyr',
	'SigninMemo' => '*Du skal være logget ind for at kunne dele dit design',
	'DeleteWarning' => 'Er du sikker på du vil slette din etiket?',
	
	'Translate1' => 'Din indkøbskurv',
	'Translate2' => 'Din indkøbskurv i Markmasters web2print butik',
	'couponButton' => 'Tilføj rabatkode',
	'coupon' => 'Rabatkode',
];