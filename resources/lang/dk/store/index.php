<?php

return [
	'Translate1' => 'Navnemærker i farve, klistermærker, refleksmærker og strygemærker',
	'Translate2' => 'Strygemærker, tøjklistermærker  og klistermærker, refleksmærker, skoetiketer, valgfrit antal',
	'Translate3' => 'Klistermærke farve med laminat',
	'Translate4' => 'Højkvalitet klistermærke med
								ekstra kraftigt lim og lamineret
								overflade. Uovertruffen
								holdbarhed for hard brug',
	'Translate5' => 'Klistermærke sort/hvid',
	'Translate6' => 'Navnemærker som kan klistres
								på gjenstande. Et rimelig
								produkt med bred anvendelse',
	'Translate7' => 'Strygemærke farve',
	'Translate8' => 'Farvede navnemærker som
								stryges fast på tøj og
								tekstiler. Tåler 60°C vask',
	'Translate9' => 'Strygemærke sort/hvid',
	'Translate10' => 'Højkvalitet navnemærker
								som stryges fast på tøj og
								tekstiler. Tåler 90°C vask.',
	'Translate11' => 'Klistermærke guld & sølv',
	'Translate12' => 'Klistermærke i guldfarvet eller sølvfarvet
								folie. Egnet til mærkning af præmier og lignende',
	'Translate13' => 'Strygefrie navnemærker',
	'Translate14' => 'Let opmærkning af tøj og andre ejendele. Let at påføre, ingen strygning, let at fjerne',
	'Translate15' => 'Klistermærke transparent',
	'Translate16' => 'Klistermærke i transparent
								folie. Egnet til mærkning af
								glasvarer',
	'Translate17' => 'Klistermærke refleks',
	'Translate18' => 'Klistermærke trykket i folie
								med reflekterende egenskaber.
								Når du vil have ekstra synlighed i det mørke efterår',

          'keywords' => 'strygemærker, tøjklistermærker, klistermærker, refleksmærker',


];
