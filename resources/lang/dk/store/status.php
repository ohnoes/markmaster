<?php

return [
	'Translate1' => 'Ordrestatus',
	'Translate2' => 'Status for din ordre',
	'Translate3' => 'Noget gik galt. Pr�v igen, eller brug et andet kort. Auth Fail',
	'Translate4' => 'Noget gik galt. Pr�v igen, eller brug et andet kort. Capture Fail',
	'Translate5' => 'Betaling godkendt. Din ordre vil blive sendt med posten om 1-2 arbeidsdage.',
	'Translate6' => 'Ordrenummer',
	'Translate7' => 'Pris',
	'Translate8' => 'Ordrestatus',
	'Translate9' => 'Link til ordre',
	'Translate10' => 'Faktura',
	'Translate11' => 'Klar til betaling',
	'Translate12' => 'Betalt',
	'Translate13' => 'Eksporteret',
	'Translate14' => 'Kanselleret',
	'Translate15' => 'N/A',
	'Translate16' => 'Ordre',
	'Translate17' => 'Faktura',

];