<?php

return [

    // Title
    'Title-sticker-color' => 'Laminerede klistermærker med farve',
    'Title-iron-color' => 'Strygemærker med farve',
    'Title-gold' => 'Guldfarvede Klistermærker',
    'Title-silver' => 'Sølvfarvede Klistermærker',
    'Title-transparent' => 'Transparente klistermærker',
    'Title-sticker' => 'Klistermærker',
    'Title-iron' => 'Strygemærker',
    'Title-reflection' => 'Klistermærker med refleks',
    'Title-sticker-fabric' => 'Tøjklistermærker',

    // Page Descriptions   Update these for CEO
	'Page-Description-sticker-color' => 'Laminerede klistermærker med farve',
    'Page-Description-iron-color' => 'Strygemærker med farve',
    'Page-Description-gold' => 'Klistermærker med guldfarve',
    'Page-Description-silver' => 'Klistermærker med sølvfarve',
    'Page-Description-transparent' => 'Klistermærker med transparent folie',
    'Page-Description-sticker' => 'Klistermærker',
    'Page-Description-iron' => 'Strygemærker',
    'Page-Description-reflection' => 'Klistermærker med refleks',
    'Page-Description-sticker-fabric' => 'Tøjklistermærker',

    // Alerts
	'Alert-login' => 'Du skal være logget ind, hvis du ønsker at uploade baggrunde eller billeder',

    // Page header
    'Header-sticker-color' => 'Klistermærke med laminatforsterkning',
    'Header-iron-color' => 'Strygemærker farve',
    'Header-gold' => 'Klistermærke guld',
    'Header-silver' => 'Klistermærke sølv',
    'Header-transparent' => 'Klistermærke transparent',
    'Header-sticker' => 'Klistermærker sort/hvid',
    'Header-iron' => 'Strygemærker sort/hvid',
    'Header-reflection' => 'Klistermærker refleks',
    'Header-sticker-fabric' => 'Tøjklistermærker',

    // Control Tabs
    'Tab-size' => 'Størrelse og form',
	'Tab-background' => 'Baggrund og baggrundsfarve',
	'Tab-text' => 'Tekst',
	'Tab-clipart' => 'Symboler og billeder',
	'Tab-cart' => 'Læg i Kurv',

    // Design Labels
    'Label-rectangle' => 'Rektangel',
    'Label-elipse' => 'Elipse',
    'Label-width-mm' => 'bredde mm',
    'Label-height-mm' => 'højde mm',
    'Label-width-height-info' => '*Bredde og højde skal være mellem 6-300mm',
    'Label-width-height-info2' => '*Bredde og højde skal være mellem 6-50mm',
    'Label-pick-size' => 'Vælg størrelsen på etiketten',
    'Label-3716' => '37mm x 16mm',
    'Label-6026' => '60mm x 26mm',
    'Label-3010' => '30mm x 10mm',
    'Label-pick-quantity' => 'Vælg antal',
    'Label-quantity' => 'Antal',
    'Label-1' => '1 stk.',
    'Label-5' => '5 stk.',
    'Label-10' => '10 stk.',
    'Label-20' => '20 stk.',
    'Label-50' => '50 stk.',
    'Label-75' => '75 stk.',
    'Label-100' => '100 stk.',
    'Label-250' => '250 stk.',
    'Label-500' => '500 stk.',
    'Label-1000' => '1000 stk.',
    'Label-price' => 'Pris: ',

    'Label-color-select' => 'Vælg farve fra tabel',
    'Label-upload-background' => 'Upload baggrund',
    'Label-upload-select-background' => 'Vælg / upload en baggrund',
    'Label-upload-mybackground' => 'Upload egen baggrund',
    'Label-backgrounds' => 'Baggrunde',
    'Label-mybackgrounds' => 'Mine Baggrunde',
    'Label-background-nonchangeable' => 'Du kan ikke endre baggrundsfarve på denne etiketten',

    'Label-your-text' => 'Tilføj tekst her!',
    'Label-add' => 'Tilføj',
    'Label-delete' => 'Fjerne',
    'Label-position' => 'Position',
    'Label-size' => 'Størrelse',
    'Label-line-distance' => 'Linjeafstand',
    'Label-rotation' => 'Rotation',
    'Label-text-format' => 'Tekst Format',
    'Label-color' => 'Farve',
    'Label-center-text' => 'Centrer Teksten',
    'Label-layer' => 'Lag Kontroll',
    'Label-thickness' => 'Tykkelse',
    'Label-line-edge' => 'Kantlinje',
    'Label-flip-clipart' => 'Flip billede',
    'Label-center-clipart' => 'Centrer billede',
    'Label-clipart-color' => 'Billede Farve',
    'Label-upload-clipart' => 'Upload foto',

    'Label-update-sticker' => 'Opdater label',
    'Label-order' => 'Bestill',

    'Label-clipart-my' => 'Mine Clipart',
    'Label-clipart-used' => 'Mest Brugt',
    'Label-clipart-animal' => 'Dyr',
    'Label-clipart-zodiac' => 'Stjernetegn',
    'Label-clipart-figures' => 'Figurer',
    'Label-clipart-symbols' => 'Symboler',
    'Label-clipart-sport' => 'Sport',
    'Label-clipart-machines' => 'Maskiner',
    'Label-clipart-shape' => 'Former',
    'Label-clipart-laundry' => 'Vaskesymboler',

    // Sticker/Label Description
    'Description-upload' => 'Du skal være logget ind, hvis du ønsker at uploade wallpapers eller billeder. Gyldig filformat: png, gif, eller jpg.
				                Baggrunde og clipart er personlige og ikke tilgængelig for ikke-kontohaver',
    'Description-sticker-color-1' => 'Klistermærker med laminat forstærkning produceret af "murfolie" der sidder særdeles godt.
                                        Derudover har vi laminerede etiketterne med en tynd klar film, der gør dem ekstremt holdbare.
                                        De er meget modstandsdygtige over for slid og opløsningsmidler.',
    'Description-sticker-color-2' => 'Disse navnelappenen er den absolut bedste kvalitet og leveret til markedets laveste priser.
                                        Etiketterne har afrundede hjørner, hvilket er en fordel, fordi de sidder bedre som kanterne ikke løsne så let.',
    'Description-sticker-color-3' => 'Mærkaterne er lige velegnet til både udendørs og indendørs brug, og det kan modstå 60 graders opvaskemaskine vask.
                                        Etiketten har mange anvendelsesmuligheder, kan det være at enkeltpersoner, virksomheder, teams eller virksomheder, hvis brug for klistermærker.
                                        Hvis du ønsker at fremme din virksomhed eller eller vil drive branding er vores etiketter perfekt.',
    'Description-sticker-color-4' => 'Andre typiske applikationer til vores selvklæbende etiket vil være værktøjer, flasker, madkasser, bøger, fotografering, computer og sportsudstyr, postkasse etiketter og lignende
                                        Vores små ovale etiketter er velegnet til mærkning af kosmetiske udstyr, kuglepenne, mobiltelefoner opgørelse, og mere
                                        Etiketterne kan også anvendes til mærke sko (mærkat ned i sålen)
                                        Brygning er populær, og hos Markmaster kan du designe dine egne øl etiketter.
                                        I vores butik kan du også bestille kontrol mærker, certificering mærker og -oblat, der er færdige skabeloner, du kan bruge som udgangspunkt og derefter uploade deres eget logo.
                                        Og hvad med at designe dit eget postkasse label?',
    'Description-sticker-color-5' => 'Der er flere design muligheder i vores online shop og kunden vælger frit størrelse, antal og form.
                                        Du kan vælge farven på tekst og baggrund, og vi har mange skrifttyper at vælge imellem.
                                        I vores symbolbank vil du helt sikkert finde noget, du kan lide.
                                        Og selvfølgelig er det muligt at uploade dine egne billeder og logoer, eller en helt færdig design.
                                        Når det kommer til montering af klistermærkerne er dette er meget let, det eneste du skal huske er, at underlaget skal være rent og tørt.',
    'Description-iron-color-1' => 'Strygefastetiket farve er en blød, holdbar mærke, der skal stryges på stof og kan modstå 60 graders vask. Dette er en nem og billig måde at brande tøj på.
                                    Produktet virker ikke irriterende på huden og ikke indeholder phtalater eller PVC.',
    'Description-iron-color-2' => 'De kan anvendes til alle former for tekstil og beklædning og kunden kan bestemme formen, antallet og størrelse.',
    'Description-iron-color-3' => 'Eksempler på, hvad der kan blive bemærket, er jakker, hatte, handsker, trøjer, bukser, gummistøvler, regntøj og lignende
                                    Produktet er også ideel til klubber og foreninger, der ønsker at markere sådan en t-shirt med dit eget logo.
                                    Virksomheder som vil bemærke arbejdstøj vil nemt kunne bruge vores designværktøjer i vor online butik for at uploade deres eget design.',
    'Description-iron-color-4' => 'Børne tøj skal mærkes med telefonen, så det er let at finde den retmæssige ejer.
                                    Det kan være en god idé at lade dit barn til at vælge ethvert symbol eller tal, der skal medtages på navnet tag. Dette er så, at de let skal kunne genkende deres ting selv om de ikke har lært at læse.
                                    Mange børn synes også, at det er sjovt, hvis en uploader et billede af sig selv for at have navnet tag.',
    'Description-iron-color-5' => 'Om navneskilte er installeret korrekt de vil smelte ind i stoffet og er derfor meget vanskeligt at fjerne igen.
                                    Men mange ønsker at give væk babytøj, når det er blevet for lille til deres egne børn, og så kan du gøre brandet mindre synlige ved at lægge på et nyt mærke uden tekst.',
    'Description-gold-1' => 'Markmasters guld farvede etiketter og klistermærker er i guld farvet vinyl med sort print.',
    'Description-gold-2' => 'Som den første leverandør af dette produkt kan Markmaster tilbyde sine kunder guld farvede etiketter i valgfri strørrelse, antal og form.',
    'Description-gold-3' => 'Kunden kan bruge vores design modul eller vælge en af vores skabeloner, der hjælper til at starte design.
                            Det er også muligt at uploade eget design og egne logoer, hvis det ønskes.',
    'Description-gold-4' => 'Guld etiketter er velegnet til alle kunder, der ønsker en lidt mere eksklusiv mærkning af deres produkter og aktiver.
                            De er perfekte til mærkning som trofæer og andre præmier. Andre ting man kan bemærke med guld farvede etiketter er postkasser,
                            præsenterer til forskellige lejligheder, merchandise, salgsfremmende produkter ETC.',
    'Description-gold-5' => 'Små klistermærker i guldfarvet folie er også fint at bruge til mærkning af personlige ejendele, såsom mobil, makeup tilfælde, kuglepenne, kameraer og lignende',
    'Description-silver-1' => 'Etiketter i sølv farved folie med sort print.',
    'Description-silver-2' => 'Markmaster har været den første virksomhed til at producere den slags etiketter, hvor kunden er helt fri til at vælge den størrelse og form, og også vælge antallet steg løst helt ned til en enkelt etiket.
                                I vores butik, er der flere muligheder for design af personaliserede etiketter og klistermærker.',
    'Description-silver-3' => 'Her finder de en nem booking løsning, eller de kan vælge en af ​​vores skabeloner til at have en base at arbejde ud fra.
                                Hvis du vil have flere muligheder og evnen til at uploade egne designede ting og enhver logo, bør man vælge vores løsning til deres design.
                                Alle designs, der er uploadet som baggrund eller symbol vil være personlig, og vil kun blive lagred på kundens egen kundeprofil i online butikken.',
    'Description-silver-4' => 'Silver Label er et produkt til kunder, der ønsker en dejlig og noget eksklusivt mærkning.
                                Sportsklubber vælger ofte denne form for etiketter til mærkning af præmier.',
    'Description-silver-5' => 'Andre anvendelser kan være mærkning af bøger, diverse merchandise, syltetøjsglas og lignende',
    'Description-transparent-1' => 'Gennemsigtige klistermærker og navneskilte er mærker trykt på transparent folie.',
    'Description-transparent-2' => 'Trykt i sort, og du kan vælge, om du ønsker at bruge vores design modul eller upload dit eget design.
                                    Størrelse, antal og form er også valgfrit.',
    'Description-transparent-3' => 'Brugssområde er stort, etiketterne passer godt der emnet ikke bør være for massiv, og hvis du ønsker en lidt diskret branding.',
    'Description-transparent-4' => 'Typiske anvendelser kan være vinduer, præmier og lignende af glas, mobil, pc, iPad, iPod, Mac, ure, briller, makeup sager, tandbørster, kuglepenne og mere',
    'Description-transparent-5' => 'Det er meget vigtigt, hvad farve det er i baggrunden mærker skal sidde på.
                                Hvis baggrunden er hvid eller blank (glas) det er helt uden betydning.
                                Sætter dig på den anden side mærker på en sort eller mørk baggrund, vil du næsten ikke kunne se trykket, når den forsvinder mod den mørke overflade.',
    'Description-sticker-1' => 'Sticky Label sort / hvid er en meget nyttig og holdbar etiket lavet af miljøvenlige materialer.',
    'Description-sticker-2' => 'Der er en rimelig og godt alternativ til alle typer af mærkning.
                                Denne etiket kommer på et kast, og du kan vælge mellem størrelse 37x16 mm og 60x26',
    'Description-sticker-3' => 'Klistermærket har mange brugsområder, såsom drikkevareflasker, thermoses, madkasser, Candy skåle, skiudstyr og andet sportsudstyr, bøger, legetøj, telefoner og lignende
                                For forhandlere af forskellige produkter, er dette det perfekte mærke til at mærke varer med.
                                Eksempler på sådanne forhandlere kan være Tupperware, Cosmopharma, Mary Kay og Forever Living',
    'Description-sticker-4' => 'Forskellige typer af håndværkere bruger også denne type etiket til at mærke diverse udstyr.
                                Det er nemt at uploade dine egne designs og logoer, eller man kan vælge at bruge skabelonerne som det fremgår i vores online butik som udgangspunkt for videre design.',
    'Description-sticker-5' => 'Denne type mærke er dårligt egnet til mærkning af tøj og tekstiler, til denne form for mærkning,
                                anbefaler vi vores strygefast etiketter i enten sort / hvid eller farve, eller vores tøjklistermærker',
    'Description-iron-1' => 'Strygfastetiketter er af høj kvalitet, nametags, der vil stryges på tøj og tekstiler.
                                Markmaster har produceret denne form for etiketter siden vores start i Norge i 1988, har det været vores bestseller i mange år og er stadig meget populær.',
    'Description-iron-2' => 'Strygefastetikettene er i sort / hvid, standard størrelse er 30x10 mm og leveres på en rulle.
                                For dem, der ønsker en overkommelig og noget diskret mærkning er dette en perfekt etiket.
                                Strygefastetiketter er velegnet til alle former for tøj og tekstiler, også uld.',
    'Description-iron-3' => 'Det er meget godt at mærke små elementer såsom handsker, sokker, huer og lignende
                                Etiketten er meget nem at påføre, den stryges direkte på tøjet og smelter ned i tekstilfibrene.',
    'Description-iron-4' => 'Brugsanvisning og beskyttelsesark til strygning er inkluderet i pakken.
                                Arket som leveres til strygning beskyttelse er ellers normal bagepapir, det er vigtigt ikke forsøge at bruge annet papir, da dette vil klamre sig til etiketterne.
                                Korrekt monteret er strygefastetiketter meget holdbare, skal de kunne modstå 90 graders vask og også tørretumbler.',
    'Description-iron-5' => 'Fordi strygefastetiketter smelter ind i tøjet, er det ikke muligt at fjerne omkring en senere ønske.
                                Alternativet er at stryge neutrale etiketter over de gamle for at blokere ud af navnet.
                                Hvis du vil have etiketter til at markere tøj til forskellige institutioner, tøj til børnehave og SFO, er strygefastetiketter det mest oplagte og bedste valg.',
    'Description-reflection-1' => 'Markmasters refleks etiketter er trykt på en klæbende film, der har reflekterende egenskaber.
                                    Vi var de første på markedet med denne type etiket og at kunden kunne vælge design.',
    'Description-reflection-2' => 'Mærkerne er i et blødt materiale, der former sig underlaget.
                                    De er velægnet til at mærke ting som cykler, cykel hjelme, støvler, sko, skoletaske, regntøj etc.
                                    Personlig etiket, der giver ekstra synlighed i mørke, lad børnene hjælpe at beslutte, hvad der skal stå på etiketten.
                                    Måske vil de have et billede af sig selv eller dit foretrukne kæledyr siddende? Dette gør at dem let kan finde igen fx cykelhjelm, når de vil cykle hjem fra skole eller uddannelse',
    'Description-reflection-3' => 'Valgfri farve, størrelse, antal og form.
                                    Men husk, at små mærker alene ikke giver tilstrækkelig synlighed.',
    'Description-reflection-4' => 'Gode ​​råd til brug af reflektorer:
                                    Da de fleste børn er placeret i tætbefolkede områder, hvor bilisterne bruger nærlys, skal reflektorer hænge lavt, fortrinsvis fra knæ og ned, for bedste synlighed.
                                    Jo flere reflekser du bærer jo bedre er det.
                                    Man skal have reflektorer på hver side af kroppen, således at man ser godt fra begge sider ved passage vejen.',
    'Description-reflection-5' => 'Refleksion udhules gradvist og bør udskiftes hver sæson, eller så snart det kommer kradse på.
                                    Husk refleksmærker erstatter ikke andet sikkerhedsudstyr såsom lys og pedal reflekser på cykel.
                                    Bliv set, bruge refleks!',
    'Description-sticker-fabric-1' => 'Tøjklistermærker som enkelt klistres på tøj eller andre ejendele.
                                    Mærkene kan klistres på alt af tøj og tekstiler som har vaskeanvisningen / tøj etiket.
                                    De er en af ​​Markmasters mest populære produkter. Dette er fordi de er lette at påføre, ingen strygning og de er nemme at fjerne.',
    'Description-sticker-fabric-2' => 'Etiketterne passer godt til at markere madkasser, forskellige flasker, legetøj, bøger, dvd og lignende.
                                    Forudsætningen for god holdbarhed er, at underlavet mærket skal sidde på er ren og tør.
                                    Om mærker er monteret korrekt kan de modstå de garanterede 40 grader vask i en vaskemaskine og opvaskemaskine.
                                    Hvis tøjet skal vaskes ved højere temperaturer eller ikke haver vaskeanvisninger, anbefaler vi vores strygefastetiketter.',
    'Description-sticker-fabric-3' => 'Derudover er der utallige designmuligheder, kan kunderne frit vælge, hvordan at designe deres label.
                                    Der er flere valg af design i vores online butik.
                                    Vælges simple design er den indstillet til 30x13 mm, det er max 3 linjer tekst, og du kan tilføje symboler.
                                    Man kan vælge farven på baggrunden og teksten, og der er masser af nice skrifttyper at vælge imellem.
                                    Hvis kunden ønsker flere muligheder, skal i vælge eget design.
                                    Her kan du vælge størrelsen og antallet, og du kan uploade egne designs og billeder.
                                    Du kan også vælge en af ​​vores skabeloner som udgangspunkt for videre design.',
    'Description-sticker-fabric-4' => 'Kun fantasien sætter grænser, måske kan du lade børnene selv komme til at vælge design og farve, så de får præcis etiketten, de ønsker. Og hvad for at uploade et billede af dit barn?
                                    Så vil det være let for dit barn at genkende deres tøj og ejendele, selv om de ikke har lært at læse.',
    'Description-sticker-fabric-5' => 'Instruktioner: Tøjklistermærker skal monters på vaskeanvisninger eller tøjmærker og masseres godt ind, det er særlig vigtigt at trykke på hjørner og kanter.
                                        Vent 24 timer før tøjet vaskes.',

];
