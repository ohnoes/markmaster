<?php
// resources/lang/no/routes.php
return [
    'accountfound'    => 'E-post adresse er allerede knyttet til en bruger, venligst login.',
    'newaccountmade'  => 'Ny bruger oprettet. Midlertidig loginoplysninger er: ',
    'newaccountmade2' => 'Vi anbefaler, at du ændre dit kodeord.',
    // other routes name
];
