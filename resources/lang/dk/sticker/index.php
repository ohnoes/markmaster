<?php

return [
	'Translate1' => 'Dine etiketter',
	'Translate2' => 'Dine tidligere designede etiketter. Klik på etiketterne for at bestille de over',
	'Translate3' => 'Dine Etiketter',
	'Translate4' => 'URL der kan deles med dine venner',
	'Translate5' => 'Etiket',
	'Translate6' => 'Type',
	'Translate7' => 'Klistermærke farve, med laminat',
	'Translate8' => 'Strygemærke farve',
	'Translate9' => 'Klistermærke i guldfarvet vinyl',
	'Translate10' => 'Klistermærke i sølvfarvet vinyl',
	'Translate11' => 'Klistermærke i transparent vinyl',
	'Translate12' => 'Klistermærke i sort/hvid',
	'Translate13' => 'Strygemærke',
	'Translate14' => 'Klistermærke i refleks',
	'Translate15' => 'Størrelse',
	'Translate16' => 'Tøjklistermærke'
	
	
];