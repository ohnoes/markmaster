<?php
// resources/lang/dk/routes.php
return [
    'tempsticker'     => 'etiketer-og-navnelapper/klistremarker',
    'tempironc'       => 'etiketer-og-navnelapper/strygefast-farve',
    'tempgold'        => 'etiketer-og-navnelapper/klistremarker-guld',
    'tempsolv'        => 'etiketer-og-navnelapper/klistremarker-solv',
    'temptrans'       => 'etiketer-og-navnelapper/klistremarker-transparent',
    'tempcheap'       => 'etiketer-og-navnelapper/billige',
    'goldorsolv'      => 'etiketer-og-navnelapper/guld-eller-solv',
    'tempiron'        => 'etiketer-og-navnelapper/strygefast',
    'tempreflection'  => 'etiketer-og-navnelapper/refleksmarker',
    'tempironfree'    => 'etiketer-og-navnelapper/strygefrie',

    'namesticker' => 'navnelapper/klistremarker',
    'nameironc' => 'navnelapper/strygefast-farve',
    'namegold' => 'navnelapper/klistremarker-guld',
    'namesolv' => 'navnelapper/klistremarker-solv',
    'nametrans' => 'navnelapper/klistremarker-transparent',
    'namecheap' => 'navnelapper/billige',
    'nameiron' => 'navnelapper/strygefast',
    'namereflection' => 'navnelapper/refleksmarker',
    'nameironfree' => 'navnelapper/strygefrie',
    'namebase' => 'navnelapper/base',

    'merkclipart' => 'markelapper/clipartget',
    'merkbackground' => 'markelapper/backgroundget',
    'merkactive' => 'markelapper/stayactive',
    'merksticker' => 'markelapper/klistremarker',
    'merkironc' => 'markelapper/strygefast-farve',
    'merkgold' => 'markelapper/klistremarker-guld',
    'merksolv' => 'markelapper/klistremarker-solv',
    'merktrans' => 'markelapper/klistremarker-transparent',
    'merkcheap' => 'markelapper/billige',
    'merkiron' => 'markelapper/strygefast',
    'merkreflection' => 'markelapper/refleksmarker',
    'merkironfree' => 'markelapper/strygefrie',
    'merkbase' => 'markelapper/base',

    // other routes name
];
