<?php

return [
	'Translate1' => 'Opret en ny bruger hos Markmaster',
	'Translate2' => 'Opret en ny bruger hos Markmasters web2print-butik',
	'Translate3' => 'Ny bruger',
	'Translate4' => 'Disse fejl skal rettes: ',
	'Translate5' => 'Organisationsnavn',
	'Translate6' => 'Fornavn',
	'Translate7' => 'Efternavn',
	'Translate8' => 'Adresse',
	'Translate9' => 'Postnr',
	'Translate10' => 'Poststed',
	'Translate11' => 'Land',
	'Translate12' => 'E-mail',
	'Translate13' => 'Adgangskode',
	'Translate14' => 'Bekræft din adgangskode',
	'Translate15' => 'Bekræft',
	'Translate16' => 'Telefon',
	'Translate17' => 'Ja tak, jeg vil have nyhedsbreve, 
								rabat koder og andre tilbud fra 
								Markmaster.no',
	'Translate18' => 'Registrere bruger',

];