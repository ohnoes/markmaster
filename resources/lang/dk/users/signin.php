<?php

return [
	'Translate1' => 'Log deg ind hos Markmaster',
	'Translate2' => 'Log deg ind hos Markmasters web2print-butikk',
	'Translate3' => 'Eksisterende kunde',
	'Translate4' => 'NB! Det er ikke muligt at bruge legitimationsoplysninger fra Mark Masters gamle online butik. Alle kunder skal oprette en ny brugerkonto ',
	'Translate5' => 'Email',
	'Translate6' => 'Indtast email',
	'Translate7' => 'Adgangskode',
	'Translate8' => 'Indtaste din adgangskode',
	'Translate9' => 'Log Ind',
	'Translate10' => 'Glemt Adgangskode',
	'Translate11' => 'Ny bruger',
	'Translate12' => 'Det er nemt at oprette en ny brugerkonto',
	'Translate13' => 'Lag ny bruger',
	
];