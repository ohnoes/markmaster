<?php

return [
	'Translate1' => 'Ændre din faktureringsadresse',
	'Translate2' => 'Ændre din faktureringsadresse på Markmasters online butik',
	'Translate3' => 'Opdater standard faktureringsadresse',
	'Translate4' => 'Disse fejl skal rettes: ',
	'Translate5' => 'Organisationsnavn:',
	'Translate6' => 'Fornavn:',
	'Translate7' => 'Efternavn:',
	'Translate8' => 'Adresse:',
	'Translate9' => 'Postnr:',
	'Translate10' => 'Poststed:',
	'Translate11' => 'Land:',
	'Translate12' => 'Opdater information',
	

];