<?php

return [
	'Translate1' => 'Ændre din leveringsadresse',
	'Translate2' => 'Ændre din leveringsadresse på Markmasters online butik',
	'Translate3' => 'Opdater standard brugerinformation',
	'Translate4' => 'Disse fejl skal rettes: ',
	'Translate5' => 'Organisationsnavn:',
	'Translate6' => 'Fornavn:',
	'Translate7' => 'Efternavn:',
	'Translate8' => 'Adresse:',
	'Translate9' => 'Postnr:',
	'Translate10' => 'Poststed:',
	'Translate11' => 'Land:',
	'Translate12' => 'Opdater information',
	'Translate13' => 'Ja tak, jeg vil have nyhedsbreve, 
								rabat koder og andre tilbud fra 
								Markmaster.no',
	'Translate14' => 'E-mail:',
	'Translate15' => 'Telefon:',
	

];