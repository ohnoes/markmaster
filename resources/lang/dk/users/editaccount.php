<?php

return [
	'Translate1' => 'Ændre dine brugerdetaljer',
	'Translate2' => 'Ændre dine brugerdetaljer på Markmasters online butik',
	'Translate3' => 'Disse fejl skal rettes:',
	'Translate4' => 'Opdater bruger info',
	'Translate5' => 'Her kan du opdatere din bruger information.',
	'Translate6' => 'Tryk på informationen du ønsker at ændre.',
	'Translate7' => 'Brugerinformation',
	'Translate8' => 'Faktureringsadresse',
	'Translate9' => 'Adgangskode',

];