<?php

return [
	'Translate1' => 'Ændre din adgangskode',
	'Translate2' => 'Ændre din adgangskode på Markmasters online butik',
	'Translate3' => 'Opdater din adgangskode',
	'Translate4' => 'Disse fejl skal rettes: ',
	'Translate5' => 'Din Adgangskode:',
	'Translate6' => 'Gammel Adgangskode',
	'Translate7' => 'Adgangskode:',
	'Translate8' => 'Adgangskode',
	'Translate9' => 'Bekræft din adgangskode:',
	'Translate10' => 'Bekræft',
	'Translate11' => 'Opdater adgangskode',

];