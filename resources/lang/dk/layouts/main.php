<?php

return [

	/////////////////////////////////////////////////////
	//  						Header 						//
	/////////////////////////////////////////////////////
	'Homepage' => 'Hjem',
	'Products' => 'Produkter',
	'ShoppingCart' => 'Indkøbskurv',
	'Login' => 'Log ind',
	'User' => 'Bruger',

	/*---- Header dropdown items for Products ----*/
	'Sticker' => 'Klistermærke farve',
	'StickerBW' => 'Klistermærke sort/hvid',
	'Ironon' => 'Strygemærker farve',
	'IrononBW' => 'Strygemærke sort/hvid',
	'Gold' => 'Guld etiket',
	'Silver' => 'Sølv etiket',
	'Transparent' => 'Transparent etiket',
	'Reflection' => 'Refleks etiket',
       'Fabricsticker' => 'Strygefrie navnemærker',

	/*---- Header dropdown items for Login ----*/
	'UserLogin' => 'Log Ind',
	'NewAccount' => 'Ny Bruger',

	/*---- Header dropdown items for User ----*/
	'UserStickers' => 'Mine Etiketter',
	'UserOrders' => 'Mine Bestillinger',
	'UserAccount' => 'Min bruger',
	'Logout' => 'Log ud',

	/////////////////////////////////////////////////////
	//  						Footer	 						//
	/////////////////////////////////////////////////////
	'Social' => 'MEDIER',
	'Help' => 'HJELP',
	'Payment' => 'BETALINGSLØSNINGER',

	/*---- Footer Social list items ----*/
	'Twitter' => 'Twitter',
	'Facebook' => 'Facebook',

	/*---- Footer Help list items ----*/
	'FAQ' => 'Spørgsmål & svar',
	'Video' => 'Video',
	'CommercialAgents' => 'Design Hjælp',
	'Contact' => 'Kontakt Os',
	'Terms' => 'Køpsvilkår',

	'loggedinas' => 'Du er logget ind som: ',

	'orgnumber' => 'org. nr.',
	'address' => 'Legevegen 16 ',
	'address2' => '5542 Karmsund',
	'phone' => 'Tlf. +47 77 69 69 61 ',
	'country' => 'Norge',
];
