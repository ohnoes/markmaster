<?php

return [
	'Translate1' => 'Klistermærke i sølv',
	'Translate2' => 'Opret dine egene sølvfarvede klistermærker. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Klistermærke sølv',
	'Translate4' => 'Etiketter i sølvfarvet vinyl med sort tryk.',
	'Translate5' => 'Brug vores design modul eller uploade et separat 
						design fra dit foretrukne program.  ',
	'Translate6' => 'Perfekt til mærkning af præmier, design produkter mm',
	'Translate7' => 'Valgfri størrelse, antal og form.',
	'Translate8' => 'Levering på ark.',
	'Translate9' => 'Vælg "Nyt design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'NO',

	
	
];