<?php

return [
	'Translate1' => 'Klistermærke med farve',
	'Translate2' => 'Opret din egen farved klistermærke. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Klistermærke med laminatforstærkning',
	'Translate4' => 'Etiketterne er produsert av "Murfolie" der sidder ekstra godt. Hertil kommer at de er overlamineret med en tynd klar film hvilket gør dem særdeles modstandsdygtig over for mekanisk slid og løsningsmiddler.',
	'Translate5' => 'Etiketten tåler opvaskemaskin 60°C.
						Typiske anvendelser omfatter værktøj, drik flasker,
						madkasser, flasker, fotografi, computer og sportsudstyr.
						Den kan også anvendes til mærking af
						sko (klæbes ned i sålen ) eller til klubmærker. ',
	'Translate6' => 'Valgfri størrelse, antal og form.',
	'Translate7' => 'Levering på ark.',
	'Translate8' => 'Vælg "Enkel Navnelapp eller Eget Design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate9' => '',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'DK',
	'Translate13' => 'Navnelapp',
	'Translate14' => '1. Enkel Navnelapp',
	'Translate15' => 'Navnelapp 30x13 mm',
	'Translate16' => '2. Eget Design',
	'Translate17' => 'Valgfri størrelse og form
',




];
