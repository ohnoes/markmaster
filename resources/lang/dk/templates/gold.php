<?php

return [
	'Translate1' => 'Guldfarvet klistermærke',
	'Translate2' => 'Opret din egen guld label. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Klistermærke guld',
	'Translate4' => 'Etiketter i guldfarvet vinyl med sort tryk.',
	'Translate5' => 'Brug vores design modul eller uploade et design af dit foretrukne program. ',
	'Translate6' => 'Perfekt til mærkning af præmier, design produkter mm',
	'Translate7' => 'Valgfri størrelse, antal og form.',
	'Translate8' => 'Leveres på ark.',
	'Translate9' => 'Vælg "Nyt design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'NO',

	
	
];