<?php

return [
	'Translate1' => 'Klistermærke med refleks',
	'Translate2' => 'Opret dine egene klistermærker med refleks. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Klistermærke refleks',
	'Translate4' => 'Nu kan du oprette dine egne refleks mærker
						med valgfri farve.
						Du kan selv bestemme størrelsen. ',
	'Translate5' => 'Mærk for eksempel cykler med en
						personlig etiket som giver ekstra 
						synlighed i mørke. ',
	'Translate6' => 'Valgfri størrelse, antal og form.',
	'Translate7' => 'Levering på ark.',
	'Translate8' => 'Vælg "Nyt design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate9' => '',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'NO',

	
	
];