<?php

return [
	'Translate1' => 'Strygemærke sort/hvid',
	'Translate2' => 'Opret dine egene strygemærker. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Strygemærke sort/hvid',
	'Translate4' => 'Vores bestseller gennem alle år. 
						For let mærkning af alle former for tøj, uld, handsker mv. 
						Nemt monteret med jern og kan vaskes op tom 95°C',
	'Translate5' => 'Fast størrelse 30 x 10 mm',
	'Translate6' => 'Levering på rulle.',
	'Translate7' => 'Vælg "Nyt design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate8' => '',
	'Translate9' => '',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'NO',

	
	
];