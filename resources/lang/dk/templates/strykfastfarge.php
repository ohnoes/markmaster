<?php

return [
	'Translate1' => 'Strygemærke med farve',
	'Translate2' => 'Opret dine egene farvede strygemærker. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Strygemærke farve',
	'Translate4' => 'Etiketterne er produceret af et materiale som
						stryges fast på tekstiler med normal strygejern. ',
	'Translate5' => 'Korrekt monteret så tolererer mærket 60°C vask.
						Brug vores design modul eller uploade et separat 
						design fra dit foretrukne program. ',
	'Translate6' => 'Perfekt til mærkning af tøj og små serier
						med transfermærker for clublogo.',
	'Translate7' => 'Valgfri størrelse, antal og form.',
	'Translate8' => 'Levering på ark.',
	'Translate9' => 'Vælg "Nyt design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'NO',

	
	
];