<?php

return [
	'Translate1' => 'Klistermærke sort/hvid',
	'Translate2' => 'Opret din egen klistermærke. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Klistermærke sort/hvid',
	'Translate4' => 'Selvklæbende hvid etiket med sort tryk. Er en billig og god mulighed. Fremstillet af øko-venligt materiale. ',
	'Translate5' => 'Egnet til mærkning af drikkevareflasker, bøger, salgsvarer f.eks homepartiesprodukter, kosmetik mm. Tåler opvaskemaskin 60°C',
	'Translate6' => '2 størrelser  37x16 mm og 60x26 mm',
	'Translate7' => 'Leveret på rulle.',
	'Translate8' => 'Vælg "Nyt design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate9' => '',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'NO',

	
	
];