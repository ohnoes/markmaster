<?php

return [
	'Translate1' => 'Strygefrie navnemærker',
	'Translate2' => 'Lav dit eget farvet tøjklistermærke. Start på et nyt design eller baseret på en av Markmasters skabeloner',
	'Translate3' => 'Strygefrie navnemærker',
	'Translate4' => 'Strygefrie navnelapper som let kan klistres på alt af tøj som har vaskeanvisning',
	'Translate5' => 'Passer også godt til mærking af andre ejendele da de fester godt på de fleste materialer. ',
	//
	'Translate6' => 'Valgfri størrelse, antal og form.',
	'Translate7' => 'Leveres på ark.',
	'Translate8' => 'Velg "Nyt Design" for å starte et design fra null, eller vælge en af vores skabeloner til at hjælpe dig starte Design.',
	'Translate9' => '',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Starte et nyt design',
	'Translate12' => 'NO',
	//
	'Translate13' => 'Strygefrie navnemærkene tåler 40 grader vaskemaskine og opvaskemaskine hvis de er korrekt monteret.',
	'Translate14' => 'Bruksanvisning for brug af strygefrie navnelapper på tøj: Mærket skal klistres på vaskeanvisningen og masseres godt ind, det
								er særlig viktigt at trykke godt på hjørner og kanter. Vente 24 t før tøjet vaskes.',
	'Translate15' => 'Til tøj som skal vaskes ved en højere temperatur end 40 grader eller ikke har vaskeanvisning, så anbefaler vi vores strygemærker.',



];
