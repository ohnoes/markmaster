<?php

return [
	'Translate1' => 'Guld eller sølvfarvet klistermærke',
	'Translate2' => 'Opret din egen guld label. Start på et nyt design eller tage utganskpunkt fra en af Mark Masters skabeloner',
	'Translate3' => 'Klistermærke guld eller sølv',
	'Translate4' => 'Etiketter i guld eller sølvfarvet vinyl med sort tryk.',
	'Translate5' => 'Brug vores design modul eller uploade et design af dit foretrukne program. ',
	'Translate6' => 'Perfekt til mærkning af præmier, design produkter mm.',
	'Translate7' => 'Valgfri størrelse, antal og form.',
	'Translate8' => 'Leveres på ark.',
	'Translate9' => 'Vælg "Nyt design" for at starte et design fra nul, eller vælge en af vores skabeloner til at hjælpe dig starte design.',
	'Translate10' => 'Nyt Design',
	'Translate11' => 'Start et nyt design',
	'Translate12' => 'DK',
	'Translate13' => 'Klistermærke guld',
	'Translate14' => 'Klistreetikett i guldfarvet 
								folie. Velegnet til mærkning af præmier og lignende',
	'Translate15' => 'Klistermærke sølv',
	'Translate16' => 'Klistreetikett i sølvfarvet 
								folie. Velegnet til mærkning af præmier og lignende',

	
	
];