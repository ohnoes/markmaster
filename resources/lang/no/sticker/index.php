<?php

return [
	'Translate1' => 'Dine etiketter',
	'Translate2' => 'Dine tidligere designet etiketter. Klikk på etikettene for å kunne bestille de om igjen.',
	'Translate3' => 'Dine Etiketter',
	'Translate4' => 'URL som kan deles med dine venner  (klikk på for å bruke som mal)',
	'Translate5' => 'Etikett',
	'Translate6' => 'Type',
	'Translate7' => 'Klisteretikett farge, med laminat',
	'Translate8' => 'Strykfastetikett farge',
	'Translate9' => 'Klistreetikett i gullfarget vinyl',
	'Translate10' => 'Klistreetikett i sølvfarget vinyl',
	'Translate11' => 'Klistreetikett i transparent vinyl',
	'Translate12' => 'Klistreetikett i sort/hvit',
	'Translate13' => 'Strykfastetikett',
	'Translate14' => 'Klistreetikett i refleks',
	'Translate15' => 'Størrelse',
	'Translate16' => 'Tøyklistremerke'
	
];