<?php
// resources/lang/no/routes.php
return [
    'accountfound'    => 'E-post adressen er allerede knyttet til en bruker, vennligst logg inn.',
    'newaccountmade'  => 'Ny bruker er opprettet. Middlertidig innloggingsinformasjon er: ',
    'newaccountmade2' => 'Vi anbefaler at du endrer ditt innloggings passord.',
    // other routes name
];
