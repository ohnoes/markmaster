<?php

return [
	'Translate1' => 'Markmaster spørsmål og svar',
	'Translate2' => 'Her er en oversikt over vanlig stilte spørsmål og svar',
	'Translate3' => 'Vanlig stilte spørsmål og svar: ',
	'Translate4' => 'Tåler strykefastetikettene bruk av tørketrommel?',
	'Translate5' => 'Ja, riktig montert skal de tåle tørketrommel. Om de løsner er det et resultat av for lav monteringstemperatur eller for kort 
							presstid. Etiketten skal delvis smelte ned i tekstilet ved montering.',
	'Translate6' => 'Kan etikettene strykes på ulltøy?',
	'Translate7' => 'Ja, ull tåler fint monteringen temperaturmessig. Monteringen på ullsokker eller -votter gjøres best ved å montere etiketten 
							på høykant slik at den ikke hindrer elastisiteten i vrangborden.',
	'Translate8' => 'Hvorfor blir skriften utydelig etter påstrykning?',
	'Translate9' => 'Som oftest skyldes dette en kombinasjon av for høy temperatur og bevegelse med strykjernet under presstiden. 
								Strykjernet skal holdes helt i ro under presstiden, og prøv gjerne med noe lavere temperatur og/ eller kortere presstid.',
	'Translate10' => 'Kan etikettene monteres ved lavere temperatur enn tre prikker på strykjernet da plagget ikke tåler så høy temperatur?',
	'Translate11' => 'Ja, etiketten fester godt ved to prikker, men den vil da ikke tåle kokvask. Det har imidlertid ingen betydning da plagget heller ikke tåler det.',
	'Translate12' => 'Hvor lang leveringstid er det på etikettene?',
	'Translate13' => ' Etikettene går vanligvis i produksjon dagen etter bestilling, og de blir da pakket og sendt så snart de er ferdig. 
								De blir sendt som brev, A-post, og litt avhengig av postgangangen vil de komme fram til kunden 1-2 dager etter 
								levering fra oss. HUSK! Det er viktig å sjekke at man har oppgitt rett leveringsadresse på kundeopplysningene. ',
	'Translate14' => 'Kan jeg endre bestillingen?',
	'Translate15' => 'Antall og str. kan ikke endres etter at bestillingen er gjennomført og betalt, men korrigering av feilstaving o.l er mulig helt fram til produksjonsstart. ',
	'Translate16' => 'Hvilke betalingsløsninger har dere?',
	'Translate17' => 'Vi har online-betaling med kort. Kreditt ( faktura ) kan tilbys til kunder etter kredittsjekk, og som kan motta EHF-faktura. Ta kontakt for mer info om dette.',
	'Translate18' => 'Er det mulig å få etikettene levert til et annet land enn Norge?',
	'Translate19' => 'Ja, det går helt fint å ha leveringsadresse i et annet land enn Norge. 
								Men vi har ikke eksportsalg, så vi trekker ikke fra norsk moms når varene skal leveres til utlandet. 
								OBS! Det kan også påløpe kostnader ved innfortolling til nytt land, dette for kundens regning.',
	'Translate20' => 'Jeg er ikke fornøyd med etikettene jeg har mottatt, kan de returneres?',
	'Translate21' => 'Ved produksjonsfeil av teknisk art erstattes etikettene fullt ut. Om det er stavefeil eller ugunstig 
								layout fra kundens side vil vi refundere beløp i henhold til gjeldende regler for personlig tilpassede 
								produkter, vanligvis 50 % av kjøpesummen.',


];