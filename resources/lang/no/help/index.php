<?php

return [
	'Translate1' => 'Markmasters avdeling for hjelp',
	'Translate2' => 'Her er en liste over ting vi kan hjelpe deg med',
	'Translate3' => 'Hjelp',
	'Translate4' => 'Spørsmål & svar',
	'Translate5' => 'Video',
	'Translate6' => 'Design hjelp',
	'Translate7' => 'Kjøpsvilkår',
	'Translate8' => 'Kontakt oss',

];