<?php

return [
	'Translate1' => 'Kontakt Markmaster',
	'Translate2' => 'Her kan du kontakte Markmaster om du har noen spørsmål eller ønsker å gi oss noen tilbakemeldinger',
	'Translate3' => 'Kontakt oss',
	'Translate4' => 'Har du spørsmål eller ønsker å gi oss noen tilbakemeldinger? Her er noen kanaler hvor du kan kontakte oss:',
	'Translate5' => 'Epost: ',
	'Translate6' => 'Telefon:  77 69 69 61',

];