<?php

return [
	'Translate1' => 'Markmasters Kjøpsvilkår',
	'Translate2' => 'Her er Markmasters  kjøpsvilkår',
	'Translate3' => 'VILKÅR',
	'Translate4' => 'Vilkårene gjelder kundens bruk og kjøp av alle varer og tjenester
								levert av Markmaster AS',
	'Translate5' => 'PRISER OG LEVERING',
	'Translate6' => 'Kunden vil få en automatisk ordrebekreftelse på mail i det Markmaster
							AS har mottatt bestillingen, og kunden plikter å sjekke at denne
							er i overensstemmelse med bestillingen hva angår produkttype, innhold,
							antall og pris.
							Alle priser er oppgitt inkl. MVA. 29,- for frakt/eksp. tilkommer.
							Varene vil bli levert innen oppgitt leveringstid, vanligvis 2-5
							arbeidsdager. Herunder regnes ikke helg- og helligdager.',
	'Translate7' => 'REKLAMASJON OG ANGREFRIST',
	'Translate8' => 'Markmaster AS er pliktig til å rette opp mangel på produkt dersom
							kunden innen rimelig tid gir tilbakemelding om dette.
							Markmaster AS sine produkter er personlig tilvirket, det er derfor
							ingen angrerett på produktene etter at de er produsert.
							Kunden er selv ansvarlig for å sjekke at deres bestillinger ikke
							inneholder stave-, tegnsettings- eller grammatiske feil.
							Markmaster AS undersøker ikke kundens bestillinger for feil før
							produktet går i bestilling, bestillingen går direkte fra web til
							print.',
	'Translate9' => 'OPPHAVSRETT',
	'Translate10' => 'Formidler kunden et eget motiv eller utøver annen innflytelse på
							produktet forsikrer kunden overfor Markmaster AS at tekst og motiv
							ikke heftes av tredjeparts rettigheter.
							Evt. opphavs-, person- eller navnerettihetskrenkelser blir i så fall
							kunden å laste.
							Dessuten forsikrer kunden at den gjennom individualiseringen av
							produktet ikke krenker andre tredjeparts rettigheter.
							Kunden fritar Markmaster AS fra alle fordringer og krav som blir
							gjordt gjeldende grunnet krenkelse av slike tredjeparts rettigheter,
							såframt kunden må forsvare pliktkrenkelsen.
							Kunden erstatter Markmaster AS alle oppstående forsvarsomkostinger og
							andre skader.',
	'Translate11' => 'ENDRING I VILKÅRENE',
	'Translate12' => 'Markmaster AS forbeholder seg retten til å endre nærværende vilkår,
		herunder som følge av endringer i lovgivningen.',

];