<?php
// resources/lang/no/routes.php
return [
    'private'         => 'privat',
    'company'         => 'bedrift',

    'home'            => '/',

    'tempsticker'     => 'etiketter-og-navnelapper/klistremerker/forslag',
    'tempironc'       => 'etiketter-og-navnelapper/strykfast-farge/forslag',
    'tempgold'        => 'etiketter-og-navnelapper/klistremerker-gull/forslag',
    'tempsolv'        => 'etiketter-og-navnelapper/klistremerker-solv/forslag',
    'temptrans'       => 'etiketter-og-navnelapper/klistremerker-transparent/forslag',
    'tempcheap'       => 'etiketter-og-navnelapper/billige/forslag',
    'goldorsolv'      => 'etiketter-og-navnelapper/gull-eller-solv/forslag',
    'tempiron'        => 'etiketter-og-navnelapper/strykfast/forslag',
    'tempreflection'  => 'etiketter-og-navnelapper/refleksmerker/forslag',
    'tempironfree'    => 'etiketter-og-navnelapper/strykefrie/forslag',

    'namesticker' => 'navnelapper/klistremerker',
    'nameironc' => 'navnelapper/strykfast-farge',
    'namegold' => 'navnelapper/klistremerker-gull',
    'namesolv' => 'navnelapper/klistremerker-solv',
    'nametrans' => 'navnelapper/klistremerker-transparent',
    'namecheap' => 'navnelapper/billige',
    'nameiron' => 'navnelapper/strykfast',
    'namereflection' => 'navnelapper/refleksmerker',
    'nameironfree' => 'navnelapper/strykefrie',
    'namebase' => 'navnelapper/base',
    'namecart' => 'navnelapper/addtocart',

    'merkclipart' => 'merkelapper/clipartget',
    'merkbackground' => 'merkelapper/backgroundget',
    'merkactive' => 'merkelapper/stayactive',
    'merksticker' => 'merkelapper/klistremerker',
    'merkironc' => 'merkelapper/strykfast-farge',
    'merkgold' => 'merkelapper/klistremerker-gull',
    'merksolv' => 'merkelapper/klistremerker-solv',
    'merktrans' => 'merkelapper/klistremerker-transparent',
    'merkcheap' => 'merkelapper/billige',
    'merkiron' => 'merkelapper/strykfast',
    'merkreflection' => 'merkelapper/refleksmerker',
    'merkironfree' => 'merkelapper/strykefrie',
    'merkbase' => 'merkelapper/base',
    'merkcart' => 'merkelapper/addtocart',

    'help' => 'help/',
    'helpdesign' => 'help/design',
    'helpfaq' => 'help/faq',
    'helpvideo' => 'help/video',
    'helpcontact' => 'help/contact',
    'helpconditions' => 'help/conditions',

    // category
    'catfabric' => 'toymerker',
    'catstick' => 'klistremerker',
    'catironfree' => 'etiketter-og-navnelapper/strykefrie',
    'catirononcolor' => 'etiketter-og-navnelapper/strykfast-farge',
    'catironon' => 'etiketter-og-navnelapper/strykfast',
    'catsticker' => 'etiketter-og-navnelapper/klistremerker',
    'catcheapsticker' => 'etiketter-og-navnelapper/sort-hvite-klistremerker',
    'catreflection' => 'etiketter-og-navnelapper/refleksmerker',
    'catgold' => 'etiketter-og-navnelapper/klistremerker-gull',
    'catsilver' => 'etiketter-og-navnelapper/klistremerker-solv',
    'cattransparent' => 'etiketter-og-navnelapper/klistremerker-transparent',
    'catcontroll' => 'kontrollmerker',
    'catbeer' => 'oletiketter',
    'catforever' => 'foreverliving-merker',



    // other routes name
];
