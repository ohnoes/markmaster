<?php

return [
	'Translate1' => 'Dine bestillinger hos Markmaster',
	'Translate2' => 'Dine bestillinger',
	'Translate3' => 'Følgende feil må rettes:',
	'Translate4' => 'Dine Bestillinger',
	'Translate5' => 'Bestillingsnummer',
	'Translate6' => 'Bestillingsdato',
	'Translate7' => 'Leverings adresse',
	'Translate8' => 'Status',
	'Translate9' => 'Pris',
	'Translate10' => 'Klar for betaling',
	'Translate11' => 'Betalt',
	'Translate12' => 'Eksportert',
	'Translate13' => 'Kansellert',
	'Translate14' => 'Hold',
	'Translate15' => 'N/A',
	'Translate16' => 'Info',
	'Translate17' => 'Betal',
	'Translate18' => 'Faktura',
	'Translate19' => 'Slett',
	'Translate20' => 'Er du sikker på at du ønsker å slette bestillingen?',
	'Translate21' => 'Klar for eksport',
	
	
];