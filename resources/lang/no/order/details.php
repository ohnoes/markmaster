<?php

return [
	'Translate1' => 'Din bestilling hos Markmaster',
	'Translate2' => 'Orderinfo',
	'Translate3' => 'URL link til etikettdesign  (klikk på for å bruke som mal)',
	'Translate4' => 'Etikett',
	'Translate5' => 'Type',
	'Translate6' => 'Størrelse mm',
	'Translate7' => 'Antall',
	'Translate8' => 'Pris inkl. mva',
	'Translate9' => 'Link',
	'Translate10' => 'Klisteretikett farge, med laminat',
	'Translate11' => 'Strykfastetikett farge',
	'Translate12' => 'Klistreetikett i gullfarget vinyl',
	'Translate13' => 'Klistreetikett i sølvfarget vinyl',
	'Translate14' => 'Klistreetikett i transparent vinyl',
	'Translate15' => 'Klistreetikett i sort/hvit',
	'Translate16' => 'Strykfastetikett',
	'Translate17' => 'Klistreetikett i refleks',
	'Translate18' => 'Edit',
	'Translate19' => 'Slett',
	'Translate20' => 'Er du sikker på at du ønsker å fjerne etiketten?',
	'Translate21' => 'Tøyklistremerke',
	
];