<?php

return [
	'Translate1' => 'Betaling',
	'Translate2' => 'Leverings og betalingsinformasjon for din bestilling',
	'Translate3' => 'Følgende feil må rettes:',
	'Translate4' => 'Leveringsinformasjon',
	'Translate5' => 'Organisasjonsnavn',
	'Translate6' => 'Fornavn',
	'Translate7' => 'Etternavn',
	'Translate8' => 'Adresse',
	'Translate9' => 'Postkode',
	'Translate10' => 'By',
	'Translate11' => 'Fylke',
	'Translate12' => 'Land: ',
	'Translate13' => 'Fakturainformasjon',
	'Translate14' => 'Fakturainformasjon er det samme som leveringsinformasjon',
	'Translate15' => 'NB! Ved å trykke på Betal bekrefter jeg herved at jeg har lest og akseptert Vilkår og betingelser',
	'Translate16' => 'Betal',
	'Translate17' => 'VILKÅR',
	'Translate18' => 'Vilkårene gjelder kundens bruk og kjøp av alle varer og tjenester
		levert av Markmaster AS',
	'Translate19' => 'PRISER OG LEVERING',
	'Translate20' => 'Kunden vil få en automatisk ordrebekreftelse på mail i det Markmaster
			AS har mottatt bestillingen, og kunden plikter å sjekke at denne
			er i overensstemmelse med bestillingen hva angår produkttype, innhold,
			antall og pris.
			Alle priser er oppgitt inkl. MVA og frakt.
			Varene vil bli levert innen oppgitt leveringstid, vanligvis 2-5
			arbeidsdager. Herunder regnes ikke helg- og helligdager.',
	'Translate21' => 'REKLAMASJON OG ANGREFRIST',
	'Translate22' => 'Markmaster AS er pliktig til å rette opp mangel på produkt dersom
			kunden innen rimelig tid gir tilbakemelding om dette.
			Markmaster AS sine produkter er personlig tilvirket, det er derfor
			ingen angrerett på produktene etter at de er produsert.
			Kunden er selv ansvarlig for å sjekke at deres bestillinger ikke
			inneholder stave-, tegnsettings- eller grammatiske feil.
			Markmaster AS undersøker ikke kundens bestillinger for feil før
			produktet går i bestilling, bestillingen går direkte fra web til
			print.',
	'Translate23' => 'OPPHAVSRETT',
	'Translate24' => 'Formidler kunden et eget motiv eller utøver annen innflytelse på
			produktet forsikrer kunden overfor Markmaster AS at tekst og motiv
			ikke heftes av tredjeparts rettigheter.
			Evt. opphavs-, person- eller navnerettihetskrenkelser blir i så fall
			kunden å laste.
			Dessuten forsikrer kunden at den gjennom individualiseringen av
			produktet ikke krenker andre tredjeparts rettigheter.
			Kunden fritar Markmaster AS fra alle fordringer og krav som blir
			gjordt gjeldende grunnet krenkelse av slike tredjeparts rettigheter,
			såframt kunden må forsvare pliktkrenkelsen.
			Kunden erstatter Markmaster AS alle oppstående forsvarsomkostinger og
			andre skader.',
	'Translate25' => 'ENDRING I VILKÅRENE',
	'Translate26' => 'Markmaster AS forbeholder seg retten til å endre nærværende vilkår,
			herunder som følge av endringer i lovgivningen.',
	'Translate27' => 'Betal',
	
];