<?php

return [
	'Translate1' => 'Transparente klistremerker',
	'Translate2' => 'Lag dine egene gjennomsiktige klistremerker og navnelapper. Start på et nytt design eller ta utganskpunk fra en av Markmasters maler',
	'Translate3' => 'Klistremerker transparent',
	'Translate4' => 'Transparente klistremerker og navnelapper er merker som er trykket på gjennomsiktig folie.',
	'Translate5' => 'Trykket er i sort, og du kan selv velge om du vil bruke vår designmodul eller laste opp egne design.
                                    Størrelse, antall og form er også valgfritt.',
	'Translate6' => 'Bruksområdet er stort, etikettene passer godt der motivet ikke skal være for massivt, og om man vil ha en litt diskret merking.',
	'Translate7' => 'Typiske bruksområder kan være vinduer, premier o.l av glass,mobil, pc, ipad, ipod, mac, klokker, briller, sminkesaker, tannbørster, kulepenner m.m',
	'Translate8' => 'Det har stor betydning hvilken farge det er på bakgrunnen merkene skal sitte på.
                                    Om bakgrunnen er hvit eller blank ( glass ) er det helt uten betydning.
                                    Setter du derimot merkene på en sort eller mørk bakgrunn vil du nesten ikke kunne se trykket da det forsvinner mot den mørke overflaten.',
	'Translate9' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Gjennomsiktige/Transparente Klistremerker',
	'keywords' => 'Transparente, gjennomsiktige, Klistremerker, navnelapper, merkelapper',
	'imagealt' => 'Gjennomsiktige/Transparente Klistremerker og navnelapper',
	'Description-transparent-1' => 'Transparente klistremerker og navnelapper er merker som er trykket på gjennomsiktig folie.',
    'Description-transparent-2' => 'Trykket er i sort, og du kan selv velge om du vil bruke vår designmodul eller laste opp egne design.
                                    Størrelse, antall og form er også valgfritt.',
    'Description-transparent-3' => 'Bruksområdet er stort, etikettene passer godt der motivet ikke skal være for massivt, og om man vil ha en litt diskret merking.',
    'Description-transparent-4' => 'Typiske bruksområder kan være vinduer, premier o.l av glass,mobil, pc, ipad, ipod, mac, klokker, briller, sminkesaker, tannbørster, kulepenner m.m',
    'Description-transparent-5' => 'Det har stor betydning hvilken farge det er på bakgrunnen merkene skal sitte på.
                                    Om bakgrunnen er hvit eller blank ( glass ) er det helt uten betydning.
                                    Setter du derimot merkene på en sort eller mørk bakgrunn vil du nesten ikke kunne se trykket da det forsvinner mot den mørke overflaten.',



];
