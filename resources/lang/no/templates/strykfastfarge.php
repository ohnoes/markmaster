<?php

return [
	'Translate1' => 'Strykfast-etikett med farge. Navnelapper som strykes fast på tekstiler',
	'Translate2' => 'Lag dine egene fargede strykfast-etikett. Strykfastetikett farge er en myk og holdbar etikett som skal strykes fast på tekstiler og tåler 60 graders vask.',
	'Translate3' => 'Strykfastetikett farge',
	'Translate4' => 'Strykfastetikett farge er en myk og holdbar etikett som skal strykes fast på tekstiler og tåler 60 graders vask. Dette er en enkel og rimelig måte å merke tøy på .
										Produktet irriterer ikke huden, og inneholder ikke Phtalater eller PVC.',
	'Translate5' => 'De kan brukes til alle slags tekstiler og tøy, og kunden bestemmer selv form, antall og størrelse.',
	'Translate6' => 'Eksempler på hva man kan merke er jakker, luer , votter, gensere, bukser, gummistøvler, regntøy o.l
										Produktet er også perfekt til lag og foreninger som ønsker å merke f.eks t-shirt med egen logo.
										Bedrifter som vil merke arbeidstøy vil enkelt kunne bruke vårt designverktøy i nettbutikken for å laste opp egen design. Vi i Markmaster er også behjelpelig med tilrettelegging av dette.',
	'Translate7' => 'Barnetøy bør merkes med telefonnummer slik at det er enkelt å finne fram til rette eier.
										Det kan være en god ide å la barnet selv få velge symbolet eller figuren som skal være med på navnelappen. Dette for at de lett skal kunne kjenne igjen sine ting selv om de ikke har lært seg å lese.
										Mange barn synes også at det er morsomt om man laster opp et bilde av de selv til å ha på navnelappen.',
	'Translate8' => 'Om navnelappene er riktig montert vil de smelte ned i tøyet og er derfor veldig vanskelig å fjerne igjen. Men mange ønsker å gi bort barnetøy når det er blitt
										for smått til egne barn, og da kan man gjøre merket mindre synlig ved å legge på et nytt merke uten tekst.',
	'Translate9' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Strykefast etiketter i farge',
	'keywords' => 'Strykfastetikett farge, navnelapper, merkelapper',
	'imagealt' => 'Fargede Strykfastetiketter og navnelapper',
	'Description-iron-color-1' => 'Strykfastetikett farge er en myk og holdbar etikett som skal strykes fast på tekstiler og tåler 60 graders vask. Dette er en enkel og rimelig måte å merke tøy på .
										Produktet irriterer ikke huden, og inneholder ikke Phtalater eller PVC.',
	'Description-iron-color-2' => 'De kan brukes til alle slags tekstiler og tøy, og kunden bestemmer selv form, antall og størrelse.',
	'Description-iron-color-3' => 'Eksempler på hva man kan merke er jakker, luer , votter, gensere, bukser, gummistøvler, regntøy o.l
										Produktet er også perfekt til lag og foreninger som ønsker å merke f.eks t-shirt med egen logo.
										Bedrifter som vil merke arbeidstøy vil enkelt kunne bruke vårt designverktøy i nettbutikken for å laste opp egen design. Vi i Markmaster er også behjelpelig med tilrettelegging av dette.',
	'Description-iron-color-4' => 'Barnetøy bør merkes med telefonnummer slik at det er enkelt å finne fram til rette eier.
										Det kan være en god ide å la barnet selv få velge symbolet eller figuren som skal være med på navnelappen. Dette for at de lett skal kunne kjenne igjen sine ting selv om de ikke har lært seg å lese.
										Mange barn synes også at det er morsomt om man laster opp et bilde av de selv til å ha på navnelappen.',
	'Description-iron-color-5' => 'Om navnelappene er riktig montert vil de smelte ned i tøyet og er derfor veldig vanskelig å fjerne igjen. Men mange ønsker å gi bort barnetøy når det er blitt
										for smått til egne barn, og da kan man gjøre merket mindre synlig ved å legge på et nytt merke uten tekst.',


];
