<?php

return [
	'Translate1' => 'Laminerte Klistremerker med farge. Bestill slitesterke navnelapper her.',
	'Translate2' => 'Lag dine egene fargede klistremerker. Klistremerker med laminatforsterkning er produsert av «murfolie» som sitter ekstremt godt.
                                        I tillegg har vi laminert etikettene med en tynn klar film som gjør at de er ekstremt holdbare. Design dine navnelapper her!',
	'Translate3' => 'Laminerte Klistremerker',
	'Translate4' => 'Klistremerker med laminatforsterkning er produsert av «murfolie» som sitter ekstremt godt.
                                        I tillegg har vi laminert etikettene med en tynn klar film som gjør at de er ekstremt holdbare.
                                        De er meget motstandsdyktige mot mekanisk slitasje og løsningsmidler.
                                        Disse navnelappenen er av absolutt beste kvalitet og leveres til markedets laveste priser.
                                        Etikettene har runde hjørner, noe som er en fordel fordi de sitter bedre ettersom kantene ikke løsner så lett.',
	'Translate5' => 'Klistremerkene passer like godt både til utendørs og innendørs bruk, og den tåler 60 graders oppvaskmaskinvask.
	Etiketten har mange bruksområder, det kan være til privatpersoner, firma, lag eller foreninger som har bruk for klistremerker.
                                        Om du vil profilere din bedrift eller eller vil drive med merkesvarebygging er våre etiketter helt perfekt.
                                        Andre typiske bruksområder for vår klistreetikett vil være navnelapper, verktøy, flasker, matbokser, bøker, foto, data og sportsutstyr, postkasseetiketter o.l',
	'Translate6' => 'Valgfri størrelse, antall og form.',
	'Translate7' => 'Leveres på ark.',
	'Translate8' => 'Velg "Enkel Navnelapp eller Egen Design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate9' => '',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Translate13' => 'Navnelapp',
	'Translate14' => 'Enkel Navnelapp',
	'Translate15' => 'Navnelapp 30x13 mm. Her kan du lage din navnelapp ved å fylle ut et skjema. Enklere blir det ikke!',
	'Translate15-1' => 'Navnelapp 30x10 mm. Her kan du lage din navnelapp ved å fylle ut et skjema. Enklere blir det ikke!',
	'Translate15-2' => 'Navnelapp 37x16 mm. Her kan du lage din navnelapp ved å fylle ut et skjema. Enklere blir det ikke!',
	'Translate16' => 'Åpen Design',
	'Translate17' => 'Med Markmasters designverktøy kan du selv komponere dine klistremerker. Her har du valgfri form og størrelse, og du kan selv posisjonere elementene på etiketten.',
	'Translate18' => 'Forslag',
	'Translate19' => 'Trenger du litt starthjelp til ditt design, eller forslag til hva du kan bruke klistremerkene til? Her finner du gode utgangspunkt for dine etiketter',

	'Description-sticker-color-1' => 'Klistreetikett med laminatforsterkning er produsert av «murfolie» som sitter ekstremt godt.
                                        I tillegg har vi laminert etikettene med en tynn klar film som gjør at de er ekstremt holdbare.
                                        De er meget motstandsdyktige mot mekanisk slitasje og løsningsmidler.
                                        Disse navnelappenen er av absolutt beste kvalitet og leveres til markedets laveste priser.
                                        Etikettene har runde hjørner, noe som er en fordel fordi de sitter bedre ettersom kantene ikke løsner så lett.',
    'Description-sticker-color-2' => 'Klistremerkene passer like godt både til utendørs og innendørs bruk, og den tåler 60 graders oppvaskmaskinvask.',
    'Description-sticker-color-3' => 'Etiketten har mange bruksområder, det kan være til privatpersoner, firma, lag eller foreninger som har bruk for klistremerker.
                                        Om du vil profilere din bedrift eller eller vil drive med merkesvarebygging er våre etiketter helt perfekt.
                                        Andre typiske bruksområder for vår klistreetikett vil være verktøy, flasker, matbokser, bøker, foto, data og sportsutstyr, postkasseetiketter o.l',
    'Description-sticker-color-4' => 'Våre små ovale etiketter passer godt til merking av sminkeutstyr, penner, mobiltelefoner m.m
                                        Etikettene kan også brukes for å merke sko (klistres nede i sålen)
                                        Ølbrygging er populært, og hos Markmaster kan du designe dine egne øl-etiketter.
                                        I vår nettbutikk kan du også bestille kontrollmerker,godkjenningsmerker og -oblat, det ligger ferdige maler du kan bruke som utgangspunkt og så laste opp egen logo.
                                        Og hva med å designe din egen postkasseetikett?',
    'Description-sticker-color-5' => 'Det finnes flere designmuligheter i vår nettbutikk, og kunden selv velger fritt størrelse, antall og form.
                                        Du kan velge farge på tekst og bakgrunn, og vi har mange skrifttyper å velge mellom.
                                        I vår symbol/figur-bank vil du helt sikkert finne noe du liker.
                                        Og naturligvis er det mulig å laste opp egne bilder og logoer, eller et helt ferdig design.
                                        Når det gjelder montering av merkene er dette svært enkelt, det eneste man må huske på er at underlaget må være rent og tørt.',
	'keywords' => 'Klistremerker, navnelapper, merkelapper',
	'imagealt' => 'Laminerte Klistremerker og navnelapper',
	'Header' => 'Forslag',
	'Title' => 'Laminerte Klistremerker'


];
