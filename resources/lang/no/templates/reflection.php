<?php

return [
	'Translate1' => 'Refleksmerker. Bli sett, bestill Markmasters refleksmerker og navnelapper her!',
	'Translate2' => 'Lag dine egene klistremerker med refleks. Markmasters refleksetiketter er trykket på en selvklebende folie som har  refleksegenskaper.
                                    Vi var de første på markedet med denne type etikett der kunden selv kunne velge utformingen.
                                    Merkene er i et mykt materiale som former seg etter underlaget',
	'Translate3' => 'Refleksmerker',
	'Translate4' => 'Markmasters refleksetiketter er trykket på en selvklebende folie som har  refleksegenskaper.
                                    Vi var de første på markedet med denne type etikett der kunden selv kunne velge utformingen.
                                    Merkene er i et mykt materiale som former seg etter underlaget',
	'Translate5' => 'De er velegnet til å merke f.eks sykler, sykkelhjelmer, støvler, sko, ransel, regntøy osv
                                    Personlig etikett som gir ekstra synlighet i mørket, la barna være med å bestemme hva som skal stå på etiketten.
                                    Kanskje vil de ha med et bilde av seg selv eller yndlingsdyret sitt? Dette gjør at de enkelt finner igjen f.eks sykkelhjelmen sin
                                    når de skal sykle hjem fra skole eller trening',
	'Translate6' => 'Valgfritt fargetrykk, størrelse, antall og form
                                    Men husk at små merker alene ikke gir god nok synlighet i mørket.',
	'Translate7' => 'Gode råd for refleksbruk:
                                    De fleste barn befinner seg i tettbebygde strøk der bilistene bruker nærlys,refleksene bør derfor henge lavt, helst fra knehøyde og ned, for best synlighet.
                                    Jo flere reflekser man har på seg jo bedre er det.
                                    Man bør ha reflekser på hver side av kroppen slik at man vises godt fra begge sider når man krysser veien.
									Refleksene blir slitt etter hvert og bør derfor byttes ut hver sesong eller så snart det kommer riper på de.
								                                    Husk at refleksmerker ikke erstatter annet sikkerhetsutstyr som lys og pedalrefleks på sykkel.
								                                    Bli sett, bruk refleks!',
	'Translate8' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate9' => '',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Refleksmerker',
	'keywords' => 'Refleksmerker, Refleks, Klistremerker, navnelapper, merkelapper',
	'imagealt' => 'Refleksmerker Klistremerker og navnelapper',
	'Description-reflection-1' => 'Markmasters refleksetiketter er trykket på en selvklebende folie som har  refleksegenskaper.
                                    Vi var de første på markedet med denne type etikett der kunden selv kunne velge utformingen.
                                    Merkene er i et mykt materiale som former seg etter underlaget',
    'Description-reflection-2' => 'De er velegnet til å merke f.eks sykler, sykkelhjelmer, støvler, sko, ransel, regntøy osv
                                    Personlig etikett som gir ekstra synlighet i mørket, la barna være med å bestemme hva som skal stå på etiketten.
                                    Kanskje vil de ha med et bilde av seg selv eller yndlingsdyret sitt? Dette gjør at de enkelt finner igjen f.eks sykkelhjelmen sin
                                    når de skal sykle hjem fra skole eller trening',
    'Description-reflection-3' => 'Valgfritt fargetrykk, størrelse, antall og form
                                    Men husk at små merker alene ikke gir god nok synlighet i mørket.',
    'Description-reflection-4' => 'Gode råd for refleksbruk:
                                    De fleste barn befinner seg i tettbebygde strøk der bilistene bruker nærlys,refleksene bør derfor henge lavt, helst fra knehøyde og ned, for best synlighet.
                                    Jo flere reflekser man har på seg jo bedre er det.
                                    Man bør ha reflekser på hver side av kroppen slik at man vises godt fra begge sider når man krysser veien.',
    'Description-reflection-5' => 'Refleksene blir slitt etter hvert og bør derfor byttes ut hver sesong eller så snart det kommer riper på de.
                                    Husk at refleksmerker ikke erstatter annet sikkerhetsutstyr som lys og pedalrefleks på sykkel.
                                    Bli sett, bruk refleks!',



];
