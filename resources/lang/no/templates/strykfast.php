<?php

return [
	'Translate1' => 'Strykfast-etiketter i sort/hvit',
	'Translate2' => 'Lag dine egene strykfastetiketter. Strykfastetiketter er høykvalitets navnelapper som strykes fast på klær og tekstiler.
                                Markmaster har produsert denne typen etiketter siden vår oppstart i Norge i 1988',
	'Translate3' => 'Strykefast sort/hvit',
	'Translate4' => 'Strykfastetiketter er høykvalitets navnelapper som strykes fast på klær og tekstiler.
                                Markmaster har produsert denne typen etiketter siden vår oppstart i Norge i 1988, den har vært vår bestseller i mange år og er ennå veldig populær.
								Navnelappene er i sort/hvit, standardstørrelse er 30x10 mm og de leveres på rull.
							    For de som ønsker en rimelig og litt disket merking er dette en perfekt etikett.',
	'Translate5' => 'Strykfastetikettene passer til alle slags klær og tekstil, også ulltøy.
                                Den er meget god til å merke små plagg så som votter, sokker, luer o.l
                                Etiketten er meget enkel å montere, den strykes direkte på plagget og skal smelte ned i tøyfibrene.
                                Bruksanvisning og strykebeskyttelse følger med i forpakningen.
								Arket som følger med som strykebeskyttelse er forøvrig vanlig bakepapir, det er viktig av man ikke forsøker å bruke matpapir da dette vil klebe seg fast i etikettene.',
	'Translate6' => 'Riktig montert er strykfastetikettene meget holdbare, de skal tåle 90 graders vask og også tørketrommel.
								Ettersom navnelappene smelter ned i tøyet er det ikke mulig å fjerne de om man senere ønsker det.
								Alternativet er at man stryker på nøytrale etiketter over de gamle for å blokkere ut navnet.
								Om man vil ha etiketter for å merke tøy til forskjellige institusjoner, klær til barnehage og SFO, er strykfastetikettene det mest opplagte og beste valget.',
	'Translate7' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate8' => '',
	'Translate9' => '',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Strykefast etiketter',
	'keywords' => 'Strykefast, Strykfast-etiketter, navnelapper, merkelapper',
	'imagealt' => 'Strykefast etiketter og navnelapper',
	'Description-iron-1' => 'Strykfastetiketter er høykvalitets navnelapper som strykes fast på klær og tekstiler.
                                Markmaster har produsert denne typen etiketter siden vår oppstart i Norge i 1988, den har vært vår bestseller i mange år og er ennå veldig populær.',
    'Description-iron-2' => 'Navnelappene er i sort/hvit, standardstørrelse er 30x10 mm og de leveres på rull.
                                For de som ønsker en rimelig og litt disket merking er dette en perfekt etikett.',
    'Description-iron-3' => 'Strykfastetikettene passer til alle slags klær og tekstil, også ulltøy.
                                Den er meget god til å merke små plagg så som votter, sokker, luer o.l
                                Etiketten er meget enkel å montere, den strykes direkte på plagget og skal smelte ned i tøyfibrene.
                                Bruksanvisning og strykebeskyttelse følger med i forpakningen.',
    'Description-iron-4' => 'Arket som følger med som strykebeskyttelse er forøvrig vanlig bakepapir, det er viktig av man ikke forsøker å bruke matpapir da dette vil klebe seg fast i etikettene.
                                Riktig montert er strykfastetikettene meget holdbare, de skal tåle 90 graders vask og også tørketrommel.',
    'Description-iron-5' => 'Ettersom navnelappene smelter ned i tøyet er det ikke mulig å fjerne de om man senere ønsker det.
                                Alternativet er at man stryker på nøytrale etiketter over de gamle for å blokkere ut navnet.
                                Om man vil ha etiketter for å merke tøy til forskjellige institusjoner, klær til barnehage og SFO, er strykfastetikettene det mest opplagte og beste valget.',



];
