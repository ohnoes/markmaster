<?php

return [
	'Translate1' => 'Gullfargede klistremerker',
	'Translate2' => 'Lag din egen gulletikett. Her kan du bestille Gullfargede navnelapper. Som første leverandør av dette produktet kan Markmaster tilby sine kunder gullfargede etiketter i valgfri strørrelse, antall og form.',
	'Translate3' => 'Klistremerker i gull',
	'Translate4' => 'Markmasters gullfargede etiketter og navnelapper er i gullfarget vinyl med sort trykk.',
	'Translate5' => 'Som første leverandør av dette produktet kan Markmaster tilby sine kunder gullfargede etiketter i valgfri strørrelse, antall og form.',
	'Translate6' => 'Kunden kan bruke vår designmodul eller velge en av våre maler som hjelper til med startdesign.
                                Det er også mulig å laste opp eget design og egne logoer dersom det er ønskelig.',
	'Translate7' => 'Gulletikettene passer for alle kunder som ønsker en litt mer eksklusiv merking av sine produkter og eiendeler.
                                De er perfekt for merking av for eksempel pokaler og andre premier. Andre ting man kan merke med gullfargede etiketter er postkasser,
                                presanger til forskjellige anledninger, salgsvarer, reklameprodukter m.m',
	'Translate8' => 'Små navnelapper i gullfarget folie er også fine å bruke til merking av personlige eiendeler, som f.eks mobil, sminkesaker, kulepenner, kamera o.l',
	'Translate9' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Gullfarget Klistremerker',
	'keywords' => 'Gull, Gullfarget, Klistremerker, navnelapper, merkelapper',
	'imagealt' => 'Gullfarget Klistremerker og navnelapper',
	'Description-gold-1' => 'Markmasters gullfargede etiketter og navnelapper er i gullfarget vinyl med sort trykk.',
    'Description-gold-2' => 'Som første leverandør av dette produktet kan Markmaster tilby sine kunder gullfargede etiketter i valgfri strørrelse, antall og form.',
    'Description-gold-3' => 'Kunden kan bruke vår designmodul eller velge en av våre maler som hjelper til med startdesign.
                                Det er også mulig å laste opp eget design og egne logoer dersom det er ønskelig.',
    'Description-gold-4' => 'Gulletikettene passer for alle kunder som ønsker en litt mer eksklusiv merking av sine produkter og eiendeler.
                                De er perfekt for merking av for eksempel pokaler og andre premier. Andre ting man kan merke med gullfargede etiketter er postkasser,
                                presanger til forskjellige anledninger, salgsvarer, reklameprodukter m.m',
    'Description-gold-5' => 'Små navnelapper i gullfarget folie er også fine å bruke til merking av personlige eiendeler, som f.eks mobil, sminkesaker, kulepenner, kamera o.l',



];
