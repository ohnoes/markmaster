<?php

return [
	'Translate1' => 'Gullfarget eller sølvfarget klistremerke',
	'Translate2' => 'Lag navnelapper i gull eller sølv. Passer meget godt til merking av pokaler og premier',
	'Translate3' => 'Gullfarget eller sølvfarget klistremerker',
	'Translate4' => 'Etiketter i gullfarget eller sølvfarget vinyl med sort trykk.',
	'Translate5' => 'Bruk vår designmodul eller last opp et eget design fra ditt favorittprogram. ',
	'Translate6' => 'Perfekt for merking av premier, designprodukter mm',
	'Translate7' => 'Valgfri størrelse, antall og form.',
	'Translate8' => 'Leveres på ark.',
	'Translate9' => 'Velg om du ønsker gullfargede eller sølvfargede klistremerker',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Translate13' => 'Klistreetikett gull',
	'Translate14' => 'Klistreetikett i gullfarget
								folie. Egner seg godt til
								merking av pokaler og
								lignende',
	'Translate15' => 'Klistreetikett sølv',
	'Translate16' => 'Klistreetikett i sølvfarget
								folie. Egner seg godt til
								merking av pokaler og
								lignende',
	'keywords' => 'Gullfarget, sølvfarget, Klistremerker, navnelapper, merkelapper',
	'imagealt' => 'Gullfarget eller sølvfarget Klistremerker og navnelapper',



];
