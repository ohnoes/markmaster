<?php

return [
	'Translate1' => 'Sølvfargede klistremerker',
	'Translate2' => 'Lag dine egene sølvfargede klistremerker og navnelapper. Markmaster har vært først ute med å produsere denne typen etiketter der kunden helt fritt kan velge størrelse og form, og i tillegg velge antall stegløst helt ned til en enkelt etikett.',
	'Translate3' => 'Klistremerker i sølv',
	'Translate4' => 'Markmaster har vært først ute med å produsere denne typen etiketter der kunden helt fritt kan velge størrelse og form, og i tillegg velge antall stegløst helt ned til en enkelt etikett.
                                I vår nettbutikk finnes det flere alternativer for utforming av personlige etiketter og navnelapper.',
	'Translate5' => 'Her finner de en enkel bestillingsløsning eller de kan velge en av våre ferdige maler for å ha et utgangspunkt å jobbe ut fra.',
	'Translate6' => 'Om man ønsker flere alternativ og mulighet for å laste opp egendesignede ting og evt logo ,skal man velge vår løsning for egen design.
                                All design som er lastet opp som bakgrunn eller symbol vil være personlig, og vil kun bli lagret på kundens egen kundeprofil i nettbutikken.',
	'Translate7' => 'Sølvfargede etiketter er et produkt for kunder som ønsker en fin og litt eksklusiv merking.
                                Idrettslag velger ofte denne typen etiketter til merking av premier.
                                Andre bruksområder kan være merking av bøker, forskjellige salgsvarer, syltetøyglass o.l',
	'Translate8' => 'Leveres på ark.',
	'Translate9' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Sølvfarget Klistremerker',
	'keywords' => 'Sølv, Sølvfarget, Klistremerker, navnelapper, merkelapper',
	'imagealt' => 'Sølvfarget Klistremerker og navnelapper',
	'Description-silver-1' => 'Etiketter i sølvfarget folie med sort trykk.',
    'Description-silver-2' => 'Markmaster har vært først ute med å produsere denne typen etiketter der kunden helt fritt kan velge størrelse og form, og i tillegg velge antall stegløst helt ned til en enkelt etikett.
                                I vår nettbutikk finnes det flere alternativer for utforming av personlige etiketter og navnelapper.',
    'Description-silver-3' => 'Her finner de en enkel bestillingsløsning eller de kan velge en av våre ferdige maler for å ha et utgangspunkt å jobbe ut fra.',
    'Description-silver-4' => 'Om man ønsker flere alternativ og mulighet for å laste opp egendesignede ting og evt logo ,skal man velge vår løsning for egen design.
                                All design som er lastet opp som bakgrunn eller symbol vil være personlig, og vil kun bli lagret på kundens egen kundeprofil i nettbutikken.',
    'Description-silver-5' => 'Sølvfargede etiketter er et produkt for kunder som ønsker en fin og litt eksklusiv merking.
                                Idrettslag velger ofte denne typen etiketter til merking av premier.
                                Andre bruksområder kan være merking av bøker, forskjellige salgsvarer, syltetøyglass o.l',


];
