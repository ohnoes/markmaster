<?php

return [
	'Translate1' => 'Billige Klistremerker i sort/hvit. Norges billigste navnelapper',
	'Translate2' => 'Lag dine egene klistremerker. Meget anvendelig og holdbar etikett tilvirket i miljøvennlig materiale.',
	'Translate3' => 'Billige Klistremerker i sort/hvit',
	'Translate4' => 'Klistremerker i sort/hvit er en meget anvendelig og holdbar etikett tilvirket i miljøvennlig materiale.
								Det er et rimelig og godt alternativ til all type merking.',
	'Translate6' => 'For forhandlere av diverse produkter er dette den perfekte etiketten å merke varene med.
								Eksempler på slike forhandlere kan være Tupperware, Cosmopharma, Mary Kay og Forever Living
								Forskjellige typer handverkere har også bruk for denne typen etikett til å merke diverse utstyr med. Tåler oppvaskemaskin 60°C',
	'Translate5' => ' Denne etiketten kommer på rull, og man kan velge mellom størrelse 37x16 mm og 60x26
							  Navnelappen har et bredt bruksområde, f.eks drikkeflasker, termoser, matbokser, kakefat, ski og annet sportsutstyr,bøker, leker, telefon o.l',
	'Translate7' => 'Leveres på rull.',
	'Translate8' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate9' => '',
	'Translate10' => 'Klistreetikett sort/hvit<br>Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Klistremerker',
       'Translate13' => 'Klistreetikett sort/hvit<br>Nytt design',
	   'Description-sticker-1' => 'Klistremerker i sort/hvit er en meget anvendelig og holdbar etikett tilvirket i miljøvennlig materiale.
                                   Det er et rimelig og godt alternativ til all type merking.',
       'Description-sticker-2' => 'Denne etiketten kommer på rull, og man kan velge mellom størrelse 37x16 mm og 60x26
                                   Navnelappen har et bredt bruksområde, f.eks drikkeflasker, termoser, matbokser, kakefat, ski og annet sportsutstyr,bøker, leker, telefon o.l',
       'Description-sticker-3' => 'For forhandlere av diverse produkter er dette den perfekte etiketten å merke varene med.
                                   Eksempler på slike forhandlere kan være Tupperware, Cosmopharma, Mary Kay og Forever Living
                                   Forskjellige typer handverkere har også bruk for denne typen etikett til å merke diverse utstyr med.',
       'Description-sticker-4' => 'Det er enkelt å laste opp egne design og logoer, eller man kan velge å bruke malene som ligger klar i nettbutikken som et utgangspunkt for videre design.',
       'Description-sticker-5' => 'Denne typen etikett egner seg dårlig for merking av tøy og tekstiler, til denne typen merking anbefaler vi
                                   våre strykfastetiketter enten i sort/hvit eller farge, eller våre strykefrie navnelapper (tøyklistremerker)',

								   'keywords' => 'Billige Klistremerker, navnelapper, merkelapper',
								   'imagealt' => 'Billige Klistremerker og navnelapper',




];
