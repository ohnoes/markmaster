<?php

return [
	'Translate1' => 'Strykefrie tøyklistremerker. Navnelapper som kan klistres direkte på plagget',
	'Translate2' => 'Strykefrie navnelapper "tøyklistremerker" klistres enkelt på tøy eller andre eiendeler.',
	'Translate3' => 'Strykefrie navnelapper',
	'Translate4' => 'Strykefrie navnelapper "tøyklistremerker" klistres enkelt på tøy eller andre eiendeler.
                                        Navnelappene kan klistres på alt av tøy og tekstiler som har vaskeanvisning/ klesmerke.
                                        De er et av Markmasters mest populære produkter. Dette fordi de er lett å montere, ingen stryking og de er lette å fjerne.',
	'Translate5' => 'Etikettene passer godt til å merke matbokser, diverse flasker, leker, bøker, dvd o.l
                                        Forutsetningen for god holdbarhet er imidlertid at underlaget som merkene skal sitte på er rent og tørt.
                                        Om merkene er montert riktig tåler de garantert 40 grader vask i vaskemaskin og oppvaskmaskin.
                                        Dersom klærne skal vaskes på høyere temperatur eller ikke har vaskeanvisning som merkene kan festes på anbefaler vi våre strykfastetiketter.',
	//
	'Translate6' => 'Valgfri størrelse, antall og form.',
	'Translate7' => 'Leveres på ark.',
	'Translate8' => 'Velg "Nytt design" for å starte et design fra null, eller velg en av våre maler som hjelper deg med startdesignet.',
	'Translate9' => '',
	'Translate10' => 'Nytt design',
	'Translate11' => 'Start ett nytt design',
	'Translate12' => 'NO',
	'Title' => 'Tøyklistremerker / Strykefrie navnelapper',
	//
	'Translate13' => 'Strykefrie navnelapper tåler 40 grader vaskemaskin og oppvaskmaskin dersom de er riktig montert.',
	'Translate14' => 'Bruksanvisning ved bruk av strykefrie navnelapper på tøy: Merket skal klistres på vaskeanvisningen/ klesmerket og masseres godt inn, det
								er spesielt viktig å trykke godt på hjørner og kanter. Vent 24 t før plagget vaskes.',
	'Translate15' => 'For klær som skal vaskes på høyere temperatur enn 40 grader eller ikke har vaskeanvisning/klesmerke anbefaler vi våre vanlige strykfastetiketter.',
	'Description-sticker-fabric-1' => 'Strykefrie navnelapper "tøyklistremerker" klistres enkelt på tøy eller andre eiendeler.
                                        Navnelappene kan klistres på alt av tøy og tekstiler som har vaskeanvisning/ klesmerke.
                                        De er et av Markmasters mest populære produkter. Dette fordi de er lett å montere, ingen stryking og de er lette å fjerne.',
    'Description-sticker-fabric-2' => 'Etikettene passer godt til å merke matbokser, diverse flasker, leker, bøker, dvd o.l
                                        Forutsetningen for god holdbarhet er imidlertid at underlaget som merkene skal sitte på er rent og tørt.
                                        Om merkene er montert riktig tåler de garantert 40 grader vask i vaskemaskin og oppvaskmaskin.
                                        Dersom klærne skal vaskes på høyere temperatur eller ikke har vaskeanvisning som merkene kan festes på anbefaler vi våre strykfastetiketter.',
    'Description-sticker-fabric-3' => 'I tillegg er det utallige designmuligheter, kunden kan fritt velge hvordan de skal utforme sin etikett.
                                        Det finnes flere valgmuligheter for design i vår nettbutikk.
                                        Velger du enkel navnelapp er størrelsen fastsatt til 30x13 mm, det er max 3 linjer med tekst og man kan legge til ønsket symbol.
                                        Man kan velge farge på bakgrunn og tekst, og det er mange fine skrifttyper å velge mellom.',
    'Description-sticker-fabric-4' => 'Ønsker kunden flere valgmuligheter skal den velge egen design.
                                        Her kan man velge størrelse og antall helt fritt, og man kan laste opp eget design og bilder.
                                        Man kan også velge en av våre ferdige maler som utgangspunkt for videre design.
                                        Det er kun fantasien som setter begrensningene, kanskje kan man la barna selv få velge motiv og farge slik at de får akkurat den etiketten de ønsker seg. Og hva med å laste opp et bilde av barnet?
                                        Da vil det være enkelt for barnet å kjenne igjen sine klær og eiendeler selv om de ikke har lært seg å lese.',
    'Description-sticker-fabric-5' => 'Bruksanvisning: Strykefrie navnelapper skal klistres på vaskeanvisninger eller klesmerker og masseres godt inn, det er spesielt viktig å trykke på hjørner og kanter.
                                        Vent 24 timer før plagget vaskes.',
	'keywords' => 'Strykefrie, navnelapper, merkelapper, tøyklistremerker',
	'imagealt' => 'Strykefrie tøyklistremerker og navnelapper',


];
