<?php

return [
	

	'Translate1' => 'Klistremerker',
	'Translate2' => 'Klistreetiketter',
	'Translate3' => 'Du må være innlogget om du ønsker å laste opp bakgrunner eller bilder',
	'Translate4' => 'Klistreetikett sort/hvit',
	'Translate5' => 'Klistreetikett gull',
	'Translate6' => 'Størrelse og form',
	'Translate7' => 'Bakgrunn og farge',
	'Translate8' => 'Tekst',
	'Translate9' => 'Symboler og bilder',
	'Translate10' => 'Legg i Handlevogn',
	'Translate11' => 'Rektangel',
	'Translate12' => 'Elipse',
	'Translate13' => 'Velg størrelse på etiketten',
	'Translate14' => '30mm x 10mm',
	'Translate15' => 'Antall',
	'Translate16' => '1 stk.',
	'Translate17' => '5 stk.',
	'Translate18' => '10 stk.',
	'Translate19' => '20 stk.',
	'Translate20' => '50 stk.',
	'Translate21' => '75 stk.',
	'Translate22' => '100 stk.',
	'Translate23' => '250 stk.',
	'Translate24' => '500 stk.',
	'Translate25' => '1000 stk.',
	'Translate26' => 'Pris: ',
	'Translate27' => 'velg/last opp en bakgrunn',
	'Translate28' => 'Bakgrunner',
	'Translate29' => 'Mine Bakgrunner',
	'Translate30' => 'Last opp egen bakgrunn',
	'Translate31' => 'Last opp bakgrunn',
	'Translate32' => 'Legg inn tekst her!',
	'Translate33' => 'Legg til',
	'Translate34' => 'Slett',
	'Translate35' => 'Tekst Format',
	'Translate36' => 'Størrelse',
	'Translate37' => 'Linje avstand',
	'Translate38' => 'Rotasjon',
	'Translate39' => 'Posisjon',
	'Translate40' => 'Sentrer Teksten',
	'Translate41' => 'Layer Kontroll',
	'Translate42' => 'Tykkelse',
	'Translate43' => 'Alle Clipart',
	'Translate44' => 'Mine Clipart',
	'Translate45' => 'Mest Brukt',
	'Translate46' => 'Dyr',
	'Translate47' => 'Stjernetegn',
	'Translate48' => 'Figurer',
	'Translate49' => 'Symboler',
	'Translate50' => 'Sport',
	'Translate51' => 'Maskiner',
	'Translate52' => 'Former',
	'Translate53' => 'Vaskesymboler',
	'Translate54' => 'Søk i denne kategorien',
	'Translate55' => 'Vend bilde',
	'Translate56' => 'Sentrer bilde',
	'Translate57' => 'Bilde Farge',
	'Translate58' => 'Slett',
	'Translate59' => 'Last opp bilde',
	'Translate60' => 'Oppdater etikett',
	'Translate61' => 'Bestill',
	'Translate62' => 'Du må være innlogget om du ønsker å laste opp bakgrunner eller bilder. Gyldig filformat: png, gif, eller jpg.
				Bakgrunner og clipart blir personlige og ikke tilgjengelig for andre enn kontohaver',
	'Translate63' => 'Laster opp fil',
	'Translate64' => 'Velg størrelse på etiketten',
	'Translate65' => '37mm x 16mm',
	'Translate66' => '60mm x 26mm',
];