<?php

return [

    // Title
    'Title-sticker-color' => 'Laminerte klistremerker med farge',
    'Title-iron-color' => 'Strykfastetikett med farge',
    'Title-gold' => 'Gullfargede klistremerker',
    'Title-silver' => 'Sølvfargede klistremerker',
    'Title-transparent' => 'Transparente klistremerker',
    'Title-sticker' => 'Klistremerker',
    'Title-iron' => 'Strykefastetiketter',
    'Title-reflection' => 'Klistremerker med refleks',
    'Title-sticker-fabric' => 'Strykefrie navnelapper',

    // Page Descriptions   Update these for CEO
	'Page-Description-sticker-color' => 'Laminerte klistremerker med farge',
    'Page-Description-iron-color' => 'Strykfastetikett med farge',
    'Page-Description-gold' => 'Klistremerker med gullfarge',
    'Page-Description-silver' => 'Klistremerker med sølvfarge',
    'Page-Description-transparent' => 'Klistremerker med transparent folie',
    'Page-Description-sticker' => 'Klistremerker',
    'Page-Description-iron' => 'Strykefastetiketter',
    'Page-Description-reflection' => 'Klistremerker med refleks',
    'Page-Description-sticker-fabric' => 'Strykefrie navnelapper',

    // Alerts
	'Alert-login' => 'Du må være innlogget om du ønsker å laste opp bakgrunner eller bilder',

    // Page header
    'Header-sticker-color' => 'Klistreetikett med laminatforsterkning',
    'Header-iron-color' => 'Strykfastetikett farge',
    'Header-gold' => 'Klistreetikett gull',
    'Header-silver' => 'Klistreetikett sølv',
    'Header-transparent' => 'Klistreetikett transparent',
    'Header-sticker' => 'Klistreetikett sort/hvit',
    'Header-iron' => 'Strykefastetikett sort/hvit',
    'Header-reflection' => 'Klistreetikett refleks',
    'Header-sticker-fabric' => 'Strykefrie navnelapper',

    // Control Tabs
    'Tab-size' => 'Størrelse og form',
	'Tab-background' => 'Bakgrunn og farge',
	'Tab-text' => 'Tekst',
	'Tab-clipart' => 'Symboler og bilder',
	'Tab-cart' => 'Legg i Handlevogn',

    // Design Labels
    'Label-rectangle' => 'Rektangel',
    'Label-elipse' => 'Elipse',
    'Label-width-mm' => 'Bredde mm',
    'Label-height-mm' => 'Høyde mm',
    'Label-width-height-info' => '*Bredde og høyde må være mellom 6-300mm',
    'Label-width-height-info2' => '*Bredde og høyde må være mellom 6-50mm',
    'Label-pick-size' => 'Velg størrelse på etiketten',
    'Label-3716' => '37mm x 16mm',
    'Label-6026' => '60mm x 26mm',
    'Label-3010' => '30mm x 10mm',
    'Label-pick-quantity' => 'Velg antall',
    'Label-quantity' => 'Antall',
    'Label-1' => '1 stk.',
    'Label-5' => '5 stk.',
    'Label-10' => '10 stk.',
    'Label-20' => '20 stk.',
    'Label-50' => '50 stk.',
    'Label-75' => '75 stk.',
    'Label-100' => '100 stk.',
    'Label-250' => '250 stk.',
    'Label-500' => '500 stk.',
    'Label-1000' => '1000 stk.',
    'Label-price' => 'Pris: ',

    'Label-color-select' => 'Velg farge fra tabell',
    'Label-upload-background' => 'Last opp Bakgrunn',
    'Label-upload-select-background' => 'Velg/last opp en bakgrunn',
    'Label-upload-mybackground' => 'Last opp egen bakgrunn',
    'Label-backgrounds' => 'Bakgrunner',
    'Label-mybackgrounds' => 'Mine Bakgrunner',
    'Label-background-nonchangeable' => 'Du kan ikke endre bakgrunnsfarge på denne etiketten',

    'Label-your-text' => 'Din tekst',
    'Label-add' => 'Legg til',
    'Label-delete' => 'Slett',
    'Label-position' => 'Posisjon',
    'Label-size' => 'Størrelse',
    'Label-line-distance' => 'Linjeavstand',
    'Label-rotation' => 'Rotasjon',
    'Label-text-format' => 'Tekst Format',
    'Label-color' => 'Farge',
    'Label-center-text' => 'Sentrer Teksten',
    'Label-layer' => 'Layer Kontroll',
    'Label-thickness' => 'Tykkelse',
    'Label-line-edge' => 'Kantlinje',
    'Label-flip-clipart' => 'Vend bilde',
    'Label-center-clipart' => 'Sentrer bilde',
    'Label-clipart-color' => 'Bilde Farge',
    'Label-upload-clipart' => 'Last opp bilde',
    'Label-center-v' => 'Sentrer V',
    'Label-center-h' => 'Sentrer H',
    'Label-flip-x' => 'Flip X',
    'Label-flip-y' => 'Flip Y',

    'Label-update-sticker' => 'Oppdater etikett',
    'Label-order' => 'Bestill',

    'Label-clipart-my' => 'Mine Clipart',
    'Label-clipart-used' => 'Mest Brukt',
    'Label-clipart-animal' => 'Dyr',
    'Label-clipart-zodiac' => 'Stjernetegn',
    'Label-clipart-figures' => 'Figurer',
    'Label-clipart-symbols' => 'Symboler',
    'Label-clipart-sport' => 'Sport',
    'Label-clipart-machines' => 'Maskiner',
    'Label-clipart-shape' => 'Former',
    'Label-clipart-laundry' => 'Vaskesymboler',

    'Label-login-upload' => 'Du må være innlogget for å laste opp bilder',

    // Sticker/Label Description
    'Description-upload' => 'Du må være innlogget om du ønsker å laste opp bakgrunner eller bilder. Gyldig filformat: png, gif, eller jpg.
				             Bakgrunner og clipart blir personlige og ikke tilgjengelig for andre enn kontohaver',
    'Description-sticker-color-1' => 'Klistreetikett med laminatforsterkning er produsert av «murfolie» som sitter ekstremt godt.
                                        I tillegg har vi laminert etikettene med en tynn klar film som gjør at de er ekstremt holdbare.
                                        De er meget motstandsdyktige mot mekanisk slitasje og løsningsmidler.
                                        Disse navnelappenen er av absolutt beste kvalitet og leveres til markedets laveste priser.
                                        Etikettene har runde hjørner, noe som er en fordel fordi de sitter bedre ettersom kantene ikke løsner så lett.',
    'Description-sticker-color-2' => 'Klistremerkene passer like godt både til utendørs og innendørs bruk, og den tåler 60 graders oppvaskmaskinvask.',
    'Description-sticker-color-3' => 'Etiketten har mange bruksområder, det kan være til privatpersoner, firma, lag eller foreninger som har bruk for klistremerker.
                                        Om du vil profilere din bedrift eller eller vil drive med merkesvarebygging er våre etiketter helt perfekt.
                                        Andre typiske bruksområder for vår klistreetikett vil være verktøy, flasker, matbokser, bøker, foto, data og sportsutstyr, postkasseetiketter o.l',
    'Description-sticker-color-4' => 'Våre små ovale etiketter passer godt til merking av sminkeutstyr, penner, mobiltelefoner m.m
                                        Etikettene kan også brukes for å merke sko (klistres nede i sålen)
                                        Ølbrygging er populært, og hos Markmaster kan du designe dine egne øl-etiketter.
                                        I vår nettbutikk kan du også bestille kontrollmerker,godkjenningsmerker og -oblat, det ligger ferdige maler du kan bruke som utgangspunkt og så laste opp egen logo.
                                        Og hva med å designe din egen postkasseetikett?',
    'Description-sticker-color-5' => 'Det finnes flere designmuligheter i vår nettbutikk, og kunden selv velger fritt størrelse, antall og form.
                                        Du kan velge farge på tekst og bakgrunn, og vi har mange skrifttyper å velge mellom.
                                        I vår symbol/figur-bank vil du helt sikkert finne noe du liker.
                                        Og naturligvis er det mulig å laste opp egne bilder og logoer, eller et helt ferdig design.
                                        Når det gjelder montering av merkene er dette svært enkelt, det eneste man må huske på er at underlaget må være rent og tørt.',
    'Description-iron-color-1' => 'Strykfastetikett farge er en myk og holdbar etikett som skal strykes fast på tekstiler og tåler 60 graders vask. Dette er en enkel og rimelig måte å merke tøy på .
                                        Produktet irriterer ikke huden, og inneholder ikke Phtalater eller PVC.',
    'Description-iron-color-2' => 'De kan brukes til alle slags tekstiler og tøy, og kunden bestemmer selv form, antall og størrelse.',
    'Description-iron-color-3' => 'Eksempler på hva man kan merke er jakker, luer , votter, gensere, bukser, gummistøvler, regntøy o.l
                                        Produktet er også perfekt til lag og foreninger som ønsker å merke f.eks t-shirt med egen logo.
                                        Bedrifter som vil merke arbeidstøy vil enkelt kunne bruke vårt designverktøy i nettbutikken for å laste opp egen design. Vi i Markmaster er også behjelpelig med tilrettelegging av dette.',
    'Description-iron-color-4' => 'Barnetøy bør merkes med telefonnummer slik at det er enkelt å finne fram til rette eier.
                                        Det kan være en god ide å la barnet selv få velge symbolet eller figuren som skal være med på navnelappen. Dette for at de lett skal kunne kjenne igjen sine ting selv om de ikke har lært seg å lese.
                                        Mange barn synes også at det er morsomt om man laster opp et bilde av de selv til å ha på navnelappen.',
    'Description-iron-color-5' => 'Om navnelappene er riktig montert vil de smelte ned i tøyet og er derfor veldig vanskelig å fjerne igjen. Men mange ønsker å gi bort barnetøy når det er blitt
                                        for smått til egne barn, og da kan man gjøre merket mindre synlig ved å legge på et nytt merke uten tekst.',
    'Description-gold-1' => 'Markmasters gullfargede etiketter og navnelapper er i gullfarget vinyl med sort trykk.',
    'Description-gold-2' => 'Som første leverandør av dette produktet kan Markmaster tilby sine kunder gullfargede etiketter i valgfri strørrelse, antall og form.',
    'Description-gold-3' => 'Kunden kan bruke vår designmodul eller velge en av våre maler som hjelper til med startdesign.
                                Det er også mulig å laste opp eget design og egne logoer dersom det er ønskelig.',
    'Description-gold-4' => 'Gulletikettene passer for alle kunder som ønsker en litt mer eksklusiv merking av sine produkter og eiendeler.
                                De er perfekt for merking av for eksempel pokaler og andre premier. Andre ting man kan merke med gullfargede etiketter er postkasser,
                                presanger til forskjellige anledninger, salgsvarer, reklameprodukter m.m',
    'Description-gold-5' => 'Små navnelapper i gullfarget folie er også fine å bruke til merking av personlige eiendeler, som f.eks mobil, sminkesaker, kulepenner, kamera o.l',
    'Description-silver-1' => 'Etiketter i sølvfarget folie med sort trykk.',
    'Description-silver-2' => 'Markmaster har vært først ute med å produsere denne typen etiketter der kunden helt fritt kan velge størrelse og form, og i tillegg velge antall stegløst helt ned til en enkelt etikett.
                                I vår nettbutikk finnes det flere alternativer for utforming av personlige etiketter og navnelapper.',
    'Description-silver-3' => 'Her finner de en enkel bestillingsløsning eller de kan velge en av våre ferdige maler for å ha et utgangspunkt å jobbe ut fra.',
    'Description-silver-4' => 'Om man ønsker flere alternativ og mulighet for å laste opp egendesignede ting og evt logo ,skal man velge vår løsning for egen design.
                                All design som er lastet opp som bakgrunn eller symbol vil være personlig, og vil kun bli lagret på kundens egen kundeprofil i nettbutikken.',
    'Description-silver-5' => 'Sølvfargede etiketter er et produkt for kunder som ønsker en fin og litt eksklusiv merking.
                                Idrettslag velger ofte denne typen etiketter til merking av premier.
                                Andre bruksområder kan være merking av bøker, forskjellige salgsvarer, syltetøyglass o.l',
    'Description-transparent-1' => 'Transparente klistremerker og navnelapper er merker som er trykket på gjennomsiktig folie.',
    'Description-transparent-2' => 'Trykket er i sort, og du kan selv velge om du vil bruke vår designmodul eller laste opp egne design.
                                    Størrelse, antall og form er også valgfritt.',
    'Description-transparent-3' => 'Bruksområdet er stort, etikettene passer godt der motivet ikke skal være for massivt, og om man vil ha en litt diskret merking.',
    'Description-transparent-4' => 'Typiske bruksområder kan være vinduer, premier o.l av glass,mobil, pc, ipad, ipod, mac, klokker, briller, sminkesaker, tannbørster, kulepenner m.m',
    'Description-transparent-5' => 'Det har stor betydning hvilken farge det er på bakgrunnen merkene skal sitte på.
                                    Om bakgrunnen er hvit eller blank ( glass ) er det helt uten betydning.
                                    Setter du derimot merkene på en sort eller mørk bakgrunn vil du nesten ikke kunne se trykket da det forsvinner mot den mørke overflaten.',
    'Description-sticker-1' => 'Klistreetikett sort/hvit er en meget anvendelig og holdbar etikett tilvirket i miljøvennlig materiale.
                                Det er et rimelig og godt alternativ til all type merking.',
    'Description-sticker-2' => 'Denne etiketten kommer på rull, og man kan velge mellom størrelse 37x16 mm og 60x26
                                Navnelappen har et bredt bruksområde, f.eks drikkeflasker, termoser, matbokser, kakefat, ski og annet sportsutstyr,bøker, leker, telefon o.l',
    'Description-sticker-3' => 'For forhandlere av diverse produkter er dette den perfekte etiketten å merke varene med.
                                Eksempler på slike forhandlere kan være Tupperware, Cosmopharma, Mary Kay og Forever Living
                                Forskjellige typer handverkere har også bruk for denne typen etikett til å merke diverse utstyr med.',
    'Description-sticker-4' => 'Det er enkelt å laste opp egne design og logoer, eller man kan velge å bruke malene som ligger klar i nettbutikken som et utgangspunkt for videre design.',
    'Description-sticker-5' => 'Denne typen etikett egner seg dårlig for merking av tøy og tekstiler, til denne typen merking anbefaler vi
                                våre strykfastetiketter enten i sort/hvit eller farge, eller våre strykefrie navnelapper (tøyklistremerker)',
    'Description-iron-1' => 'Strykfastetiketter er høykvalitets navnelapper som strykes fast på klær og tekstiler.
                                Markmaster har produsert denne typen etiketter siden vår oppstart i Norge i 1988, den har vært vår bestseller i mange år og er ennå veldig populær.',
    'Description-iron-2' => 'Navnelappene er i sort/hvit, standardstørrelse er 30x10 mm og de leveres på rull.
                                For de som ønsker en rimelig og litt disket merking er dette en perfekt etikett.',
    'Description-iron-3' => 'Strykfastetikettene passer til alle slags klær og tekstil, også ulltøy.
                                Den er meget god til å merke små plagg så som votter, sokker, luer o.l
                                Etiketten er meget enkel å montere, den strykes direkte på plagget og skal smelte ned i tøyfibrene.
                                Bruksanvisning og strykebeskyttelse følger med i forpakningen.',
    'Description-iron-4' => 'Arket som følger med som strykebeskyttelse er forøvrig vanlig bakepapir, det er viktig av man ikke forsøker å bruke matpapir da dette vil klebe seg fast i etikettene.
                                Riktig montert er strykfastetikettene meget holdbare, de skal tåle 90 graders vask og også tørketrommel.',
    'Description-iron-5' => 'Ettersom navnelappene smelter ned i tøyet er det ikke mulig å fjerne de om man senere ønsker det.
                                Alternativet er at man stryker på nøytrale etiketter over de gamle for å blokkere ut navnet.
                                Om man vil ha etiketter for å merke tøy til forskjellige institusjoner, klær til barnehage og SFO, er strykfastetikettene det mest opplagte og beste valget.',
    'Description-reflection-1' => 'Markmasters refleksetiketter er trykket på en selvklebende folie som har  refleksegenskaper.
                                    Vi var de første på markedet med denne type etikett der kunden selv kunne velge utformingen.
                                    Merkene er i et mykt materiale som former seg etter underlaget',
    'Description-reflection-2' => 'De er velegnet til å merke f.eks sykler, sykkelhjelmer, støvler, sko, ransel, regntøy osv
                                    Personlig etikett som gir ekstra synlighet i mørket, la barna være med å bestemme hva som skal stå på etiketten.
                                    Kanskje vil de ha med et bilde av seg selv eller yndlingsdyret sitt? Dette gjør at de enkelt finner igjen f.eks sykkelhjelmen sin
                                    når de skal sykle hjem fra skole eller trening',
    'Description-reflection-3' => 'Valgfritt fargetrykk, størrelse, antall og form
                                    Men husk at små merker alene ikke gir god nok synlighet i mørket.',
    'Description-reflection-4' => 'Gode råd for refleksbruk:
                                    De fleste barn befinner seg i tettbebygde strøk der bilistene bruker nærlys,refleksene bør derfor henge lavt, helst fra knehøyde og ned, for best synlighet.
                                    Jo flere reflekser man har på seg jo bedre er det.
                                    Man bør ha reflekser på hver side av kroppen slik at man vises godt fra begge sider når man krysser veien.',
    'Description-reflection-5' => 'Refleksene blir slitt etter hvert og bør derfor byttes ut hver sesong eller så snart det kommer riper på de.
                                    Husk at refleksmerker ikke erstatter annet sikkerhetsutstyr som lys og pedalrefleks på sykkel.
                                    Bli sett, bruk refleks!',
    'Description-sticker-fabric-1' => 'Strykefrie navnelapper "tøyklistremerker" klistres enkelt på tøy eller andre eiendeler.
                                        Navnelappene kan klistres på alt av tøy og tekstiler som har vaskeanvisning/ klesmerke.
                                        De er et av Markmasters mest populære produkter. Dette fordi de er lett å montere, ingen stryking og de er lette å fjerne.',
    'Description-sticker-fabric-2' => 'Etikettene passer godt til å merke matbokser, diverse flasker, leker, bøker, dvd o.l
                                        Forutsetningen for god holdbarhet er imidlertid at underlaget som merkene skal sitte på er rent og tørt.
                                        Om merkene er montert riktig tåler de garantert 40 grader vask i vaskemaskin og oppvaskmaskin.
                                        Dersom klærne skal vaskes på høyere temperatur eller ikke har vaskeanvisning som merkene kan festes på anbefaler vi våre strykfastetiketter.',
    'Description-sticker-fabric-3' => 'I tillegg er det utallige designmuligheter, kunden kan fritt velge hvordan de skal utforme sin etikett.
                                        Det finnes flere valgmuligheter for design i vår nettbutikk.
                                        Velger du enkel navnelapp er størrelsen fastsatt til 30x13 mm, det er max 3 linjer med tekst og man kan legge til ønsket symbol.
                                        Man kan velge farge på bakgrunn og tekst, og det er mange fine skrifttyper å velge mellom.',
    'Description-sticker-fabric-4' => 'Ønsker kunden flere valgmuligheter skal den velge egen design.
                                        Her kan man velge størrelse og antall helt fritt, og man kan laste opp eget design og bilder.
                                        Man kan også velge en av våre ferdige maler som utgangspunkt for videre design.
                                        Det er kun fantasien som setter begrensningene, kanskje kan man la barna selv få velge motiv og farge slik at de får akkurat den etiketten de ønsker seg. Og hva med å laste opp et bilde av barnet?
                                        Da vil det være enkelt for barnet å kjenne igjen sine klær og eiendeler selv om de ikke har lært seg å lese.',
    'Description-sticker-fabric-5' => 'Bruksanvisning: Strykefrie navnelapper skal klistres på vaskeanvisninger eller klesmerker og masseres godt inn, det er spesielt viktig å trykke på hjørner og kanter.
                                        Vent 24 timer før plagget vaskes.',

];
