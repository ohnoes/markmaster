<?php

return [
	'Translate1' => 'Navnelapper i farger, klistrelapper, refleksmerker, skoetiketter og strykemerker.',
	'Translate2' => 'Strykefrie navnelapper i farge, klistrelapper, refleksmerker, strykemerker, klistremerker, skoetiketter, valgfriitt antall',
	'Translate3' => 'Klistremerker med laminat',
	'Translate4' => 'Høykvalitets klistreetikett med
								ekstra sterkt lim og laminert
								overflate. Uovertruffen
								slitestyrke for røft bruk.',
	'Translate5' => 'Billige Klistremerker',
	'Translate6' => 'Navnelapper som kan klistres
								på gjenstander. Et rimelig
								produkt med bredt
								bruksområde.',
	'Translate7' => 'Strykfast farge',
	'Translate8' => 'Fargede navnelapper som
								strykes fast på klær og
								tekstiler. Tåler 60°C vask.',
	'Translate9' => 'Strykfast sort/hvit',
	'Translate10' => 'Høykvalitets navnelapper
								som strykes fast på klær og
								tekstiler. Tåler 90°C vask.',
	'Translate11' => 'Klistremerke gull & sølv',
	'Translate12' => 'Klistreetikett i gullfarget eller sølvfarget
								folie. Egner seg godt til
								merking av pokaler og
								lignende.',
	'Translate13' => 'Strykefrie navnelapper',
	'Translate14' => 'For enkel merking av både klær og andre eiendeler.
								Lett å montere,ingen stryking, enkelt å fjerne.
								',
	'Translate15' => 'Klistremerke transparent',
	'Translate16' => 'Klistreetikett i transparent
								folie. Egner seg godt til
								merking av glassgjenstander.',
	'Translate17' => 'Refleksmerke',
	'Translate18' => 'Klistreetikett trykket i folie
								med refleksegenskaper.
								Når du ønsker ekstra synlighet
								i høstmørket.',
	'keywords' => 'Navnelapper, merkelapper, refleksmerker, strykemerker, klistremerker, skoetiketter',
	'Usage' => 'BRUKSOMRÅDE',
	'Stickers' => 'Klistremerker',
	'Stickers-info' => 'Velg mellom flere forskjellige typer klistremerker til både privatpersoner og bedrifter',
	'Fabric' => 'Tøymerker',
	'Fabric-info' => 'Her finner du både våre populære strykefrie navnelapper som enkelt klistres på tøy, samt de tradisjonelle strykfast etikettene',

];
