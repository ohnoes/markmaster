<?php

return [
	'Translate1' => 'Bestillingsstatus',
	'Translate2' => 'Status for din bestilling',
	'Translate3' => 'Noe gikk galt. Vennligst prøv igjen eller bruk et annet kort. Auth Fail',
	'Translate4' => 'Noe gikk galt. Vennligst prøv igjen eller bruk et annet kort. Capture Fail',
	'Translate5' => 'Betaling godkjent. Din bestilling vil bli sendt med posten om 1-2 arbeidsdager.',
	'Translate6' => 'Bestillingsnummer',
	'Translate7' => 'Pris',
	'Translate8' => 'Bestillingsstatus',
	'Translate9' => 'Link til bestilling',
	'Translate10' => 'Faktura',
	'Translate11' => 'Klar for betaling',
	'Translate12' => 'Betalt',
	'Translate13' => 'Eksporert',
	'Translate14' => 'Kansellert',
	'Translate15' => 'N/A',
	'Translate16' => 'Bestilling',
	'Translate17' => 'Faktura',
	'Translate18' => 'Klar for eksport',

];
