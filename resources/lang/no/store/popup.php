<?php

return [
	'Title' => 'Porto og Ekspedisjonsgebyr',
	'Part1' => 'Markmaster tilbyr gratis frakt for alle bestillinger over 145kr.',
  'Part2' => 'Din bestilling er på: ',
  'Part3' => ' fra å kvalifisere for gratis frakt.',
  'Part4' => 'Ønsker du å fortsette til utsjekk?',
	'Button-confirm' => 'Fortsett likevel',
  'Button-negative' => 'Avbryt utsjekk',

];
