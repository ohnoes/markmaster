<?php

return [
	
	/////////////////////////////////////////////////////
	//  						Messages 					//
	/////////////////////////////////////////////////////
	'Errors' => 'Please fix these errors: ',
	
	/////////////////////////////////////////////////////
	//  						Heading   					//
	/////////////////////////////////////////////////////
	'ShoppingCart' => 'Handlevogn',
	
	/////////////////////////////////////////////////////
	//  					Table headings   			    //
	/////////////////////////////////////////////////////
	'URL' => 'URL link til etikettdesign*' ,
	'Stickertype' => 'Etikett',
	'ProductType' => 'Type',
	'Size' => 'Størrelse mm',
	'Quantity' => 'Antall',
	'Price' => 'Pris inkl. mva',
	
	
	/////////////////////////////////////////////////////
	//  					Product types    			    //
	/////////////////////////////////////////////////////
	'Sticker' => 'Klisteretikett farge, med laminat',
	'StickerBW' => 'Klistreetikett i sort/hvit',
	'Ironon' => 'Strykfastetikett farge',
	'IrononBW' => 'Strykfastetikett',
	'Gold' => 'Klistreetikett i gullfarget vinyl',
	'Silver' => 'Klistreetikett i sølvfarget vinyl',
	'Transparent' => 'Klistreetikett i transparent vinyl',
	'Reflection' => 'Klistreetikett i refleks',
	'Fabricsticker' => 'Tøyklistremerke',
	'CouponCode' => 'Rabattkode',
	
	/////////////////////////////////////////////////////
	//  					Buttons			   			    //
	/////////////////////////////////////////////////////
	'EditButton' => 'Edit',
	'DeleteButton' => 'Slett',
	'StoreButton' => 'Tilbake til butikken',
	'CheckoutButton' => 'Gå til kasse',
	
	/////////////////////////////////////////////////////
	//  					Extra Lang			   		    //
	/////////////////////////////////////////////////////
	'Shipping' => 'Porto og Ekspedisjonsgebyr',
	'SigninMemo' => '*Du må være innlogget for å få kunne dele ditt design',
	'DeleteWarning' => 'Er du sikker på at du vil slette din etikett?',
	
	'Translate1' => 'Din handlevogn',
	'Translate2' => 'Din handlevogn i Markmasters web2print butikk',
	'couponButton' => 'Legg til rabattkode',
	'coupon' => 'Rabattkode',
];