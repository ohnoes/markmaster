<?php

return [
	'Translate1' => 'Endre din leveringsadresse',
	'Translate2' => 'Endre din leveringsadresse på Markmasters nettbutikk',
	'Translate3' => 'Oppdater standard brukerinformasjon',
	'Translate4' => 'Følgende feil må rettes: ',
	'Translate5' => 'Organisasjonsnavn:',
	'Translate6' => 'Fornavn:',
	'Translate7' => 'Etternavn:',
	'Translate8' => 'Adresse:',
	'Translate9' => 'Postnr:',
	'Translate10' => 'Poststed:',
	'Translate11' => 'Land:',
	'Translate12' => 'Oppdater informasjon',
	'Translate13' => 'Ja takk, jeg vil ha nyhetsbrev, 
								rabattkoder og andre tilbud fra 
								Markmaster.no',
	'Translate14' => 'Epost:',
	'Translate15' => 'Telefon:',
	

];