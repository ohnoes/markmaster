<?php

return [
	'Translate1' => 'Opprett en ny bruker hos Markmaster',
	'Translate2' => 'Opprett en ny bruker hos Markmasters web2print-butikk',
	'Translate3' => 'Ny bruker',
	'Translate4' => 'Følgende feil må rettes: ',
	'Translate5' => 'Organisasjonsnavn',
	'Translate6' => 'Fornavn',
	'Translate7' => 'Etternavn',
	'Translate8' => 'Adresse',
	'Translate9' => 'Postnr',
	'Translate10' => 'Poststed',
	'Translate11' => 'Land',
	'Translate12' => 'Epost',
	'Translate13' => 'Passord',
	'Translate14' => 'Bekreft ditt passord',
	'Translate15' => 'Bekreft',
	'Translate16' => 'Telefon',
	'Translate17' => 'Ja takk, jeg vil ha nyhetsbrev, 
								rabattkoder og andre tilbud fra 
								Markmaster.no',
	'Translate18' => 'Registrer bruker',

];