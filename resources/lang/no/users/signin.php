<?php

return [
	'Translate1' => 'Logg deg på hos Markmaster',
	'Translate2' => 'Logg deg på hos Markmasters web2print-butikk',
	'Translate3' => 'Eksisterende kunde',
	'Translate4' => ' ',
	'Translate5' => 'Email',
	'Translate6' => 'Skriv inn email',
	'Translate7' => 'Passord',
	'Translate8' => 'Skriv inn ditt passord',
	'Translate9' => 'Logg inn',
	'Translate10' => 'Glemt Passord',
	'Translate11' => 'Ny bruker',
	'Translate12' => 'Det er lett å opprette en ny brukerkonto',
	'Translate13' => 'Lag ny bruker',
	
];