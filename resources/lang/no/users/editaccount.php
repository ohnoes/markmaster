<?php

return [
	'Translate1' => 'Endre dine brukeropplysninger',
	'Translate2' => 'Endre på dine brukeropplysninger på Markmasters nettbutikk',
	'Translate3' => 'Følgende feil må rettes:',
	'Translate4' => 'Oppdater brukerinfo',
	'Translate5' => 'Her kan du oppdatere din bruker informasjon.',
	'Translate6' => 'Trykk på informasjonen du ønsker å endre.',
	'Translate7' => 'Brukerinformasjon',
	'Translate8' => 'Fakturaadresse',
	'Translate9' => 'Passord',

];