<?php

return [
	'Translate1' => 'Endre din faktura-adresse',
	'Translate2' => 'Endre din faktura-adresse på Markmasters nettbutikk',
	'Translate3' => 'Oppdater standard fakturaadresse',
	'Translate4' => 'Følgende feil må rettes: ',
	'Translate5' => 'Organisasjonsnavn:',
	'Translate6' => 'Fornavn:',
	'Translate7' => 'Etternavn:',
	'Translate8' => 'Adresse:',
	'Translate9' => 'Postnr:',
	'Translate10' => 'Poststed:',
	'Translate11' => 'Land:',
	'Translate12' => 'Oppdater informasjon',
	

];