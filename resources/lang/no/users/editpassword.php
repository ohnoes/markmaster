<?php

return [
	'Translate1' => 'Endre ditt passord',
	'Translate2' => 'Endre ditt passord på Markmasters nettbutikk',
	'Translate3' => 'Oppdater ditt passord',
	'Translate4' => 'Følgende feil må rettes: ',
	'Translate5' => 'Ditt Passord:',
	'Translate6' => 'Gammelt Passord',
	'Translate7' => 'Passord:',
	'Translate8' => 'Passord',
	'Translate9' => 'Bekreft ditt passord:',
	'Translate10' => 'Bekreft',
	'Translate11' => 'Oppdater passord',

];