<?php

return [

	/////////////////////////////////////////////////////
	//  						Header 						//
	/////////////////////////////////////////////////////
	'Homepage' => 'HJEM',
	'Products' => 'PRODUKTER',
	'ShoppingCart' => 'HANDLEVOGN',
	'Login' => 'LOGG INN',
	'User' => 'BRUKER',

	/*---- Header dropdown items for Products ----*/
	'Sticker' => 'Klistremerker',
	'StickerBW' => 'Billige Klistremerker',
	'Ironon' => 'Strykfast farge',
	'IrononBW' => 'Strykfast sort/hvit',
	'Gold' => 'Klistremerke Gull',
	'Silver' => 'Klistremerke Sølv',
	'Transparent' => 'Klistremerke transparent',
	'Reflection' => 'Refleksmerke',
	'Fabricsticker' => 'Strykefrie navnelapper',

	/*---- Header dropdown items for Login ----*/
	'UserLogin' => 'Logg Inn',
	'NewAccount' => 'Ny Bruker',

	/*---- Header dropdown items for User ----*/
	'UserStickers' => 'Mine Etiketter',
	'UserOrders' => 'Mine Bestillinger',
	'UserAccount' => 'Min bruker',
	'Logout' => 'Logg ut',

	/////////////////////////////////////////////////////
	//  						Footer	 						//
	/////////////////////////////////////////////////////
	'Social' => 'MEDIER',
	'Help' => 'HJELP',
	'Payment' => 'BETALINGSLØSNINGER',

	/*---- Footer Social list items ----*/
	'Twitter' => 'Twitter',
	'Facebook' => 'Facebook',

	/*---- Footer Help list items ----*/
	'FAQ' => 'Spørsmål & svar',
	'Video' => 'Video',
	'CommercialAgents' => 'Designhjelp',
	'Contact' => 'Kontakt oss',
	'Terms' => 'Kjøpsvilkår',

	'loggedinas' => 'Du er logget inn som: ',
	'loginmsg' => 'Takk for at du logget inn',

	'orgnumber' => 'org. nr.',
	'address' => 'Legevegen 16 ',
	'address2' => '5542 Karmsund',
	'phone' => 'Tlf. 77 69 69 61 ',
	'country' => 'Norge',
];
