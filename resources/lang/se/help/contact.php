<?php

return [
	'Translate1' => 'Kontakta Markmaster',
	'Translate2' => 'Här kan du kontakta Markmaster om du har några frågor eller önskar att ge oss  återkoppling',
	'Translate3' => 'Kontaka oss',
	'Translate4' => 'Har du frågor eller önsker att ge oss några återkopplingar? Här är några kanaler där du kan kontakta oss:',
	'Translate5' => 'Epost: ',
	'Translate6' => 'Tlf. 031-799 55 19',

];