<?php

return [
	'Translate1' => 'Markmaster frågor och svar',
	'Translate2' => 'Här är en lista över vanliga frågor och svar',
	'Translate3' => 'Vanliga frågor och svar: ',
	'Translate4' => 'Tål strykfastetiketterna att torktumlas?',
	'Translate5' => 'Ja, Om etiketten lossnar i torktumlaren, är detta ett resultat av antingen för låg monteringstemperatur eller för kort presstid.
							Etiketten ska delvis smälta ner i tyget, man ska kunna se konturerna av textilen genom etiketten',
	'Translate6' => 'Kan jag montera / stryka etiketterna på ulltyg?',
	'Translate7' => 'Montering på ullsockar eller vantar görs bäst när etiketten sätts på högkant dvs. att den inte låser elasticiteten. 
								Ull tål hög monteringstemperatur med strykjärn, OBS använd skyddspapperet. (vanlig bakplåtspapper, ej matpapper)',
	'Translate8' => 'Varför blir texten otydlig efter montering?',
	'Translate9' => 'Detta kan orsakas av för hög temperatur och/eller rörelse i strykjärnet under appliceringen. Etiketten kan då röra sig lite vilket kan resultera i otydlig text. 
								Håll strykjärnet still under appliceringen, pröva även att sänka temperaturen lite eller något kortare presstid.',
	'Translate10' => 'Kan etiketterna monteras med lägre temperatur än tre prickar på strykjärnet, då plagget inte tål så hög temperatur?',
	'Translate11' => 'Ja, det går utmärkt. Etiketten fäster bra med 2 prickar, men den tål inte koktvätt. Å andra sidan är det inte många kläder som tål det.',
	'Translate12' => 'Hur lång leveranstid är det på etiketterna?',
	'Translate13' => ' Etiketterna går vanligtvis i produktion dagen efter beställning, och de blir då packat och sänt så snart de är färdiga. 
								De blir sänt som brev, A-post, och lite avhänigt av postgången vill de komma fram till kunden 1-2 dagar efter 
								leverering från oss. OBS! Det är viktigt att kontrollera att man har angivit rätt leveransadress på kundupplysningar. ',
	'Translate14' => 'Kan jeg ändra beställningen?',
	'Translate15' => 'Antal och storlek kan inte ändras etfter att beställningen är utförd och betald, men det som skall trykkas på etiketten kan ändras helt fram til produktionsstart. ',
	'Translate16' => 'Vilka  betalningslösningar  har ni?',
	'Translate17' => 'Vi har online-betalning med kort. (Visa, Mastercard, Maestro)',
	'Translate18' => 'Är det möjligt att få etiketterna levererat till alla länder?',
	'Translate19' => 'Ja, det går helt bra att ha leveransadress til ett annat land än Norge. 
								Men vi gör inte utförsel av varan när vi sänder till andra länder. (vi drar inte av den norska momsen når vi sänder till utlandet.) 
								OBS! Det kan också tilkomma kostnader vid nnförtullning till nytt land, detta för kundens räkning.',
	'Translate20' => 'Jag är inte nöjd med etiketterna jag har mottagit, kan de returneras?',
	'Translate21' => 'Vid produktionsfel av teknisk art ersätter vi etiketterna fullt ut. Om det är stavfel eller ogynnsam 
								layout från kundens sida refundera belopp enligt gällande regler för personligt anpassade 
								produkter, vanligen 50 % av kjöpsumman.',


];