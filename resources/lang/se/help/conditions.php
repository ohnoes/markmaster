﻿<?php

return [
	'Translate1' => 'Markmasters Kjøpsvilkår',
	'Translate2' => 'Her er Markmasters  kjøpsvilkår',
	'Translate3' => 'VILLKOR',
	'Translate4' => 'Vilkorena gäller kundens inköp och användning av varor och tjänster
		levererat av Markmaster AS',
	'Translate5' => 'PRISER OCH LEVERANS',
	'Translate6' => 'Kunden får en automatisk orderbekräftelse på mail i det Markmaster
			AS har mottagit beställningen. Kunden är skyldig att kontrollera att denna
			överensstämmer med beställningen när det gäller produkttyp, innehåll,
			antal och pris.
			Alla priser är angivna priser inkl. Moms.
			Varorna kommer att levereras inom angiven leveranstid, vanligtvis 2-5
			arbetsdagar. Inte helgdagar.',
	'Translate7' => 'REKLAMATION OCH ÅNGERRÄTT',
	'Translate8' => 'Markmaster AS är skyldig att åtgärda brister på produktet om
			kunden innom rimlig tid ger besked om detta.
			Markmaster AS produkter är personligt tilverkat, det är därför
			ingen ångerrätt på produkterna efter att de är producerat.
			Kunden är själv ansvarig för att kontrollera att beställningen inte innehåller stav, eller gramatiska fel.
			Markmaster AS kontrollerar inte kunden beställning för fel innan
			beställningen går i produktion. Beställningen går direkt från web til
			print.',
	'Translate9' => 'UPPHOVSRÄTT',
	'Translate10' => 'Om kunden översänder ett eget motiv eller inverkar på produkten på annat sätt (personlig utformning av text), 
	försäkrar kunden Markmaster AS att text och motiv är fria från tredje parts rättigheter. 
	Eventuella brott mot upphovsrätten, personlighetsrätten eller namnrätten belastar i detta fall helt och hållet kunden. 
	Kunden försäkrar även att han eller hon inte skadar någon av tredje parts övriga rättigheter genom individualiseringen av produkten.
    Kunden befriar Markmaster AS från alla fordringar och anspråk som görs gällande på grund av brott mot sådana rättigheter 
	som tredje part har, om kunden är ansvarig för att en förpliktelse inte har uppfyllts. 
	Kunden ersätter Markmaster AS för alla kostnader för försvaret och övriga kostnader som uppstår.',
	'Translate11' => 'ÄNDRA VILKÅR',
	'Translate12' => 'Markmaster AS förbehåller sig rätten att ändra nuvarrande vilkår,
			bland annat som en följd av förändringar i lagstiftningen.',

];