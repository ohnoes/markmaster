<?php

return [
	'Translate1' => 'Markmasters avdelning för hjälp',
	'Translate2' => 'Här är en lista med saker vi kan hjälpa dig med',
	'Translate3' => 'Hjälp',
	'Translate4' => 'Frågor & svar',
	'Translate5' => 'Video',
	'Translate6' => 'Design hjälp',
	'Translate7' => 'Köpevillkor',
	'Translate8' => 'Kontakta oss',

];