<?php
// resources/lang/se/routes.php
return [
    'tempsticker'     => 'etiketter-och-namnlappar/klistermarker',
    'tempironc'       => 'etiketter-och-namnlappar/strykfast-farg',
    'tempgold'        => 'etiketter-och-namnlappar/klistermarker-guld',
    'tempsolv'        => 'etiketter-och-namnlappar/klistermarker-silver',
    'temptrans'       => 'etiketter-och-namnlappar/klistermarker-transparent',
    'tempcheap'       => 'etiketter-och-namnlappar/billiga',
    'goldorsolv'      => 'etiketter-och-namnlappar/guld-eller-silver',
    'tempiron'        => 'etiketter-och-namnlappar/strykfast',
    'tempreflection'  => 'etiketter-och-namnlappar/reflexmarker',
    'tempironfree'    => 'etiketter-och-namnlappar/strykfria',

    'namesticker' => 'namnlappar/klistermarker',
    'nameironc' => 'namnlappar/strykfast-farg',
    'namegold' => 'namnlappar/klistermarker-guld',
    'namesolv' => 'namnlappar/klistermarker-silver',
    'nametrans' => 'namnlappar/klistermarker-transparent',
    'namecheap' => 'namnlappar/billiga',
    'nameiron' => 'namnlappar/strykfast',
    'namereflection' => 'namnlappar/reflexmarker',
    'nameironfree' => 'namnlappar/strykfria',
    'namebase' => 'namnlappar/base',

    'merkclipart' => 'marklappar/clipartget',
    'merkbackground' => 'marklappar/backgroundget',
    'merkactive' => 'marklappar/stayactive',
    'merksticker' => 'marklappar/klistermarker',
    'merkironc' => 'marklappar/strykfast-farge',
    'merkgold' => 'marklappar/klistermarker-guld',
    'merksolv' => 'marklappar/klistermarker-silver',
    'merktrans' => 'marklappar/klistermarker-transparent',
    'merkcheap' => 'marklappar/billiga',
    'merkiron' => 'marklappar/strykfast',
    'merkreflection' => 'marklappar/reflexmarker',
    'merkironfree' => 'marklappar/strykfria',
    'merkbase' => 'marklappar/base',

    // other routes name
];
