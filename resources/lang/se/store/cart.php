<?php

return [
	
	/////////////////////////////////////////////////////
	//  						Messages 					//
	/////////////////////////////////////////////////////
	'Errors' => 'Please fix these errors: ',
	
	/////////////////////////////////////////////////////
	//  						Heading   					//
	/////////////////////////////////////////////////////
	'ShoppingCart' => 'Kundvagn',
	
	/////////////////////////////////////////////////////
	//  					Table headings   			    //
	/////////////////////////////////////////////////////
	'URL' => 'URL link till etikettdesign*' ,
	'Stickertype' => 'Etikett',
	'ProductType' => 'Sort',
	'Size' => 'Storlek mm',
	'Quantity' => 'Antal',
	'Price' => 'Pris inkl. moms',
	
	
	/////////////////////////////////////////////////////
	//  					Product types    			    //
	/////////////////////////////////////////////////////
	'Sticker' => 'Klisteretikett färg',
	'StickerBW' => 'Klisteretikett svart vit',
	'Ironon' => 'Strykfastetikett färg',
	'IrononBW' => 'Strykfastetikett svart vit',
	'Gold' => 'Guld etikett',
	'Silver' => 'Silver etikett',
	'Transparent' => 'Transparant etikett',
	'Reflection' => 'Reflex etikett',
	'Fabricsticker' => 'Strykfria namnlappar',
	'CouponCode' => 'Rabattkod',
	
	/////////////////////////////////////////////////////
	//  					Buttons			   			    //
	/////////////////////////////////////////////////////
	'EditButton' => 'Edit',
	'DeleteButton' => 'Radera',
	'StoreButton' => 'Tillbaka till butiken',
	'CheckoutButton' => 'Gå til kassa',
	
	/////////////////////////////////////////////////////
	//  					Extra Lang			   		    //
	/////////////////////////////////////////////////////
	'Shipping' => 'Porto og Expeditionskonstnader',
	'SigninMemo' => '*Du måste vara inloggad för att kunna dela dela din design',
	'DeleteWarning' => 'Är du säker på att du vill radera din etikett?',
	
	'Translate1' => 'Din kundvagn',
	'Translate2' => 'Din kundvagn i Markmasters web2print butik',
	'couponButton' => 'Lägg till rabattkod',
	'coupon' => 'Rabattkod',
];