<?php

return [
	'Translate1' => 'Namnlappar i färg, klisterlappar, reflexmärker och stryklappar',
	'Translate2' => 'Strykfria namnlappar, namnetiketter och märklappar, reflexmärker, skoetiketter, valfritt antal',
	'Translate3' => 'Klisteretikett i färg med laminat',
	'Translate4' => 'Högkvalitetsetikett med
								extra starkt lim och laminerat
								tryck. Oöverträffad
								slitstyrka för tuff användning',
	'Translate5' => 'Klisteretikett svart/vit',
	'Translate6' => 'Namnlappar som kan klistras
								på objekt. En  billig
								produkt med stort
								användningsområde',
	'Translate7' => 'Strykfastetikett färg',
	'Translate8' => 'Färgade namnlappar som
								styrks fast på kläder och
								textilier. Tål 60°C maskintvätt',
	'Translate9' => 'Strykfastetikett svart/vit',
	'Translate10' => 'Namnlappar i hög kvalitet
								som styrks fast på kläder och
								textilier. Tål 90°C maskintvätt',
	'Translate11' => 'Guld /silver etikett',
	'Translate12' => 'Klisteretikett i guld/silverfärgad
								folie.  Lämplig för
								märkning av pokaler 
								mm',
	'Translate13' => 'Strykfria namnlappar',
	'Translate14' => 'Våra strykfria namnlappar fäster på de flesta material',

	'Translate15' => 'Transparant etikett',
	'Translate16' => 'Klisteretikett i transparent
								folie. Lämplig för
								märkning av glas',
	'Translate17' => 'Reflexetikett',
	'Translate18' => 'Klisteretikett tryckt på folie
								med reflexegenskaper.
								När du vill synas extra bra
								i höstmörkret',
          'keywords' => 'Namnlappar, namnetiketter, klisterlappar, märklappar',


];