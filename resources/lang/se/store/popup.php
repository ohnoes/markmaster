<?php

return [
	'Title' => 'Porto och expeditionsavgift',
	'Part1' => 'Markmaster erbjuder fri frakt för alla beställningar över 145kr.',
  'Part2' => 'Din beställning är på: ',
  'Part3' => 'från beloppet för gratis frakt.',
  'Part4' => 'Vill du fortsätta till kassan?',
	'Button-confirm' => 'Fortsätt',
  'Button-negative' => 'Gå tillbaka',

];
