<?php

return [
	'Translate1' => 'Best�llningsstatus',
	'Translate2' => 'Status f�r din best�llning',
	'Translate3' => 'Det uppstod ett fel. V�nligen f�rs�k igen eller anv�nd ett annat kort. Auth Fail',
	'Translate4' => 'Det uppstod ett fel. V�nligen f�rs�k igen eller anv�nd ett annat kort. Capture Fail',
	'Translate5' => 'Betalning godk�nt. Din best�llning vill s�ndas med posten om 1-2 arbetsdagar.',
	'Translate6' => 'Best�llningsnummer',
	'Translate7' => 'Pris',
	'Translate8' => 'Best�llningsstatus',
	'Translate9' => 'Link til best�llning',
	'Translate10' => 'Faktura',
	'Translate11' => 'Klar for betalning',
	'Translate12' => 'Betald',
	'Translate13' => 'Exporterad',
	'Translate14' => 'Raderad',
	'Translate15' => 'N/A',
	'Translate16' => 'Best�llning',
	'Translate17' => 'Faktura',

];