<?php

return [
	'Translate1' => 'Dina etiketter',
	'Translate2' => 'Dina tidligare designede etiketter. Klicka på etiketterna för att beställa de på nytt.',
	'Translate3' => 'Dina Etiketter',
	'Translate4' => 'URL som kan delas med dina vänner',
	'Translate5' => 'Etikett',
	'Translate6' => 'Sort',
	'Translate7' => 'Klisteretikett färg',
	'Translate8' => 'Strykfastetikett färg',
	'Translate9' => 'Guld etikett',
	'Translate10' => 'Silver etikett',
	'Translate11' => 'Transparant etikett',
	'Translate12' => 'Klisteretikett svart vit',
	'Translate13' => 'Strykfastetikett svart vit',
	'Translate14' => 'Reflex etikett',
	
];