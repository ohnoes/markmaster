<?php

return [
	'Translate1' => 'Leveransadress',
	'Translate2' => 'Orderbekräftelse',
	'Translate3' => 'Datum',
	'Translate4' => 'Kundnr.',
	'Translate5' => 'Ordernr.',
	'Translate6' => 'Fakturaadress',
	'Translate7' => 'Etikett',
	'Translate8' => 'Type',
	'Translate9' => 'Storlek mm',
	'Translate10' => 'Antal',
	'Translate11' => 'Pris inkl. moms',
	'Translate12' => 'Klisteretikett färg med laminat',
	'Translate13' => 'Strykfastetikett färg',
	'Translate14' => 'Klisteretikett i guldfärgad folie',
	'Translate15' => 'Klisteretikett i silverfärgad folie',
	'Translate16' => 'Klisteretikett i transparent folie',
	'Translate17' => 'Klisteretikett svart vit',
	'Translate18' => 'Strykfastetikett',
	'Translate19' => 'Sum. inkl. moms.',
	'Translate20' => 'Porto/expeditionsavgift.',
	'Translate21' => 'Total sum.',
	
];