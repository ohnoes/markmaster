<?php

return [
	'Translate1' => 'Din beställning hos Markmaster',
	'Translate2' => 'Orderinfo',
	'Translate3' => 'URL link till etikettdesign',
	'Translate4' => 'Etikett',
	'Translate5' => 'Typ',
	'Translate6' => 'Storlek mm',
	'Translate7' => 'Antal',
	'Translate8' => 'Pris inkl. moms',
	'Translate9' => 'Link',
	'Translate10' => 'Klisteretikett  färg, med laminat',
	'Translate11' => 'Strykfastetikett färg',
	'Translate12' => 'Klisteretikett guld',
	'Translate13' => 'Klisteretikett silver',
	'Translate14' => 'Klisteretikett transparant',
	'Translate15' => 'Klisteretikett  svart vit',
	'Translate16' => 'Strykfastetikett svart vit',
	'Translate17' => 'Klisteretikett reflex',
	'Translate18' => 'Edit',
	'Translate19' => 'Ta bort',
	'Translate20' => 'Âr du säker på att du vill ta bort etiketten?',
	
];