<?php

return [
	'Translate1' => 'Betalning',
	'Translate2' => 'Leverans och betalningsinformation för din beställning',
	'Translate3' => 'Följande fel måste rättas:',
	'Translate4' => 'Leveransinformation',
	'Translate5' => 'Organisationsnamn',
	'Translate6' => 'Förnamn',
	'Translate7' => 'Efternamn',
	'Translate8' => 'Adress',
	'Translate9' => 'Postnummer',
	'Translate10' => 'Ort',
	'Translate11' => 'Kommun',
	'Translate12' => 'Land: ',
	'Translate13' => 'Fakturainformation',
	'Translate14' => 'Fakturainformation är den samma som leveransinformation',
	'Translate15' => 'OBS! Genom att trycka Betal bekräftar jag härmed att jag har läst och godkänt Villkor ',
	'Translate16' => 'Betala',
	'Translate17' => 'VILLKOR',
	'Translate18' => 'Vilkorena gäller kundens inköp och användning av varor och tjänster
		levererat av Markmaster AS',
	'Translate19' => 'PRISER OG LEVERANS',
	'Translate20' => 'Kunden får en automatisk orderbekräftelse på mail i det Markmaster
			AS har mottagit beställningen. Kunden är skyldig att kontrollera att denna
			överensstämmer med beställningen när det gäller produkttyp, innehåll,
			antal och pris.
			Alla priser är angivna priser inkl. Moms.
			Varorna kommer att levereras inom angiven leveranstid, vanligtvis 2-5
			arbetsdagar. Inte helgdagar.',
	'Translate21' => 'REKLAMATION OCH ÅNGERRÄTT',
	'Translate22' => 'Markmaster AS är skyldig att åtgärda brister på produktet om
			kunden innom rimlig tid ger besked om detta.
			Markmaster AS produkter är personligt tilverkat, det är därför
			ingen ångerrätt på produkterna efter att de är producerat.
			Kunden är själv ansvarig för att kontrollera att beställningen inte innehåller stav, eller gramatiska fel.
			Markmaster AS kontrollerar inte kunden beställning för fel innan
			beställningen går i produktion. Beställningen går direkt från web til
			print.',
	'Translate23' => 'UPPHOVSRÄTT',
	'Translate24' => 'Om kunden översänder ett eget motiv eller inverkar på produkten på annat sätt (personlig utformning av text), 
	försäkrar kunden Markmaster AS att text och motiv är fria från tredje parts rättigheter. 
	Eventuella brott mot upphovsrätten, personlighetsrätten eller namnrätten belastar i detta fall helt och hållet kunden. 
	Kunden försäkrar även att han eller hon inte skadar någon av tredje parts övriga rättigheter genom individualiseringen av produkten.
	
    Kunden befriar Markmaster AS från alla fordringar och anspråk som görs gällande på grund av brott mot sådana rättigheter 
	som tredje part har, om kunden är ansvarig för att en förpliktelse inte har uppfyllts. 
	Kunden ersätter Markmaster AS för alla kostnader för försvaret och övriga kostnader som uppstår.',
	
	
	'Translate25' => 'ÄNDRA VILKÅR',
	'Translate26' => 'Markmaster AS förbehåller sig rätten att ändra nuvarrande vilkår,
			bland annat som en följd av förändringar i lagstiftningen.',
	'Translate27' => 'Betal',
	
];