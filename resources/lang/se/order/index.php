<?php

return [
	'Translate1' => 'Dina beställningar hos Markmaster',
	'Translate2' => 'Dina beställningar',
	'Translate3' => 'Följande fel måste rättas:',
	'Translate4' => 'Dina beställningar',
	'Translate5' => 'Beställningsnummer',
	'Translate6' => 'Beställningsdatum',
	'Translate7' => 'Leverans adress',
	'Translate8' => 'Status',
	'Translate9' => 'Pris',
	'Translate10' => 'Klar för betalning',
	'Translate11' => 'Betald',
	'Translate12' => 'Exsporterad',
	'Translate13' => 'Raderad',
	'Translate14' => 'Hold',
	'Translate15' => 'N/A',
	'Translate16' => 'Info',
	'Translate17' => 'Betala',
	'Translate18' => 'Faktura',
	'Translate19' => 'Ta bort',
	'Translate20' => 'Är du säker på att du vill ta bort beställningen?',
	
	
];