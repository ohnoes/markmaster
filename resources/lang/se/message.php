<?php
// resources/lang/no/routes.php
return [
    'accountfound'    => 'E-postadress är redan registread på en användare, vänligen logga inn.',
    'newaccountmade'  => 'Ny användare har upprättats. Tillfälliga inloggningsuppgifter är: ',
    'newaccountmade2' => 'Vi rekommenderar att du ändrar ditt lösenord för inloggning.',
    // other routes name
];
