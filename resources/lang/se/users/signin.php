<?php

return [
	'Translate1' => 'Logga dig in hos Markmaster',
	'Translate2' => 'Logga dig in hos Markmasters web2print-butik',
	'Translate3' => 'Befintlig kund',
	'Translate4' => 'OBS! Det är inte möjligt att använda påloggningen från Markmaster gamla nettbutik. Alla gamla kunder (1/12-2014 och äldre)måste upprätta sig som ny kund',
	'Translate5' => 'Email',
	'Translate6' => 'Skriv in email',
	'Translate7' => 'Lösenord',
	'Translate8' => 'Skriv in ditt lösenord',
	'Translate9' => 'Logga In',
	'Translate10' => 'Glemt Lösenord',
	'Translate11' => 'Ny kund',
	'Translate12' => 'Det er lätt att upprätta ett nytt kundkonto',
	'Translate13' => 'Skapa nytt kundkonto',
	
];