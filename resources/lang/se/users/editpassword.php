<?php

return [
	'Translate1' => 'Ändra ditt lösenord',
	'Translate2' => 'Ändra ditt lösenord på Markmasters nettbutik',
	'Translate3' => 'Uppdatera ditt lösenord',
	'Translate4' => 'Följande fel måste rättas: ',
	'Translate5' => 'Ditt lösenord:',
	'Translate6' => 'Gammalt lösenord',
	'Translate7' => 'Lösenord:',
	'Translate8' => 'Lösenord',
	'Translate9' => 'Bekräfta ditt lösenord:',
	'Translate10' => 'Bekräfta',
	'Translate11' => 'Uppdatera lösenord',

];