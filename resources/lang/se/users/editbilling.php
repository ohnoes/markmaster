<?php

return [
	'Translate1' => 'Ändra din faktura-adress',
	'Translate2' => 'Ändra din faktura-adress på Markmasters nettbutik',
	'Translate3' => 'Uppdatera standard fakturaadress',
	'Translate4' => 'Följande fel måste rättas: ',
	'Translate5' => 'Företag/organisationsnamn:',
	'Translate6' => 'Förnamn:',
	'Translate7' => 'Efternamn:',
	'Translate8' => 'Adress:',
	'Translate9' => 'Postnr:',
	'Translate10' => 'Ort:',
	'Translate11' => 'Land:',
	'Translate12' => 'Uppdatera information',
	

];