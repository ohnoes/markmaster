<?php

return [
	'Translate1' => 'Ändra dina inloggningsuppgifter',
	'Translate2' => 'Ändra dina inloggningsuppgifter på Markmasters nettbutik',
	'Translate3' => 'Följande fel måste rättas:',
	'Translate4' => 'Uppdatera kund info',
	'Translate5' => 'Här kan du uppdatera din kundinformation.',
	'Translate6' => 'Tryck på informationen du önskar att ändra.',
	'Translate7' => 'Kundinformation',
	'Translate8' => 'Faktureringsadress',
	'Translate9' => 'Lösenord',

];