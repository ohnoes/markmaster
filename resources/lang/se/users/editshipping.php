<?php

return [
	'Translate1' => 'Ändra din leveransadress',
	'Translate2' => 'Ändra din leveransadress på Markmasters nettbutik',
	'Translate3' => 'Uppdatera standard kundinformation',
	'Translate4' => 'Följande fel måste rättas: ',
	'Translate5' => 'Företag/organisationsnamn:',
	'Translate6' => 'Förnamn:',
	'Translate7' => 'Efternamn:',
	'Translate8' => 'Adress:',
	'Translate9' => 'Postnr:',
	'Translate10' => 'Ort:',
	'Translate11' => 'Land:',
	'Translate12' => 'Uppdatera information',
	'Translate13' => 'Ja tack, jag vill ha nyhetsbrev, 
								rabattkoder och andra erbjudanden från 
								Markmaster.com',
	'Translate14' => 'E-post:',
	'Translate15' => 'Telefon:',
	

];