<?php

return [
	'Translate1' => 'Klistermärke svart vit',
	'Translate2' => 'Skapa din egen klisteretikett. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Klistermärke svart vit',
	'Translate4' => 'Självhäftande hvit namnlapp med svart tryck. Är ett billigt och bra alternativ. Tilvärkat i ett miljövänligt material. ',
	'Translate5' => 'Passar för märkning av barnens matlådor, drickflaskor, böcker, och f.eks homepartiesprodukter, kosmetik mm. Tål diskmaskin 60°C',
	'Translate6' => '2 storlekar  37x16 mm och 60x26 mm',
	'Translate7' => 'Levereras på rull.',
	'Translate8' => 'Välg "Nytt Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate9' => '',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',

	
	
];