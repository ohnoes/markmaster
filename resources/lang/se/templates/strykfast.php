<?php

return [
	'Translate1' => 'Strykfast-etikett svart vit',
	'Translate2' => 'Skapa din egen strykfast-etikett. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Strykfastetikett svart vit',
	'Translate4' => 'Vår bästsäljare genom alla år. 
						För enkel märkning av alla slags kläder, handskar m.m. 
						Monteras enkelt med strykjärn och tål tvätt upp t.o.m  95°C',
	'Translate5' => 'Fast storlek 30 x 10 mm',
	'Translate6' => 'Levereras på rulle.',
	'Translate7' => 'Välg "Nytt Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate8' => '',
	'Translate9' => '',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',

	
	
];