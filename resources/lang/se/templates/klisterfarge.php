<?php

return [
	'Translate1' => 'Färgklistermärken med laminat',
	'Translate2' => 'Skapa din egen namnlapp i färg. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Färgklistermärken med laminatförstärkning',
	'Translate4' => 'Etiketterna är tillverkat av "Murfolie" som sitter extra bra. Dessutom är de överlaminerade med en tunn klar film som gör dem extremt tåliga mot mekanisk slitage och lösningsmedel.',
	'Translate5' => 'Typisk användningsområden är barnens matlådor, drickflaskor, böcker, och f.eks homepartiesprodukter, foto, data og sportsutstyr, verktyg och kosmetik mm. Tål diskmaskin 60°C .
						Den kan också användas för att märka
						skor (klistras nere på sulan) eller till klubbmerken. ',
	'Translate6' => 'Valfri storlek, antal och form.',
	'Translate7' => 'Levereras på ark.',
	'Translate8' => 'Välg "Enkel Namnlapp eller Egen Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate9' => '',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',
	'Translate13' => 'Namnlapp',
	'Translate14' => '1. Enkel Namnlapp',
	'Translate15' => 'Namnlapp 30x13 mm',
	'Translate16' => '2. Egen Design',
	'Translate17' => 'Valfri storlek och form',



];
