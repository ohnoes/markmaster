<?php

return [
	'Translate1' => 'Klistermärke med reflex',
	'Translate2' => 'Skapa din egen klisteretikett med reflex. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Klistermärke med reflex',
	'Translate4' => 'Nu kan du skapa dina egna reflexsmerken
						med valfritt färgtryck.
						Du kan själv definiera storleken. ',
	'Translate5' => 'Märk din cykel med en
						personlig etikett som ger extra 
						synlighet i mörkret. ',
	'Translate6' => 'Valfri storlek, antal och form.',
	'Translate7' => 'Levereras på ark.',
	'Translate8' => 'Välg "Nytt Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate9' => '',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',

	
	
];