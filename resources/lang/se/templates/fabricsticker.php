<?php

return [
	'Translate1' => 'Strykfria namnlappar',
	'Translate2' => 'Skapa din egen namnlapp i färg. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Strykfria namnlappar',
	'Translate4' => 'Namnlappar som enkelt kan klistras på allt av kläder som har tvättrådsetikett/märkeslapp',
	'Translate5' => 'Passar också till att märka andra ägodelar då de fäster på allt. ',
	//
	'Translate6' => 'Valfri storlek, antal och form.',
	'Translate7' => 'Levereras på ark.',
	'Translate8' => 'Välg "Nytt Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate9' => '',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',
	//
	'Translate13' => 'Riktigt monterat tål strykfria namnlappar 40 grader i tvättmaskin och 60 grader i diskmaskin',
	'Translate14' => 'Instruktion
Klistra namnlappen på en ren och torr plats.
På kläder sitter namnlappen bäst på tvättinstruktionen eller på lappen med storlek/märke.

Pressa ner namnlappen så att den får bra fäste i underlaget, var extra noga med lappens hörn.
Vänta 24 timmar innan plagget utsätts för tvätt.',
	'Translate15' => 'För kläder som skall tvättas på högre temperatur än 40 grader eller inte har tvättrådsetikett/märkeslapp rekomenderar vi våra vanliga strykfastetiketter.',

	
	
];


