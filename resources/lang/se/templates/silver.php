<?php

return [
	'Translate1' => 'Silverfärgat klistermärke',
	'Translate2' => 'Skapa din egen silveretikett. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Klisteretikett silver',
	'Translate4' => 'Etiketter i silverfärgat vinyl med svart tryck.',
	'Translate5' => 'Använd vår designmodul eller lasta opp en egen design från ditt favoritprogram. ',
	'Translate6' => 'Perfekt för märkning av premier, designprodukter mm',
	'Translate7' => 'Valfri storlek, antal och form.',
	'Translate8' => 'Levereras på ark.',
	'Translate9' => 'Välg "Nytt Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',

	
	
];