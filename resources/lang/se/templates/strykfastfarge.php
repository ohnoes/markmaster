<?php

return [
	'Translate1' => 'Strykfast-etikett i färg',
	'Translate2' => 'Skapa din egen strykfast-etikett i färg. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Namnlapp i färg',
	'Translate4' => 'Etiketten är producerad i ett material som
						stryks fast på tyg med vanligt strykjärn. ',
	'Translate5' => 'Riktigt monterat så tål märket 60°C tvätt.
						Använd vår designmodul eller lasta opp en egen design från ditt favoritprogram.',
	'Translate6' => 'Perfekt för märkning kläder och små serier
						med transfermärken till klubbar.',
	'Translate7' => 'Valfri storlek, antal och form.',
	'Translate8' => 'Levereras på ark.',
	'Translate9' => 'Välg "Nytt Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',

	
	
];