<?php

return [
	'Translate1' => 'Guld eller silverfärgat klistermärke',
	'Translate2' => 'Skapa din egen guld / silver etikett. Starta på ett nytt design eller använd en av Markmasters mallar som starthjälp',
	'Translate3' => 'Klisteretikett guld / silver',
	'Translate4' => 'Etiketter i guld / silverfärgat vinyl med svart tryck.',
	'Translate5' => 'Använd vår designmodul eller lasta opp en egen design från ditt favoritprogram. ',
	'Translate6' => 'Perfekt för märkning av premier, designprodukter mm',
	'Translate7' => 'Valfri storlek, antal och form.',
	'Translate8' => 'Levereras på ark.',
	'Translate9' => 'Välg "Nytt Design" för att starta ett design från noll, eller välg en av våra mallar som hjälper dig med designen.',
	'Translate10' => 'Nytt Design',
	'Translate11' => 'Starta ett nytt design',
	'Translate12' => 'SE',
	'Translate13' => 'Guldfärgat klistermärke',
	'Translate14' => 'Etikett i guldfärgad 
								vinyl med svart tryck. För postlådor, produktmärkning, pokaler och premier mm.',
	'Translate15' => 'Silverfärgat klistermärke',
	'Translate16' => 'Etikett i silverfärgad 
								vinyl med svart tryck. För postlådor, produktmärkning, pokaler och premier mm.',

	
	
];