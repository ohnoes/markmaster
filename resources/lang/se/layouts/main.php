<?php

return [

	/////////////////////////////////////////////////////
	//  						Header 						//
	/////////////////////////////////////////////////////
	'Homepage' => 'Hem',
	'Products' => 'Produkter',
	'ShoppingCart' => 'Kundvagn',
	'Login' => 'Logga In',
	'User' => 'Användare',

	/*---- Header dropdown items for Products ----*/
	'Sticker' => 'Klisteretikett färg',
	'StickerBW' => 'Klisteretikett svart vit',
	'Ironon' => 'Strykfastetikett färg',
	'IrononBW' => 'Strykfastetikett svart vit',
	'Gold' => 'Guld etikett',
	'Silver' => 'Silver etikett',
	'Transparent' => 'Transparant etikett',
	'Reflection' => 'Reflex etikett',
       'Fabricsticker' => 'Strykfria namnlappar',


	/*---- Header dropdown items for Login ----*/
	'UserLogin' => 'Logga In',
	'NewAccount' => 'Ny Användare',

	/*---- Header dropdown items for User ----*/
	'UserStickers' => 'Mina Etiketter',
	'UserOrders' => 'Mina Beställningar',
	'UserAccount' => 'Användarkonto',
	'Logout' => 'Logga ut',

	/////////////////////////////////////////////////////
	//  						Footer	 						//
	/////////////////////////////////////////////////////
	'Social' => 'MEDIER',
	'Help' => 'HJÄLP',
	'Payment' => 'BETALNINGSALTERNATIV',

	/*---- Footer Social list items ----*/
	'Twitter' => 'Twitter',
	'Facebook' => 'Facebook',

	/*---- Footer Help list items ----*/
	'FAQ' => 'Frågor & svar',
	'Video' => 'Video',
	'CommercialAgents' => 'Design Hjälp',
	'Contact' => 'Kontakta Oss',
	'Terms' => 'Köpvillkor',

	'orgnumber' => 'org. nr.',
	'address' => 'Legevegen 16',
	'address2' => '5542 Karmsund',
	'phone' => 'Tel. 031-799 55 19',
	'country' => 'Norge',
];
