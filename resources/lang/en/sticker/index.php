<?php

return [
	'Translate1' => 'Your labels',
	'Translate2' => 'Your previous designed labels. Click on the label to purchase more.',
	'Translate3' => 'Your Labels',
	'Translate4' => 'Shareable URL',
	'Translate5' => 'Label',
	'Translate6' => 'Type',
	'Translate7' => 'Stickers colored, laminated',
	'Translate8' => 'Iron on labels, colored',
	'Translate9' => 'Gold Sticker',
	'Translate10' => 'Silver Sticker',
	'Translate11' => 'Transparent Sticker',
	'Translate12' => 'Stickers in black and white',
	'Translate13' => 'Iron on labels',
	'Translate14' => 'Reflective Sticker',
	'Translate15' => 'Size',
	'Translate16' => 'Fabric Sticker'

];
