<?php
// resources/lang/no/routes.php
return [
    'accountfound'    => 'That e-mail address is already registered as a user, please login.',
    'newaccountmade'  => 'Your user account have now been created. Temporary login informasion is: ',
    'newaccountmade2' => 'We recommend that you change your password.',
    // other routes name
];
