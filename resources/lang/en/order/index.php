<?php

return [
	'Translate1' => 'Your orders at Markmaster',
	'Translate2' => 'Your orders',
	'Translate3' => 'Please fix these errors:',
	'Translate4' => 'Your Orders',
	'Translate5' => 'Ordernumber',
	'Translate6' => 'Order date',
	'Translate7' => 'Shipping address',
	'Translate8' => 'Status',
	'Translate9' => 'Price',
	'Translate10' => 'Ready for payment',
	'Translate11' => 'Paid',
	'Translate12' => 'Exported',
	'Translate13' => 'Cancelled',
	'Translate14' => 'Hold',
	'Translate15' => 'N/A',
	'Translate16' => 'Info',
	'Translate17' => 'Pay',
	'Translate18' => 'Invoice',
	'Translate19' => 'Delete',
	'Translate20' => 'Are you sure you want to delete your order?',
	
	
];