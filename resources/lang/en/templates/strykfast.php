<?php

return [
	'Translate1' => 'Iron on label in black and white',
	'Translate2' => 'Create your own iron on labels. Start a new design or use one of Markmasters templates',
	'Translate3' => 'White iron on labels with black print', 
	'Translate4' => 'Our best seller! 
						Labeling all sorts of fabrics, clothing, mittens, and more. Easily attached with an steam iron and can sustain up to 95°C/200°F wash.',
	'Translate5' => 'Size is 30 x 10 mm / 1.2 x 0.4 inches',
	'Translate6' => 'Produced and delivered on a roll.', 
	'Translate7' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate8' => '',
	'Translate9' => '',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',

	
	
];