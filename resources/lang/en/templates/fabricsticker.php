﻿<?php

return [
	'Translate1' => 'Fabric sticker', 
	'Translate2' => 'Create your own fabric stickers. Start a new design or use one of Markmasters templates',
	'Translate3' => 'Fabric sticker', 
	'Translate4' => 'are name labels that can be attached, without a steam iron, on all clothing with a wash instructions tag',
	'Translate5' => 'Can also be used on other possesions as the adhesive stick to most materials. ',
	//
	'Translate6' => 'With optional size, quantity and shape.', 
	'Translate7' => 'Produced on a sheet of paper.',
	'Translate8' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate9' => '',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',
	//
	'Translate13' => 'If they are correctly attached they can sustain up to 40°C/104°F wash.', 
	'Translate14' => 'Attach the label on the wash instructions tag, and massage on the sticker. It is importaint to put pressure on all corners and edges. 
								Please wait 24 hours before washing the fabric.', 
	'Translate15' => 'For clothing that are going to be washed on a temperature higher than 40°C/104°F, or are missing a wash instruction tag, we recommend our iron on labels.',
	

	
	
];