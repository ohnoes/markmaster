<?php

return [
	'Translate1' => 'Gold or silver sticker',
	'Translate2' => 'Make your own gold or silver sticker. Start with a new design or use one of Markmasters templates',
	'Translate3' => 'Gold or silver sticker',
	'Translate4' => 'Labels with a golden or silver background in black print.',
	'Translate5' => 'Use our design module or upload your own design from your favorite program.',
	'Translate6' => 'Well suited for labeling of trophies, design products etc.',
	'Translate7' => 'With optional size, quantity and shape.',
	'Translate8' => 'Produced on a sheet of paper.',
	'Translate9' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',
	'Translate13' => 'Gold sticker',
	'Translate14' => 'A sticker with a golden background. Excellent for labeling trophies etc.',
	'Translate15' => 'Silver sticker',
	'Translate16' => 'A sticker with a silver background. Excellent for labeling trophies etc.',
	
	
];