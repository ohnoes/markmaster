﻿<?php

return [
	'Translate1' => 'Reflective sticker',
	'Translate2' => 'Make your own sticker with reflection. Start a new design or use one of Markmasters templates',
	'Translate3' => 'Reflective sticker',
	'Translate4' => 'You can now make your own reflective labels with optional colors. You can choose the size yourself. ',
	'Translate5' => 'For example, label your bikes with a personal label that makes you more visible in the dark.',
	'Translate6' => 'With optional size, quantity, and shape.',
	'Translate7' => 'Produced on a sheet of paper.',
	'Translate8' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate9' => '',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',

	
	
];