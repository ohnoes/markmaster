<?php

return [
	'Translate1' => 'Update billing address',
	'Translate2' => 'Update your billing address at Markmaster',
	'Translate3' => 'Update billing address',
	'Translate4' => 'The following errors need to be corrected: ',
	'Translate5' => 'Organization:',
	'Translate6' => 'First name:',
	'Translate7' => 'Last name:',
	'Translate8' => 'Address:',
	'Translate9' => 'Zip code:',
	'Translate10' => 'City:',
	'Translate11' => 'Country:',
	'Translate12' => 'Update information',
	

];