<?php

return [
	'Translate1' => 'Markmasters Terms and Conditions',
	'Translate2' => 'Here are Markmasters Terms and Conditions',
	'Translate3' => 'TERMS AND CONDITIONS',
	'Translate4' => 'These terms concerns the customer\'s payment and use of the goods and services delivered by Markmaster AS',
	'Translate5' => 'PRICE AND SHIPPING',
	'Translate6' => 'An automatic order confirmation will be emailed out as soon as Markmaster AS has received the payment, 
			and it\'s the customers responsibility to make sure that all the information (product type, content, quantity and price) is correct. 
			All prices displayed are including VAT. The estimated shipping time for your order depends on destination, but most 
			orders whould arrive within 2 weeks.',
	'Translate7' => 'RETURN AND REPLACEMENTS',
	'Translate8' => 'It\'s Markmaster AS duty to replace or refund any missing or defected products if the customer notifies Markmaster within a reasonable time.
			Markmaster AS products are customized for each user, therefore there is no right of withdrawel after the product has been produced. 
			It is the customers responsibility to make sure that their orders does not contain any typographical- or grammatical errors. 
			Markmaster AS does not inspect each product for errors before production, since the order goes straight from web to print.',
	'Translate9' => 'COPYRIGHTED MATERIAL',
	'Translate10' => 'If the customer uploads their own cliparts or images, the customer assures Markmaster AS that no text or picture are a third party copyright infringement.
			Any infringements will be on the customer. The customer release Markmaster AS from all injunctions and demands made based on these thir parties infringements.
			The customer will be responsible for all legal fees or other damages caused by the infridgement.',
	'Translate11' => 'SUBJECT TO CHANGE',
	'Translate12' => 'These terms and conditions are subject to change without notice, from time to time in our sole discretion.',

];