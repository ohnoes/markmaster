<?php

return [
	'Translate1' => 'Order status',
	'Translate2' => 'Status for your order',
	'Translate3' => 'Something went wrong. Please try again or use a different card. Auth Fail',
	'Translate4' => 'Something went wrong. Please try again or use a different card. Capture Fail',
	'Translate5' => 'Payment accepted. Your order will be processed and shipped within 1-2 business days.',
	'Translate6' => 'Order number',
	'Translate7' => 'Price',
	'Translate8' => 'Order status',
	'Translate9' => 'Your order',
	'Translate10' => 'Invoice',
	'Translate11' => 'Ready for payment',
	'Translate12' => 'Paid',
	'Translate13' => 'Processed',
	'Translate14' => 'Cancelled',
	'Translate15' => 'N/A',
	'Translate16' => 'Order',
	'Translate17' => 'Invoice',

];