﻿<?php

return [
	
	/////////////////////////////////////////////////////
	//  						Messages 					//
	/////////////////////////////////////////////////////
	'Errors' => 'Please fix these errors: ',
	
	/////////////////////////////////////////////////////
	//  						Heading   					//
	/////////////////////////////////////////////////////
	'ShoppingCart' => 'Shopping cart',
	
	/////////////////////////////////////////////////////
	//  					Table headings   			    //
	/////////////////////////////////////////////////////
	'URL' => 'URL link to label design*' ,
	'Stickertype' => 'Label',
	'ProductType' => 'Type',
	'Size' => 'Size mm',
	'Quantity' => 'Quantity',
	'Price' => 'Price inc. VAT',
	
	
	/////////////////////////////////////////////////////
	//  					Product types    			    //
	/////////////////////////////////////////////////////
	'Sticker' => 'Stickers colored, laminated',
	'StickerBW' => 'Stickers in black/white',
	'Ironon' => 'Iron on labels colored',
	'IrononBW' => 'Iron on labels',
	'Gold' => 'Gold sticker',
	'Silver' => 'Silver sticker',
	'Transparent' => 'Transparent sticker',
	'Reflection' => 'Reflective sticker',
	'Fabricsticker' => 'Fabric sticker',
	'CouponCode' => 'Coupon code',
	
	/////////////////////////////////////////////////////
	//  					Buttons			   			    //
	/////////////////////////////////////////////////////
	'EditButton' => 'Edit',
	'DeleteButton' => 'Delete',
	'StoreButton' => 'Back to store',
	'CheckoutButton' => 'Checkout',
	
	/////////////////////////////////////////////////////
	//  					Extra Lang			   		    //
	/////////////////////////////////////////////////////
	'Shipping' => 'Shipping and Handling Fees',
	'SigninMemo' => '*You have to login to share your design',
	'DeleteWarning' => 'Are you sure you want to delete your label?',
	
	'Translate1' => 'Your shopping cart',
	'Translate2' => 'Your shopping cart in Markmasters web2print store',
	'couponButton' => 'Add coupon code',
	'coupon' => 'Coupon code',
];