<?php

return [
	'Title' => 'Shipping and Expediting',
	'Part1' => 'Markmaster offers free shipping for all orders over 145kr.',
  'Part2' => 'Your order is currently: ',
  'Part3' => 'away to qualify for free shipping.',
  'Part4' => 'Do you want to continue checking out?',
	'Button-confirm' => 'Continue',
  'Button-negative' => 'Back to store',

];
