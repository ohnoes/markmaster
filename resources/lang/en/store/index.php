﻿<?php

return [
	'Translate1' => 'Markmaster',
	'Translate2' => 'Markmasters web2print store. Order labels, stickers, iron on labels and more.',
	'Translate3' => 'Laminated stickers',
	'Translate4' => 'High quality laminated stickers with
								extra strong adhesive with unparalleled 
								durability for heavy use.',
	'Translate5' => 'Stickers in black/white',
	'Translate6' => 'Labels that can stick to objects. A fair product with a broad usuage',
	'Translate7' => 'Iron on label colored',
	'Translate8' => 'Colored labels that can be ironed on to clothes and textiles. Can handle a 60°C wash',
	'Translate9' => 'Iron on labels black/white',
	'Translate10' => 'High quality labels that can be ironed onto clothes and textiles. Can handle a 90°C wash',
	'Translate11' => 'Gold/Silver sticker',
	'Translate12' => 'A sticker with a golden or silver background. Excellent for labeling trophies etc.',
	'Translate13' => 'Fabric sticker',
	'Translate14' => 'A sticker that can be used on fabric. Easy to attach with no need of a steam iron.',
	'Translate15' => 'Transparent sticker',
	'Translate16' => 'Transparent sticker. Excellent for labeling objects made of glass.',
	'Translate17' => 'Reflective sticker',
	'Translate18' => 'Label with reflective properties. For greater visibility in the dark.',
	
	];