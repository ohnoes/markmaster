<?php

return [
	'Translate1' => 'Sticker black/white',
	'Translate2' => 'Make your own stickers. Start with a new design or start with one or Markmasters templates',
	'Translate3' => 'Sticker black/white',
	'Translate4' => 'White sticker with black print. Is an affordable and good alternative. Made with environmental friendly material. ',
	'Translate5' => 'Excellent for labeling bottles, books, and commodities like homeparties products and cosmetics etc. Can handle a 60°C wash',
	'Translate6' => '2 sizes  37x16 mm and 60x26 mm',
	'Translate7' => 'Produced and delivered in a roll.',
	'Translate8' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate9' => '',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',
];