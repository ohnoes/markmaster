<?php

return [
	'Translate1' => 'Gold sticker',
	'Translate2' => 'Make your own gold sticker. Start with a new design or use one of Markmasters templates',
	'Translate3' => 'Gold sticker',
	'Translate4' => 'Labels with a golden background in black print.',
	'Translate5' => 'Use our design module or upload your own design from your favorite program.',
	'Translate6' => 'Well suited for labeling of trophies, design products etc.',
	'Translate7' => 'With optional size, quantity and shape.',
	'Translate8' => 'Produced on a sheet of paper.',
	'Translate9' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',

	
	
];