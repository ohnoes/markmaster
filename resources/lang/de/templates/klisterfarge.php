<?php

return [
	'Translate1' => 'Colored stickers',
	'Translate2' => 'Create your own colored stickers. Start a new design or use one of Markmasters templates',
	'Translate3' => 'Laminated Stickers',
	'Translate4' => 'The labels are created with an extra strong adhesive. They are also laminated which makes them very durable against mechanical wear and solvents.',
	'Translate5' => 'The labels can handle a wash up to 60°C/140°F.
						Well suited for tools, water bottles, lunch boxes, cameras, computers, and sports equipment.
						You  can also label your shoes (attach the sticker on your shoe insert). ',
	'Translate6' => 'With optional size, quantity and shape.',
	'Translate7' => 'Produced on a sheet of paper.',
	'Translate8' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate9' => '',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',
	'Translate13' => 'Nametag',

	

];
