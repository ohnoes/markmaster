<?php

return [
	'Translate1' => 'Colored iron on labels', 
	'Translate2' => 'Create your own colored iron on labels. Start a new design or use one of Markmasters templates',
	'Translate3' => 'Iron on label, colored', 
	'Translate4' => 'The labels are produced in a material that are attach to fabric by using a normal steam iron. ',  
	'Translate5' => 'If they are correctly attached they can sustain up to 60°C/140°F wash.
						You can use our design module or upload your own design created in your favorite program.',
	'Translate6' => 'Perfect to label clothing.',
	'Translate7' => 'With optional size, quantity and shape.', 
	'Translate8' => 'Produced on a sheet of paper.',
	'Translate9' => 'Select "New Design" to start a design from scratch, or pick one of our templates as a starting point.',
	'Translate10' => 'New Design',
	'Translate11' => 'Start a new design',
	'Translate12' => 'NO',

	
	
];