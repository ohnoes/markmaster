<?php

return [
	'Translate1' => 'Update shipping address', 
	'Translate2' => 'Update shipping address at Markmaster',
	'Translate3' => 'Update your shipping address',
	'Translate4' => 'The following errors need to be corrected: ',
	'Translate5' => 'Organization:',
	'Translate6' => 'First name:',
	'Translate7' => 'Last name:',
	'Translate8' => 'Address:',
	'Translate9' => 'Zip code:',
	'Translate10' => 'City:',
	'Translate11' => 'Country:',
	'Translate12' => 'Update information',
	'Translate13' => 'Yes, I would like to receive email 
								information about discounts 
								and offers from Markmaster.no',
	'Translate14' => 'Email:',
	'Translate15' => 'Phone:',
	

];