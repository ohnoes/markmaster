<?php

return [
	'Translate1' => 'Create a new user at Markmaster',
	'Translate2' => 'Create a new user at Markmasters web2print store',
	'Translate3' => 'New user',
	'Translate4' => 'The following errors need to be corrected: ',
	'Translate5' => 'Organization',
	'Translate6' => 'First name',
	'Translate7' => 'Last name',
	'Translate8' => 'Address',
	'Translate9' => 'Zip code',
	'Translate10' => 'City',
	'Translate11' => 'Country',
	'Translate12' => 'Email',
	'Translate13' => 'Password',
	'Translate14' => 'Confirm password',
	'Translate15' => 'Confirm',
	'Translate16' => 'Phone',
	'Translate17' => 'Yes, I would like to receive email 
								information about discounts 
								and offers from Markmaster.no',
	'Translate18' => 'Register user',

];