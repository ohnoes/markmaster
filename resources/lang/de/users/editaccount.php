<?php

return [
	'Translate1' => 'Update account information', 
	'Translate2' => 'Update your account information at Markmaster',
	'Translate3' => 'Please fix these errors:',
	'Translate4' => 'Update account information', 
	'Translate5' => 'Update your user information here',
	'Translate6' => 'Please click the information you wish to update',
	'Translate7' => 'User information ',
	'Translate8' => 'Billing information',
	'Translate9' => 'Password',

];