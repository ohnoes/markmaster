<?php

return [
	'Translate1' => 'Login at Markmaster',
	'Translate2' => 'Login at Markmasters web2print store',
	'Translate3' => 'Existing customer',
	'Translate4' => ' ', 
	'Translate5' => 'Email',
	'Translate6' => 'Enter email',
	'Translate7' => 'Password',
	'Translate8' => 'Enter password',
	'Translate9' => 'Login',
	'Translate10' => 'Forgotten password',
	'Translate11' => 'New user',
	'Translate12' => 'It is easy to create a new account',
	'Translate13' => 'Create new user',
	
];