<?php

return [
	'Translate1' => 'Update password',
	'Translate2' => 'Update your password at Markmaster',
	'Translate3' => 'Update password',
	'Translate4' => 'The following errors need to be corrected: ',
	'Translate5' => 'Your Password:',
	'Translate6' => 'Old Password',
	'Translate7' => 'Password:',
	'Translate8' => 'Password',
	'Translate9' => 'Confirm Password:',
	'Translate10' => 'Confirm',
	'Translate11' => 'Update password',

];