<?php

return [
	'Translate1' => 'Payment',
	'Translate2' => 'Shipping and payment address for your order',
	'Translate3' => 'The following errors need to be corrected:',
	'Translate4' => 'Shipping Address',
	'Translate5' => 'Org.',
	'Translate6' => 'Firstname',
	'Translate7' => 'Lastname',
	'Translate8' => 'Address',
	'Translate9' => 'Zipcode',
	'Translate10' => 'City',
	'Translate11' => 'State',
	'Translate12' => 'Country: ',
	'Translate13' => 'Billing Address',
	'Translate14' => 'Billing address is the same as shipping address',
	'Translate15' => 'OBS! By clicking pay I confirm that I have read and accepted the Terms and Conditions',
	'Translate16' => 'Pay',
	'Translate17' => 'TERMS AND CONDITIONS',
	'Translate18' => 'These terms concerns the customer\'s payment and use of the goods and services delivered by Markmaster AS',
	'Translate19' => 'PRICE AND SHIPPING',
	'Translate20' => 'An automatic order confirmation will be emailed out as soon as Markmaster AS has received the payment, 
			and it\'s the customers responsibility to make sure that all the information (product type, content, quantity and price) is correct. 
			All prices displayed are including VAT and shipping. The estimated shipping time for your order depends on destination, but most 
			orders whould arrive within 2 weeks.',
	'Translate21' => 'RETURN AND REPLACEMENTS',
	'Translate22' => 'It\'s Markmaster AS duty to replace or refund any missing or defected products if the customer notifies Markmaster within a reasonable time.
			Markmaster AS products are customized for each user, therefore there is no right of withdrawel after the product has been produced. 
			It is the customers responsibility to make sure that their orders does not contain any typographical- or grammatical errors. 
			Markmaster AS does not inspect each product for errors before production, since the order goes straight from web to print.',
	'Translate23' => 'COPYRIGHTED MATERIAL',
	'Translate24' => 'If the customer uploads their own cliparts or images, the customer assures Markmaster AS that no text or picture are a third party copyright infringement.
			Any infringements will be on the customer. The customer release Markmaster AS from all injunctions and demands made based on these thir parties infringements.
			The customer will be responsible for all legal fees or other damages caused by the infridgement.',
	'Translate25' => 'SUBJECT TO CHANGE',
	'Translate26' => 'These terms and conditions are subject to change without notice, from time to time in our sole discretion.',
	'Translate27' => 'Pay',
	
];