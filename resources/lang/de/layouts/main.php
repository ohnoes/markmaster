<?php

return [
	
	/////////////////////////////////////////////////////
	//  						Header 						//
	/////////////////////////////////////////////////////
	'Homepage' => 'Homepage',
	'Products' => 'Products',
	'ShoppingCart' => 'Shopping cart',
	'Login' => 'Login',
	'User' => 'User',
	
	/*---- Header dropdown items for Products ----*/
	'Sticker' => 'Sticker colored',
	'StickerBW' => 'Sticker black/white',
	'Ironon' => 'Iron on label colored',
	'IrononBW' => 'Iron on label black/white',
	'Gold' => 'Gold label',
	'Silver' => 'Silver label',
	'Transparent' => 'Transparent label',
	'Reflection' => 'Reflection label',
	'Fabricsticker' => 'Fabric sticker',
	
	/*---- Header dropdown items for Login ----*/
	'UserLogin' => 'Login',
	'NewAccount' => 'New user',
	
	/*---- Header dropdown items for User ----*/
	'UserStickers' => 'My labels',
	'UserOrders' => 'My orders',
	'UserAccount' => 'My account',
	'Logout' => 'Logout',
	
	/////////////////////////////////////////////////////
	//  						Footer	 						//
	/////////////////////////////////////////////////////
	'Social' => 'Social media',
	'Help' => 'Help',
	'Payment' => 'Payment solutions',
	
	/*---- Footer Social list items ----*/
	'Twitter' => 'Twitter',
	'Facebook' => 'Facebook',
	
	/*---- Footer Help list items ----*/
	'FAQ' => 'Questions and answers',
	'Video' => 'Video',
	'CommercialAgents' => 'Design Help',
	'Contact' => 'Contact us',
	'Terms' => 'Purchase terms',
	
	'loggedinas' => 'You are logged in as: ',
	'loginmsg' => 'Thank you for logging in',
	
	'orgnumber' => 'org. number',
	'address' => '6133 Sherwood Way',
	'address2' => 'San Angelo TX 76901',
	'phone' => '(325) 227-1377',
	'country' => 'USA',
	
];