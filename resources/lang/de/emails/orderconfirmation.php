<?php

return [
	'Translate1' => 'Shipping address',
	'Translate2' => 'Order confirmation', 
	'Translate3' => 'Date',
	'Translate4' => 'Customernr.',
	'Translate5' => 'Ordernr.',
	'Translate6' => 'Billing address',
	'Translate7' => 'Label',
	'Translate8' => 'Type',
	'Translate9' => 'Size mm',
	'Translate10' => 'Quantity',
	'Translate11' => 'Price inc. VAT',
	'Translate12' => 'Colored sticker, laminated',
	'Translate13' => 'Colored Iron on label',
	'Translate14' => 'Golden sticker',
	'Translate15' => 'Silver sticker',
	'Translate16' => 'Transparent sticker',
	'Translate17' => 'Black and white sticker',
	'Translate18' => 'Iron on label',
	'Translate22' => 'Reflective sticker',
	'Translate23' => 'Fabric sticker',
	'Translate19' => 'Price inc. VAT',
	'Translate20' => 'Shipping and handling',
	'Translate21' => 'Total sum.',
	
];