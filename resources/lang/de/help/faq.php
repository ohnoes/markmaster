<?php

return [
	'Translate1' => 'Markmaster frequently asked questions',
	'Translate2' => 'Here is a list of frequently asked questions and answers',
	'Translate3' => 'Frequently asked questions and answers: ',
	'Translate4' => 'Can clothing with iron on labels be tumble dried?', 
	'Translate5' => 'Yes. If you attach the labels correctly, they should endure tumble dryers. If the labels are falling off, that is a result of too low temperature during the iron on process. 
							 The label is supposed to slightly melt into the fabric.',
	'Translate6' => 'Can you use iron on labels at wool?',  
	'Translate7' => 'Yes, you can iron on the label on wool products. If you are attaching the label on wool socks or mittens, it is best to attach the label vertically, to keep the elasticity.',
	'Translate8' => 'Why does the text become indistinctly after being attached?', 
	'Translate9' => 'This is most commonly occurs because of a combination of to high temperature on the iron, and movement with the iron.  
							  Try to keep the iron completly still, and lower the temperature slightly.',
	'Translate10' => 'Can you attach the label at lower temperatures than 3 marks on the iron, since the clothing cannot sustain such a high temperature?', 
	'Translate11' => 'Yes, the label will attach, but could fall off during high temperature wash. However, the clothing should not be washed in high temperatures either, so this should not matter.',
	'Translate12' => 'What is the estimated shipping time?',  
	'Translate13' => 'The labels will usually be produced the day after the order is placed. They are then packed and shipped as soon as they are done. 
								How long it will take to reach the destination depends on location. Most orders will arrive within 2 weeks.', 
	'Translate14' => 'Can I update my order?',
	'Translate15' => 'Quantity and size cannot be changed after the payment. You can however change the design on the sticker up until the production starts. ',
	'Translate16' => 'What payment methods are available?', 
	'Translate17' => 'Online payments via credit card, we accept MasterCard, Maestro, Visa, Diners Club and American Express.',
	'Translate18' => 'Do you ship outside of Norway?', 
	'Translate19' => 'Yes, we ship worldwide.',
	'Translate20' => 'I am not pleased with my labels, can I return them?', 
	'Translate21' => 'If the stickers are misprinted or damaged by Markmaster, the stickers will be fully refunded. 
								If there are typographical errors or layout/design errors made by the customer, we refund based on the laws set for personal customized products, 
								normally 50% of the first original price.', 

 
];