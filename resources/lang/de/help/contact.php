<?php

return [
	'Translate1' => 'Contact Markmaster',
	'Translate2' => 'Contact Markmaster here if you have any questions or if you wish to give us feedback',
	'Translate3' => 'Contact us',
	'Translate4' => 'Do you have any questions or just wish to give us feedback?  Here are some ways to get the hold of us:',
	'Translate5' => 'Email: ',
	'Translate6' => 'Phone:  0047 77 69 69 61',

];