<?php

return [
	'Translate1' => 'Markmasters help center',
	'Translate2' => 'Here is a list of things we can help you with',
	'Translate3' => 'Help',
	'Translate4' => 'Questions and answers',
	'Translate5' => 'Video',
	'Translate6' => 'Design help',
	'Translate7' => 'Purchase terms',
	'Translate8' => 'Contact us',

];