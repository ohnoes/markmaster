<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('lang/{language}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);


Route::group(['middleware' => 'markmaster\Http\Middleware\Admin'], function()
{
  // ClipartController
  Route::get('admin/cliparts/', 'ClipartController@getIndex');
  Route::post('admin/cliparts/create', 'ClipartController@postCreate');
  Route::post('admin/cliparts/destroy', 'ClipartController@postDestroy');
  Route::post('admin/cliparts/edit', 'ClipartController@postEdit');

  // CategoriesController
  Route::get('admin/categories/', 'CategoriesController@getIndex');
  Route::post('admin/categories/create', 'CategoriesController@postCreate');
  Route::post('admin/categories/destroy', 'CategoriesController@postDestroy');

  // ProductsController
  Route::get('admin/products/', 'ProductsController@getIndex');
  Route::post('admin/products/create', 'ProductsController@postCreate');
  Route::post('admin/products/destroy', 'ProductsController@postDestroy');

  // AdminController
  Route::get('admin/preview', 'AdminController@getPreview');
  Route::get('admin/', 'AdminController@getIndex');
  Route::get('admin/export', 'AdminController@getExport');
  Route::get('admin/icast', 'AdminController@getIcast');
  Route::get('admin/processexports', 'AdminController@getProcessexports');
  Route::get('admin/processicast', 'AdminController@getProcessicast');
  Route::get('admin/orders', 'AdminController@getOrders');
  Route::get('admin/orderedit/{id}', 'AdminController@getOrderedit');
  Route::get('admin/credit/{id}', 'AdminController@getCredit');
  Route::post('admin/credit/{id}', 'AdminController@postCredit');
  Route::post('admin/orderedit/{id}', 'AdminController@postOrderedit');
  Route::post('admin/updatesticker', 'AdminController@postUpdatesticker');
  Route::get('admin/orderdelete/{id}', 'AdminController@getOrderdelete');
  Route::get('admin/orderreset/{id}', 'AdminController@getOrderreset');
  Route::get('admin/ordersearch', 'AdminController@getOrdersearch');
  Route::get('admin/usersearch', 'AdminController@getUsersearch');
  Route::post('admin/updateorder/{id}', 'AdminController@postUpdateorder');
  Route::post('admin/updatecustomer/{id}', 'AdminController@postUpdatecustomer');
  Route::get('admin/customers', 'AdminController@getCustomers');
  Route::get('admin/customeredit/{id}', 'AdminController@getCustomeredit');
  Route::get('admin/customerdelete/{id}', 'AdminController@getCustomerdelete');
  Route::get('admin/adminsticker/{id}', 'AdminController@getAdminsticker');
  Route::get('admin/removeitem/{id}', 'AdminController@getRemoveitem');
  Route::get('admin/clipartget', 'AdminController@getClipartget');
  Route::post('admin/clipartget', 'AdminController@postClipartget');
  Route::get('admin/backgroundget', 'AdminController@getBackgroundget');
  Route::post('admin/backgroundget', 'AdminController@postBackgroundget');
  Route::post('admin/agent', 'AdminController@postAgent');
  Route::get('admin/agent', 'AdminController@getAgent');
  Route::get('admin/currency', 'AdminController@getCurrency');
  Route::post('admin/currency', 'AdminController@postCurrency');
  Route::post('admin/updatestickerstatus/{id}', 'AdminController@postUpdatestickerstatus');

  Route::get('admin/newiexport', 'AdminController@getNewiexport');
  Route::get('admin/newexport', 'AdminController@getNewexport');
  Route::get('admin/singleexport/{id}', 'AdminController@getSingleexport');
  Route::get('admin/fixstickers', 'AdminController@getFixstickers');

  Route::post('admin/handleexport', array('uses'=>'AdminController@postHandleexport'));

});

 	Route::post('clipart/create', array('uses'=>'ClipartController@postCreate'));
	Route::post('clipart/destroy', array('uses'=>'ClipartController@postDestroy'));
	Route::post('clipart/edit', array('uses'=>'ClipartController@postEdit'));

 	Route::post('design/backgroundget', array('uses'=>'DesignController@postBackgroundget'));
	Route::post('design/clipartget', array('uses'=>'DesignController@postClipartget'));
  Route::post('design/addtocart', array('uses'=>'DesignController@postAddtocart'));

 	Route::post('order/reg', array('uses'=>'OrdersController@postReg'));

 	Route::post('reminders/remind', array('uses'=>'RemindersController@postRemind'));
	Route::post('reminders/reset', array('uses'=>'RemindersController@postReset'));

 	Route::post('templates/create', array('uses'=>'TemplatesController@postCreate'));


// StoreController
Route::get('store/', 'StoreController@getIndex');
Route::get('store/view/{id}', 'StoreController@getView');
Route::get('store/category/{cat_id}', 'StoreController@getCategory');
Route::get('store/search', 'StoreController@getSearch');
Route::get('store/currency/{id}', 'StoreController@getCurrency');
Route::post('store/addtocart', 'StoreController@postAddtocart');
Route::post('store/updatecart', 'StoreController@postUpdatecart');
Route::get('store/cart', 'StoreController@getCart');
Route::get('store/checkout', 'StoreController@getCheckout');
Route::post('store/reg', 'StoreController@postReg');
Route::get('store/status', 'StoreController@getStatus');
Route::get('store/removeitem/{rowid}', 'StoreController@getRemoveitem');
Route::get('store/contact', 'StoreController@getContact');
Route::post('store/coupon', 'StoreController@postCoupon');

// UsersController
Route::get('users/newaccount', 'UsersController@getNewaccount');
Route::post('users/create', 'UsersController@postCreate');
Route::post('users/edit', 'UsersController@postEdit');
Route::get('users/signin', 'UsersController@getSignin');
Route::get('users/editaccount', 'UsersController@getEditaccount');
Route::post('users/signin', 'UsersController@postSignin');
Route::get('users/signout', 'UsersController@getSignout');
Route::get('users/editshipping/{id}', 'UsersController@getEditshipping');
Route::get('users/editbilling/{id}', 'UsersController@getEditbilling');
Route::get('users/editpassword/{id}', 'UsersController@getEditpassword');
Route::post('users/editpassword', 'UsersController@postEditpassword');
Route::post('users/editbilling', 'UsersController@postEditbilling');
Route::post('users/editshipping', 'UsersController@postEditshipping');


// OrdersController
Route::get('order/', 'OrdersController@getIndex');
Route::get('order/details/{id}', 'OrdersController@getDetails');
Route::get('order/pay/{id}', 'OrdersController@getPay');
Route::get('order/delete/{id}', 'OrdersController@getDelete');
Route::post('order/reg', 'OrdersController@postReg');
Route::get('order/deletesticker/{id}', 'OrdersController@getDeletesticker');

// RemindersController
Route::get('password/remind', ['as' => 'password.remind', 'uses' => 'RemindersController@getRemind']);
Route::post('password/remind', ['as' => 'password.remind', 'uses' => 'RemindersController@postRemind']);
Route::get('password/reset{token?}', ['as' => 'password.reset', 'uses' => 'RemindersController@getReset']);
Route::post('password/reset', ['as' => 'password.reset', 'uses' => 'RemindersController@postReset']);

// StickersController
Route::get('stickers/', 'StickersController@getIndex');

// InvoiceController
Route::get('invoice/invoice/{id}', 'InvoiceController@getInvoice');
Route::get('invoice/invoicegen/{id}', 'InvoiceController@getInvoicegen');
Route::get('invoice/credit/{id}', 'InvoiceController@getCredit');
Route::get('invoice/creditgen/{id}', 'InvoiceController@getCreditgen');
Route::get('invoice/pakkseddel/{id}', 'InvoiceController@getPakkseddel');
Route::get('invoice/pakkseddelgen/{id}', 'InvoiceController@getPakkseddelgen');

// DesignController
Route::get(trans('routes.merkclipart'), ['as' => 'merkclipart', 'uses' => 'DesignController@getClipartget']);
Route::get(trans('routes.merkbackground'), ['as' => 'merkbackground', 'uses' => 'DesignController@getBackgroundget']);
Route::get(trans('routes.merkactive'), ['as' => 'merkactive', 'uses' => 'DesignController@getStayactive']);
Route::get(trans('routes.merksticker'), ['as' => 'merksticker', 'uses' => 'DesignController@getKlistremerker']);
Route::get(trans('routes.merkironc'), ['as' => 'merkironc', 'uses' => 'DesignController@getStrykfast_farge']);
Route::get(trans('routes.merkgold'), ['as' => 'merkgold', 'uses' => 'DesignController@getKlistremerker_gull']);
Route::get(trans('routes.merksolv'), ['as' => 'merksolv', 'uses' => 'DesignController@getKlistremerker_solv']);
Route::get(trans('routes.merktrans'), ['as' => 'merktrans', 'uses' => 'DesignController@getKlistremerker_transparent']);
Route::get(trans('routes.merkcheap'), ['as' => 'merkcheap', 'uses' => 'DesignController@getBillige']);
Route::get(trans('routes.merkiron'), ['as' => 'merkiron', 'uses' => 'DesignController@getStrykfast']);
Route::get(trans('routes.merkreflection'), ['as' => 'merkreflection', 'uses' => 'DesignController@getRefleksmerker']);
Route::get(trans('routes.merkironfree'), ['as' => 'merkironfree', 'uses' => 'DesignController@getStrykefrie']);
Route::get(trans('routes.merkbase'), ['as' => 'merkbase', 'uses' => 'DesignController@getBase']);
Route::post(trans('routes.merkclipart'), ['as' => 'merkclipart', 'uses' => 'DesignController@postClipartget']);
Route::post(trans('routes.merkbackground'), ['as' => 'merkbackground', 'uses' => 'DesignController@postBackgroundget']);
Route::post(trans('routes.merkcart'), ['as' => 'merkcart', 'uses' => 'DesignController@postAddtocart']);
Route::get(trans('routes.merkcart'), ['as' => 'merkcart', 'uses' => 'DesignController@getAddtocart']);

Route::get('merkelapper/store-test', 'DesignController@getStoreTest');
Route::get('merkelapper/tempgen', 'DesignController@getTempgen');

// CategoriesController!
Route::get(trans('routes.catfabric'), ['as' => 'catfabric', 'uses' => 'CategoriesController@getFabric']);
Route::get(trans('routes.catstick'), ['as' => 'catstick', 'uses' => 'CategoriesController@getStick']);
Route::get(trans('routes.catironfree'), ['as' => 'catironfree', 'uses' => 'CategoriesController@getIronfree']);
Route::get(trans('routes.catirononcolor'), ['as' => 'catirononcolor', 'uses' => 'CategoriesController@getIrononcolor']);
Route::get(trans('routes.catironon'), ['as' => 'catironon', 'uses' => 'CategoriesController@getIronon']);
Route::get(trans('routes.catsticker'), ['as' => 'catsticker', 'uses' => 'CategoriesController@getSticker']);
Route::get(trans('routes.catcheapsticker'), ['as' => 'catcheapsticker', 'uses' => 'CategoriesController@getCheap']);
Route::get(trans('routes.catreflection'), ['as' => 'catreflection', 'uses' => 'CategoriesController@getReflection']);
Route::get(trans('routes.catgold'), ['as' => 'catgold', 'uses' => 'CategoriesController@getGold']);
Route::get(trans('routes.catsilver'), ['as' => 'catsilver', 'uses' => 'CategoriesController@getSilver']);
Route::get(trans('routes.cattransparent'), ['as' => 'cattransparent', 'uses' => 'CategoriesController@getTransparent']);
Route::get(trans('routes.catcontroll'), ['as' => 'catcontroll', 'uses' => 'CategoriesController@getControll']);
Route::get(trans('routes.catbeer'), ['as' => 'catbeer', 'uses' => 'CategoriesController@getBeer']);
Route::get(trans('routes.catforever'), ['as' => 'catforever', 'uses' => 'CategoriesController@getForever']);

// TemplatesController
Route::get(trans('routes.tempsticker'), ['as' => 'tempsticker', 'uses' => 'TemplatesController@getKlistremerker']);
Route::get(trans('routes.tempironc'), ['as' => 'tempironc', 'uses' => 'TemplatesController@getStrykfast_farge']);
Route::get(trans('routes.tempgold'), ['as' => 'tempgold', 'uses' => 'TemplatesController@getKlistremerker_gull']);
Route::get(trans('routes.tempsolv'), ['as' => 'tempsolv', 'uses' => 'TemplatesController@getKlistremerker_solv']);
Route::get(trans('routes.temptrans'), ['as' => 'temptrans', 'uses' => 'TemplatesController@getKlistremerker_transparent']);
Route::get(trans('routes.tempcheap'), ['as' => 'tempcheap', 'uses' => 'TemplatesController@getBillige']);
Route::get(trans('routes.goldorsolv'), ['as' => 'goldorsolv', 'uses' => 'TemplatesController@getGull_eller_solv']);
Route::get(trans('routes.tempiron'), ['as' => 'tempiron', 'uses' => 'TemplatesController@getStrykfast']);
Route::get(trans('routes.tempreflection'), ['as' => 'tempreflection', 'uses' => 'TemplatesController@getRefleksmerker']);
Route::get(trans('routes.tempironfree'), ['as' => 'tempironfree', 'uses' => 'TemplatesController@getStrykefrie']);
Route::post('etiketter-og-navnelapper/create', 'TemplatesController@postCreate');

// NametagController
Route::get(trans('routes.namesticker'), ['as' => 'namesticker', 'uses' => 'NametagController@getKlistremerker']);
Route::get(trans('routes.nameironc'), ['as' => 'nameironc', 'uses' => 'NametagController@getStrykfast_farge']);
Route::get(trans('routes.namegold'), ['as' => 'namegold', 'uses' => 'NametagController@getKlistremerker_gull']);
Route::get(trans('routes.namesolv'), ['as' => 'namesolv', 'uses' => 'NametagController@getKlistremerker_solv']);
Route::get(trans('routes.nametrans'), ['as' => 'nametrans', 'uses' => 'NametagController@getKlistremerker_transparent']);
Route::get(trans('routes.namecheap'), ['as' => 'namecheap', 'uses' => 'NametagController@getBillige']);
Route::get(trans('routes.nameiron'), ['as' => 'nameiron', 'uses' => 'NametagController@getStrykfast']);
Route::get(trans('routes.namereflection'), ['as' => 'namereflection', 'uses' => 'NametagController@getRefleksmerker']);
Route::get(trans('routes.nameironfree'), ['as' => 'nameironfree', 'uses' => 'NametagController@getStrykefrie']);
Route::get(trans('routes.namebase'), ['as' => 'namebase', 'uses' => 'NametagController@getBase']);
Route::post(trans('routes.namecart'), ['as' => 'merkcart', 'uses' => 'DesignController@postAddtocart']);
Route::get(trans('routes.namecart'), ['as' => 'merkcart', 'uses' => 'DesignController@getAddtocart']);

Route::get(trans('routes.home'), ['as' => 'home', 'uses' => 'StoreController@getIndex']);

Route::get(trans('routes.help'), ['as' => 'help', 'uses' => 'HelpController@getIndex']);
Route::get(trans('routes.helpdesign'), ['as' => 'helpdesign', 'uses' => 'HelpController@getDesign']);
Route::get(trans('routes.helpfaq'), ['as' => 'helpfaq', 'uses' => 'HelpController@getFaq']);
Route::get(trans('routes.helpvideo'), ['as' => 'helpvideo', 'uses' => 'HelpController@getVideo']);
Route::get(trans('routes.helpcontact'), ['as' => 'helpcontact', 'uses' => 'HelpController@getContact']);
Route::get(trans('routes.helpconditions'), ['as' => 'helpconditions', 'uses' => 'HelpController@getConditions']);


		Route::get('/storeTest', array('uses'=>'DesignController@getStoreTest'));

		Route::get('/tempGen', array('uses'=>'DesignController@getTempGen'));

		Route::get('/backgroundGet', function() {
			return View::make('backgroundGet');

		});

		Route::get('/clipartGet', function() {
			return View::make('clipartGet');

		});

		Route::get('/stickerSubmit', function() {
			return View::make('stickerSubmit');

		});

		Route::get('/conditions', function() {
			return View::make('conditions');

		});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('sitemap', function(){
  // Overall language loop ($lang is the language codes)
  foreach(config('translatable.locales') as $lang => $language) {
    $loopmap = App::make("sitemap");

    // loop through the routes lang file
    $routes = Lang::get('routes');
    foreach($routes as $route_name => $theroute) {
      $translations = array();
      foreach (config('translatable.locales') as $lang2 => $language2) {
              $translations[] = array(
                'language' => $lang2,
                'url' => URL::to('/'. $lang2 . '/' . trans('routes.' . $route_name, [], $lang2)));
      }
      $loopmap->add(URL::to('/'. $lang . '/' . trans('routes.' . $route_name, [], $lang)), '2017-04-04T10:30:00+02:00', '1.0', 'monthly', [], null, $translations);
    }

    // save the language sitemap
    $loopmap->store('xml', 'sitemap_'.$lang);
  }

   // create new sitemap object
   $sitemap = App::make("sitemap");

   $sitemap->addSitemap(URL::to('sitemap_no.xml', '', '2017-04-04T10:30:00+02:00'));
   $sitemap->addSitemap(URL::to('sitemap_en.xml', '', '2017-04-04T10:30:00+02:00'));
   $sitemap->addSitemap(URL::to('sitemap_se.xml', '', '2017-04-04T10:30:00+02:00'));
   $sitemap->addSitemap(URL::to('sitemap_dk.xml', '', '2017-04-04T10:30:00+02:00'));
   $sitemap->addSitemap(URL::to('sitemap_is.xml', '', '2017-04-04T10:30:00+02:00'));

   // generate your sitemap (format, filename)
   $sitemap->store('sitemapindex', 'sitemap');
   // this will generate file mysitemap.xml to your public folder
   return $sitemap->render('xml');
});
