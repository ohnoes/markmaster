User-agent: *
Disallow: */lang/
Disallow: */currency/
Disallow: */clipartget
Disallow: */backgroundget
Disallow: */designer

Sitemap: https://www.markmaster.com/sitemap.xml
